#include <termios.h>
#include <ctype.h>
#include <unistd.h>


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ctype.h>
#include <assert.h>
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <dlfcn.h>
#include <inttypes.h>
#include <openssl/ssl.h>

#include "cavium_defines.h"
#include "cavium_mgmt.h"
#include "cavium_crypto.h"
#include "openssl_util.h"

#include "openssl/crypto.h"
#include "openssl/pem.h"
#include "openssl/x509.h"
#include "openssl/bn.h"
#include "openssl/rsa.h"
#include "openssl/rand.h"
#include "openssl/err.h"
#include "openssl/ec.h"
#include "openssl/aes.h"
#include "openssl/des.h"


#include "eTPkcs11.h"
#include "pkcs11t.h"


#include "Cfm2Helper.h"

extern unsigned int session_handle;
// Prototype Declarations
void Help_AllCommands(char *pAppName); // All Commands Available
int CfmUtil_main(int argc, char **argv);
void HexPrint(Uint8 * data, Uint32 len);
int ReadFileByMap(char *pbFileName,
          char **ppMemBlock, unsigned int *pulMemSize);
int ReadBinaryFile(char *pbFileName, char **ppMemBlock, unsigned int *pulMemSize); 
int WriteBinaryFile(char *pbFileName,
            char *pMemBlock, unsigned long ulMemSize);
int getAttributeValue(Uint32 ulAttribute,
              Uint8 ** pAttribute, Uint32 * ulAttributeLen);
unsigned int GetConsoleString(char *pPrompt,
                  char *pBuffer, unsigned int nBufferSize);
char **GetCommandLineArgs(char *buff, int *_argc);
int GetPassword(char *pPrompt,
        char *pPrompt2, char *pBuffer, int *pulBufferLen);
Uint32 zeroize(int argc, char **argv);
Uint32 login(int argc, char **argv);
Uint32 generatePEK(int argc, char **argv);
Uint32 storeUserFixedKey(int argc, char** argv);

void initDynamicBufferVector();


void clearAndResetDynamicBufferVector();


/****************************************************************************\
 *  *
 *   * FUNCTION     : GetCommandLineArgs
 *    *
 *     * DESCRIPTION  : converts the buffer into a "argv" like array
 *      *
 *       * RETURN VALUE : int
 *        *
 *         \****************************************************************************/
char **GetCommandLineArgs(char *buff, int *_argc);


/****************************************************************************\
 *  *
 *   * FUNCTION     : validateLabel
 *    *
 *     * DESCRIPTION  : -
 *      - Validates Label
 *       - if string contains double cote (or) dnt have atleast one non special character returns 1 (true)
 *        - esle, returns 0 (false)
 *         *
 *          \****************************************************************************/
Uint8 validateLabel(char *string, Uint32 len);
/****************************************************************************\
 *  *
 *   * FUNCTION     : readStringArg
 *    *
 *     * DESCRIPTION  : -
 *      - reads a string arg from pBuffer
 *       - first tries to read a file with the file name pBuffer
 *        - if that fails, copies the data into the pointer
 *         - return 1 (true) on success
 *          *
 *           \****************************************************************************/
Uint8 readStringArg(char *pBuffer,
            char **pTarget,
            Uint32 * pwTargetLen, Uint32 bFileExpected, char *msg);

Uint8 readStringArgByMap(char *pBuffer,
             char **pTarget,
             Uint32 * pwTargetLen,
             Uint32 bFileExpected, char *msg);
Uint8 readArgAsString(char *pBuffer,
                   char **pTarget,
                   Uint32 * pwTargetLen);

/****************************************************************************\
 *  *
 *   * FUNCTION     : readIntegerArg
 *    *
 *     * DESCRIPTION  : -
 *      - reads an integer arg from the vector of args
 *       *
 *        \****************************************************************************/
Uint8 readIntegerArg(char *pBuffer, Uint32 * pulValue);

Uint8 readIntegerArrayArg(char *pBuffer, Uint32 ** pTarget, Uint32 * pwCount);

/****************************************************************************\
 *  *
 *   * FUNCTION     : main
 *    *
 *     * DESCRIPTION  : The "main" function of the Cfm2Util Utility to allow command
 *      *                line arguments to be used to make Cavium Shim API calls.
 *       *
 *        * PARAMETERS   : argc, **argv
 *         *
 *          * RETURN VALUE : int
 *           *
 *            \****************************************************************************/

int openScriptFile(char *pFileName);

int readLineFromScriptFile(char *pBuffer, unsigned int nBufferSize);

static int cmpstringp(const void *p1, const void *p2);


void closeScriptFile();



#define WORKAROUND_FOR_BUG_19921
#ifdef WORKAROUND_FOR_BUG_19921
static int read_configuration(Int8 * file, BoardConfiguration * config);
#endif
/****************************************************************************
 *  *
 *   * FUNCTION     : initializeHSM
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 initializeHSM(int argc, char **argv);
    

Uint32 deleteUser(int argc, char **argv);
  

Uint32 createUser(int argc, char **argv);
  

Uint32 listUsers(int argc, char **argv);

    


/****************************************************************************
 *  *
 *   * FUNCTION     : loginStatus
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 loginStatus(int argc, char **argv);
  

/****************************************************************************
 *  *
 *   * FUNCTION     : getHSMInfo
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 getHSMInfo(int argc, char **argv);

/*Certificate related commands*/
#ifdef CERT_OPS
Uint32 createNode(int argc, char **argv);


Uint32 exportPartnCSR(int argc, char **argv);

Uint32 importPartnCert(int argc, char **argv);


Uint32 exportAppCert(int argc, char **argv);


Uint32 exportPartnCert(int argc, char **argv);

Uint32 importCavClientCert(int argc, char **argv);

Uint32 exportCavClientCert(int argc, char **argv);


Uint32 deleteCavClientCert(int argc, char **argv);

/*Not supported for now*/
Uint32 listCavClientCert(int argc, char **argv);

#endif
/****************************************************************************
 *  *
 *   * FUNCTION     : getPartitionInfo
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 getPartitionInfo(int argc, char **argv);



#ifndef CLOUD_HSM_CLIENT
Uint32 closeAllSessions(int argc, char **argv);



#endif

/****************************************************************************
 *  *
 *   * FUNCTION     : changePswd
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 changePswd(int argc, char **argv);


/****************************************************************************
 *  *
 *   * FUNCTION     : load_lib
 *    *
 *     * DESCRIPTION  : Loads the HSM libraries using PKCS11 Interface
 *      *
 *       * PARAMETERS   : Absolute linrary path
 *        *
 *         *****************************************************************************/

static int  inline load_lib(const char *libpath);

    

/****************************************************************************
 *  *
 *   * FUNCTION     : lib_close
 *    *
 *     * DESCRIPTION  : Closes the HSM module
 *      *
 *       * PARAMETERS   : none
 *        *
 *         *****************************************************************************/

static int inline lib_close();





/****************************************************************************
 *  *
 *   * FUNCTION     : login
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 login(int argc, char **argv);



/****************************************************************************
 *  *
 *   * FUNCTION     : logout
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 logout(int argc, char **argv);



/****************************************************************************
 *  *
 *   * FUNCTION     : zeroize
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 zeroize(int argc, char **argv);



/****************************************************************************
 *  *
 *   * FUNCTION     : genECCKeyPair
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 genECCKeyPair(int argc, char **argv);


/****************************************************************************
 *  *
 *   * FUNCTION     : genDSAKeyPair
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 genDSAKeyPair(int argc, char **argv);

  

/****************************************************************************
 *  *
 *   * FUNCTION     : eccSign
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 eccSign(int argc, char **argv);



/****************************************************************************
 *  *
 *   * FUNCTION     : eccVerify
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 eccVerify(int argc, char **argv);




/****************************************************************************
 *  *
 *   * FUNCTION     : RSASign
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 rsaSign(int argc, char **argv);



/****************************************************************************
 *  *
 *   * FUNCTION     : RSAVerify
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 rsaVerify(int argc, char **argv);

  

/****************************************************************************
 *  *
 *   * FUNCTION     : DSASign
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 dsaSign(int argc, char **argv);



/****************************************************************************
 *  *
 *   * FUNCTION     : DSAVerify
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/

Uint32 dsaVerify(int argc, char **argv);

   

Uint32 sign(int argc, char **argv);



Uint32 verify(int argc, char **argv);


/****************************************************************************
 *  *
 *   * FUNCTION     : listECCCurveIds
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 listECCCurveIds(int argc, char **argv);

 

/****************************************************************************
 *  *
 *   * FUNCTION     : aesWrapUnwrap
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 aesWrapUnwrap(int argc, char **argv);

    

#ifdef ENABLE_EAP_PAC
/****************************************************************************
 *  *
 *   * FUNCTION     : eapPacHs
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 eapPacHs(int argc, char **argv);



#endif

/****************************************************************************
 *  *
 *   * FUNCTION     : genRSAKeyPair
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 genRSAKeyPair(int argc, char **argv);

 

/****************************************************************************
 *  *
 *   * FUNCTION     : createPublicKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 createPublicKey(int argc, char **argv);



/****************************************************************************
 *  *
 *   * FUNCTION     : importPublicKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 importPublicKey(int argc, char **argv);

  

/****************************************************************************
 *  *
 *   * FUNCTION     : exportPublicKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 exportPublicKey(int argc, char **argv);

  

/****************************************************************************
 *  *
 *   * FUNCTION     : GeneratePEK
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 generatePEK(int argc, char **argv);

   

/****************************************************************************
 *  *
 *   * FUNCTION     : GenerateKEK
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 generateKEK(int argc, char **argv);

    
/****************************************************************************
 *  *
 *   * FUNCTION     : exportPrivateKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 exportPrivateKey(int argc, char **argv);


int get_kek_from_file(char *file, Uint8* kek);

 
Uint8 wrap_with_kek(Uint8 *pKey,Uint32 ulKeyLen,Uint8 *pIV,Uint8 *wrappedKey,Uint32 *wrappedKeyLen);

  

Uint8 unwrap_with_kek(Uint8 *wrappedKey,Uint32 wrappedKeyLen,Uint8 *pIV,Uint8 *unwrappedKey,Uint32 *unwrappedKeyLen);



/****************************************************************************
 *  *
 *   * FUNCTION     : importPrivateKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 importPrivateKey(int argc, char **argv);

/****************************************************************************
 *  *
 *   * FUNCTION     : importRawRSAPrivateKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 importRawRSAPrivateKey(int argc, char **argv);


/****************************************************************************
 *  *
 *   * FUNCTION     : wrapKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 wrapKey(int argc, char **argv);

/****************************************************************************
 *  *
 *   * FUNCTION     : unWrapKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 unWrapKey(int argc, char **argv);

/****************************************************************************
 *  *
 *   * FUNCTION     : convert2CaviumPrivKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 convert2CaviumPrivKey(int argc, char **argv);

   /****************************************************************************
 *  *
 *   * FUNCTION     : getFWVer
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 getFWVer(int argc, char **argv);



/****************************************************************************
 *  *
 *   * FUNCTION     : getLoginFailCount
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 getLoginFailCount(int argc, char **argv);

   

/****************************************************************************
 *  *
 *   * FUNCTION     : genPBEKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 genPBEKey(int argc, char **argv);


/****************************************************************************
 *  *
 *   * FUNCTION     : genSymKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 genSymKey(int argc, char **argv);

Uint32 exSymKey(int argc, char **argv);

/****************************************************************************
 *  *
 *   * FUNCTION     : imSymKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 imSymKey(int argc, char **argv);

/****************************************************************************
 *  *
 *   * FUNCTION     : deleteKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 deleteKey(int argc, char **argv);

/****************************************************************************
 *  *
 *   * FUNCTION     : Error2String
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 Error2String(int argc, char **argv);



/****************************************************************************
 *  *
 *   * FUNCTION     : getAuditLogs
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 getAuditLogs(int argc, char **argv);

    
/****************************************************************************
 *  *
 *   * FUNCTION     : findKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 findKey(int argc, char **argv);


/****************************************************************************
 *  *
 *   * FUNCTION     : getAttribute
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 getAttribute(int argc, char **argv);

    

/****************************************************************************
 *  *
 *   * FUNCTION     : setAttribute
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 setAttribute(int argc, char **argv);



/****************************************************************************
 *  *
 *   * FUNCTION     : listAttributes
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 listAttributes(int argc, char **argv);



/****************************************************************************
 *  *
 *   * FUNCTION     : insertMaskedObject
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 insertMaskedObject(int argc, char **argv);

  

/****************************************************************************
 *  *
 *   * FUNCTION     : extractMaskedObject
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 extractMaskedObject(int argc, char **argv);


/****************************************************************************
 *  *
 *   * FUNCTION     : dumpItUtil
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 dumpItUtil(int argc, char **argv);



/****************************************************************************
 *  *
 *   * FUNCTION     : cloneSourceStart
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 cloneSourceStart(int argc, char **argv);



/****************************************************************************
 *  *
 *   * FUNCTION     : cloneSourceEnd
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 cloneSourceEnd(int argc, char **argv);


/****************************************************************************
 *  *
 *   * FUNCTION     : cloneTargetStart
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 cloneTargetStart(int argc, char **argv);


/****************************************************************************
 *  *
 *   * FUNCTION     : cloneTargetEnd
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 cloneTargetEnd(int argc, char **argv);


#ifdef NIC_ENABLE
/****************************************************************************
 *  * *
 *   * * FUNCTION     : setNetConfig
 *    * *
 *     * * DESCRIPTION  :
 *      * *
 *       * * PARAMETERS   :
 *        * *
 *         * *****************************************************************************/
Uint32 setNetConfig(int argc, char **argv);


#endif



/****************************************************************************
 *  *
 *   * FUNCTION     : storeUserFixedKey
 *    *
 *     * DESCRIPTION  :
 *      *
 *       * PARAMETERS   :
 *        *
 *         *****************************************************************************/
Uint32 storeUserFixedKey(int argc, char** argv);



Uint32 backupPartition(int argc, char **argv);



Uint32 restorePartition(int argc, char **argv);




/*****************************************************************************
 *  *
 *   * FUNCTION     : getCaviumPrivKey
 *    *
 *     * DESCRIPTION  : Returns private key file in PEM format for given private key handle
 *      *
 *       * PARAMETERS   : input:   Keyhandle
 *        *                         file name to which private key is to be written
 *         *
 *          *                output:  Returns  non-zero on Error
 *           *                                  0 on Success
 *            *
 *             ******************************************************************************/
Uint32 getCaviumPrivKey(int argc, char **argv);



/************************************************************************************
 *  *
 *   * FUNCTION     : IsValidKeyHandlefile
 *    *
 *     * DESCRIPTION  : checks input key file has a valid private key handle.
 *      *
 *       * PARAMETERS   : input:  PrivateKey file name
 *        *
 *         *                output:  Returns  non-zero on Error
 *          *                                  0 on Success
 *           *
 *            *******************************************************************************/

Uint32 IsValidKeyHandlefile(int argc, char **argv);


Uint32 storeCert(int argc, char **argv);



Uint32 getCertReq(int argc, char **argv);



Uint32 getCert(int argc, char **argv);


Uint32 getSourceRandom(int argc, char **argv);



Uint32 validateCert(int argc, char **argv);


Uint32 sourceKeyExchange(int argc, char **argv);


Uint32 targetKeyExchange(int argc, char **argv);




/****************************************************************************\
 *  *
 *   * FUNCTION     : CfmUtil_main
 *    *
 *     * DESCRIPTION  : The "main" function of the Cfm2Util Utility to allow command
 *      *                line arguments to be used to make Cavium Shim API calls.
 *       *                This function is called each time a command is entered.
 *        *
 *         * PARAMETERS   : argc, **argv
 *          *
 *           * RETURN VALUE : int
 *            *
 *             \****************************************************************************/
int CfmUtil_main(int argc, char **argv);




/****************************************************************************\
 *  *
 *   * FUNCTION     : HexPrint
 *    *
 *     * DESCRIPTION  : Displays Data of a Given Length in Hexidecimal Format
 *      *
 *       * PARAMETERS   : data, len
 *        *
 *         * RETURN VALUE : none
 *          *
 *           \****************************************************************************/
void HexPrint(Uint8 * data, Uint32 len);

/****************************************************************************\
 *  *
 *   * FUNCTION     : Help_AllCommands
 *    *
 *     * DESCRIPTION  : Displays Help for All Commands Available
 *      *
 *       * PARAMETERS   : pAppName
 *        *
 *         * RETURN VALUE : none
 *          *
 *           \****************************************************************************/
void Help_AllCommands(char *pAppName);

 

/****************************************************************************\
 *  *
 *   * FUNCTION     : ReadBinaryFile
 *    *
 *     * DESCRIPTION  : Reads a binary file with the input file, allocates memory
 *      *                to read it and returns the content using the input pointers.
 *       *                Returns 1 if successful.
 *        *
 *         * PARAMETERS   : char *pbFileName
 *          *                char **ppMemBlock
 *           *                unsigned long *pulMemSize
 *            *
 *             * RETURN VALUE : int
 *              *
 *               \****************************************************************************/
int ReadBinaryFile(char *pbFileName, char **ppMemBlock,
           unsigned int *pulMemSize);


int map_file(char *inPathName, void **outDataPtr, size_t *outDataLength);

 
int ReadFileByMap(char *pbFileName, char **ppMemBlock,
          unsigned int *pulMemSize);

  

int WriteBinaryFile(char *pbFileName, char *pMemBlock,
            unsigned long ulMemSize);



unsigned int GetConsoleString(char *pPrompt,
                  char *pBuffer, unsigned int nBufferSize);


int getAttributeValue(Uint32 ulAttribute,
              Uint8 ** pAttribute, Uint32 * ulAttributeLen);

 

/*
 *  * getPswdString()
 *   * ==============
 *    * This function retrieves a pin string from the user.
 *     * It modifies the console mode before starting so that the
 *      * characters the user types are not echoed, and a '*'
 *       * character is displayed for each typed character instead.
 *        *
 *         * Backspace is supported, but we don't get any fancier than that.
 *          *
 *           * If -1 is passed in for ulMinLen, the size constraints are not checked.
 *            *
 *             * return codes
 *              *       0 == OK
 *               *       1 == problem with len constraints
 *                *       2 == problem with input params
 *                 *       3+ == problems manipulating the console
 *                  */

Uint32 getPswdString(char *pw, int *pulLen, int ulMinLen, int ulMaxLen);

/********************************************************
 *  *
 *   * GetPassword()
 *    *
 *     *  returns size of password read, also asigne new len to pulBufferLen.
 *      ********************************************************/
int GetPassword(char *pPrompt, char *pPrompt2, char *pBuffer,
        int *pulBufferLen);


