#include"Cfm2Util.h"
typedef char *CHAR_PTR;
int verbose = 1;
#define print_verbose if(1 == verbose) printf
#define print_output if(2 == verbose) printf

CK_FUNCTION_LIST_PTR func_list = NULL;  
static void *module = NULL;

char *libname="/usr/local/ssl/lib/engines/libpkcs11.so";



#define BUFSIZE MAX_DATA_LENGTH
char *BUILD_DIR="/etc/cavium";
char *partition_name = NULL;
int dev_id = 0;
unsigned int application_id = -1;
unsigned int session_handle = -1;
Uint32 fipsState = 0;
char *fips_state[] = { "zeroized",
    "non-FIPS mode with single factor authentication",
    "non-FIPS mode with two factor authentication(unsupported)",
    "FIPS mode with single factor authentication",
    "FIPS mode with two factor authentication"
};
char *kek_methods[] = { "ECDH", "RSA" };
char *cloning_methods[] = { "Not Supported", "ECDH", "RSA" };

char *nitrox_config[] = { "Not configured",
    "16 cores",
    "24 cores",
    "32 cores",
    "55/63 cores"
};
char *userType[] = { "Unknown",
    "CU",
    "CO",
    "PCO",
    "HMCO",
    "AU"
};

#define VECTOR_SIZE             100
#define MAX_FILENAME_SIZE       256
#define CAV_SIG_IMPORTED_KEY    0xCAFEFEED
#define CAV_SIG_HSM_KEY         0xEEFCFCEA

#define VECTOR_SIZE             100
#define MAX_FILENAME_SIZE       256
#define CAV_SIG_IMPORTED_KEY    0xCAFEFEED
#define CAV_SIG_HSM_KEY         0xEEFCFCEA

CHAR_PTR vector[VECTOR_SIZE + 2];

CHAR_PTR dyn_vector[VECTOR_SIZE + 2];

Uint32 ulDynVectorSize = 0;
void initDynamicBufferVector()
{
    for (ulDynVectorSize = 0; ulDynVectorSize < VECTOR_SIZE;
         ulDynVectorSize++)
        dyn_vector[ulDynVectorSize] = 0;

    ulDynVectorSize = 0;
}

void clearAndResetDynamicBufferVector()
{
    Uint32 i = 0;
    for (i = 0; i < VECTOR_SIZE; i++) {
        if (dyn_vector[i] != 0)
            free(dyn_vector[i]);
        dyn_vector[i] = 0;
    }

    ulDynVectorSize = 0;
}

/****************************************************************************\
 *
 * FUNCTION     : GetCommandLineArgs
 *
 * DESCRIPTION  : converts the buffer into a "argv" like array
 *
 * RETURN VALUE : int
 *
 \****************************************************************************/
char **GetCommandLineArgs(char *buff, int *_argc)
{
    char *delim = " ";
    char *token = 0;
    *_argc = 1;                 // start at one to include the app name at location 0

    token = strtok(buff, delim);
    while ((token != 0) && (*_argc < VECTOR_SIZE)) {
        vector[*_argc] = malloc(strlen(token) + 1);
        if (!vector[*_argc]) {
            printf("\n\tInternal Memory Error (GetCommandLineArgs)\n");
            exit(1);
        }
        strcpy(vector[*_argc], token);
        (*_argc)++;
        token = strtok(0, delim);
    }

    if (*_argc >= 100)
        *_argc = 1;

    // set the argc value

    return vector;
}

/****************************************************************************\
 *
 * FUNCTION     : validateLabel
 *
 * DESCRIPTION  : -
 - Validates Label
 - if string contains double cote (or) dnt have atleast one non special character returns 1 (true)
 - esle, returns 0 (false)
 *
 \****************************************************************************/
Uint8 validateLabel(char *string, Uint32 len)
{
    int i;
    for (i = 0; i < len; i++)
    {
        if ((string[i] == '\"'))
        {
            printf("\n\tLabel with double cote Character is not allowed \n");
            return TRUE;
        }
    }

    for (i = 0; i < len; i++) {
        if (((string[i] >= 'a') && (string[i] <= 'z')) ||
            ((string[i] >= 'A') && (string[i] <= 'Z')) ||
            ((string[i] >= '0') && (string[i] <= '9')))
            return FALSE;
    }
    printf("\n\tLabel should contain atleast one non Special Character\n");
    return TRUE;
}
/****************************************************************************\
 *
 * FUNCTION     : readStringArg
 *
 * DESCRIPTION  : -
 - reads a string arg from pBuffer
 - first tries to read a file with the file name pBuffer
 - if that fails, copies the data into the pointer
 - return 1 (true) on success
 *
 \****************************************************************************/
Uint8 readStringArg(char *pBuffer,
            char **pTarget,
            Uint32 * pwTargetLen, Uint32 bFileExpected, char *msg)
{
    if ((pBuffer == 0) || (pTarget == 0) || (pwTargetLen == 0))
        return 0;

    // try to read data from file
    if (ReadBinaryFile(pBuffer, (char **)pTarget, pwTargetLen)) {
        // flag this buffer to be cleaned up later
        dyn_vector[ulDynVectorSize] = *pTarget;
        ulDynVectorSize = ulDynVectorSize + 1;
    } else {
        if (bFileExpected)
            if (msg) {
                printf
                    ("\n\tWARNING: Failed to read \"%s\" from the file \"%s\".  Using supplied parameter.\n",
                     msg, pBuffer);
                return FALSE;
            }
        // Otherwise, the string param was supplied explicitly
        *pTarget = malloc(strlen(pBuffer) + 1);
        memcpy(*pTarget, pBuffer, strlen(pBuffer) + 1);
        *pwTargetLen = strlen(pBuffer);
    }

    return 1;
}

Uint8 readStringArgByMap(char *pBuffer,
             char **pTarget,
             Uint32 * pwTargetLen,
             Uint32 bFileExpected, char *msg)
{
    if ((pBuffer == 0) || (pTarget == 0) || (pwTargetLen == 0))
        return 0;

    // try to read data from file
    if (ReadFileByMap(pBuffer, (char **)pTarget, pwTargetLen)) {
        // flag this buffer to be cleaned up later
        dyn_vector[ulDynVectorSize] = *pTarget;
        ulDynVectorSize = ulDynVectorSize + 1;
    } else {
        if (bFileExpected)
            if (msg) {
                printf
                    ("\n\tWARNING: Failed to read \"%s\" from the file \"%s\".  Using supplied parameter.\n",
                     msg, pBuffer);
                return 0;
            }
        // Otherwise, the string param was supplied explicitly
        *pTarget = malloc(strlen(pBuffer) + 1);
        memcpy(*pTarget, pBuffer, strlen(pBuffer) + 1);
        *pwTargetLen = strlen(pBuffer);
    }

    return 1;
}
Uint8 readArgAsString(char *pBuffer,
                   char **pTarget,
                   Uint32 * pwTargetLen)
{
    if ((pBuffer == 0) || (pTarget == 0) || (pwTargetLen == 0))
        return 0;
    *pTarget = malloc(strlen(pBuffer) + 1);
    memcpy(*pTarget, pBuffer, strlen(pBuffer) + 1);
    *pwTargetLen = strlen(pBuffer);
    return 1;
}

/****************************************************************************\
 *
 * FUNCTION     : readIntegerArg
 *
 * DESCRIPTION  : -
 - reads an integer arg from the vector of args
 *
 \****************************************************************************/
Uint8 readIntegerArg(char *pBuffer, Uint32 * pulValue)
{
    char *ptr = NULL;

    if ((pBuffer == 0) || (pulValue == 0))
        return 0;

    /* Check if it contains any chars other than integer */
    ptr = pBuffer;
    while(ptr && *ptr != '\0'){
        if(*ptr >= '0' && *ptr <= '9')
            ptr++;
        else
            return 0;
    }

    *pulValue = atoi(pBuffer);

    return 1;
}

Uint8 readIntegerArrayArg(char *pBuffer, Uint32 ** pTarget, Uint32 * pwCount)
{

    char *token = 0;
    char *seps = (char *)",";
    char *temp = 0;
    int wBufferSizeInBytes = 0;

    *pwCount = 0;

    if ((pBuffer == 0) || (pTarget == 0) || (pwCount == 0))
        return 0;

    //if (GetDynBuffer((void**)&temp, strlen(pBuffer)+1) != CKR_OK)
    temp = (char *)malloc(strlen(pBuffer) + 1);
    if (!temp)
        return 0;

    strcpy(temp, pBuffer);
    // count the tokens
    token = strtok(temp, seps);
    while (token != NULL) {
        (*pwCount)++;
        //printf( "token: %s\n", token );
        token = strtok(NULL, seps);
    }

    // allocate the buffer for the tokens
    wBufferSizeInBytes = *pwCount * sizeof(Uint32);

    *pTarget = (Uint32 *) malloc(wBufferSizeInBytes);
    if (!pTarget) {
        if (temp)
            free(temp);
        return 0;
    }

    /*
       if (GetDynBuffer((void**)pTarget, wBufferSizeInBytes) != CKR_OK)
       return 0;
       */

    // populate the array of ints
    *pwCount = 0;
    token = strtok(pBuffer, seps);
    while (token != NULL) {
        (*pTarget)[*pwCount] = atoi(token);
        (*pwCount)++;
        token = strtok(NULL, seps);
    }

    if (temp)
        free(temp);

    return 1;
}

/****************************************************************************\
 *
 * FUNCTION     : main
 *
 * DESCRIPTION  : The "main" function of the Cfm2Util Utility to allow command
 *                line arguments to be used to make Cavium Shim API calls.
 *
 * PARAMETERS   : argc, **argv
 *
 * RETURN VALUE : int
 *
 \****************************************************************************/

FILE *pScriptFile = 0;
int openScriptFile(char *pFileName)
{
    pScriptFile = fopen(pFileName, "r");
    return (pScriptFile == 0);
}

int readLineFromScriptFile(char *pBuffer, unsigned int nBufferSize)
{
    unsigned int nCharsRead = 0;
    char ch;
    if (fread(&ch, 1, 1, pScriptFile) == 0)
        ch = EOF;
    while ((ch != EOF) && (ch != '\n')) {
        if ((ch != '\r') && ((nCharsRead + 1) < nBufferSize)) {
            pBuffer[nCharsRead++] = ch;
        }
        if (fread(&ch, 1, 1, pScriptFile) == 0)
            ch = EOF;
    }
    // Null terminate the string before returning
    pBuffer[nCharsRead] = 0;
    return (ch == EOF);
}

static int cmpstringp(const void *p1, const void *p2)
{
    /* The actual arguments to this function are "pointers to
     *               pointers to char", but strcmp(3) arguments are "pointers
     *                             to char", hence the following cast plus dereference */

    return strlen(*(char * const *)p1) !=  strlen(*(char * const *)p2)?
        (strlen(*(char * const *)p1) - strlen(*(char * const *)p2)):strcmp(*(char * const *)p1, *(char * const *)p2);
}


void closeScriptFile()
{
    if (pScriptFile)
        fclose(pScriptFile);
}


#define WORKAROUND_FOR_BUG_19921
#ifdef WORKAROUND_FOR_BUG_19921
static int read_configuration(Int8 * file, BoardConfiguration * config)
{

    Uint32 cond_code = 0;
    FILE *fd = NULL;
    char buf[256] = { 0 };
    char *pos = NULL;
    int line = 0;

    if ((!file) || (!config)) {
        return ERR_INVALID_INPUT;
    }

    print_debug("reading configuration from file %s \n", file);
    fd = fopen(file, "r");
    if (fd == NULL) {
        print_debug("Could not open configuration file '%s' for reading.\n",
                file);
        return ERR_OPEN_FILE;
    }

    memcpy(&config->label, "cavium", strlen("cavium"));

    while (fgets(buf, sizeof(buf), fd)) {
        line++;

        if (buf[0] == '#')
            continue;
        pos = buf;
        while (*pos != '\0') {
            if (*pos == '\n') {
                *pos = '\0';
                break;
            }
            pos++;
        }
        if (buf[0] == '\0')
            continue;
        pos = strchr(buf, '=');
        if (pos == NULL) {
            print_debug("Line %d: Invalid line '%s'\n", line, buf);
            cond_code = ERR_INVALID_INPUT;
            goto error;
        }
        *pos = '\0';
        pos++;

        if (strcmp(buf, CONFIG_PARAM_FIPS_STATE) == 0) {
#ifdef API_DEBUG
            if ((strcmp(pos, "0") != 0) && (strcmp(pos, "2") != 0)
                && (strcmp(pos, "3") != 0)) {
                print_debug("Invalid FIPS state %s\n", pos);
                cond_code = ERR_INVALID_INPUT;
                goto error;
            }
#endif
            config->fipsState = (atoi(pos));
        } else if (strcmp(buf, CONFIG_PARAM_ETH_FLAG) == 0) {
            config->eth_flag = (atoi(pos));
        } else if (strcmp(buf, CONFIG_PARAM_AUTH_PATH) == 0) {
            config->auth_path = (atoi(pos));
        } else if (strcmp(buf, CONFIG_PARAM_IP_ADDR) == 0) {
            config->netConfig.ipAddr = (atoi(pos));
        } else if (strcmp(buf, CONFIG_PARAM_NET_MASK) == 0) {
            config->netConfig.netMask = (atoi(pos));
        } else if (strcmp(buf, CONFIG_PARAM_GATEWAY) == 0) {
            config->netConfig.gateway = (atoi(pos));
        } else if (strcmp(buf, CONFIG_PARAM_NON_FIPS_ALGO) == 0) {
            config->nonFipsAlgo = (atoi(pos));
        } else if (strcmp(buf, CONFIG_PARAM_LOGIN_FAIL_COUNT) == 0) {
            config->loginFailureCount = (atoi(pos));
        } else if (strcmp(buf, CONFIG_PARAM_SECRET_KEY_WRAP) == 0) {
            config->secretKeyWrapping = (atoi(pos));
        } else if (strcmp(buf, CONFIG_PARAM_SECRET_KEY_UNWRAP) == 0) {
            config->secretKeyUnWrapping = (atoi(pos));
        } else if (strcmp(buf, CONFIG_PARAM_PRIVATE_KEY_WRAP) == 0) {
            config->privateKeyWrapping = (atoi(pos));
        } else if (strcmp(buf, CONFIG_PARAM_PRIVATE_KEY_UNWRAP) == 0) {
            config->privateKeyUnWrapping = (atoi(pos));
        } else if (strcmp(buf, CONFIG_PARAM_MAX_PSWD_LEN) == 0) {
            config->maxPswdLength = atoi(pos);
        } else if (strcmp(buf, CONFIG_PARAM_MIN_PSWD_LEN) == 0) {
            config->minPswdLength = atoi(pos);
        } else if (strcmp(buf, CONFIG_PARAM_CLONING) == 0) {
            config->cloning = (atoi(pos));
            //TODO: Added temporary fix for Bug #19290
            if ( atoi(pos) == 2 ) {
                printf("\n\tError: Invalid value for \"-cloning\" field\n\n");
                cond_code = ERR_INVALID_INPUT;
                goto error;
            }
        } else if (strcmp(buf, CONFIG_PARAM_CERT_AUTH) == 0) {
            config->certAuth= atoi(pos);
        } else if (strcmp(buf, CONFIG_PARAM_KEK_METHOD) == 0) {
            config->kek_method = (atoi(pos));
        } else if (strcmp(buf, CONFIG_PARAM_GROUP_ID) == 0) {
            memset(&config->g_id, 0, MAX_GROUP_ID_LEN);
            if (strlen(pos) > MAX_GROUP_ID_LEN) {
                print_debug
                    ("Invalid group-id length %zd, max group-id length is %d\n",
                     strlen(pos), MAX_GROUP_ID_LEN);
                cond_code = ERR_DATA_LEN_RANGE;
                goto error;
            }
            memcpy(&config->g_id, pos, strlen(pos));
            print_debug("group id = %s\n", config->g_id);
        } else if (strcmp(buf, CONFIG_PARAM_HSM_LABEL) == 0) {
            memset(&config->label, 0, MAX_LABEL_LENGTH);
            if (strlen(pos) > MAX_LABEL_LENGTH) {
                print_debug
                    ("Invalid label length %zd, max label length is %d\n",
                     strlen(pos), MAX_LABEL_LENGTH);
                cond_code = ERR_DATA_LEN_RANGE;
                goto error;
            }
            memcpy(&config->label, pos, strlen(pos));
            print_debug("HSM label = %s\n", config->label);
        } else {
            print_debug("Line %d: unknown configuration item '%s'\n", line,
                    buf);
            cond_code = ERR_INVALID_INPUT;
            goto error;
        }
    }

error:
    if (fd)
        fclose(fd);
    return cond_code;
}
#endif
/****************************************************************************
 *
 * FUNCTION     : initializeHSM
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 initializeHSM(int argc, char **argv)
{
    HSMInfo hsm_info = { };
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bCOPswd = FALSE;
    char *pCOPswd = 0;
    Uint32 ulCOPswdLen = 0;

    Uint8 bUserPswd = FALSE;
    char *pUserPswd = 0;
    Uint32 ulUserPswdLen = 0;


    Uint8 bUserName = FALSE;
    char *pUserName = 0;
    Uint32 ulUserNameLen = 0;

    Uint8 bOfficerName = FALSE;
    char *pOfficerName = 0;
    Uint32 ulOfficerNameLen = 0;


    Uint8 bConf = FALSE;
    char *pConfFile = 0;
    Uint32 ulConfFileNameLen = 0;

    Uint8 bAuthMode = FALSE;
    Uint32 ulAuthMode = 0;

#ifndef CLOUD_HSM_CLIENT
    Uint8 pEncPswd[PSWD_ENC_KEY_MODULUS] = { 0 };
    Uint32 ulEncPswdLen = PSWD_ENC_KEY_MODULUS;

    Uint8  bDualFactorHost   = FALSE;
    char*  pDualFactorHost   = 0;
    Uint32 ulDualFactorHost  = 0;

    Uint8  bDualFactorPort   = FALSE;
    Uint32 pDualFactorPort   = 0;

    Uint8  bDualFactorCert   = FALSE;
    char*  pDualFactorCert   = 0;
#endif

    Uint8  bDualFactorCOSlot   = FALSE;
    Uint32 pDualFactorCOSlot   = 0;

    Uint8  bDualFactorCOPin   = FALSE;
    char*  pDualFactorCOPin   = 0;
    Uint32 ulDualFactorCOPin  = 0;

    Uint8  bDualFactorCOToken   = FALSE;
    char*  pDualFactorCOToken   = 0;
    Uint32 ulDualFactorCOToken  = 0;

    Uint8  bDualFactorCOUserPin   = FALSE;
    char*  pDualFactorCOUserPin   = 0;
    Uint32 ulDualFactorCOUserPin  = 0;

    Uint8  bDualFactorCUSlot   = FALSE;
    Uint32 pDualFactorCUSlot   = 0;

    Uint8  bDualFactorCUPin   = FALSE;
    char*  pDualFactorCUPin   = 0;
    Uint32 ulDualFactorCUPin  = 0;

    Uint8  bDualFactorCUToken   = FALSE;
    char*  pDualFactorCUToken   = 0;
    Uint32 ulDualFactorCUToken  = 0;

    Uint8  bDualFactorCUUserPin   = FALSE;
    char*  pDualFactorCUUserPin   = 0;
    Uint32 ulDualFactorCUUserPin  = 0;

    Uint8 *pPublicKey = NULL;
    Uint8 *pSignature = NULL;

    Uint32 ulNumParams = 0;
    char *pInitParams[32] = { "0" };
    Uint8 bFipsState = FALSE;
    Uint8 bLoginFailCount = FALSE;
    Uint8 bMaxPswdLen = FALSE;
    Uint8 bMinPswdLen = FALSE;
    Uint8 bCloning = FALSE;
    Uint8 bKekMethod = FALSE;
    Uint8 bKeyWrap = FALSE;
    Uint8 bKeyUnwrap = FALSE;
    Uint8 bCertAuth = FALSE;
    Uint8 bLabel = FALSE;
    Uint8 bGroupId = FALSE;

    int min_pwd_len = -1;
    int max_pwd_len = -1;

    InitTokenCommand cmdInit = { {{0}} };

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0) {
            bHelp = TRUE;
            break;
        }
        // CO pin
        else if (!bCOPswd && (strcmp(argv[i], "-p") == 0) && (argc > i + 1))
            bCOPswd = readArgAsString(argv[i + 1], &pCOPswd, &ulCOPswdLen);

        // CU pin
        else if (!bUserPswd && (strcmp(argv[i], "-u") == 0) && (argc > i + 1))
            bUserPswd =
                readArgAsString(argv[i + 1], &pUserPswd, &ulUserPswdLen);

        else if (!bUserName && (strcmp(argv[i], "-sU") == 0)
             && (argc > i + 1))
            bUserName =
                readArgAsString(argv[i + 1], &pUserName, &ulUserNameLen);

        else if (!bOfficerName && (strcmp(argv[i], "-sO") == 0)
             && (argc > i + 1))
            bOfficerName =
                readArgAsString(argv[i + 1], &pOfficerName, &ulOfficerNameLen);

        else if (!bAuthMode && (strcmp(argv[i], "-a") == 0) && (argc > i + 1)) {
            if(!strcmp(argv[i + 1],"0") || !strcmp(argv[i + 1],"1"))
            {
                ulAuthMode = atoi(argv[i + 1]);
                bAuthMode = TRUE;
            }
        } else if ((!bFipsState) && (strcmp(argv[i], "-fips_state") == 0)
               && (argc > i + 1)) {
            pInitParams[ulNumParams++] = "fips_state";
            pInitParams[ulNumParams++] = argv[i + 1];
            fipsState = atoi(argv[i + 1]);
            bFipsState = TRUE;
        } else if ((!bLoginFailCount)
               && (strcmp(argv[i], "-login_fail_count") == 0)
               && (argc > i + 1)) {
            pInitParams[ulNumParams++] = "login_fail_count";
            pInitParams[ulNumParams++] = argv[i + 1];
            bLoginFailCount = TRUE;
        } else if ((!bMaxPswdLen)
               && (strcmp(argv[i], "-max_password_length") == 0)
               && (argc > i + 1)) {
            pInitParams[ulNumParams++] = "max_password_length";
            pInitParams[ulNumParams++] = argv[i + 1];
            max_pwd_len = atoi(argv[i+1]);
            bMaxPswdLen = TRUE;
        } else if ((!bMinPswdLen)
               && (strcmp(argv[i], "-min_password_length") == 0)
               && (argc > i + 1)) {
            pInitParams[ulNumParams++] = "min_password_length";
            pInitParams[ulNumParams++] = argv[i + 1];
            min_pwd_len = atoi(argv[i+1]);
            bMinPswdLen = TRUE;
        } else if ((!bCloning) && (strcmp(argv[i], "-cloning") == 0)
               && (argc > i + 1)) {
            pInitParams[ulNumParams++] = "cloning";
            pInitParams[ulNumParams++] = argv[i + 1];
            bCloning = TRUE;
        } else if ((!bKekMethod) && (strcmp(argv[i], "-kek_method") == 0)
               && (argc > i + 1)) {
            pInitParams[ulNumParams++] = "kek_method";
            pInitParams[ulNumParams++] = argv[i + 1];
            bKekMethod = TRUE;
        } else if ((!bKeyWrap) && (strcmp(argv[i], "-secret_key_wrap") == 0)
               && (argc > i + 1)) {
            pInitParams[ulNumParams++] = "secret_key_wrap";
            pInitParams[ulNumParams++] = argv[i + 1];
            bKeyWrap = TRUE;
        } else if ((!bKeyUnwrap)
               && (strcmp(argv[i], "-secret_key_unwrap") == 0)
               && (argc > i + 1)) {
            pInitParams[ulNumParams++] = "secret_key_unwrap";
            pInitParams[ulNumParams++] = argv[i + 1];
            bKeyUnwrap = TRUE;
        } else if ((!bCertAuth)
               && (strcmp(argv[i], "-cert_auth") == 0)
               && (argc > i + 1)) {
            pInitParams[ulNumParams++] = "cert_auth";
            pInitParams[ulNumParams++] = argv[i + 1];
            bCertAuth= TRUE;
        } else if ((!bLabel) && (strcmp(argv[i], "-label") == 0)
               && (argc > i + 1)) {
            pInitParams[ulNumParams++] = "label";
            pInitParams[ulNumParams++] = argv[i + 1];
            bLabel = TRUE;
        } else if ((!bGroupId) && (strcmp(argv[i], "-group_id") == 0)
               && (argc > i + 1)) {
            pInitParams[ulNumParams++] = "group_id";
            pInitParams[ulNumParams++] = argv[i + 1];
            bGroupId = TRUE;
        }
        // conf file name
        else if (!bConf && (strcmp(argv[i], "-f") == 0) && (argc > i + 1)) {
            ulConfFileNameLen = strlen(BUILD_DIR) + strlen(argv[i + 1]);
            pConfFile = malloc(ulConfFileNameLen + 1);
            memset(pConfFile, 0, ulConfFileNameLen + 1);
            strcat(pConfFile, BUILD_DIR);
            strcat(pConfFile, argv[i + 1]);
            bConf = TRUE;
        }
#ifndef CLOUD_HSM_CLIENT
        //  dual factor authentication hostname
        else if ((!bDualFactorHost ) && (strcmp(argv[i],"-dh")==0) && (argc > i+1))
        {
            bDualFactorHost = readArgAsString(argv[i+1], &pDualFactorHost, &ulDualFactorHost);
        }
        //  dual factor authentication port
        else if ((!bDualFactorPort ) && (strcmp(argv[i],"-dp")==0) && (argc > i+1))
        {
            bDualFactorPort = readIntegerArg(argv[i+1], &pDualFactorPort);
        }
        //  dual factor authentication server certificate path
        else if ((!bDualFactorCert ) && (strcmp(argv[i],"-dc")==0) && (argc > i+1))
        {
            pDualFactorCert = argv[i+1];
            bDualFactorCert = TRUE;
        }
#endif
        //  dual factor authentication CO token slot
        else if ((!bDualFactorCOSlot ) && (strcmp(argv[i],"-dn")==0) && (argc > i+1))
        {
            if(!isdigit(argv[i+1][0])) {
                pDualFactorCOSlot = -1;
                bDualFactorCOSlot = TRUE;
            } else {
                bDualFactorCOSlot = readIntegerArg(argv[i+1], &pDualFactorCOSlot);
            }
        }
        //  dual factor authentication CO SO pin
        else if ((!bDualFactorCOPin ) && (strcmp(argv[i],"-ds")==0) && (argc > i+1))
        {
            bDualFactorCOPin = readArgAsString(argv[i+1], &pDualFactorCOPin, &ulDualFactorCOPin);
        }
        //  dual factor authentication CO token label
        else if ((!bDualFactorCOToken ) && (strcmp(argv[i],"-dt")==0) && (argc > i+1))
        {
            bDualFactorCOToken = readArgAsString(argv[i+1], &pDualFactorCOToken, &ulDualFactorCOToken);
        }
        //  dual factor authentication CO user pin
        else if ((!bDualFactorCOUserPin ) && (strcmp(argv[i],"-du")==0) && (argc > i+1))
        {
            bDualFactorCOUserPin = readArgAsString(argv[i+1], &pDualFactorCOUserPin, &ulDualFactorCOUserPin);
        }
        //  dual factor authentication CU token slot
        else if ((!bDualFactorCUSlot ) && (strcmp(argv[i],"-dun")==0) && (argc > i+1))
        {
            if(!isdigit(argv[i+1][0])) {
                pDualFactorCUSlot = -1;
                bDualFactorCUSlot = TRUE;
            } else {
                bDualFactorCUSlot = readIntegerArg(argv[i+1], &pDualFactorCUSlot);
            }
        }
        //  dual factor authentication CU SO pin
        else if ((!bDualFactorCUPin ) && (strcmp(argv[i],"-dus")==0) && (argc > i+1))
        {
            bDualFactorCUPin = readArgAsString(argv[i+1], &pDualFactorCUPin, &ulDualFactorCUPin);
        }
        //  dual factor authentication CU token label
        else if ((!bDualFactorCUToken ) && (strcmp(argv[i],"-dut")==0) && (argc > i+1))
        {
            bDualFactorCUToken = readArgAsString(argv[i+1], &pDualFactorCUToken, &ulDualFactorCUToken);
        }
        //  dual factor authentication CU user pin
        else if ((!bDualFactorCUUserPin ) && (strcmp(argv[i],"-duu")==0) && (argc > i+1))
        {
            bDualFactorCUUserPin = readArgAsString(argv[i+1], &pDualFactorCUUserPin, &ulDualFactorCUUserPin);
        }

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bCOPswd) {
        printf("\n\tError: CO pin (-p) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bOfficerName) {
        printf("\n\tError: Officer Name (-sO) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && (bUserPswd^bUserName)) {
        if (!bUserPswd)
            printf("\n\tError: CU pin (-u) is missing.\n");
        if (!bUserName)
            printf("\n\tError: Username (-sU) is missing\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bAuthMode) {
        printf("\n\tError: Authentication mode (-a) is missing / Wrong parameters passed.\n");
        bHelp = TRUE;
    }

    if (fipsState == 3) {
        if(ulAuthMode != 1) {
            printf("\n\tError: Authentication mode (-a) must be set to 1 for fips_state 3.\n");
            bHelp = TRUE;
        }
    }

#ifdef CLOUD_HSM_CLIENT
    if (!bHelp && (fipsState == 3)) {
        if ( ! (bDualFactorCOSlot && bDualFactorCOPin && bDualFactorCOToken && bDualFactorCOUserPin)) {
            printf("\n\tError: For network dual factor authentication -dn,-ds,-dt and -du options must be specified.\n");
            bHelp = TRUE;
        }
        if (bUserPswd && bUserName) {
            if ( ! (bDualFactorCUSlot && bDualFactorCUPin && bDualFactorCUToken && bDualFactorCUUserPin)) {
                printf("\n\tError: For network dual factor authentication -dun,-dus,-dut and -duu options must be specified.\n");
                bHelp = TRUE;
            }
        }
    }
#else
    if (!bHelp && (bDualFactorHost || bDualFactorPort || bDualFactorCert ||
               bDualFactorCOSlot || bDualFactorCOPin || bDualFactorCOToken || bDualFactorCOUserPin))
    {
        if ( ! (bDualFactorHost && bDualFactorPort && bDualFactorCert &&
            bDualFactorCOSlot && bDualFactorCOPin && bDualFactorCOToken && bDualFactorCOUserPin)) {
            printf("\n\tError: For network dual factor authentication -dh,-dp,-dc,-dn,-ds,-dt and -du options must be specified.\n");
            bHelp = TRUE;
        }

        if (bUserPswd && bUserName) {
            if ( !(bDualFactorCUSlot && bDualFactorCUPin && bDualFactorCUToken && bDualFactorCUUserPin)) {
                printf("\n\tError: For network dual factor authentication -dun,-dus,-dut and -duu\n\toptions must be specified if CU user is created.\n");
                bHelp = TRUE;
            }
        } else if (bDualFactorCUSlot || bDualFactorCUPin
               || bDualFactorCUToken || bDualFactorCUUserPin) {
            printf ("\n\tError: CU options -u and -sU are missing\n");
            bHelp = TRUE;
        }
    }
#endif

    if (bDualFactorCOSlot) {
        if(pDualFactorCOSlot == -1) {
            printf("\n\tError: Invalid value for -dn.");
            bHelp = TRUE;
        }
    }

    if (bDualFactorCUSlot) {
        if(pDualFactorCUSlot == -1) {
            printf("\n\tError: Invalid value for -dun.");
            bHelp = TRUE;
        }
    }

    if (!bHelp && ( (ulCOPswdLen < MIN_PSWD_LEN) || (ulCOPswdLen > MAX_PSWD_LEN) )) {
        printf("\n\tError: Incorrect password length.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bUserPswd && ( (ulUserPswdLen < MIN_PSWD_LEN) || (ulUserPswdLen > MAX_PSWD_LEN) )) {
        printf("\n\tError: Incorrect password length.\n");
        bHelp = TRUE;
    }

    if ( (ulDualFactorCOPin > MAX_PSWD_LEN) || (ulDualFactorCOToken > MAX_LABEL_LENGTH) || (ulDualFactorCOUserPin > MAX_PSWD_LEN) ||
         (ulDualFactorCUPin > MAX_PSWD_LEN) || (ulDualFactorCUToken > MAX_LABEL_LENGTH) || (ulDualFactorCUUserPin > MAX_PSWD_LEN) ) {
        printf("\n\tError: Dual factor argument(s) length incorrect.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf
            ("\nInitializes the HSM with a CO Password, User Password, HSM Label and a FIPS enabled flag.");
        printf("\n");
        printf("\nSyntax: initHSM -h -p <CO Pswd> -sO <CO username> -u <CU Pswd> -sU <CU username> -a <auth_lvl> \
               \n                   [-fips_state <0/2/3>]"
               "[-login_fail_count <count>] \
               \n                   [-max_password_length <length>] [-min_password_length <length>]");
        //TODO: Added temporary fix for Bug #19290
        printf("\n                   [-cloning <0/1>] [-kek_method <0/1>]");
        printf("\n                   [-secret_key_wrap <0/1>] [-secret_key_unwrap <0/1>] \
               \n                   [-cert_auth <0/1>] \
               \n                   [-label <HSM label>] [-group_id <gid> ] [-f < HSM configuration file>]");
#ifndef CLOUD_HSM_CLIENT
        printf("\n                   [-dh <host> -dp <port> -dc <cert> -dn <slot> -ds <so pin> -dt <token label>");
#else
        printf("\n                   [");
#endif
        printf("\n                   -dn <CO slot> -ds <CO so pin> -dt <CO token label> -du <CO user pin> -dun <CU slot> \
               \n                   -dus <CU so pin> -dut <CU token label> -duu <CU user pin>]");

        printf("\n");
        printf("\nWhere: -h   displays this information");
        printf("\n       -p   specifies the CO password");
        printf("\n       -sO  specifies the CO username");
        printf("\n       -u   specifies the CU password (Optional)");
        printf("\n       -sU  specifies the CU username (Optional)");
        printf("\n       -a   specifies the authentication mode \
               \n            '0' for single-factor authentication with username and password\
               \n            '1' for two-factor authentication with additional key from USB dongle");
        printf
            ("\n\n    User can specify HSM configuration parameter values through a file\
             \n    or through command line arguments.\
             \n    Otherwise default values (specified in [ ]) are used:  \n");

        printf("\n Using configuration file :");

        printf("\n -f   specifies the HSM configuration file name (hsm_config) \
               \n      (It assumes path to configuration file is %s)",
               BUILD_DIR);

        printf("\n\n Usage with command line arguments:");
        printf("\n\t -fips_state            specifies the fips level [2], \
               \n\t                        0 for non-fips mode \
               \n\t                        2 for fips mode \
               \n\t                        3 for fips mode with 2-factor auth");
        printf
            ("\n\t -login_fail_count      specifies login failure count [20]");
        printf
            ("\n\t -max_password_length   specifies maxium password length [14]");
        printf
            ("\n\t -min_password_length   specifies minimum password length [7]");
        //TODO: Added temporary fix for Bug #19290
        printf("\n\t -cloning               specifies cloning method [1], \
               \n\t                        0 - cloning is not supported, \
               \n\t                        1 - using ECC");


        printf("\n\t -kek_method            specifies KEK method [0], \
               \n\t                        0 - ECC, 1- RSA");

        printf("\n\t -cert_auth             specifies cert_auth [0], \
               \n\t                        0 - disable, 1- enable");

        printf
            ("\n\t -secret_key_wrap       support for secret key wrapping [1] \
             \n\t                        0 - not supported, 1-supported");
        printf
            ("\n\t -secret_key_unwrap     support for secret key unwrapping[1] \
             \n\t                        0 - not supported, 1-supported");

        printf("\n\t -label                 specifies HSM label [cavium]");
        printf
            ("\n\t -group_id              specifies cloning group [group0]");
#ifndef CLOUD_HSM_CLIENT
        printf("\n\t -dh                    specifies the hostname for network dual factor authentication server");
        printf("\n\t -dp                    specifies the port used by network dual factor authentication server");
        printf("\n\t -dc                    specifies the certificate used by network dual factor authentication server");
#endif
        printf("\n\t -dn                    specifies the slot number for SmartCard(CO) in network dual factor authentication server");
        printf("\n\t -ds                    specifies the so pin for SmartCard(CO) in network dual factor authentication server");
        printf("\n\t -dt                    specifies the token label for SmartCard(CO) in network dual factor authentication server");
        printf("\n\t -du                    specifies the user pin for SmartCard(CO) in network dual factor authentication server");
        printf("\n\t -dun                   specifies the slot number for SmartCard(CU) in network dual factor authentication server");
        printf("\n\t -dus                   specifies the so pin for SmartCard(CU) in network dual factor authentication server");
        printf("\n\t -dut                   specifies the token label for SmartCard(CU) in network dual factor authentication server");
        printf("\n\t -duu                   specifies the user pin for SmartCard(CU) in network dual factor authentication server");
        printf("\n\n");
        return ulRet;
    }

    if (fipsState == 3) ulAuthMode = 1;

    // invoke the command
    if (ulAuthMode) {
        if (bConf) {
            printf("Make sure that fips_state in %s is set to 3!!\n",
                   pConfFile);
            getchar();
        } else if (fipsState != 3) {
            printf
                ("Incorrect configuration: FIPS state %d and auth level %d!!\n",
                 fipsState, ulAuthMode);
            fipsState = -1;
            return ERR_INVALID_INPUT;
        }
    }
    if(bMinPswdLen || bMaxPswdLen) {
        if(bMinPswdLen && bMaxPswdLen && (min_pwd_len > max_pwd_len)) {
            printf("Minimum Password length[%d] greater than maximum password length[%d]\n", min_pwd_len, max_pwd_len);
            return ERR_INVALID_INPUT;
        }
        if(bMinPswdLen && (ulCOPswdLen < min_pwd_len)) {
            printf("CO Password length[%d] less than minimum specified[%d]\n",ulCOPswdLen, min_pwd_len);
            return ERR_INVALID_INPUT;
        }
        if(bMinPswdLen && (ulUserPswdLen < min_pwd_len)) {
            printf("CU Password length[%d] less than minimum specified[%d]\n",ulCOPswdLen, min_pwd_len);
            return ERR_INVALID_INPUT;
        }
        if(bMaxPswdLen && (ulCOPswdLen > max_pwd_len)) {
            printf("CO Password length[%d] greater than maximum specified[%d]\n",ulCOPswdLen, max_pwd_len);
            return ERR_INVALID_INPUT;
        }
        if(bMaxPswdLen && (ulUserPswdLen > max_pwd_len)) {
            printf("CU Password length[%d] greater than maximum specified[%d]\n",ulCOPswdLen, max_pwd_len);
            return ERR_INVALID_INPUT;
        }
    }

    if (bConf)
        printf("\nInitialize using Configuration file %s \n", pConfFile);
    else if (ulNumParams > 0)
        printf("\nInitialize using command line options \n");
    else
        printf("\nInitialize using default values \n");

#ifdef WORKAROUND_FOR_BUG_19921
    if(bConf) {
        Uint32 cond_code = 0;
        cond_code = read_configuration(pConfFile, &cmdInit.config);
        printf("\nError: Initialize using Configuration file %s, file not found \n", pConfFile);
        if (cond_code != SUCCESS) {
            free(pConfFile);
            return ERR_INVALID_INPUT;
        }
        if ((ulCOPswdLen < cmdInit.config.minPswdLength) || (ulCOPswdLen > cmdInit.config.maxPswdLength) ) {
            printf("\n\tError: Incorrect password length.\n");
            free(pConfFile);
            return ERR_INVALID_INPUT;
        }
        if(bUserPswd && ((ulUserPswdLen < cmdInit.config.minPswdLength) || (ulUserPswdLen > cmdInit.config.maxPswdLength))) {
            printf("\n\tError: Incorrect password length.\n");
            free(pConfFile);
            return ERR_INVALID_INPUT;
        }
    }
#endif
    if (bConf)
        /* Step 1: Load Config file */
        ulRet = Cfm2InitHSM(session_handle, pConfFile);
    else
        ulRet = Cfm2InitHSMNew(session_handle, pInitParams, ulNumParams);

    if (ulRet) {
        printf("Cfm2InitHSM failed %s\n", Cfm2ResultAsString(ulRet));
        if (bConf) {
            if (ulRet != RET_HSM_NOT_INITIALIZED)
                printf("\n\tPlease verify  %s..\n", pConfFile);
            free(pConfFile);
        }
        if (ulRet == RET_HSM_NOT_INITIALIZED)
            printf("Please login as CO with default credentials \n");
        return ulRet;
    }

    if (bConf) {
        printf("\n\t Loaded the Configuration file \n");
        free(pConfFile);
    }

    if ((bCOPswd && !strcmp(pCOPswd, "default"))
        || (bUserPswd && !strcmp(pUserPswd, "default"))) {
        printf("\n\t CO/CU pin should not be \"default\" \n");
        return ulRet;
    }

    generatePEK(0, NULL);
#ifndef CLOUD_HSM_CLIENT
    ulRet = encrypt_pswd(session_handle,
                 (Uint8 *) pCOPswd, ulCOPswdLen,
                 pEncPswd, &ulEncPswdLen);
    if (ulRet != RET_OK) {

        printf("password encryption failed ulRet %d: %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        return ulRet;
    }

    if (ulAuthMode) {
        if(!bDualFactorHost) {
            printf("\n\t******Insert Crypto Officer SmartCard******\n");
            printf("Hit Enter key to continue\n");
            getchar();

#ifdef USE_SMARTCARD_AS_POSSESSION_FACTOR
            ulRet = initSmartCard(1);
            if (ulRet != RET_OK) {
                printf("\n\tFailed to initialize Smart Card\n");
                shutdownSmartCard();
                return ulRet;
            }
#endif
            if (pPublicKey == NULL)
                pPublicKey = malloc(PSWD_ENC_KEY_MODULUS * 2);
            if (pSignature == NULL)
                pSignature = malloc(PSWD_ENC_KEY_MODULUS);
            if ((pSignature == NULL) || (pPublicKey == NULL)) {
                printf("\n\t Failed to allocate memory\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }

            memset(pPublicKey, 0, PSWD_ENC_KEY_MODULUS*2);
            memset(pSignature, 0, PSWD_ENC_KEY_MODULUS);

            ulRet = generate_key_pair_on_eToken(session_handle, pOfficerName, pCOPswd);
            if (ulRet != 0) {
                printf("\n\t Failed to generate key pair on eToken\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }
            ulRet = get_public_key_from_eToken(session_handle, pOfficerName, pCOPswd, pPublicKey);
            if (ulRet != 0) {
                printf("\n\t Failed to get public key from eToken\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }
            ulRet = get_signature_from_eToken(session_handle, pOfficerName, pCOPswd, pEncPswd, ulEncPswdLen, pSignature);
            if (ulRet != 0) {
                printf("\n\t Failed to get signature from eToken\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }
#ifdef USE_SMARTCARD_AS_POSSESSION_FACTOR
            shutdownSmartCard();
#endif
        } else { //dual factor authentication over network
            printf("\n\tPlease ensure dual factor authentication server is running\n");
            SSL_CTX *ctx = NULL;
            int server = 0;
            SSL *ssl = NULL;
            df_response resp_data;

            if (pPublicKey == NULL)
                pPublicKey = malloc(PSWD_ENC_KEY_MODULUS * 2);
            if (pSignature == NULL)
                pSignature = malloc(PSWD_ENC_KEY_MODULUS);
            if ((pSignature == NULL) || (pPublicKey == NULL)) {
                printf("\n\t Failed to allocate memory\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }

            memset(pPublicKey, 0, PSWD_ENC_KEY_MODULUS*2);
            memset(pSignature, 0, PSWD_ENC_KEY_MODULUS);

            SSL_library_init();
            ctx = InitSSL_CTX();

            if(!SSL_CTX_load_verify_locations(ctx, pDualFactorCert, NULL)){
                ulRet = -1;
                printf("\n\tCould not load the certificate trust chain\n");
                goto ssl_fail;
            }

            SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);
            server = OpenSocketConnection(pDualFactorHost, pDualFactorPort);
            ssl = SSL_new(ctx);
            SSL_set_fd(ssl, server);

            ulRet =  SSL_connect(ssl);
            if ( ulRet == -1 ) {
                printf("\n\nSSL connect failed\n");
                goto ssl_fail;
            }

            df_request req_data;

            req_data.command_type = DF_INITIALIZE;
            req_data.init_data.reinitialize = 1;
            req_data.init_data.slot = pDualFactorCOSlot;
            memcpy(req_data.init_data.SOPin,pDualFactorCOPin,ulDualFactorCOPin+1);
            memcpy(req_data.init_data.tokenLabel,pDualFactorCOToken,ulDualFactorCOToken+1);
            memcpy(req_data.init_data.userPin,pDualFactorCOUserPin,ulDualFactorCOUserPin+1);

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code != 0) {
                    printf("\n\tFailed to get initialize SmartCard over network\n");
                    ulRet = -1;
                    goto token_fail;
                }
            } else {
                printf("\n\tSSL_read failed\n");
                ulRet = -1;
                goto token_fail;
            }

            memset(&req_data,0,sizeof(req_data));
            memset(&resp_data,0,sizeof(resp_data));

            req_data.command_type = DF_GENERATE_KEY_PAIR;
            req_data.gen_key_data.session_handle = session_handle;
            memcpy(req_data.gen_key_data.userName,pOfficerName,ulOfficerNameLen+1);
            memcpy(req_data.gen_key_data.password,pCOPswd,ulCOPswdLen+1);

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code != 0) {
                    printf("\n\tFailed to get generate key pair over network\n");
                    ulRet = -1;
                    goto token_fail;
                }
            } else {
                printf("\n\tSSL_read failed\n");
                ulRet = -1;
                goto token_fail;
            }

            memset(&req_data,0,sizeof(req_data));
            memset(&resp_data,0,sizeof(resp_data));

            req_data.command_type = DF_GET_KEY;
            req_data.get_key_data.session_handle = session_handle;
            memcpy(req_data.get_key_data.userName,pOfficerName,ulOfficerNameLen+1);
            memcpy(req_data.get_key_data.password,pCOPswd,ulCOPswdLen+1);

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code == 0) {
                    memcpy(pPublicKey,&(resp_data.data),2*PSWD_ENC_KEY_MODULUS);
                } else {
                    printf("\n\tFailed to get public key over network\n");
                    ulRet = -1;
                    goto token_fail;
                }
            } else {
                printf("\n\tSSL_read failed\n");
                ulRet = -1;
                goto token_fail;
            }

            memset(&req_data,0,sizeof(req_data));
            memset(&resp_data,0,sizeof(resp_data));

            req_data.command_type = DF_GET_SIGNATURE;
            req_data.get_sign_data.session_handle = session_handle;
            memcpy(req_data.get_sign_data.userName,pOfficerName,ulOfficerNameLen+1);
            memcpy(req_data.get_sign_data.password,pCOPswd,ulCOPswdLen+1);
            memcpy(req_data.get_sign_data.pEncPswd,pEncPswd,ulEncPswdLen);
            req_data.get_sign_data.ulEncPswdLen = ulEncPswdLen;

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code == 0) {
                    memcpy(pSignature,&(resp_data.data),PSWD_ENC_KEY_MODULUS);
                } else {
                    printf("\n\tFailed to get signature over network\n");
                    ulRet = -1;
                    goto token_fail;
                }
            } else {
                printf("\n\tSSL_read failed\n");
                ulRet = -1;
                goto token_fail;
            }

token_fail:
            memset(&req_data,0,sizeof(req_data));
            req_data.command_type = DF_END;
            SSL_write(ssl, &req_data, sizeof(req_data));

ssl_fail:
            if(ssl) SSL_free(ssl);
            if(server) close(server);
            if(ctx) SSL_CTX_free(ctx);
            if(ulRet == -1) return ulRet;
        }
    }

    /* Step 2: Create CO pin, Create CU pin */
    ulRet = Cfm2CreateUser2(session_handle,
                CN_CRYPTO_OFFICER,
                (Uint8 *) pOfficerName, ulOfficerNameLen,
                (Uint8 *) pEncPswd, ulEncPswdLen,
                pPublicKey, pSignature);
#else
    if (ulAuthMode) {
        if (pPublicKey == NULL)
            pPublicKey = malloc(PSWD_ENC_KEY_MODULUS * 2);
        if (pSignature == NULL)
            pSignature = malloc(PSWD_ENC_KEY_MODULUS);
        if ((pSignature == NULL) || (pPublicKey == NULL)) {
            printf("\n\t Failed to allocate memory\n");
            ulRet = ERR_MEMORY_ALLOC_FAILURE;
            return ulRet;
        }

        memset(pPublicKey, 0, PSWD_ENC_KEY_MODULUS*2);
        memset(pSignature, 0, PSWD_ENC_KEY_MODULUS);

        memcpy(pPublicKey, pDualFactorCOPin, MAX_PSWD_LENGTH+1);
        memcpy(pPublicKey+MAX_PSWD_LENGTH+1, pDualFactorCOToken, MAX_LABEL_LENGTH+1);
        memcpy(pPublicKey+MAX_PSWD_LENGTH+MAX_LABEL_LENGTH+2, pDualFactorCOUserPin, MAX_PSWD_LENGTH+1);
        memcpy(pPublicKey+(2*MAX_PSWD_LENGTH)+MAX_LABEL_LENGTH+3, &pDualFactorCOSlot, sizeof(pDualFactorCOSlot));
    }
    ulRet = Cfm2CreateUser2(session_handle,
                CN_CRYPTO_OFFICER,
                (Uint8 *) pOfficerName, ulOfficerNameLen,
                (Uint8 *) pCOPswd, ulCOPswdLen,
                pPublicKey, pSignature);

#endif
    if (ulRet) {
        printf("Cfm2CreateUser2 for CO failed %s \n",
               Cfm2ResultAsString(ulRet));
        printf
            ("If you exit now.. it will take some time for HSM to comeout of the critical stage\n");
        return ulRet;
    }

    if (bUserPswd && bUserName) {

#ifndef CLOUD_HSM_CLIENT
        ulRet = encrypt_pswd(session_handle,
                     (Uint8 *) pUserPswd, ulUserPswdLen,
                     pEncPswd, &ulEncPswdLen);
        if (ulRet != RET_OK) {
            printf("password encryption failed ulRet %d: %s\n",
                   ulRet, Cfm2ResultAsString(ulRet));
            return ulRet;
        }

        if (ulAuthMode) {
            if(!bDualFactorHost) {
                printf("\n\t******Insert Crypto User SmartCard******\n");
                printf("Hit Enter key to continue\n");
                getchar();

#ifdef USE_SMARTCARD_AS_POSSESSION_FACTOR
                ulRet = initSmartCard(1);
                if (ulRet != RET_OK) {
                    printf("\n\tFailed to initialize Smart Card\n");
                    shutdownSmartCard();
                    return ulRet;
                }
#endif
                if (pPublicKey == NULL)
                    pPublicKey = malloc(PSWD_ENC_KEY_MODULUS * 2);
                if (pSignature == NULL)
                    pSignature = malloc(PSWD_ENC_KEY_MODULUS);
                if ((pSignature == NULL) || (pPublicKey == NULL)) {
                    printf("\n\t Failed to allocate memory\n");
                    ulRet = ERR_MEMORY_ALLOC_FAILURE;
                    return ulRet;
                }

                memset(pPublicKey, 0, PSWD_ENC_KEY_MODULUS*2);
                memset(pSignature, 0, PSWD_ENC_KEY_MODULUS);

                ulRet = generate_key_pair_on_eToken(session_handle, pUserName, pUserPswd);
                if (ulRet != 0) {
                    printf("\n\t Failed to generate key pair on eToken\n");
                    ulRet = ERR_MEMORY_ALLOC_FAILURE;
                    return ulRet;
                }
                ulRet = get_public_key_from_eToken(session_handle, pUserName, pUserPswd, pPublicKey);
                if (ulRet != 0) {
                    printf("\n\t Failed to get public key from eToken\n");
                    ulRet = ERR_MEMORY_ALLOC_FAILURE;
                    return ulRet;
                }
                ulRet = get_signature_from_eToken(session_handle, pUserName, pUserPswd, pEncPswd, ulEncPswdLen, pSignature);
                if (ulRet != 0) {
                    printf("\n\t Failed to get signature from eToken\n");
                    ulRet = ERR_MEMORY_ALLOC_FAILURE;
                    return ulRet;
                }
#ifdef USE_SMARTCARD_AS_POSSESSION_FACTOR
                shutdownSmartCard();
#endif
            } else { //dual factor authentication over network
                SSL_CTX *ctx = NULL;
                int server = 0;
                SSL *ssl = NULL;
                df_response resp_data;

                if (pPublicKey == NULL)
                    pPublicKey = malloc(PSWD_ENC_KEY_MODULUS * 2);
                if (pSignature == NULL)
                    pSignature = malloc(PSWD_ENC_KEY_MODULUS);
                if ((pSignature == NULL) || (pPublicKey == NULL)) {
                    printf("\n\t Failed to allocate memory\n");
                    ulRet = ERR_MEMORY_ALLOC_FAILURE;
                    return ulRet;
                }

                memset(pPublicKey, 0, PSWD_ENC_KEY_MODULUS*2);
                memset(pSignature, 0, PSWD_ENC_KEY_MODULUS);

                SSL_library_init();
                ctx = InitSSL_CTX();

                if(!SSL_CTX_load_verify_locations(ctx, pDualFactorCert, NULL)){
                    ulRet = -1;
                    printf("\n\tCould not load the certificate trust chain\n");
                    goto ssl_fail_CU;
                }

                SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);
                server = OpenSocketConnection(pDualFactorHost, pDualFactorPort);
                ssl = SSL_new(ctx);
                SSL_set_fd(ssl, server);

                ulRet =  SSL_connect(ssl);
                if ( ulRet == -1 ) {
                    printf("\n\nSSL connect failed\n");
                    goto ssl_fail_CU;
                }

                df_request req_data;

                req_data.command_type = DF_INITIALIZE;
                req_data.init_data.reinitialize = 1;
                req_data.init_data.slot = pDualFactorCUSlot;
                memcpy(req_data.init_data.SOPin,pDualFactorCUPin,ulDualFactorCUPin+1);
                memcpy(req_data.init_data.tokenLabel,pDualFactorCUToken,ulDualFactorCUToken+1);
                memcpy(req_data.init_data.userPin,pDualFactorCUUserPin,ulDualFactorCUUserPin+1);

                SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
                if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                    if(resp_data.response_code != 0) {
                        printf("\n\tFailed to get initialize SmartCard over network");
                        ulRet = -1;
                        goto token_fail_CU;
                    }
                } else {
                    printf("\n\tSSL_read failed");
                    ulRet = -1;
                    goto token_fail_CU;
                }

                memset(&req_data,0,sizeof(req_data));
                memset(&resp_data,0,sizeof(resp_data));

                req_data.command_type = DF_GENERATE_KEY_PAIR;
                req_data.gen_key_data.session_handle = session_handle;
                memcpy(req_data.gen_key_data.userName,pUserName,ulUserNameLen+1);
                memcpy(req_data.gen_key_data.password,pUserPswd,ulUserPswdLen+1);

                SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */

                if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                    if(resp_data.response_code != 0) {
                        printf("\n\tFailed to get generate key pair over network");
                        ulRet = -1;
                        goto token_fail_CU;
                    }
                } else {
                    printf("\n\tSSL_read failed");
                    ulRet = -1;
                    goto token_fail_CU;
                }

                memset(&req_data,0,sizeof(req_data));
                memset(&resp_data,0,sizeof(resp_data));

                req_data.command_type = DF_GET_KEY;
                req_data.get_key_data.session_handle = session_handle;
                memcpy(req_data.get_key_data.userName,pUserName,ulUserNameLen+1);
                memcpy(req_data.get_key_data.password,pUserPswd,ulUserPswdLen+1);

                SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
                if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                    if(resp_data.response_code == 0) {
                        memcpy(pPublicKey,&(resp_data.data),2*PSWD_ENC_KEY_MODULUS);
                    } else {
                        printf("\n\tFailed to get public key over network");
                        ulRet = -1;
                        goto token_fail_CU;
                    }
                } else {
                    printf("\n\tSSL_read failed");
                    ulRet = -1;
                    goto token_fail_CU;
                }

                memset(&req_data,0,sizeof(req_data));
                memset(&resp_data,0,sizeof(resp_data));

                req_data.command_type = DF_GET_SIGNATURE;
                req_data.get_sign_data.session_handle = session_handle;
                memcpy(req_data.get_sign_data.userName,pUserName,ulUserNameLen+1);
                memcpy(req_data.get_sign_data.password,pUserPswd,ulUserPswdLen+1);
                memcpy(req_data.get_sign_data.pEncPswd,pEncPswd,ulEncPswdLen);
                req_data.get_sign_data.ulEncPswdLen = ulEncPswdLen;

                SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
                if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                    if(resp_data.response_code == 0) {
                        memcpy(pSignature,&(resp_data.data),PSWD_ENC_KEY_MODULUS);
                    } else {
                        printf("\n\tFailed to get signature over network");
                        ulRet = -1;
                        goto token_fail_CU;
                    }
                } else {
                    printf("\n\tSSL_read failed");
                    ulRet = -1;
                    goto token_fail_CU;
                }

token_fail_CU:
                memset(&req_data,0,sizeof(req_data));
                req_data.command_type = DF_END;
                SSL_write(ssl, &req_data, sizeof(req_data));

ssl_fail_CU:
                if(ssl) SSL_free(ssl);
                if(server) close(server);
                if(ctx) SSL_CTX_free(ctx);
                if(ulRet == -1) return ulRet;
            }
        }

        ulRet = Cfm2CreateUser2(session_handle,
                    CN_CRYPTO_USER,
                    (Uint8 *) pUserName, ulUserNameLen,
                    (Uint8 *) pEncPswd, ulEncPswdLen,
                    pPublicKey, pSignature);
#else
        //add here
        if (ulAuthMode) {
            if (pPublicKey == NULL)
                pPublicKey = malloc(PSWD_ENC_KEY_MODULUS * 2);
            if (pSignature == NULL)
                pSignature = malloc(PSWD_ENC_KEY_MODULUS);
            if ((pSignature == NULL) || (pPublicKey == NULL)) {
                printf("\n\t Failed to allocate memory\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }

            memset(pPublicKey, 0, PSWD_ENC_KEY_MODULUS*2);
            memset(pSignature, 0, PSWD_ENC_KEY_MODULUS);

            memcpy(pPublicKey, pDualFactorCUPin, MAX_PSWD_LENGTH+1);
            memcpy(pPublicKey+MAX_PSWD_LENGTH+1, pDualFactorCUToken, MAX_LABEL_LENGTH+1);
            memcpy(pPublicKey+MAX_PSWD_LENGTH+MAX_LABEL_LENGTH+2, pDualFactorCUUserPin, MAX_PSWD_LENGTH+1);
            memcpy(pPublicKey+(2*MAX_PSWD_LENGTH)+MAX_LABEL_LENGTH+3, &pDualFactorCUSlot, sizeof(pDualFactorCUSlot));
        }

        ulRet = Cfm2CreateUser2(session_handle,
                    CN_CRYPTO_USER,
                    (Uint8 *) pUserName, ulUserNameLen,
                    (Uint8 *) pUserPswd, ulUserPswdLen,
                    pPublicKey, pSignature);
#endif
        if (ulRet) {
            printf("Cfm2CreateUser2 for CU failed %s \n",
                   Cfm2ResultAsString(ulRet));
            printf
                ("If you exit now.. it will take some time for HSM to comeout of the critical stage\n");
            return ulRet;
        }
    }


    /* Step 3: Init HSM done */
    printf("\n\tPlease wait, this may take few minutes.");
    printf("\n");

    if (pPublicKey) free(pPublicKey);
    if (pSignature) free(pSignature);

    ulRet = Cfm2InitHSMDone(session_handle);
    if (ulRet) {
        printf("\n\tInitHSMDone Failure: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        return ulRet;
    }

    printf("\n\t Created the Crypto Officer [Login: %s Password: %s ]\n",
           pOfficerName, pCOPswd);
    if (bUserPswd && bUserName)
        printf("\n\t Created the Crypto User [Login: %s Password: %s ]\n",
               pUserName, pUserPswd);
    printf("\n\tCfm2InitHSMDone returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    if (ulRet == 0) {
        application_id = 0;
            if ((ulRet =
                 Cfm2Initialize3(dev_id, DIRECT, (Uint8 *) partition_name,
                         &application_id)) != 0)
            {
                printf
                    ("\n\tFailed to reinitialize the library with error: 0x%02x : %s\n",
                     ulRet, Cfm2ResultAsString(ulRet));
                return ulRet;
            }
        ulRet = Cfm2OpenSession2(application_id, &session_handle);
        if (ulRet) {
            printf
                ("\n\tFailed to open a session after library reinitialization with error: 0x%02x : %s\n",
                 ulRet, Cfm2ResultAsString(ulRet));
            return ulRet;
        }
        if ((ulRet = Cfm2GetHSMInfo2(session_handle, &hsm_info)) != 0) {
            printf("\n\tCfm2GetHSMInfo2 returned: 0x%02x : %s\n",
                   ulRet, Cfm2ResultAsString(ulRet));
            goto end;
        }

        fipsState = betoh32(hsm_info.uiFipsState);
        printf("\n Current FIPS mode is: %d\n", fipsState);
    }
end:
    return ulRet;
}

Uint32 deleteUser(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bUserType = FALSE;
    char *pUserType = 0;
    Uint32 ulUserType = 0;
    Uint32 ulTemp = 0;

    Uint8 bName = FALSE;
    Uint8 *pUserName = NULL;
    Uint32 ulUserNameLen = 0;

    for (i = 2; i < argc; i = i + 2) {
        if ((strcmp(argv[i], "-u") == 0) && (argc > i + 1))
            bUserType =
                readArgAsString(argv[i + 1], (char **)&pUserType, &ulTemp);
        else if ((strcmp(argv[i], "-n") == 0) && (argc > i + 1))
            bName =
                readArgAsString(argv[i + 1], (char **)&pUserName,
                          &ulUserNameLen);
    }
    if (!bHelp && !bUserType) {
        printf("\n\tError: User Type (-u) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bUserType) {
        if ((strcmp(pUserType, "CO") == 0)) {
            printf("\n\tError: PCO cannot be deleted.\n");
            bHelp = TRUE;
        } else if ((strcmp(pUserType, "CU") == 0))
            ulUserType = CN_CRYPTO_USER;
        else if ((strcmp(pUserType, "AU") == 0))
            ulUserType = CN_APPLIANCE_USER;
        else {
            printf("\n\tError: Invalid user type specified.\n");
            bHelp = TRUE;
        }
    }


    if (!bHelp && !bName) {
        printf("\n\tError: User Name (-n) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nDelete an user of given name ");
        printf("\n");
        printf("\nSyntax: deleteUser -u <CU/AU> -n <name>  ");
        printf("\n");
        printf("\nWhere: -h   displays this information   ");
        printf("\n       -u   Type of the user            ");
        printf("\n       -n   Name of the user to be deleted");
        printf("\n");
        return -1;
    }

    ulRet = Cfm2DeleteUser(session_handle, ulUserType, pUserName, ulUserNameLen);
    printf("\n\tCfm2DeleteUser returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    return ulRet;
}

Uint32 createUser(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bName = FALSE;
    Uint8 *pUserName = NULL;
    Uint32 ulUserNameLen = 0;

    Uint8 bUserType = FALSE;
    char *pUserType = 0;
    Uint32 ulUserType = 0;
    Uint32 ulTemp = 0;

    Uint8 bPswd = FALSE;
    Uint8 *pPswd = NULL;
    Uint32 ulPswdLen = 0;

#ifndef CLOUD_HSM_CLIENT
    Uint8 pEncPswd[PSWD_ENC_KEY_MODULUS] = { 0 };
    Uint32 ulEncPswdLen = PSWD_ENC_KEY_MODULUS;

    Uint8  bDualFactorHost   = FALSE;
    char*  pDualFactorHost   = 0;
    Uint32 ulDualFactorHost  = 0;

    Uint8  bDualFactorPort   = FALSE;
    Uint32 pDualFactorPort   = 0;

    Uint8  bDualFactorCert   = FALSE;
    char*  pDualFactorCert   = 0;
#endif

    Uint8  bDualFactorSlot   = FALSE;
    Uint32 pDualFactorSlot   = 0;

    Uint8  bDualFactorPin   = FALSE;
    char*  pDualFactorPin   = 0;
    Uint32 ulDualFactorPin  = 0;

    Uint8  bDualFactorToken   = FALSE;
    char*  pDualFactorToken   = 0;
    Uint32 ulDualFactorToken  = 0;

    Uint8  bDualFactorUserPin   = FALSE;
    char*  pDualFactorUserPin   = 0;
    Uint32 ulDualFactorUserPin  = 0;

    Uint8 *pPublicKey = NULL;
    Uint8 *pSignature = NULL;

    for (i = 2; i < argc; i = i + 2) {
        if ((strcmp(argv[i], "-u") == 0) && (argc > i + 1))
            bUserType =
                readArgAsString(argv[i + 1], (char **)&pUserType, &ulTemp);
        else if ((strcmp(argv[i], "-s") == 0) && (argc > i + 1))
            bName =
                readArgAsString(argv[i + 1], (char **)&pUserName,
                          &ulUserNameLen);
        else if ((strcmp(argv[i], "-p") == 0) && (argc > i + 1))
            bPswd =
                readArgAsString(argv[i + 1], (char **)&pPswd, &ulPswdLen);
#ifndef CLOUD_HSM_CLIENT
        //  dual factor authentication hostname
        else if ((!bDualFactorHost ) && (strcmp(argv[i],"-dh")==0) && (argc > i+1))
            bDualFactorHost = readArgAsString(argv[i+1], &pDualFactorHost, &ulDualFactorHost);

        //  dual factor authentication port
        else if ((!bDualFactorPort ) && (strcmp(argv[i],"-dp")==0) && (argc > i+1))
            bDualFactorPort = readIntegerArg(argv[i+1], &pDualFactorPort);

        //  dual factor authentication server certificate path
        else if ((!bDualFactorCert ) && (strcmp(argv[i],"-dc")==0) && (argc > i+1)) {
            pDualFactorCert = argv[i+1];
            bDualFactorCert = TRUE;
        }
#endif

        //  dual factor authentication token slot
        else if ((!bDualFactorSlot ) && (strcmp(argv[i],"-dn")==0) && (argc > i+1))
        {
            if(!isdigit(argv[i+1][0])) {
                pDualFactorSlot = -1;
                bDualFactorSlot = TRUE;
            } else {
                bDualFactorSlot = readIntegerArg(argv[i+1], &pDualFactorSlot);
            }
        }
        //  dual factor authentication SO pin
        else if ((!bDualFactorPin ) && (strcmp(argv[i],"-ds")==0) && (argc > i+1))
        {
            bDualFactorPin = readArgAsString(argv[i+1], &pDualFactorPin, &ulDualFactorPin);
        }
        //  dual factor authentication token label
        else if ((!bDualFactorToken ) && (strcmp(argv[i],"-dt")==0) && (argc > i+1))
        {
            bDualFactorToken = readArgAsString(argv[i+1], &pDualFactorToken, &ulDualFactorToken);
        }
        //  dual factor authentication user pin
        else if ((!bDualFactorUserPin ) && (strcmp(argv[i],"-du")==0) && (argc > i+1))
        {
            bDualFactorUserPin = readArgAsString(argv[i+1], &pDualFactorUserPin, &ulDualFactorUserPin);
        }
        else
            bHelp = TRUE;
    }
    if (!bHelp && !bName) {
        printf("\n\tError: User Name (-s) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bUserType) {
        printf("\n\tError: User Type (-u) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bPswd) {
        printf("\n\tError: User Password (-p) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bUserType) {
        if ((strcmp(pUserType, "CO") == 0))
            ulUserType = CN_CRYPTO_OFFICER;
        else if ((strcmp(pUserType, "CU") == 0))
            ulUserType = CN_CRYPTO_USER;
        else if ((strcmp(pUserType, "AU") == 0))
            ulUserType = CN_APPLIANCE_USER;
        else {
            printf("\n\tError: Invalid user type specified.\n");
            bHelp = TRUE;
        }
    }

#ifdef CLOUD_HSM_CLIENT
    if (!bHelp && (fipsState == 3) && (ulUserType != CN_APPLIANCE_USER)) {
        if ( ! (bDualFactorSlot && bDualFactorPin && bDualFactorToken && bDualFactorUserPin)) {
            printf("\n\tError: For network dual factor authentication -dn,-ds,-dt and -du options must be specified.\n");
            bHelp = TRUE;
        }
    }
#else
    if (!bHelp && (bDualFactorHost || bDualFactorPort ||  bDualFactorCert || bDualFactorSlot ||
               bDualFactorPin || bDualFactorToken || bDualFactorUserPin))
    {
        if ( ! (bDualFactorHost && bDualFactorPort &&  bDualFactorCert && bDualFactorSlot &&
            bDualFactorPin && bDualFactorToken && bDualFactorUserPin)) {
            printf("\n\tError: For network dual factor authentication -dh,-dp,-dc,-dn,-ds,-dt and -du options must be specified.\n");
            bHelp = TRUE;
        }
    }
#endif

    if (bDualFactorSlot) {
        if(pDualFactorSlot == -1) {
            printf("\n\tError: Invalid value for -dn.");
            bHelp = TRUE;
        }
    }

    if (!bHelp && ( (ulPswdLen < MIN_PSWD_LEN) || (ulPswdLen > MAX_PSWD_LEN) )) {
        printf("\n\tError: Incorrect password length.\n");
        bHelp = TRUE;
    }

    if ( (ulDualFactorPin > MAX_PSWD_LENGTH) || (ulDualFactorToken > MAX_LABEL_LENGTH) || (ulDualFactorUserPin > MAX_PSWD_LENGTH) ) {
        printf("\n\tError: Dual factor argument(s) length incorrect.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nCreate a new CO/CU/AU with the given name and password ");
        printf("\n");
        printf("\nSyntax: createUser -u <CO/CU/AU> -s <name> -p <password> ");
#ifndef CLOUD_HSM_CLIENT
        printf("[-dh <host> -dp <port> -dc <cert> ");
#else
        printf("[");
#endif
        printf("-dn <slot> -ds <SO pin> -dt <token label> -du <user pin>]" );
        printf("\n");
        printf("\nWhere: -h   displays this information             ");
        printf("\n       -u   Type of the user                      ");
        printf("\n       -s   Name of the user                      ");
        printf("\n       -p   Password (min length:7, max length:32)");
        printf("\n            Note:- Actual lengths may vary based on the initHSM configuration");

#ifndef CLOUD_HSM_CLIENT
        printf("\n       -dh specifies the hostname for network dual factor authentication server");
        printf("\n       -dp specifies the port used by network dual factor authentication server");
        printf("\n       -dc specifies the certificate used by network dual factor authentication server");
#endif
        printf("\n       -dn  specifies the slot number for SmartCard in network dual factor authentication server");
        printf("\n       -ds  specifies the so pin for SmartCard in network dual factor authentication server");
        printf("\n       -dt  specifies the token label for SmartCard in network dual factor authentication server");
        printf("\n       -du  specifies the user pin for SmartCard in network dual factor authentication server");
        printf("\n\n");
        return -1;
    }

    printf("\n\tCreating %s with Name [%s] Password [%s]\n", pUserType,
           pUserName, pPswd);

#ifndef CLOUD_HSM_CLIENT
    ulRet = encrypt_pswd(session_handle,
                 (Uint8 *) pPswd, ulPswdLen,
                 pEncPswd, &ulEncPswdLen);
    if (ulRet != RET_OK) {
        printf("password encryption failed ulRet %d: %s\n",
               ulRet, Cfm2ResultAsString(ulRet));
        return ulRet;
    }


    if ((fipsState == 3) &&
        (ulUserType != CN_APPLIANCE_USER)) {
        if(!bDualFactorHost) {
            printf("\n\t******Insert Crypto User SmartCard******\n");
            printf("Hit Enter key to continue\n");
            getchar();

#ifdef USE_SMARTCARD_AS_POSSESSION_FACTOR
            ulRet = initSmartCard(1);
            if (ulRet != RET_OK) {
                printf("\n\tFailed to initialize Smart Card\n");
                shutdownSmartCard();
                return ulRet;
            }
#endif
            if (pPublicKey == NULL)
                pPublicKey = malloc(PSWD_ENC_KEY_MODULUS * 2);
            if (pSignature == NULL)
                pSignature = malloc(PSWD_ENC_KEY_MODULUS);
            if ((pSignature == NULL) || (pPublicKey == NULL)) {
                printf("\n\t Failed to allocate memory\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }

            memset(pPublicKey, 0, PSWD_ENC_KEY_MODULUS*2);
            memset(pSignature, 0, PSWD_ENC_KEY_MODULUS);

            ulRet = generate_key_pair_on_eToken(session_handle, (char *)pUserName, (char *)pPswd);
            if (ulRet != 0) {
                printf("\n\t Failed to generate key pair on eToken\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }
            ulRet = get_public_key_from_eToken(session_handle, (char *)pUserName, (char *)pPswd, pPublicKey);
            if (ulRet != 0) {
                printf("\n\t Failed to get public key from eToken\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }
            ulRet = get_signature_from_eToken(session_handle, (char *)pUserName, (char *)pPswd, pEncPswd, ulEncPswdLen, pSignature);
            if (ulRet != 0) {
                printf("\n\t Failed to get signature from eToken\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }
#ifdef USE_SMARTCARD_AS_POSSESSION_FACTOR
            shutdownSmartCard();
#endif
        } else {
            SSL_CTX *ctx = NULL;
            int server = 0;
            SSL *ssl = NULL;
            df_response resp_data;
            df_request req_data;

            if (pPublicKey == NULL)
                pPublicKey = malloc(PSWD_ENC_KEY_MODULUS * 2);
            if (pSignature == NULL)
                pSignature = malloc(PSWD_ENC_KEY_MODULUS);
            if ((pSignature == NULL) || (pPublicKey == NULL)) {
                printf("\n\t Failed to allocate memory\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }

            memset(pPublicKey, 0, PSWD_ENC_KEY_MODULUS*2);
            memset(pSignature, 0, PSWD_ENC_KEY_MODULUS);

            SSL_library_init();
            ctx = InitSSL_CTX();

            if(!SSL_CTX_load_verify_locations(ctx, pDualFactorCert, NULL)){
                ulRet = -1;
                printf("\n\tCould not load the certificate trust chain\n");
                goto ssl_fail;
            }

            SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);
            server = OpenSocketConnection(pDualFactorHost, pDualFactorPort);
            ssl = SSL_new(ctx);
            SSL_set_fd(ssl, server);

            ulRet =  SSL_connect(ssl);
            if ( ulRet == -1 ) {
                printf("\n\nSSL connect failed\n");
                goto ssl_fail;
            }

            req_data.command_type = DF_INITIALIZE;
            req_data.init_data.reinitialize = 1;
            req_data.init_data.slot = pDualFactorSlot;
            memcpy(req_data.init_data.SOPin,pDualFactorPin,ulDualFactorPin+1);
            memcpy(req_data.init_data.tokenLabel,pDualFactorToken,ulDualFactorToken+1);
            memcpy(req_data.init_data.userPin,pDualFactorUserPin,ulDualFactorUserPin+1);

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code != 0) {
                    printf("\n\tFailed to get initialize SmartCard over network\n");
                    ulRet = -1;
                    goto token_fail;
                }
            } else {
                printf("\n\tSSL_read failed\n");
                ulRet = -1;
                goto token_fail;
            }

            memset(&req_data,0,sizeof(req_data));
            memset(&resp_data,0,sizeof(resp_data));

            req_data.command_type = DF_GENERATE_KEY_PAIR;
            req_data.gen_key_data.session_handle = session_handle;
            memcpy(req_data.gen_key_data.userName,pUserName,ulUserNameLen+1);
            memcpy(req_data.gen_key_data.password,pPswd,ulPswdLen+1);

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code != 0) {
                    printf("\n\tFailed to get generate key pair over network");
                    ulRet = -1;
                    goto token_fail;
                }
            } else {
                printf("\n\tSSL_read failed");
                ulRet = -1;
                goto token_fail;
            }

            memset(&req_data,0,sizeof(req_data));
            memset(&resp_data,0,sizeof(resp_data));

            req_data.command_type = DF_GET_KEY;
            req_data.get_key_data.session_handle = session_handle;
            memcpy(req_data.get_key_data.userName,pUserName,ulUserNameLen+1);
            memcpy(req_data.get_key_data.password,pPswd,ulPswdLen+1);

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code == 0) {
                    memcpy(pPublicKey,&(resp_data.data),2*PSWD_ENC_KEY_MODULUS);
                } else {
                    printf("\n\tFailed to get public key over network");
                    ulRet = -1;
                    goto token_fail;
                }
            } else {
                printf("\n\tSSL_read failed");
                ulRet = -1;
                goto token_fail;
            }

            memset(&req_data,0,sizeof(req_data));
            memset(&resp_data,0,sizeof(resp_data));

            req_data.command_type = DF_GET_SIGNATURE;
            req_data.get_sign_data.session_handle = session_handle;
            memcpy(req_data.get_sign_data.userName,pUserName,ulUserNameLen+1);
            memcpy(req_data.get_sign_data.password,pPswd,ulPswdLen+1);
            memcpy(req_data.get_sign_data.pEncPswd,pEncPswd,ulEncPswdLen);
            req_data.get_sign_data.ulEncPswdLen = ulEncPswdLen;

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code == 0) {
                    memcpy(pSignature,&(resp_data.data),PSWD_ENC_KEY_MODULUS);
                } else {
                    printf("\n\tFailed to get signature over network");
                    ulRet = -1;
                    goto token_fail;
                }
            } else {
                printf("\n\tSSL_read failed");
                ulRet = -1;
                goto token_fail;
            }

token_fail:
            memset(&req_data,0,sizeof(req_data));
            req_data.command_type = DF_END;
            SSL_write(ssl, &req_data, sizeof(req_data));

ssl_fail:
            if(ssl) SSL_free(ssl);
            if(server) close(server);
            if(ctx) SSL_CTX_free(ctx);
            if(ulRet == -1) return ulRet;
        }
    }

    ulRet =
        Cfm2CreateUser2(session_handle, ulUserType,
                pUserName, ulUserNameLen,
                pEncPswd, ulEncPswdLen,
                pPublicKey, pSignature);
#else
    if ((fipsState == 3) &&
        (ulUserType != CN_APPLIANCE_USER)) {
        if (pPublicKey == NULL)
            pPublicKey = malloc(PSWD_ENC_KEY_MODULUS * 2);
        if (pSignature == NULL)
            pSignature = malloc(PSWD_ENC_KEY_MODULUS);
        if ((pSignature == NULL) || (pPublicKey == NULL)) {
            printf("\n\t Failed to allocate memory\n");
            ulRet = ERR_MEMORY_ALLOC_FAILURE;
            return ulRet;
        }

        memset(pPublicKey, 0, PSWD_ENC_KEY_MODULUS*2);
        memset(pSignature, 0, PSWD_ENC_KEY_MODULUS);

        memcpy(pPublicKey, pDualFactorPin, MAX_PSWD_LENGTH+1);
        memcpy(pPublicKey+MAX_PSWD_LENGTH+1, pDualFactorToken, MAX_LABEL_LENGTH+1);
        memcpy(pPublicKey+MAX_PSWD_LENGTH+MAX_LABEL_LENGTH+2, pDualFactorUserPin, MAX_PSWD_LENGTH+1);
        memcpy(pPublicKey+(2*MAX_PSWD_LENGTH)+MAX_LABEL_LENGTH+3, &pDualFactorSlot, sizeof(pDualFactorSlot));
    }

    ulRet =
        Cfm2CreateUser2(session_handle, ulUserType,
                pUserName, ulUserNameLen,
                pPswd,ulPswdLen,
                pPublicKey, pSignature);
#endif

    printf("\n\tCfm2createUser returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    if(pSignature) free(pSignature);
    if(pPublicKey) free(pPublicKey);

    return ulRet;
}

Uint32 listUsers(int argc, char **argv)
{
    Uint32 ulRet = 0;
    UserInfo *users = NULL;
    Uint32 count = 0, total = 0;
    int i = 0, found = 0;
    int last_found_user_id = 0;

    if (argc > 2) {
        printf("\n\tThis command doesn't expect any arguments\n");
        printf("\nDescription:");
        printf("\n\tlistUsers lists all users of the current partition\n");
        return ulRet;
    }

    ulRet = Cfm2ListUsers(session_handle, 0, NULL, &count, &total);
    if (ulRet != RET_OK) {
        printf("\n\tCfm2ListUsers returned: 0x%02x : %s\n",
               ulRet, Cfm2ResultAsString(ulRet));
        return ulRet;
    }

    printf("\n\tNumber Of Users found %d\n\n", total);

    users = (UserInfo *)malloc(total * sizeof(UserInfo));
    if (users == NULL) {
        printf("malloc failure \n");
        ulRet = ERR_MEMORY_ALLOC_FAILURE;
        return ulRet;
    }
    memset((uint8_t *)users, 0, (total * sizeof(UserInfo)));

    do {
        if (total > MAX_LIST_USERS_CNT) count = MAX_LIST_USERS_CNT;
        ulRet = Cfm2ListUsers(session_handle, last_found_user_id, users+found, &count, &total);
        if (ulRet != RET_OK) break;
        found += count;
        last_found_user_id = betoh32(users[found-1].userID);
    } while (count != total);

    printf("\tIndex\tUser Type\tUser Name\tLoginFailureCnt\n");
    for (i = 0; i < found; i++) {
        printf("\t%d\t%s\t\t%s\t%d\n", i+1, userType[betoh32(users[i].ulUserType)], users[i].userName, betoh32(users[i].loginFailureCount));
    }
    printf("\n\tCfm2ListUsers returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    return ulRet;
}


/****************************************************************************
 *
 * FUNCTION     : loginStatus
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 loginStatus(int argc, char **argv)
{
    Uint32 ulRet = 0;
    unsigned int state = 0xFF;

    if (argc > 2) {
        printf
            ("\n\tThis command doesn't expect any arguments and allowed only on initialized HSM\n");
        printf("\nDescription:");
        printf
            ("\n\tloginStatus informs if CU or CO is logged into the initialized HSM through current application.\n");
        return ulRet;
    }

    ulRet = Cfm2GetLoginStatus(session_handle, &state);
    if (ulRet != 0) {
        printf("\n\tCfm2GetLoginStatus Failure: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        return ulRet;
    }
    switch (state) {
    case STATE_RO_PUBLIC_SESSION:
    case STATE_RW_PUBLIC_SESSION:
        {
            printf("\n\tNo user has logged-in\n");
            break;
        }
    case STATE_RO_USER_FUNCTIONS:
    case STATE_RW_USER_FUNCTIONS:
        {
            printf("\n\tCU has logged-in\n");
            break;
        }
    case STATE_RW_APP_USER_FUNCTIONS:
        {
            printf("\n\tAU has logged-in\n");
            break;
        }
    case STATE_RW_CO_FUNCTIONS:
        {
            printf("\n\tCO has logged-in\n");
            break;
        }
    default:
        printf("\n\tinvalid state %d\n", state);
        break;
    }
    return 0;
}

/****************************************************************************
 *
 * FUNCTION     : getHSMInfo
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 getHSMInfo(int argc, char **argv)
{
    Uint32 ulRet = 0;
    int temperature = 0;
    HSMInfo hsm_info = { };

    if (argc > 2) {
        printf("\n\tThis command doesn't expect any arguments\n");
        printf("\nDescription:");
        printf("\n\tgetHSMInfo returns HSM information\n");
        return ulRet;
    }

    ulRet = Cfm2GetHSMInfo2(session_handle, &hsm_info);
    if (ulRet != 0) {
        printf("\n\tCfm2GetHSMInfo2 Failure: 0x%02x : %s\n",
               ulRet, Cfm2ResultAsString(ulRet));
        return ulRet;
    }
    printf("\n");
    printf("\tLabel                :%.50s\n", hsm_info.label);
    printf("\tModel                :%s\n", hsm_info.model);
    printf("\tSerial Number        :%s\n", hsm_info.serialNumber);
    printf("\tHSM Flags            :%d\n", hsm_info.uiFlags);
    printf("\tFIPS state           :%d [%s]\n", betoh32(hsm_info.uiFipsState),
           fips_state[betoh32(hsm_info.uiFipsState) + 1]);
    printf("\n");
    printf("\tManufacturer ID      : \n");
    printf("\tDevice ID            :%02X\n",
           betoh32(hsm_info.manufacturerID.dev_id));
    printf("\tClass Code           :%02X\n",
           betoh32(hsm_info.manufacturerID.class_code));
    printf("\tSystem vendor ID     :%02X\n",
           betoh32(hsm_info.manufacturerID.vendor_id));
    printf("\tSubSystem ID         :%02X\n",
           betoh32(hsm_info.manufacturerID.subsystem_id));
    printf("\n");
    printf("\n");
    printf("\tTotalPublicMemory    :%d\n", betoh32(hsm_info.uiTotalRAM));
    printf("\tFreePublicMemory     :%d\n", betoh32(hsm_info.uiFreeRAM));
    printf("\tTotalPrivateMemory   :%d\n", betoh32(hsm_info.uiTotalFlash));
    printf("\tFreePrivateMemory    :%d\n", betoh32(hsm_info.uiFreeFlash));
    printf("\n");
    printf("\tHardware Major       :%d\n", hsm_info.hardwareVersion.major);
    printf("\tHardware Minor       :%d\n", hsm_info.hardwareVersion.minor);
    printf("\n");
    printf("\tFirmware Major       :%d\n", hsm_info.firmwareVersion.major);
    printf("\tFirmware Minor       :%02d\n", hsm_info.firmwareVersion.minor);
    printf("\n");
    temperature = betoh32(hsm_info.uiTemperature);
    printf("\tTemperature          :%d C\n", (char)temperature);
    printf("\n");
    printf("\tBuild Number         :%s\n", hsm_info.buildNum);
    printf("\n");
    printf("\tFirmware ID          :%s\n", hsm_info.firmwareString + 9);
    printf("\n");

    return ulRet;
}
/*Certificate related commands*/
#ifdef CERT_OPS
Uint32 createNode(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i     = 0;

    char  *partnCert     = NULL;
    Uint8   bPartnCert    = FALSE;

    char  *partnOwnerCert     = NULL;
    Uint8   bPartnOwnerCert    = FALSE;

    char  *userFixedKey       = NULL;
    Uint8  bUserFixedKey      = FALSE;

    Uint8   buf[4096] = {};
    Uint32   buf_len = 4096;
    unsigned int state  = 0xFF;

    char *cmd[4] = { NULL, NULL, "-f" ,userFixedKey};

    Uint8   bHelp = FALSE;


    for (i = 2; i < argc; i=i+2)
    {
        if ( (strcmp(argv[i],"-f")==0) && (argc > i+1) )
        {
            partnCert = argv[i+1];
            bPartnCert = TRUE;
        }
        else if ((strcmp(argv[i],"-o")==0) && (argc > i+1) )
        {
            partnOwnerCert = argv[i+1];;
            bPartnOwnerCert = TRUE;
        }
        else if ((strcmp(argv[i],"-k")==0) && (argc > i+1) )
        {
            userFixedKey = argv[i+1];;
            bUserFixedKey = TRUE;
        }
        else
            bHelp = TRUE;
    }

    if(!bHelp && !bPartnCert )
    {
        printf("Error: Partition certificate (-f) is missing\n");
        bHelp = TRUE;
    }
    if(!bHelp && !bPartnOwnerCert )
    {
        printf("Error: Partition owner certificate (-o) is missing\n");
        bHelp = TRUE;
    }
    if(!bHelp && !bUserFixedKey )
    {
        printf("Error: User Fixed Key (-k) is missing\n");
        bHelp = TRUE;
    }
    if(bHelp)
    {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nImport partition owner and partition certificate along with the user fixed key.");
        printf("\n");
        printf("\nSyntax: createNode -f <Partition Certificate> -o <Partition Owner Certificate>\n\t -k <User Fixed Key>");
        printf("\n");
        printf("\nWhere:");
        printf("\n       -f   File Path to Partition Certificate ");
        printf("\n       -o   File Path to Partition Owner Certificate ");
        printf("\n       -k   File Path to User Fixed Key ");
        printf("\n");
        return ulRet;

    }
    ulRet = Cfm2GetLoginStatus(session_handle, &state);
    if (ulRet != 0)
    {
        printf("\n\tError: Cfm2GetLoginStatus Failure: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        return ulRet;
    }

    if(state != STATE_RW_CO_FUNCTIONS)
    {
        printf("\n\tError: Permission denied: 0x%02x : %s\n", RET_CO_NOT_LOGGED_IN,
               Cfm2ResultAsString(RET_CO_NOT_LOGGED_IN));
        return ulRet;
    }
    ulRet = storeUserFixedKey(4,(char **)cmd);
    if(ulRet)
        goto error;

    ulRet = read_file(partnOwnerCert, buf, &buf_len);
    if ((ulRet != 0) || (buf_len == 0)) {
        printf("\n\tError: Empty file: %s\n", partnOwnerCert);
        return -1;
    }
    ulRet = Cfm2StoreCert(session_handle, PARTITION_OWNER_CERT, buf, buf_len);
    if(ulRet)
    {
        printf("Failed to store Partition Owner certificate\n");
        goto error;
    }
    memset(buf, 0, sizeof(buf));
    ulRet = read_file(partnCert, buf, &buf_len);
    if ((ulRet != 0) || (buf_len == 0)) {
        printf("\n\tError: Empty file: %s\n", partnCert);
        return -1;
    }
    ulRet = Cfm2StoreCert(session_handle, PARTITION_CERT, buf, buf_len);
    if(ulRet)
    {
        printf("Failed to store Partition certificate\n");
        goto error;
    }

error:
    return ulRet;
}

Uint32 exportPartnCSR(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i     = 0;

    Uint8  *appliPartnCSR     = NULL;
    Uint8   bAppliPartnCSR = FALSE;

    Uint8   bHelp = FALSE;


    for (i = 2; i < argc; i=i+2)
    {
        if ( (strcmp(argv[i],"-a")==0) && (argc > i+1) )
        {
            appliPartnCSR = (Uint8 *)argv[i+1];
            bAppliPartnCSR = TRUE;
        }
        else
            bHelp = TRUE;
    }

    if(!bHelp && !bAppliPartnCSR )
    {
        printf("Error: AppliancePartnCSR Target path (-a) is missing\n");
        bHelp = TRUE;
    }

    if(bHelp)
    {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nExport partition CSR for a given partition.");
        printf("\n");
        printf("\nSyntax: exportPartnCSR -a <AppliancePartnCSR_path>");
        printf("\n");
        printf("\nWhere:");
        printf("\n       -a   File Path to export AppliancePartnCSR ");
        printf("\n");
        return ulRet;

    }
    ulRet = cldPartnExportCSR( session_handle, appliPartnCSR);
    if (ulRet)
    {
        printf("\n\tError Exporting CSR: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
    }
    else
        printf("ExportPartitionCSR Operation success\n");
    return ulRet;
}

Uint32 importPartnCert(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i     = 0;

    Uint8  *appliPartnCert     = NULL;
    Uint8   bAppliPartnCert = FALSE;

    Uint8  *POCert     = NULL;
    Uint8   bPOCert = FALSE;

    Uint8   bHelp = FALSE;

    FILE *fd;
    char *cmd = NULL;
    char *err = NULL;
    int BUFLEN = 100;
    char buf[BUFLEN];

    unsigned int state  = 0xFF;

    for (i = 2; i < argc; i=i+2)
    {
        if ( (strcmp(argv[i],"-a")==0) && (argc > i+1) )
        {
            appliPartnCert = (Uint8 *)argv[i+1];
            bAppliPartnCert = TRUE;
        }
        else if ( (strcmp(argv[i],"-o")==0) && (argc > i+1) )
        {
            POCert = (Uint8 *)argv[i+1];
            bPOCert = TRUE;
        }
        else
            bHelp = TRUE;
    }

    if(!bHelp && !bAppliPartnCert )
    {
        printf("Error: AppliancePartnCert source path (-a) is missing\n");
        bHelp = TRUE;
    }
    if(!bHelp && !bPOCert)
    {
        printf("Error: Partition Owner Certificate source path (-o) is missing");
        bHelp = TRUE;
    }

    if(bHelp)
    {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nImport partition Certificate for a given partition.");
        printf("\n");
        printf("\nSyntax: importPartnCert -a <AppliancePartnCert_path> -o <POCert_path>");
        printf("\n");
        printf("\nWhere:");
        printf("\n       -a   source AppliancePartnCert ");
        printf("\n       -o   source POCert ");
        printf("\n");
        return ulRet;

    }

    cmd = (char *)malloc(strlen((char *)appliPartnCert) + 100);
    sprintf(cmd,"openssl x509 -in %s -noout > /tmp/cmdout 2>&1",appliPartnCert);
    system(cmd);
    fd  = fopen("/tmp/cmdout", "rb");
    if (!fd) {
        ulRet = ERR_INVALID_INPUT;
        printf("unable to open file\n");
        return ulRet;
    }
    fread(buf, sizeof(char), BUFLEN, fd);
    sprintf(cmd,"rm -rf /tmp/cmdout");
    system(cmd);
    free(cmd);
    err = strstr(buf, "error");
    if(err)
    {
        memset(buf,0,BUFLEN);
        printf("\n\tAppliancePartnCert file %s either not valid or does not exist\n", appliPartnCert);
        ulRet = ERR_INVALID_INPUT;
        goto end;
    }

    ulRet = Cfm2GetLoginStatus(session_handle, &state);
    if (ulRet != 0)
    {
        printf("\n\tError: Cfm2GetLoginStatus Failure: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        goto end;
    }

    if(state != STATE_RW_CO_FUNCTIONS)
    {
        printf("\n\tError: Permission denied: 0x%02x : %s\n", RET_CO_NOT_LOGGED_IN,
               Cfm2ResultAsString(RET_CO_NOT_LOGGED_IN));
        goto end;
    }
    ulRet = cldPartnImportCert( session_handle, appliPartnCert,
                    POCert);

    printf("\n\tImporting Certificate: 0x%02x : %s\n", ulRet,
           Cfm2ResultAsString(ulRet));
    if(!ulRet)
    {
        printf("Cav-server will be restarted. Please reconnect Cav-Client after making required changes in config file.\n");
    }

end:
    if (fd)
        fclose(fd);

    return ulRet;
}

Uint32 exportAppCert(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i     = 0;

    Uint8  *appliCert     = NULL;
    Uint8   bappliCert = 0;

    char   appliType = 0;

    Uint8   bHelp = FALSE;


    for (i = 2; i < argc; i=i+2)
    {
        if ( (strcmp(argv[i],"-s")==0) && (argc > i+1) && !appliType )
        {
            appliType = atoi(argv[i+1]);
        }
        else if ( (strcmp(argv[i],"-f")==0) && (argc > i+1) && !bappliCert )
        {
            appliCert = (Uint8 *)argv[i+1];
            bappliCert = TRUE;

        }

        else
            bHelp = TRUE;
    }

    if(!bHelp && !bappliCert )
    {
        printf("Error: ApplianceCert target path (-f) is missing\n");
        bHelp = TRUE;
    }

    if(bHelp)
    {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nExport appliance signed partition Certificate for a given partition.");
        printf("\n");
        printf("\nSyntax: exportAppCert -f <AppliancePartnCert_path> -s <1/2/3>");
        printf("\n");
        printf("\nWhere:");
        printf("\n       1 source AppliancePartnCert ");
        printf("\n       2  source ApplianceCert");
        printf("\n       3  source CaviumCert");
        printf("\n");
        return ulRet;

    }
    ulRet = cldAppExportCert( session_handle, appliCert, appliType );
    printf("\n\tExporting Certificate: 0x%02x : %s\n", ulRet,
           Cfm2ResultAsString(ulRet));
    if(ulRet)
        printf("Certificate export failed\n");

    return ulRet;
}

Uint32 exportPartnCert(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i     = 0;

    Uint8  *appliPartnCert     = NULL;
    Uint8   bAppliPartnCert = FALSE;

    Uint8  *POCert     = NULL;
    Uint8   bPOCert = FALSE;

    Uint8   bHelp = FALSE;


    for (i = 2; i < argc; i=i+2)
    {
        if ( (strcmp(argv[i],"-a")==0) && (argc > i+1) )
        {
            appliPartnCert = (Uint8 *)argv[i+1];
            bAppliPartnCert = TRUE;
        }
        else if ( (strcmp(argv[i],"-o")==0) && (argc > i+1) )
        {
            POCert = (Uint8 *)argv[i+1];
            bPOCert = TRUE;
        }
        else
            bHelp = TRUE;
    }

    if(!bHelp && !bAppliPartnCert )
    {
        printf("Error: AppliancePartnCert source path (-a) is missing\n");
        bHelp = TRUE;
    }
    if(!bHelp && !bPOCert)
    {
        printf("Error: Partition Owner Certificate source path (-o) is missing");
        bHelp = TRUE;
    }

    if(bHelp)
    {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nExport partition Certificate for a given partition.");
        printf("\n");
        printf("\nSyntax: exportPartnCert -a <AppliancePartnCert_path> -o <POCert_path>");
        printf("\n");
        printf("\nWhere:");
        printf("\n       -a   source AppliancePartnCert ");
        printf("\n       -o   source POCert ");
        printf("\n");
        return ulRet;

    }

    ulRet = cldPartnExportCert( session_handle, appliPartnCert,
                    POCert);
    printf("\n\tExporting Certificate: 0x%02x : %s\n", ulRet,
           Cfm2ResultAsString(ulRet));
    if(ulRet)
        printf("Certificates not imported\n");

    return ulRet;
}
Uint32 importCavClientCert(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i     = 0;

    Uint8  *CertName     = NULL;
    Uint32 CertName_len  = 0;
    Uint8   bCertName = FALSE;

    Uint8  *CertPath     = NULL;
    Uint8   bCertPath = FALSE;

    Uint8   bHelp = FALSE;
    FILE *fd;
    char *cmd = NULL;
    char *err = NULL;
    int BUFLEN =100;
    char buf[BUFLEN];

    unsigned int state  = 0xFF;

    for (i = 2; i < argc; i=i+2)
    {
        if ( (strcmp(argv[i],"-cn")==0) && (argc > i+1) )
        {
            bCertName = readArgAsString(argv[i+1], (char **)&CertName, &CertName_len);
            if( CertName_len > MAX_CERT_NAME_LEN)
            {
                printf("Certificate Name too long. Max allowed length is %d",MAX_CERT_NAME_LEN);
                bHelp = TRUE;
            }
        }
        else if ( (strcmp(argv[i],"-c")==0) && (argc > i+1) )
        {
            CertPath = (Uint8 *)argv[i+1];
            bCertPath = TRUE;
        }
        else
            bHelp = TRUE;
    }

    if(!bHelp && !bCertName )
    {
        printf("Error: CertName (-cn) is missing\n");
        bHelp = TRUE;
    }
    if(!bHelp && !bCertPath )
    {
        printf("Error: Cav-Client Certificate source path (-c) is missing\n");
        bHelp = TRUE;
    }

    if(bHelp)
    {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nImport Cav-Client certificate for a given partition.");
        printf("\n");
        printf("\nSyntax: importCavClientCert -c <CavClientCert_Path> -cn <Certificate Name>");
        printf("\n");
        printf("\nWhere: -c   source CavClientCert   ");
        printf("\n       -cn  CavClient Certificate ");
        printf("\n");
        return ulRet;

    }

    cmd = (char *)malloc(strlen((char*)CertPath) + 100);
    sprintf(cmd,"openssl x509 -in %s -noout > /tmp/cmdout 2>&1",CertPath);
    system(cmd);
    fd  = fopen("/tmp/cmdout", "rb");
    if (!fd) {
        ulRet = ERR_INVALID_INPUT;
        printf("unable to open file\n");
        return ulRet;
    }
    fread(buf, sizeof(char), BUFLEN, fd);
    sprintf(cmd,"rm -rf /tmp/cmdout");
    system(cmd);
    free(cmd);
    err = strstr(buf, "error");
    if(err)
    {
        memset(buf,0,BUFLEN);
        printf("\n\tCertificate file %s is either not valid or does not exist\n", CertPath);
        ulRet = ERR_INVALID_INPUT;
        goto end;
    }

    ulRet = Cfm2GetLoginStatus(session_handle, &state);
    if (ulRet != 0)
    {
        printf("\n\tError: Cfm2GetLoginStatus Failure: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        goto end;
    }

    if(state != STATE_RW_CO_FUNCTIONS)
    {
        printf("\n\tError: Permission denied: 0x%02x : %s\n", RET_CO_NOT_LOGGED_IN,
               Cfm2ResultAsString(RET_CO_NOT_LOGGED_IN));
        goto end;
    }

    ulRet = cldPartnImportCavCert( session_handle, CertName, CertPath);
    printf("\n\tImporting Cav-Client Certificate: 0x%02x : %s\n", ulRet,
           Cfm2ResultAsString(ulRet));
    if(!ulRet)
    {
        printf("Cav-server will be restarted. Please reconnect Cav-Client after making required changes in config file.\n");
    }

end:
    if (fd)
        fclose(fd);

    return ulRet;
}

Uint32 exportCavClientCert(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i     = 0;

    Uint8  *CertName     = NULL;
    Uint32  CertName_len = 0;
    Uint8   bCertName = FALSE;

    Uint8  *CertPath     = NULL;
    Uint8   bCertPath = FALSE;

    Uint8   bHelp = FALSE;


    for (i = 2; i < argc; i=i+2)
    {
        if ( (strcmp(argv[i],"-cn")==0) && (argc > i+1) )
        {
            bCertName = readArgAsString(argv[i+1], (char **)&CertName, &CertName_len);
            if( CertName_len > MAX_CERT_NAME_LEN)
            {
                printf("Certificate Name too long. Max allowed length is %d",MAX_CERT_NAME_LEN);
                bHelp = TRUE;
            }
        }
        else if ( (strcmp(argv[i],"-c")==0) && (argc > i+1) )
        {
            CertPath = (Uint8 *)argv[i+1];
            bCertPath = TRUE;
        }
        else
            bHelp = TRUE;
    }

    if(!bHelp && !bCertName )
    {
        printf("Error: CertName (-cn) is missing\n");
        bHelp = TRUE;
    }
    if(!bHelp && !bCertPath )
    {
        printf("Error: Cav-Client Certificate target path (-c) is missing\n");
        bHelp = TRUE;
    }

    if(bHelp)
    {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nExport Cav-Client certificate for a given partition.");
        printf("\n");
        printf("\nSyntax: exportCavClientCert -c <CavClientCert_Path> -cn <Certificate Name>");
        printf("\n");
        printf("\nWhere: -c   source CavClientCert   ");
        printf("\n       -cn  CavClient Certificate ");
        printf("\n");
        return ulRet;

    }
    ulRet = cldPartnExportCavCert( session_handle, CertName, CertPath);
    printf("\n\tExport Cav-Client Certificate: 0x%02x : %s\n", ulRet,
           Cfm2ResultAsString(ulRet));

    return ulRet;
}

Uint32 deleteCavClientCert(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i     = 0;

    Uint8  *CertName     = NULL;
    Uint32  CertName_len = 0;
    Uint8   bCertName = FALSE;

    Uint8   bHelp = FALSE;

    unsigned int state  = 0xFF;

    for (i = 2; i < argc; i=i+2)
    {
        if ( (strcmp(argv[i],"-cn")==0) && (argc > i+1) )
        {
            bCertName = readArgAsString(argv[i+1], (char **)&CertName, &CertName_len);
            if( CertName_len > MAX_CERT_NAME_LEN)
            {
                printf("Certificate Name too long. Max allowed length is %d",MAX_CERT_NAME_LEN);
                bHelp = TRUE;
            }
        }
        else
            bHelp = TRUE;
    }

    if(!bHelp && !bCertName )
    {
        printf("Error: CertName (-cn) is missing\n");
        bHelp = TRUE;
    }

    if(bHelp)
    {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nDelete a Cav-Client certificate for a given partition.");
        printf("\n");
        printf("\nSyntax: deleteCavClientCert -cn <Certificate Name>");
        printf("\n");
        printf("\nWhere: ");
        printf("\n       -cn  CavClient Certificate ");
        printf("\n");
        return ulRet;

    }
    ulRet = Cfm2GetLoginStatus(session_handle, &state);
    if (ulRet != 0)
    {
        printf("\n\tError: Cfm2GetLoginStatus Failure: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        return ulRet;
    }

    if(state != STATE_RW_CO_FUNCTIONS)
    {
        printf("\n\tError: Permission denied: 0x%02x : %s\n", RET_CO_NOT_LOGGED_IN,
               Cfm2ResultAsString(RET_CO_NOT_LOGGED_IN));
        return ulRet;
    }


    ulRet = cldPartnDeleteCavCert( session_handle, CertName);
    printf("\n\tDelete Cav-Client Certificate: 0x%02x : %s\n", ulRet,
           Cfm2ResultAsString(ulRet));
    if(!ulRet)
    {
        printf("Cav-server will be restarted. Please reconnect Cav-Client after making required changes in config file.\n");
    }

    return ulRet;
}
/*Not supported for now*/
Uint32 listCavClientCert(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint8   bHelp = FALSE;
    unsigned int state  = 0xFF;

    if (argc > 2)
    {
        bHelp = TRUE;
    }

    if(bHelp)
    {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nList all Cav-Client certificates for a given partition in JSON format.");
        printf("\n");
        printf("\nSyntax: listCavClientCert -n <pname>");
        printf("\n");
        printf("\nWhere: ");
        printf("\n");
        return ulRet;

    }

    ulRet = Cfm2GetLoginStatus(session_handle, &state);
    if (ulRet != 0)
    {
        printf("\n\tError: Cfm2GetLoginStatus Failure: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        return ulRet;
    }

    if(state != STATE_RW_CO_FUNCTIONS)
    {
        printf("\n\tError: Permission denied: 0x%02x : %s\n", RET_CO_NOT_LOGGED_IN,
               Cfm2ResultAsString(RET_CO_NOT_LOGGED_IN));
        return ulRet;
    }

    ulRet = cldPartnListCavCert(session_handle);
    printf("\n\tListing Cav-Client Certificates: 0x%02x : %s\n", ulRet,
           Cfm2ResultAsString(ulRet));

    return ulRet;
}
#endif
/****************************************************************************
 *
 * FUNCTION     : getPartitionInfo
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 getPartitionInfo(int argc, char **argv)
{
    Uint32 ulRet = 0;
    PartitionInfo info = { };

    if (argc > 2) {
        printf("\n\tThis command doesn't expect any arguments\n");
        printf("\nDescription:");
        printf("\n\tgetPartitionInfo returns Partition's information\n");
        return ulRet;
    }

    printf("\n\tGetting Partition Info");
    ulRet =
        Cfm2GetPartitionInfo(session_handle, (Uint8 *) partition_name,
                     strlen(partition_name), &info);
    if (ulRet != 0) {
        printf("\n\tCfm2GetPartitionInfo returned: 0x%02x : %s\n",
               ulRet, Cfm2ResultAsString(ulRet));
        return ulRet;
    }

    printf("\n");
    printf("\tname                        :%s  \n", info.name);
    printf("\tstatus                      :%s  \n",
           ((info.status == 0) ? "free" : "occupied"));
    printf("\tFIPS state                  :%d [%s]\n", ((char)info.FipsState),
           fips_state[(char)info.FipsState + 1]);
    printf("\tMaxUsers                    :%5d \n", betoh16(info.MaxUsers));
    printf("\tAvailableUsers              :%5d \n", betoh16(info.AvailableUsers));
    printf("\tMaxOfficers                 :%5d \n", betoh16(info.MaxOfficers));
    printf("\tAvailableOfficers           :%5d \n", betoh16(info.AvailableOfficers));
    printf("\tMaxUserKeys                 :%5d \n", betoh32(info.MaxUserKeys));
    printf("\tOccupiedUserKeys            :%5d \n",
           betoh32(info.OccupiedUserKeys));
    printf("\tOccupiedSessionKeys         :%5d \n",
           betoh32(info.OccupiedSessionKeys));
    printf("\tTotalSSLCtxs                :%5d \n", betoh32(info.MaxSSLContexts));
    printf("\tOccupiedSSLCtxs             :%5d \n",
           betoh32(info.OccupiedSSLContexts));
    printf("\tMaxAcclrDevCount            :%5d \n", info.MaxAcclrDevs);
    printf("\tSessionCount                :%5d \n", betoh32(info.SessionCount));
    printf("\tMaxPswdLen                  :%5d \n", betoh32(info.MaxPinLen));
    printf("\tMinPswdLen                  :%5d \n", betoh32(info.MinPinLen));
    printf("\tCloningMethod               :%5d \n", info.CloningMethod);
    printf("\tKekMethod                   :%5d \n", info.KekMethod);
    printf("\tCertAuth                    :%5d \n", info.certAuth);
    printf("\tMValue                      :%5d \n", info.MValue);
    printf("\tNvalue                      :%5d \n", info.NValue);
    printf("\tExport with user keys\n"
           "\t (Other than KEK)          : %s \n",
           (htobe32(info.partn_policy_bits) & 0x0f)?"Enabled":"Disabled");
    printf("\tMCO backup/restore          : %s \n",
           (htobe32(info.partn_policy_bits) & 0xf0)?"Enabled":"Disabled");
    printf("\n");


    return ulRet;
}

#ifndef CLOUD_HSM_CLIENT
Uint32 closeAllSessions(int argc, char **argv)
{
    Uint32 ulRet = 0;

    if (argc > 2) {
        printf
            ("\n\tThis experimental command doesn't expect any arguments\n");
        printf("\nDescription:");
        printf
            ("\n\tCloseAllSessions closes all the sessions opened in the current application\n");
        return ulRet;
    }

    ulRet = Cfm2CloseAllSessions(session_handle);
    printf("\n\tCfm2CloseAllSessions returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    if(!ulRet)
        session_handle = 0;

    return ulRet;
}

Uint32 closePartitionSessions(int argc, char **argv)
{
    Uint32 ulRet = 0;

    if (argc > 2) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nCloses all the sessions opened in the partition");
        printf("\n");
        printf("\nSyntax: closePartitionSessions ");
        printf("\n");
        return ulRet;
    }

    ulRet =
        Cfm2ClosePartitionSessions(session_handle, (Uint8 *) partition_name,
                       strlen(partition_name));
    printf("\n\tCfm2ClosePartitionSessions returned: 0x%02x : %s\n", ulRet,
           Cfm2ResultAsString(ulRet));
    if (ulRet == 0) {
        application_id = 0;
        ulRet =
            Cfm2Initialize3(dev_id, DIRECT, (Uint8 *) partition_name,
                    &application_id);
        if (ulRet) {
            printf("\n\tCfm2Initialize3 returned: 0x%02x : %s\n",
                   ulRet, Cfm2ResultAsString(ulRet));
            return ulRet;
        }

        ulRet = Cfm2OpenSession2(application_id, &session_handle);
        if (ulRet) {
            printf("\n\tCfm2OpenSessions2 returned: 0x%02x : %s\n",
                   ulRet, Cfm2ResultAsString(ulRet));
            return ulRet;
        }
        printf("\n\tSessions are closed, Please re-login to HSM\n");
    }
    return ulRet;
}

#endif

/****************************************************************************
 *
 * FUNCTION     : changePswd
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 changePswd(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bUserType = FALSE;
    char *pUserType = 0;
    Uint32 ulUserTypeLen = 0;
    Uint32 ulUserType = 0;

    Uint8 bNewUserPswd = FALSE;
    char *pNewUserPswd = 0;
    Uint32 ulNewUserPswdLen = 0;

    Uint8 bName = FALSE;
    char *pUserName = 0;
    Uint32 ulNameLen = 0;

#ifndef CLOUD_HSM_CLIENT
    Uint8 pEncPswd[PSWD_ENC_KEY_MODULUS] = { 0 };
    Uint32 ulEncPswdLen = PSWD_ENC_KEY_MODULUS;

    Uint8  bDualFactorHost   = FALSE;
    char*  pDualFactorHost   = 0;
    Uint32 ulDualFactorHost  = 0;

    Uint8  bDualFactorPort   = FALSE;
    Uint32 pDualFactorPort   = 0;

    Uint8  bDualFactorCert   = FALSE;
    char*  pDualFactorCert   = 0;
#endif
    Uint8  bDualFactorSlot   = FALSE;
    Uint32 pDualFactorSlot   = 0;

    Uint8  bDualFactorPin   = FALSE;
    char*  pDualFactorPin   = 0;
    Uint32 ulDualFactorPin  = 0;

    Uint8 bDualFactorToken = FALSE;
    char *pDualFactorToken = 0;
    Uint32 ulDualFactorToken = 0;

    Uint8 bDualFactorUserPin = FALSE;
    char *pDualFactorUserPin = 0;
    Uint32 ulDualFactorUserPin = 0;

    Uint8 *pPublicKey = NULL;
    Uint8 *pSignature = NULL;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // user type
        else if ((!bUserType) && (strcmp(argv[i], "-u") == 0)
             && (argc > i + 1))
            bUserType =
                readArgAsString(argv[i + 1], &pUserType, &ulUserTypeLen);

        // new pin
        else if ((!bNewUserPswd) && (strcmp(argv[i], "-n") == 0)
             && (argc > i + 1))
            bNewUserPswd =
                readArgAsString(argv[i + 1], &pNewUserPswd, &ulNewUserPswdLen);
        // name
        else if ((!bName) && (strcmp(argv[i], "-s") == 0) && (argc > i + 1))
            bName = readArgAsString(argv[i + 1], &pUserName, &ulNameLen);
#ifndef CLOUD_HSM_CLIENT
        //  dual factor authentication hostname
        else if ((!bDualFactorHost ) && (strcmp(argv[i],"-dh")==0) && (argc > i+1))
        {
            bDualFactorHost = readArgAsString(argv[i+1], &pDualFactorHost, &ulDualFactorHost);
        }
        //  dual factor authentication port
        else if ((!bDualFactorPort ) && (strcmp(argv[i],"-dp")==0) && (argc > i+1))
        {
            bDualFactorPort = readIntegerArg(argv[i+1], &pDualFactorPort);
        }
        //  dual factor authentication server certificate path
        else if ((!bDualFactorCert ) && (strcmp(argv[i],"-dc")==0) && (argc > i+1))
        {
            pDualFactorCert = argv[i+1];
            bDualFactorCert = TRUE;
        }
#endif
        //  dual factor authentication token slot
        else if ((!bDualFactorSlot ) && (strcmp(argv[i],"-dn")==0) && (argc > i+1))
        {
            if(!isdigit(argv[i+1][0])) {
                pDualFactorSlot = -1;
                bDualFactorSlot = TRUE;
            } else {
                bDualFactorSlot = readIntegerArg(argv[i+1], &pDualFactorSlot);
            }
        }

        //  dual factor authentication SO pin
        else if ((!bDualFactorPin ) && (strcmp(argv[i],"-ds")==0) && (argc > i+1))
        {
            bDualFactorPin = readArgAsString(argv[i+1], &pDualFactorPin, &ulDualFactorPin);
        }
        //  dual factor authentication token label
        else if ((!bDualFactorToken) && (strcmp(argv[i], "-dt") == 0) &&
             (argc > i + 1)) {
            bDualFactorToken =
                readArgAsString(argv[i + 1], &pDualFactorToken,
                          &ulDualFactorToken);
        }
        //  dual factor authentication user pin
        else if ((!bDualFactorUserPin) && (strcmp(argv[i], "-du") == 0)
             && (argc > i + 1)) {
            bDualFactorUserPin =
                readArgAsString(argv[i + 1], &pDualFactorUserPin,
                          &ulDualFactorUserPin);
        }
        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bUserType) {
        printf("\n\tError: User Type (-u) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bNewUserPswd) {
        printf("\n\tError: New password (-n) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bName) {
        printf("\n\tError: User name (-s) is missing.\n");
        bHelp = TRUE;
    }
#ifdef CLOUD_HSM_CLIENT
    if (!bHelp && (fipsState == 3) && (ulUserType != CN_APPLIANCE_USER)) {
        if( ! (bDualFactorSlot && bDualFactorPin &&
               bDualFactorToken && bDualFactorUserPin) ) {
            printf("\n\tError: For network dual factor authentication -dn, -dt, -du and -ds options must be specified.\n");
            bHelp = TRUE;
        }
    }
#else
    if (!bHelp && (bDualFactorHost || bDualFactorPort || bDualFactorCert ||
               bDualFactorSlot || bDualFactorPin ||
               bDualFactorToken || bDualFactorUserPin))
    {
        if( ! (bDualFactorHost && bDualFactorPort && bDualFactorCert &&
               bDualFactorSlot && bDualFactorPin &&
               bDualFactorToken && bDualFactorUserPin) ) {
            printf("\n\tError: For network dual factor authentication -dh,-dp,-dc,-dn,-dt,-du and -ds options must be specified.\n");
            bHelp = TRUE;
        }
    }
#endif

    if (bDualFactorSlot) {
        if(pDualFactorSlot == -1) {
            printf("\n\tError: Invalid value for -dn.");
            bHelp = TRUE;
        }
    }

    if (!bHelp && bUserType) {
        if ((strcmp(pUserType, "CO") == 0))
            ulUserType = CN_CRYPTO_OFFICER;
        else if ((strcmp(pUserType, "CU") == 0))
            ulUserType = CN_CRYPTO_USER;
        else if ((strcmp(pUserType, "AU") == 0))
            ulUserType = CN_APPLIANCE_USER;
        else {
            printf("\n\tError: Invalid user type specified.\n");
            bHelp = TRUE;
        }
    }

    if (!bHelp && ( (ulNewUserPswdLen < MIN_PSWD_LEN) || (ulNewUserPswdLen > MAX_PSWD_LEN) )) {
        printf("\n\tError: Incorrect password length.\n");
        bHelp = TRUE;
    }

    if (ulDualFactorPin > MAX_PSWD_LEN) {
        printf("\n\tError: Incorrect SO pin length.n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nChange the user's password or challenge.");
        printf("\n");
        printf
            ("\nSyntax: changePswd -h -u <user_type> -n <new user password> -s <user name> ");
#ifndef CLOUD_HSM_CLIENT
        printf("[-dh <host> -dp <port> -dc <cert> ");
#else
        printf("[");
#endif
        printf("-dn <slot> -ds <SO pin> -dt <token label> -du <user pin>]" );
        printf("\n\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -u  specifies the user type as \"CO\" or \"CU\" or \"AU\"");
        printf("\n       -n  specifies the new user password");
        printf("\n       -s  specifies the user name");
#ifndef CLOUD_HSM_CLIENT
        printf("\n       -dh specifies the hostname for network dual factor authentication server");
        printf("\n       -dp specifies the port used by network dual factor authentication server");
        printf("\n       -dc specifies the certificate used by network dual factor authentication server");
#endif
        printf("\n       -dn  specifies the slot number for SmartCard in network dual factor authentication server");
        printf("\n       -ds  specifies the so pin for SmartCard in network dual factor authentication server");
        printf("\n       -dt specifies the token label for SmartCard(CO) in network dual factor authentication server");
        printf("\n       -du specifies the user pin for SmartCard(CO) in network dual factor authentication server");
        printf("\n");
        return ulRet;
    }
#ifndef CLOUD_HSM_CLIENT
    ulRet = encrypt_pswd(session_handle,
                 (Uint8 *) pNewUserPswd, ulNewUserPswdLen,
                 pEncPswd, &ulEncPswdLen);
    if (ulRet != RET_OK) {
        printf("password encryption failed ulRet %d: %s\n",
               ulRet, Cfm2ResultAsString(ulRet));
        return ulRet;
    }

    if ((fipsState == 3) &&
        (ulUserType != CN_APPLIANCE_USER)) {
        if(!bDualFactorHost) {
            printf("\n\t******Insert Crypto User SmartCard******\n");
            printf("Hit Enter key to continue\n");
            getchar();

#ifdef USE_SMARTCARD_AS_POSSESSION_FACTOR
            ulRet = initSmartCard(1);
            if (ulRet != RET_OK) {
                printf("\n\tFailed to initialize Smart Card\n");
                shutdownSmartCard();
                return ulRet;
            }

            ulRet = change_user_password_on_eToken(pNewUserPswd);
            if (ulRet != 0) {
                printf("\n\t Failed to change user password on eToken\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }
#endif

            if (pPublicKey == NULL)
                pPublicKey = malloc(PSWD_ENC_KEY_MODULUS * 2);
            if (pSignature == NULL)
                pSignature = malloc(PSWD_ENC_KEY_MODULUS);
            if ((pSignature == NULL) || (pPublicKey == NULL)) {
                printf("\n\t Failed to allocate memory\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }

            memset(pPublicKey, 0, PSWD_ENC_KEY_MODULUS*2);
            memset(pSignature, 0, PSWD_ENC_KEY_MODULUS);

            ulRet = generate_key_pair_on_eToken(session_handle, pUserName, pNewUserPswd);
            if (ulRet != 0) {
                printf("\n\t Failed to generate key pair on eToken\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }
            ulRet = get_public_key_from_eToken(session_handle, pUserName, pNewUserPswd, pPublicKey);
            if (ulRet != 0) {
                printf("\n\t Failed to get public key from eToken\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }
            ulRet = get_signature_from_eToken(session_handle, pUserName, pNewUserPswd, pEncPswd, ulEncPswdLen, pSignature);
            if (ulRet != 0) {
                printf("\n\t Failed to get signature from eToken\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }
#ifdef USE_SMARTCARD_AS_POSSESSION_FACTOR
            shutdownSmartCard();
#endif
        } else { //dual factor authentication over network
            printf("\n\tPlease ensure dual factor authentication server is running\n");
            SSL_CTX *ctx = NULL;
            int server = 0;
            SSL *ssl = NULL;
            df_response resp_data;

            if (pPublicKey == NULL)
                pPublicKey = malloc(PSWD_ENC_KEY_MODULUS * 2);
            if (pSignature == NULL)
                pSignature = malloc(PSWD_ENC_KEY_MODULUS);
            if ((pSignature == NULL) || (pPublicKey == NULL)) {
                printf("\n\t Failed to allocate memory\n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                return ulRet;
            }

            memset(pPublicKey, 0, PSWD_ENC_KEY_MODULUS*2);
            memset(pSignature, 0, PSWD_ENC_KEY_MODULUS);

            SSL_library_init();
            ctx = InitSSL_CTX();

            if(!SSL_CTX_load_verify_locations(ctx, pDualFactorCert, NULL)){
                ulRet = -1;
                printf("\n\tCould not load the certificate trust chain\n");
                goto ssl_fail;
            }

            SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);

            server = OpenSocketConnection(pDualFactorHost, pDualFactorPort);
            ssl = SSL_new(ctx);
            SSL_set_fd(ssl, server);

            ulRet =  SSL_connect(ssl);
            if ( ulRet == -1 ) {
                printf("\n\nSSL connect failed\n");
                goto ssl_fail;
            }

            df_request req_data;
            req_data.command_type = DF_INITIALIZE;
            req_data.init_data.reinitialize = 1;
            req_data.init_data.slot = pDualFactorSlot;
            memcpy(req_data.init_data.SOPin, pDualFactorPin,
                   ulDualFactorPin + 1);
            memcpy(req_data.init_data.tokenLabel, pDualFactorToken,
                   ulDualFactorToken + 1);
            memcpy(req_data.init_data.userPin, pDualFactorUserPin,
                   ulDualFactorUserPin + 1);

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code != 0) {
                    printf("\n\tFailed to get initialize SmartCard over network\n");
                    ulRet = -1;
                    goto token_fail;
                }
            } else {
                printf("\n\tSSL_read failed\n");
                ulRet = -1;
                goto token_fail;
            }

            memset(&req_data,0,sizeof(req_data));
            memset(&resp_data,0,sizeof(resp_data));

            req_data.command_type = DF_CHANGE_PASSWORD;
            memcpy(req_data.change_pswd_data.SOPin,pDualFactorPin,ulDualFactorPin+1);
            memcpy(req_data.change_pswd_data.password,pNewUserPswd,ulNewUserPswdLen+1);

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */

            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code != 0) {
                    printf("\n\tFailed to get change password over network");
                    ulRet = -1;
                    goto token_fail;
                }
            } else {
                printf("\n\tSSL_read failed");
                ulRet = -1;
                goto token_fail;
            }

            memset(&req_data,0,sizeof(req_data));
            memset(&resp_data,0,sizeof(resp_data));

            req_data.command_type = DF_GENERATE_KEY_PAIR;
            req_data.gen_key_data.session_handle = session_handle;
            memcpy(req_data.gen_key_data.userName,pUserName,ulNameLen+1);
            memcpy(req_data.gen_key_data.password,pNewUserPswd,ulNewUserPswdLen+1);

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code != 0) {
                    printf("\n\tFailed to get generate key pair over network");
                    ulRet = -1;
                    goto token_fail;
                }
            } else {
                printf("\n\tSSL_read failed");
                ulRet = -1;
                goto token_fail;
            }

            memset(&req_data,0,sizeof(req_data));
            memset(&resp_data,0,sizeof(resp_data));

            req_data.command_type = DF_GET_KEY;
            req_data.get_key_data.session_handle = session_handle;
            memcpy(req_data.get_key_data.userName,pUserName,ulNameLen+1);
            memcpy(req_data.get_key_data.password,pNewUserPswd,ulNewUserPswdLen+1);

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code == 0) {
                    memcpy(pPublicKey,&(resp_data.data),2*PSWD_ENC_KEY_MODULUS);
                } else {
                    printf("\n\tFailed to get public key over network");
                    ulRet = -1;
                    goto token_fail;
                }
            } else {
                printf("\n\tSSL_read failed");
                ulRet = -1;
                goto token_fail;
            }

            memset(&req_data,0,sizeof(req_data));
            memset(&resp_data,0,sizeof(resp_data));

            req_data.command_type = DF_GET_SIGNATURE;
            req_data.get_sign_data.session_handle = session_handle;
            memcpy(req_data.get_sign_data.userName,pUserName,ulNameLen+1);
            memcpy(req_data.get_sign_data.password,pNewUserPswd,ulNewUserPswdLen+1);
            memcpy(req_data.get_sign_data.pEncPswd,pEncPswd,ulEncPswdLen);
            req_data.get_sign_data.ulEncPswdLen = ulEncPswdLen;

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code == 0) {
                    memcpy(pSignature,&(resp_data.data),PSWD_ENC_KEY_MODULUS);
                } else {
                    printf("\n\tFailed to get signature over network");
                    ulRet = -1;
                    goto token_fail;
                }
            } else {
                printf("\n\tSSL_read failed");
                ulRet = -1;
                goto token_fail;
            }

token_fail:
            memset(&req_data,0,sizeof(req_data));
            req_data.command_type = DF_END;
            SSL_write(ssl, &req_data, sizeof(req_data));

ssl_fail:
            if(ssl) SSL_free(ssl);
            if(server) close(server);
            if(ctx) SSL_CTX_free(ctx);
            if(ulRet == -1) return ulRet;
        }
    }
    ulRet = Cfm2ChangeUserPswd(session_handle,
                   ulUserType,
                   (Uint8 *) pUserName, ulNameLen,
                   (Uint8 *) pEncPswd,
                   ulEncPswdLen,
                   pPublicKey, pSignature);
#else

    if ((fipsState == 3) &&
        (ulUserType != CN_APPLIANCE_USER)) {
        if (pPublicKey == NULL)
            pPublicKey = malloc(PSWD_ENC_KEY_MODULUS * 2);
        if (pSignature == NULL)
            pSignature = malloc(PSWD_ENC_KEY_MODULUS);
        if ((pSignature == NULL) || (pPublicKey == NULL)) {
            printf("\n\t Failed to allocate memory\n");
            ulRet = ERR_MEMORY_ALLOC_FAILURE;
            return ulRet;
        }
        memset(pPublicKey, 0, PSWD_ENC_KEY_MODULUS*2);
        memset(pSignature, 0, PSWD_ENC_KEY_MODULUS);

        memcpy(pPublicKey, pDualFactorPin, MAX_PSWD_LENGTH+1);
        memcpy(pPublicKey+MAX_PSWD_LENGTH+1, &pDualFactorSlot, sizeof(pDualFactorSlot));
    }

    ulRet = Cfm2ChangeUserPswd(session_handle,
                   ulUserType,
                   (Uint8 *) pUserName, ulNameLen,
                   (Uint8 *) pNewUserPswd,
                   ulNewUserPswdLen,
                   pPublicKey, pSignature);
#endif

    printf("\n\tCfm2ChangeUserPswd returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    if(pSignature) free(pSignature);
    if(pPublicKey) free(pPublicKey);

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : load_lib
 *
 * DESCRIPTION  : Loads the HSM libraries using PKCS11 Interface
 *
 * PARAMETERS   : Absolute linrary path
 *
 *****************************************************************************/

static int  inline load_lib(const char *libpath)
{
    CK_RV rv = -1;
    CK_C_GetFunctionList get_function_list;                                     

    module = dlopen (libpath, RTLD_NOW);
    if (!module){
        print_verbose("couldn't open library: %s: %s", libpath, dlerror ());
        return -1;
    }

    /* Lookup the appropriate function in library */
    get_function_list = (CK_C_GetFunctionList)dlsym (module, "C_GetFunctionList");
    if (!get_function_list){
        print_verbose("couldn't find function 'C_GetFunctionList' in library: %s: %s",
                libpath, dlerror ());
        return -1;
    }

    /* Get the function list */
    rv = (get_function_list) (&func_list);
    if(rv != CKR_OK || !func_list){
        print_verbose("couldn't get function list: %d", (int)rv);
        return -1;
    }
   
    return rv;
}

/****************************************************************************
 *
 * FUNCTION     : lib_close
 *
 * DESCRIPTION  : Closes the HSM module
 *
 * PARAMETERS   : none
 *
 *****************************************************************************/

static int inline lib_close()
{
   if(module)  dlclose(module);
   return 0;
}



/****************************************************************************
 *
 * FUNCTION     : login
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 login(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bPassword = FALSE;
    char *pPassword = 0;
    Uint32 ulPasswordLen = 0;

    Uint8 bUserType = FALSE;
    char *pUserType = 0;
    Uint32 ulUserTypeLen = 0;
    Uint32 ulUserType = 0;

    Uint8 bUserName = FALSE;
    char *pUserName = 0;
    Uint32 ulUserNameLen = 0;

	char pPin[256];
	int n_pin;
    CK_TOKEN_INFO pInfo;   
	int slotID=1; //Default Value
	CK_RV rv;
	CK_SESSION_HANDLE session;
	
#ifndef CLOUD_HSM_CLIENT
    Uint8 pEncPswd[PSWD_ENC_KEY_MODULUS] = { 0 };
    Uint32 ulEncPswdLen = PSWD_ENC_KEY_MODULUS;

    Uint8  bDualFactorHost   = FALSE;
    char*  pDualFactorHost   = 0;
    Uint32 ulDualFactorHost  = 0;

    Uint8  bDualFactorPort   = FALSE;
    Uint32 pDualFactorPort   = 0;

    Uint8  bDualFactorCert   = FALSE;
    char*  pDualFactorCert   = 0;

    Uint8  bDualFactorSlot   = FALSE;
    Uint32 pDualFactorSlot   = 0;
#endif

    Uint8 *pSignature = NULL;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        //  password
        else if ((!bPassword) && (strcmp(argv[i], "-p") == 0)
             && (argc > i + 1))
            bPassword =
                readArgAsString(argv[i + 1], &pPassword, &ulPasswordLen);

        //  user type
        else if ((!bUserType) && (strcmp(argv[i], "-u") == 0)
             && (argc > i + 1))
            bUserType =
                readArgAsString(argv[i + 1], &pUserType, &ulUserTypeLen);

        //  user name
        else if ((!bUserName) && (strcmp(argv[i], "-s") == 0)
             && (argc > i + 1))
            bUserName =
                readArgAsString(argv[i + 1], &pUserName, &ulUserNameLen);

#ifndef CLOUD_HSM_CLIENT
        //  dual factor authentication hostname
        else if ((!bDualFactorHost ) && (strcmp(argv[i],"-dh")==0) && (argc > i+1))
            bDualFactorHost = readArgAsString(argv[i+1], &pDualFactorHost, &ulDualFactorHost);

        //  dual factor authentication port
        else if ((!bDualFactorPort ) && (strcmp(argv[i],"-dp")==0) && (argc > i+1))
            bDualFactorPort = readIntegerArg(argv[i+1], &pDualFactorPort);

        //  dual factor authentication server certificate path
        else if ((!bDualFactorCert ) && (strcmp(argv[i],"-dc")==0) && (argc > i+1)) {
            pDualFactorCert = argv[i+1];
            bDualFactorCert = TRUE;
        }
        //  dual factor authentication token slot
        else if ((!bDualFactorSlot ) && (strcmp(argv[i],"-dn")==0) && (argc > i+1))
        {
            if(!isdigit(argv[i+1][0])) {
                pDualFactorSlot = -1;
                bDualFactorSlot = TRUE;
            } else {
                bDualFactorSlot = readIntegerArg(argv[i+1], &pDualFactorSlot);
            }
        }
#endif

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bUserType) {
        printf("\n\tError: User type (-u) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bPassword) {
        printf("\n\tError: Password (-p) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && bUserType) {
        if ((strcmp(pUserType, "CO") == 0))
            //ulUserType = CN_CRYPTO_OFFICER;
            ulUserType = 0;
        else if ((strcmp(pUserType, "CU") == 0))
            ulUserType = CN_CRYPTO_USER;
        else if ((strcmp(pUserType, "AU") == 0))
            ulUserType = CN_APPLIANCE_USER;
        else {
            printf("\n\tError: Invalid user type specified.\n");
            bHelp = TRUE;
        }
    }
    if (!bHelp && !bUserName) {
        printf("\n\tError: Username (-s) is missing.\n");
        bHelp = TRUE;
    }

#ifndef CLOUD_HSM_CLIENT
    /* For network dual factor authentication, all four options must be specified */
    if (!bHelp && (bDualFactorHost || bDualFactorPort ||  bDualFactorCert || bDualFactorSlot))
    {
        if ( ! (bDualFactorHost && bDualFactorPort &&  bDualFactorCert && bDualFactorSlot)) {
            printf("\n\tError: For network dual factor authentication -dh,-dp,-dc and -dn options must be specified.\n");
            bHelp = TRUE;
        }
    }

    if (bDualFactorSlot) {
        if(pDualFactorSlot == -1) {
            printf("\n\tError: Invalid value for -dn.");
            bHelp = TRUE;
        }
    }
#endif

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nLogin to the HSM providing the user type and password.");
        printf("\n");
        printf
            ("\nSyntax: loginHSM -h -u <user type> -p <password> -s <username> ");
#ifndef CLOUD_HSM_CLIENT
        printf("[-dh <host> -dp <port> -dc <cert> -dn <slot>]");
#endif
        printf("\n\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -u  specifies the user type as \"CO\" or \"CU\" or \"AU\"");
        printf("\n       -s  specifies the user name");
        printf("\n       -p  specifies the user password");
#ifndef CLOUD_HSM_CLIENT
        printf("\n       -dh specifies the hostname for network dual factor authentication server");
        printf("\n       -dp specifies the port used by network dual factor authentication server");
        printf("\n       -dc specifies the certificate used by network dual factor authentication server");
        printf("\n       -dn specifies the slot number for SmartCard in network dual factor authentication server");
#endif
        printf("\n");
        return RET_INVALID_INPUT;
    }

	 strcpy(pPin,pUserName);
	 strncat(pPin,":",sizeof(unsigned char));
	 strncat(pPin+strlen(pUserName)+1,pPassword,strlen(pPassword));		
	 n_pin = strlen(pPin);
	
	   rv=load_lib(libname);
	 
		if(rv!=0){
			print_verbose("\n Error: loading library %s\n ", libname);
			goto end;
		}

		if(func_list == NULL){
			print_verbose("\n Function pointers are not loaded");
			goto end;
		}

	   rv = (func_list->C_Initialize) (NULL);
	   if(CKR_OK != rv ){
			print_verbose("\nC_Initialize() failed with %08lx\n", rv);
			goto end;
	   }
		rv = (func_list->C_OpenSession)(slotID, CKF_SERIAL_SESSION | CKF_RW_SESSION, NULL, NULL, &session);
		if(CKR_OK != rv ){
			print_verbose("\nC_OpenSession() failed with %08lx\n", rv);
			goto end;
    	}
	else
		session_handle = session;

	  slotID=1;
		rv = (func_list->C_Login)(session, ulUserType, (CK_UTF8CHAR_PTR)pPin, n_pin);
		if(rv != CKR_OK){
			  print_verbose("\nInvalid Credentials: Login Failed");
			  goto end;
		}
	
	
	end:
	   lib_close();
    printf("\n\tCfm2LoginHSM returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(rv));
    if (pSignature) free(pSignature);
return session_handle;
}

/****************************************************************************
 *
 * FUNCTION     : logout
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 logout(int argc, char **argv)
{
    Uint32 ulRet = 0;

    if (argc > 2) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nLogout from the HSM.");
        printf("\n");
        printf("\nSyntax: logoutHSM -h\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2LogoutHSM2(session_handle);
    printf("\n\tCfm2LogoutHSM2 returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : zeroize
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 zeroize(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;
    char file[256] = { };
    HSMInfo hsm_info = { };

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nZeroize the HSM.");
        printf("\n");
        printf("\nSyntax: zeroizeHSM -h\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n");
        return ulRet;
    }

    printf("\n\tPlease wait, this may take few minutes.");
    printf("\n");

    ulRet = Cfm2ZeroizeHSM(session_handle);
    printf("\n\tCfm2Zeroize returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    if (ulRet == 0) {
        char *cmd = NULL;
        sprintf(file, "%s/nfbe%d/%s_%d", BUILD_DIR, dev_id, KEK_FILE,
            PARTITION_INDEX(session_handle));
        cmd = (char *)malloc(strlen(file) + 20);
        if (cmd) {
            sprintf(cmd, "rm -f %s", file);
            system(cmd);
            free(cmd);
        } else {
            printf
                ("\n\tWARNING!!! Unable to delete KeK file because of malloc failure\n");
        }

        application_id = 0;
            if ((ulRet =
                 Cfm2Initialize3(dev_id, DIRECT, (Uint8 *) partition_name,
                         &application_id)) != 0)
            {
                printf
                    ("\n\tFailed to re-initialize the library with error: 0x%02x : %s\n",
                     ulRet, Cfm2ResultAsString(ulRet));
                return ulRet;
            }
        ulRet = Cfm2OpenSession2(application_id, &session_handle);
        if (ulRet) {
            printf
                ("\n\tFailed to open a session after library reinitialization with error: 0x%02x : %s\n",
                 ulRet, Cfm2ResultAsString(ulRet));
            return ulRet;
        }
        if ((ulRet = Cfm2GetHSMInfo2(session_handle, &hsm_info)) != 0) {
            printf("\n\tCfm2GetHSMInfo2 returned: 0x%02x : %s\n",
                   ulRet, Cfm2ResultAsString(ulRet));
            goto end;
        }

        fipsState = betoh32(hsm_info.uiFipsState);
        printf("\n Current FIPS mode is: %d\n", fipsState);

    }
end:
    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : genECCKeyPair
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 genECCKeyPair(int argc, char **argv)
{

    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bCurveId = FALSE;
    Uint32 ulCurveId = 0;

    Uint8 bID = FALSE;
    char *pID = 0;
    Uint32 ulIDLen = 0;

    Uint8 bLabel = FALSE;
    char *pLabel = 0;
    Uint32 ulLabelLen = 0;
    Uint8 ucKeyLocation = STORAGE_FLASH;

    Uint64 ulECCPublicKey64 = 0;
    Uint64 ulECCPrivateKey64 = 0;
    Uint32 ulECCPublicKey32 = 0;
    Uint32 ulECCPrivateKey32 = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // CurveId
        else if ((!bCurveId) && (strcmp(argv[i], "-i") == 0)
             && (argc > i + 1)) {
            bCurveId = readIntegerArg(argv[i + 1], &ulCurveId);
            ulCurveId = get_openssl_curve_id(ulCurveId);
            if (!ulCurveId) {
                printf("\n\tError: Invalid EC Curve ID.\n");
                printf("\nThe following are the HSM supported ECC Curves");
                printf("\n");
                printf("\n      NID_X9_62_prime192v1     - 1");
                printf("\n      NID_X9_62_prime256v1     - 2");
                printf("\n      NID_sect163k1            - 3");
                printf("\n      NID_sect163r2            - 4");
                printf("\n      NID_sect233k1            - 5");
                printf("\n      NID_sect233r1            - 6");
                printf("\n      NID_sect283k1            - 7");
                printf("\n      NID_sect283r1            - 8");
                printf("\n      NID_sect409k1            - 9");
                printf("\n      NID_sect409r1            - 10");
                printf("\n      NID_sect571k1            - 11");
                printf("\n      NID_sect571r1            - 12");
                printf("\n      NID_secp224r1            - 13");
                printf("\n      NID_secp384r1            - 14");
                printf("\n      NID_secp521r1            - 15");
                printf("\n");
                return 0;
            }
        }
        // Label
        else if ((!bLabel) && (strcmp(argv[i], "-l") == 0) && (argc > i + 1))
            bLabel = readArgAsString(argv[i + 1], &pLabel, &ulLabelLen);

        // Key ID
        else if ((!bID) && (strcmp(argv[i], "-id") == 0) && (argc > i + 1))
            bID = readArgAsString(argv[i + 1], &pID, &ulIDLen);

        else if (strcmp(argv[i], "-sess") == 0) {
            ucKeyLocation = STORAGE_RAM;
            i--;                //This for loops skips i by 2. so go with it.
        }

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bCurveId) {
        printf("\n\tError: Curve id (-i) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bLabel) {
        printf("\n\tError: Key label (-l) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bLabel && validateLabel(pLabel, ulLabelLen))
        bHelp = TRUE;

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nGenerate ECC key pair specifying the curve id");
        printf("\nand key label.");
        printf("\n");
        printf("\nSyntax: genECCKeyPair -h -i <EC curve id> -l <label> [-sess] [-id <key ID>]\n");
        printf("\n");
        printf("\nWhere: -h    displays this information");
        printf("\n       -i    specifies the Curve ID");
        printf("\n       -l    specifies the key label");
        printf("\n       -sess specifies key as session key");
        printf("\n       -id   specifies key ID");
        printf("\n\nThe following are the HSM supported ECC Curves");
        printf("\n");
        printf("\n      NID_X9_62_prime192v1     - 1");
        printf("\n      NID_X9_62_prime256v1     - 2");
        printf("\n      NID_sect163k1            - 3");
        printf("\n      NID_sect163r2            - 4");
        printf("\n      NID_sect233k1            - 5");
        printf("\n      NID_sect233r1            - 6");
        printf("\n      NID_sect283k1            - 7");
        printf("\n      NID_sect283r1            - 8");
        printf("\n      NID_sect409k1            - 9");
        printf("\n      NID_sect409r1            - 10");
        printf("\n      NID_sect571k1            - 11");
        printf("\n      NID_sect571r1            - 12");
        printf("\n      NID_secp224r1            - 13");
        printf("\n      NID_secp384r1            - 14");
        printf("\n      NID_secp521r1            - 15");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2GenerateKeyPair(session_handle,
                    KEY_TYPE_ECDSA,
                    0, 0, ulCurveId,
                    0, NULL, 0, NULL,
                    (Uint8 *) pID, ulIDLen,
                    (Uint8 *) pLabel, ulLabelLen,
                    (Uint8 *) pLabel, ulLabelLen,
                    ucKeyLocation,
                    (Uint64 *) & ulECCPublicKey64,
                    (Uint64 *) & ulECCPrivateKey64);

    printf("\n\tCfm2GenerateKeyPair returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    if (ulRet == 0) {
        ulECCPublicKey32 = (Uint32) ulECCPublicKey64;
        ulECCPrivateKey32 = (Uint32) ulECCPrivateKey64;
        printf
            ("\n\tCfm2GenerateKeyPair:    public key handle: %d    private key handle: %d\n",
             (Uint32) ulECCPublicKey32, (Uint32) ulECCPrivateKey32);
    }
    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : genDSAKeyPair
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 genDSAKeyPair(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bMod = FALSE;
    Uint32 ulModLen = 0;

    Uint8 bID = FALSE;
    char *pID = 0;
    Uint32 ulIDLen = 0;

    Uint8 bLabel = FALSE;
    char *pLabel = 0;
    Uint32 ulLabelLen = 0;
    Uint8 ucKeyLocation = STORAGE_FLASH;

    Uint64 ulDSAPublicKey64 = 0;
    Uint64 ulDSAPrivateKey64 = 0;
    Uint32 ulDSAPublicKey32 = 0;
    Uint32 ulDSAPrivateKey32 = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // Mod
        else if ((!bMod) && (strcmp(argv[i], "-m") == 0) && (argc > i + 1))
            bMod = readIntegerArg(argv[i + 1], &ulModLen);

        // Label
        else if ((!bLabel) && (strcmp(argv[i], "-l") == 0) && (argc > i + 1))
            bLabel = readArgAsString(argv[i + 1], &pLabel, &ulLabelLen);

        // Key ID
        else if ((!bID) && (strcmp(argv[i], "-id") == 0) && (argc > i + 1))
            bID = readArgAsString(argv[i + 1], &pID, &ulIDLen);

        else if (strcmp(argv[i], "-sess") == 0) {
            ucKeyLocation = STORAGE_RAM;
            i--;                //This for loops skips i by 2. so go with it.
        }

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bMod) {
        printf("\n\tError: Modulus size (-m) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bLabel) {
        printf("\n\tError: Key label (-l) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bLabel && validateLabel(pLabel, ulLabelLen))
        bHelp = TRUE;


    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf
            ("\nGenerate DSA key pair specifying modulus length and key label.");
        printf("\n");
        printf("\nSyntax: genDSAKeyPair -h -m <modulus length> -l <label> [-sess] [-id <key ID>]\n");
        printf("\n");
        printf("\nWhere: -h    displays this information");
        printf("\n       -m    specifies the modulus length in bits:");
        printf("\n             Should be either 1024, 2048 or 3072");
        printf("\n       -l    specifies the key label");
        printf("\n       -sess specifies key as session key");
        printf("\n       -id   specifies key ID");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2GenerateKeyPair(session_handle,
                    KEY_TYPE_DSA,
                    ulModLen, 0, 0,
                    0, NULL, 0, NULL,
                    (Uint8 *) pID, ulIDLen,
                    (Uint8 *) pLabel, ulLabelLen,
                    (Uint8 *) pLabel, ulLabelLen,
                    ucKeyLocation,
                    (Uint64 *) & ulDSAPublicKey64,
                    (Uint64 *) & ulDSAPrivateKey64);

    printf("\n\tCfm2GenerateKeyPair returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    if (ulRet == 0) {
        ulDSAPublicKey32 = (Uint32) ulDSAPublicKey64;
        ulDSAPrivateKey32 = (Uint32) ulDSAPrivateKey64;
        printf
            ("\n\tCfm2GenerateKeyPair:    public key handle: %d    private key handle: %d\n",
             (Uint32) ulDSAPublicKey32, (Uint32) ulDSAPrivateKey32);
    }
    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : eccSign
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 eccSign(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bECCPrivateKey64 = FALSE;
    Uint64 ulECCPrivateKey64 = 0;

    Uint32 ulMech = CRYPTO_MECH_ECDSA_SHA1;

    Uint8 bMsg = FALSE;
    Uint32 ulMsgLen = 0;
    char *pMsg = NULL;

    Uint32 ulSigLen = 512;
    Uint8 pSig[512] = { };
    char *pSigFile = "sig.file";

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        // Key handle
        else if ((!bECCPrivateKey64) && (strcmp(argv[i], "-k") == 0)
             && (argc > i + 1))
            bECCPrivateKey64 =
                readIntegerArg(argv[i + 1], (Uint32 *) & ulECCPrivateKey64);
        // Msg
        else if ((!bMsg) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1)) {
            bMsg =
                readStringArg(argv[i + 1], &pMsg, &ulMsgLen, 1,
                          "Message to be signed");
        } else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bMsg) {
        printf("\n\tError: Message file (-f) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bECCPrivateKey64) {
        printf("\n\tError: Private Key (-k) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nGenerates ECC signature on the given data");
        printf("\n");
        printf("\nSyntax: eccSign -h -f <msg file> -k <key handle>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -f  Message File");
        printf("\n       -k  ECC Private key handle");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2Sign2(session_handle,
              (Uint8 *) pMsg,
              ulMsgLen,
              (Uint8 *) pSig, &ulSigLen, ulECCPrivateKey64, ulMech, 0);

    printf("\n\tCfm2Sign2 returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    if (ulRet == 0) {
        ulRet = write_file(pSigFile, pSig, ulSigLen);
        printf("\n\tsignature is written to file %s\n", pSigFile);
    }

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : eccVerify
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 eccVerify(int argc, char **argv)
{

    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bECCKey64 = FALSE;
    Uint64 ulECCKey64 = 0;

    Uint32 ulMech = CRYPTO_MECH_ECDSA_SHA1;

    Uint8 bMsg = FALSE;
    Uint8 *pMsg = NULL;
    Uint32 ulMsgLen = 0;

    Uint8 bSig = FALSE;
    char *pSig = NULL;
    Uint32 ulSigLen = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // Key handle
        else if ((!bECCKey64) && (strcmp(argv[i], "-k") == 0)
             && (argc > i + 1))
            bECCKey64 = readIntegerArg(argv[i + 1], (Uint32 *) & ulECCKey64);

        // Msg
        else if ((!bMsg) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1)) {
            bMsg =
                readStringArg(argv[i + 1], (char **)&pMsg, &ulMsgLen, 1,
                          "Message");
        }
        // Sig
        else if ((!bSig) && (strcmp(argv[i], "-s") == 0) && (argc > i + 1))
            bSig =
                readStringArg(argv[i + 1], &pSig, &ulSigLen, 1, "Signature");

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bMsg) {
        printf("\n\tError: Message file (-f) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bSig) {
        printf("\n\tError: Signature file (-s) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bECCKey64) {
        printf("\n\tError: Public Key (-k) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nVerifies ECC key signature on the given data");
        printf("\n");
        printf("\nSyntax: eccVerify -h -f <msg file> "
               "-s <signature file> -k <key handle>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -f  Message File");
        printf("\n       -s  Signature File");
        printf("\n       -k  ECC Public key handle");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2Verify2(session_handle,
                pMsg,
                ulMsgLen,
                (Uint8 *) pSig, ulSigLen, ulECCKey64, ulMech, 0);

    printf("\n\tCfm2Verify2 returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : RSASign
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 rsaSign(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bMech = FALSE;
    Uint32 ulMech = 0;

    Uint8 bRSAPrivateKey64 = FALSE;
    Uint64 ulRSAPrivateKey64 = 0;

    Uint8 bMsg = FALSE;
    Uint8 *pMsg = NULL;
    Uint32 ulMsgLen = 0;

    Uint32 ulSigLen = 512;
    Uint8 pSig[512] = { };
    char *pSigFile = "sig.file";

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // Key handle
        else if ((!bRSAPrivateKey64) && (strcmp(argv[i], "-k") == 0)
             && (argc > i + 1))
            bRSAPrivateKey64 =
                readIntegerArg(argv[i + 1], (Uint32 *) & ulRSAPrivateKey64);

        // Mechanism
        else if ((!bMech) && (strcmp(argv[i], "-m") == 0) && (argc > i + 1)) {
            bMech = readIntegerArg(argv[i + 1], (Uint32 *) & ulMech);
            ulMech = get_sign_mechanism(ulMech);
        }
        // Msg
        else if ((!bMsg) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1)) {
            bMsg =
                readStringArg(argv[i + 1], (char **)&pMsg, &ulMsgLen, 1,
                          "Message to be signed");
        }

        else
            bHelp = TRUE;

    }

    // ensure that we have all the required args
    if (!bHelp && !bMsg) {
        printf("\n\tError: Message file (-f) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bRSAPrivateKey64) {
        printf("\n\tError: Private Key (-k) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bMech) {
        printf("\n\tError: Mechanism (-m) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nGenerates RSA key signature on the given data");
        printf("\n");
        printf
            ("\nSyntax: rsaSign -h -f <msg file> -k <key handle> -m <signature mechanism>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -f  Message File");
        printf("\n       -k  RSA Private key handle");
        printf("\n       -m  Signature Mechanism");
        printf("\n            SHA1_RSA_PKCS       - 0");
        printf("\n            SHA256_RSA_PKCS     - 1");
        printf("\n            SHA384_RSA_PKCS     - 2");
        printf("\n            SHA512_RSA_PKCS     - 3");
        printf("\n            SHA224_RSA_PKCS     - 4");
        printf("\n            SHA1_RSA_PKCS_PSS   - 5");
        printf("\n            SHA256_RSA_PKCS_PSS - 6");
        printf("\n            SHA384_RSA_PKCS_PSS - 7");
        printf("\n            SHA512_RSA_PKCS_PSS - 8");
        printf("\n            SHA224_RSA_PKCS_PSS - 9");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2Sign2(session_handle,
              pMsg,
              ulMsgLen, pSig, &ulSigLen, ulRSAPrivateKey64, ulMech, 0);

    printf("\n\tCfm2Sign2 returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    if (ulRet == 0) {
        ulRet = write_file(pSigFile, pSig, ulSigLen);
        printf("\n\tsignature is written to file %s\n", pSigFile);
    }

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : RSAVerify
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 rsaVerify(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bMech = FALSE;
    Uint32 ulMech = 0;

    Uint8 bRSAKey64 = FALSE;
    Uint64 ulRSAKey64 = 0;

    Uint8 bMsg = FALSE;
    char *pMsg = NULL;
    Uint32 ulMsgLen = 0;

    Uint8 bSig = FALSE;
    char *pSig = NULL;
    Uint32 ulSigLen = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // Key handle
        else if ((!bRSAKey64) && (strcmp(argv[i], "-k") == 0)
             && (argc > i + 1))
            bRSAKey64 = readIntegerArg(argv[i + 1], (Uint32 *) & ulRSAKey64);

        // Mechanism
        else if ((!bMech) && (strcmp(argv[i], "-m") == 0) && (argc > i + 1)) {
            bMech = readIntegerArg(argv[i + 1], (Uint32 *) & ulMech);
            ulMech = get_sign_mechanism(ulMech);
        }
        // Msg
        else if ((!bMsg) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1))
            bMsg = readStringArg(argv[i + 1], &pMsg, &ulMsgLen, 1, "Message");

        // Sig
        else if ((!bSig) && (strcmp(argv[i], "-s") == 0) && (argc > i + 1))
            bSig =
                readStringArg(argv[i + 1], &pSig, &ulSigLen, 1, "Signature");

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bMsg) {
        printf("\n\tError: Message file (-f) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bSig) {
        printf("\n\tError: Signature file (-s) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bRSAKey64) {
        printf("\n\tError: Public Key (-k) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bMech) {
        printf("\n\tError: Mechanism (-m) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nVerifies RSA key signature on the given data");
        printf("\n");
        printf("\nSyntax: rsaVerify -h -f <msg file> "
               "-s <signature file> -k <key handle> -m <Verify mechanism>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -f  Message File");
        printf("\n       -s  Signature File");
        printf("\n       -k  RSA Public key handle");
        printf("\n       -m  Verification Mechanism");
        printf("\n            SHA1_RSA_PKCS       - 0");
        printf("\n            SHA256_RSA_PKCS     - 1");
        printf("\n            SHA384_RSA_PKCS     - 2");
        printf("\n            SHA512_RSA_PKCS     - 3");
        printf("\n            SHA224_RSA_PKCS     - 4");
        printf("\n            SHA1_RSA_PKCS_PSS   - 5");
        printf("\n            SHA256_RSA_PKCS_PSS - 6");
        printf("\n            SHA384_RSA_PKCS_PSS - 7");
        printf("\n            SHA512_RSA_PKCS_PSS - 8");
        printf("\n            SHA224_RSA_PKCS_PSS - 9");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2Verify2(session_handle,
                (Uint8 *) pMsg,
                ulMsgLen,
                (Uint8 *) pSig, ulSigLen, ulRSAKey64, ulMech, 0);

    printf("\n\tCfm2RsaVerify returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : DSASign
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 dsaSign(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bDSAPrivateKey64 = FALSE;
    Uint64 ulDSAPrivateKey64 = 0;

    Uint32 ulMech = CRYPTO_MECH_DSA_SHA1;

    Uint8 bMsg = FALSE;
    Uint8 *pMsg = NULL;
    Uint32 ulMsgLen = 0;

    Uint32 ulSigLen = 512;
    Uint8 pSig[512] = { };
    char *pSigFile = "sig.file";

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // Key handle
        else if ((!bDSAPrivateKey64) && (strcmp(argv[i], "-k") == 0)
             && (argc > i + 1))
            bDSAPrivateKey64 =
                readIntegerArg(argv[i + 1], (Uint32 *) & ulDSAPrivateKey64);

        // Msg
        else if ((!bMsg) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1)) {
            bMsg =
                readStringArg(argv[i + 1], (char **)&pMsg, &ulMsgLen, 1,
                          "Message to be signed");
        }

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bMsg) {
        printf("\n\tError: Message file (-f) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bDSAPrivateKey64) {
        printf("\n\tError: Private Key (-k) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nGenerates DSA key signature on the given data");
        printf("\n");
        printf("\nSyntax: dsaSign -h -f <msg file> -k <key handle>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -f  Message File");
        printf("\n       -k  DSA Private key handle");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2Sign2(session_handle,
              pMsg,
              ulMsgLen, pSig, &ulSigLen, ulDSAPrivateKey64, ulMech, 0);

    printf("\n\tCfm2Sign2 returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    if (ulRet == 0) {
        ulRet = write_file(pSigFile, pSig, ulSigLen);
        printf("\n\tsignature is written to file %s\n", pSigFile);
    }

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : DSAVerify
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/

Uint32 dsaVerify(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint32 ulMech = CRYPTO_MECH_DSA_SHA1;

    Uint8 bDSAKey64 = FALSE;
    Uint64 ulDSAKey64 = 0;

    Uint8 bMsg = FALSE;
    Uint8 *pMsg = NULL;
    Uint32 ulMsgLen = 0;

    Uint8 bSig = FALSE;
    char *pSig = NULL;
    Uint32 ulSigLen = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // Key handle
        else if ((!bDSAKey64) && (strcmp(argv[i], "-k") == 0)
             && (argc > i + 1))
            bDSAKey64 = readIntegerArg(argv[i + 1], (Uint32 *) & ulDSAKey64);

        // Msg
        else if ((!bMsg) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1))
            bMsg =
                readStringArg(argv[i + 1], (char **)&pMsg, &ulMsgLen, 1,
                          "Message");

        // Sig
        else if ((!bSig) && (strcmp(argv[i], "-s") == 0) && (argc > i + 1))
            bSig =
                readStringArg(argv[i + 1], &pSig, &ulSigLen, 1, "Signature");

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bMsg) {
        printf("\n\tError: Message file (-f) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bSig) {
        printf("\n\tError: Signature file (-s) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bDSAKey64) {
        printf("\n\tError: Private Key (-k) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nVerifies DSA key signature on the given data");
        printf("\n");
        printf("\nSyntax: dsaVerify -h -f <msg file> "
               "-s <signature file> -k <key handle>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -m  Message File");
        printf("\n       -s  Signature File");
        printf("\n       -k  DSA Public key handle");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2Verify2(session_handle,
                pMsg,
                ulMsgLen,
                (Uint8 *) pSig, ulSigLen, ulDSAKey64, ulMech, 0);

    printf("\n\tCfm2Verify2 returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

Uint32 sign(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bPrivateKey = FALSE;
    Uint64 ulPrivateKey = 0;

    Uint8 bMech = FALSE;
    Uint32 ulMech = 0;

    Uint8 bMsg = FALSE;
    Uint8 *pMsg = NULL;
    Uint32 ulMsgLen = 0;

    Uint32 ulSigLen = 512;
    Uint8 pSig[512] = { };
    char *pSigFile = "sig.file";

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        // Key handle
        else if ((!bPrivateKey) && (strcmp(argv[i], "-k") == 0)
             && (argc > i + 1))
            bPrivateKey =
                readIntegerArg(argv[i + 1], (Uint32 *) & ulPrivateKey);
        // Mechanism
        else if ((!bMech) && (strcmp(argv[i], "-m") == 0) && (argc > i + 1)) {
            bMech = readIntegerArg(argv[i + 1], (Uint32 *) & ulMech);
            ulMech = get_sign_mechanism(ulMech);
        }
        // Msg
        else if ((!bMsg) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1)) {
            bMsg =
                readStringArg(argv[i + 1], (char **)&pMsg, &ulMsgLen, 1,
                          "Message to be signed");
        } else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bMsg) {
        printf("\n\tError: Message file (-f) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bPrivateKey) {
        printf("\n\tError: Private Key (-k) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bMech) {
        printf("\n\tError: Mechanism (-m) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf
            ("\nGenerates signature on the given data with given Private Key");
        printf("\n");
        printf
            ("\nSyntax: sign -h -f <msg file> -k <key handle> -m <signature mechanism>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -f  Message File");
        printf("\n       -k  Private key handle");
        printf("\n       -m  Signature Mechanism");
        printf("\n            SHA1_RSA_PKCS       - 0");
        printf("\n            SHA256_RSA_PKCS     - 1");
        printf("\n            SHA384_RSA_PKCS     - 2");
        printf("\n            SHA512_RSA_PKCS     - 3");
        printf("\n            SHA224_RSA_PKCS     - 4");
        printf("\n            SHA1_RSA_PKCS_PSS   - 5");
        printf("\n            SHA256_RSA_PKCS_PSS - 6");
        printf("\n            SHA384_RSA_PKCS_PSS - 7");
        printf("\n            SHA512_RSA_PKCS_PSS - 8");
        printf("\n            SHA224_RSA_PKCS_PSS - 9");
        printf("\n            DSA_SHA1            - 10");
        printf("\n            DSA_SHA224          - 11");
        printf("\n            DSA_SHA256          - 12");
        printf("\n            DSA_SHA384          - 13");
        printf("\n            DSA_SHA512          - 14");
        printf("\n            ECDSA_SHA1          - 15");
        printf("\n            ECDSA_SHA224        - 16");
        printf("\n            ECDSA_SHA256        - 17");
        printf("\n            ECDSA_SHA384        - 18");
        printf("\n            ECDSA_SHA512        - 19");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2Sign2(session_handle,
              pMsg,
              ulMsgLen, pSig, &ulSigLen, ulPrivateKey, ulMech, 0);

    printf("\n\tCfm2Sign2 returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    if (ulRet == 0) {
        ulRet = write_file(pSigFile, pSig, ulSigLen);
        printf("\n\tsignature is written to file %s\n", pSigFile);
    }
    return ulRet;
}

Uint32 verify(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bMech = FALSE;
    Uint32 ulMech = 0;

    Uint8 bKey = FALSE;
    Uint64 ulKey = 0;

    Uint8 bMsg = FALSE;
    Uint8 *pMsg = NULL;
    Uint32 ulMsgLen = 0;

    Uint8 bSig = FALSE;
    char *pSig = NULL;
    Uint32 ulSigLen = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // Key handle
        else if ((!bKey) && (strcmp(argv[i], "-k") == 0) && (argc > i + 1))
            bKey = readIntegerArg(argv[i + 1], (Uint32 *) & ulKey);

        // Mechanism
        else if ((!bMech) && (strcmp(argv[i], "-m") == 0) && (argc > i + 1)) {
            bMech = readIntegerArg(argv[i + 1], (Uint32 *) & ulMech);
            ulMech = get_sign_mechanism(ulMech);
        }
        // Msg
        else if ((!bMsg) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1))
            bMsg =
                readStringArg(argv[i + 1], (char **)&pMsg, &ulMsgLen, 1,
                          "Message");

        // Sig
        else if ((!bSig) && (strcmp(argv[i], "-s") == 0) && (argc > i + 1))
            bSig =
                readStringArg(argv[i + 1], &pSig, &ulSigLen, 1, "Signature");

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bMsg) {
        printf("\n\tError: Message file (-f) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bSig) {
        printf("\n\tError: Signature file (-s) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bKey) {
        printf("\n\tError: Private Key (-k) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nVerifies signature on the given data with give Public Key");
        printf("\n");
        printf("\nSyntax: verify -h -f <msg file> "
               "-s <signature file> -k <key handle> -m <verify mechanism>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -f  Message File");
        printf("\n       -s  Signature File");
        printf("\n       -k  Public key handle");
        printf("\n       -m  Verification Mechanism");
        printf("\n            SHA1_RSA_PKCS       - 0");
        printf("\n            SHA256_RSA_PKCS     - 1");
        printf("\n            SHA384_RSA_PKCS     - 2");
        printf("\n            SHA512_RSA_PKCS     - 3");
        printf("\n            SHA224_RSA_PKCS     - 4");
        printf("\n            SHA1_RSA_PKCS_PSS   - 5");
        printf("\n            SHA256_RSA_PKCS_PSS - 6");
        printf("\n            SHA384_RSA_PKCS_PSS - 7");
        printf("\n            SHA512_RSA_PKCS_PSS - 8");
        printf("\n            SHA224_RSA_PKCS_PSS - 9");
        printf("\n            DSA_SHA1            - 10");
        printf("\n            DSA_SHA224          - 11");
        printf("\n            DSA_SHA256          - 12");
        printf("\n            DSA_SHA384          - 13");
        printf("\n            DSA_SHA512          - 14");
        printf("\n            ECDSA_SHA1          - 15");
        printf("\n            ECDSA_SHA224        - 16");
        printf("\n            ECDSA_SHA256        - 17");
        printf("\n            ECDSA_SHA384        - 18");
        printf("\n            ECDSA_SHA512        - 19");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2Verify2(session_handle,
                pMsg,
                ulMsgLen, (Uint8 *) pSig, ulSigLen, ulKey, ulMech, 0);

    printf("\n\tCfm2Verify2 returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : listECCCurveIds
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 listECCCurveIds(int argc, char **argv)
{
    printf("\n");
    printf("\nDescription");
    printf("\n===========");
    printf("\nThe following are HSM supported ECC CurveIds");
    printf("\n");
    printf("\n      NID_X9_62_prime192v1            = %d", 0x00000001);
    printf("\n      NID_X9_62_prime256v1            = %d", 0x00000002);
    printf("\n      NID_sect163k1                   = %d", 0x00000003);
    printf("\n      NID_sect163r2                   = %d", 0x00000004);
    printf("\n      NID_sect233k1                   = %d", 0x00000005);
    printf("\n      NID_sect233r1                   = %d", 0x00000006);
    printf("\n      NID_sect283k1                   = %d", 0x00000007);
    printf("\n      NID_sect283r1                   = %d", 0x00000008);
    printf("\n      NID_sect409k1                   = %d", 0x00000009);
    printf("\n      NID_sect409r1                   = %d", 0x0000000A);
    printf("\n      NID_sect571k1                   = %d", 0x0000000B);
    printf("\n      NID_sect571r1                   = %d", 0x0000000C);
    printf("\n      NID_secp224r1                   = %d", 0x0000000D);
    printf("\n      NID_secp384r1                   = %d", 0x0000000E);
    printf("\n      NID_secp521r1                   = %d", 0x0000000F);
    printf("\n\n");
    return 0;
}

/****************************************************************************
 *
 * FUNCTION     : aesWrapUnwrap
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 aesWrapUnwrap(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bKey = FALSE;
    Uint8 *pKey = NULL;
    Uint32 ulKeyLen = 0;

    Uint8 bWrappingKey = FALSE;
    Uint64 ulWrappingKey64 = 0;

    Uint8 bMode = FALSE;
    Uint32 ulMode = 0;

    Uint8 bIV = FALSE;
    Uint8 *pIV = NULL;

    Uint8 *pResultData = NULL;
    Uint32 ulResultLen = 0;

    Uint32 ulPadLen = 0;
    Uint8 *pInputKey = NULL;
    Uint32 ulInputKeyLen = 0;

    char *key_file = NULL;
    Uint32 ulTemp = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // file to be wrapped or unwrapped
        else if ((!bKey) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1))
            bKey =
                readStringArg(argv[i + 1], (char **)&pKey, &ulKeyLen, 1,
                          "key file");
        // wrapping key
        else if ((!bWrappingKey) && (strcmp(argv[i], "-w") == 0)
             && (argc > i + 1)) {
            bWrappingKey = readIntegerArg(argv[i + 1], &ulTemp);
            ulWrappingKey64 = ulTemp;
        }
        // wrapping iv
        else if ((!bIV) && (strcmp(argv[i], "-i") == 0) && (argc > i + 1)) {
            Uint32 ulIVLen = 0;
            BIGNUM *n = NULL;
            bIV =
                readStringArg(argv[i + 1], (char **)&pIV, &ulIVLen, 0, "IV");
            n = BN_new();
            BN_hex2bn(&n, (char *)pIV);
            ulIVLen = BN_num_bytes(n);
            BN_bn2bin(n, pIV);
            BN_free(n);
            if (ulIVLen != 8) {
                printf("\nError: Invalid IV length %d\n", ulIVLen);
                bHelp = TRUE;
            }
        }
        // wrapping/unwrapping mode
        else if ((!bMode) && (strcmp(argv[i], "-m") == 0) && (argc > i + 1))
            bMode = readIntegerArg(argv[i + 1], &ulMode);

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bKey) {
        printf("\n\tError: file to be wrapped/unwrapped (-f) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bWrappingKey) {
        printf("\n\tError: Handle of wrapping Key (-w) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bIV) {
        printf("\n\tError: Handle of wrapping IV (-i) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bMode) {
        printf("\n\tError: wrapping or unwrapping mode (-m) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nWraps a buffer with a specified AES key.");
        printf("\n");
        printf
            ("\nSyntax: aesWrapUnwrap -h -f <file to wrap/unwrap> -w <wrapping/unwrapping key> -i <wrapping IV> -m <wrap/unwrap mode> \n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf
            ("\n       -w  specifies the handle of the AES wrapping/unwrapping key");
        printf("\n       -i  specifies the IV to be used");
        printf
            ("\n       -f  file to be wrapped or unwrapped (Supported file size is < 4K bytes)");
        printf
            ("\n       -m  specifies the mode 1 for wrapping 0 for unwrapping");
        printf("\n");
        return ulRet;
    }

    if (ulMode) {
        /* first apply padding then do AES Key wrap */
        ulPadLen = 8 - (ulKeyLen & 0x07);
        ulResultLen = ulKeyLen + ulPadLen + 8;
        pInputKey = (Uint8 *) malloc(ulKeyLen + ulPadLen);
        if (pInputKey == NULL) {
            printf(" malloc failure \n");
            return ulRet;
        }
        memset(pInputKey, 0x0, ulKeyLen + ulPadLen);
        memcpy(pInputKey, pKey, ulKeyLen);
        for (i = ulKeyLen; i < (ulKeyLen + ulPadLen); i++) {
            pInputKey[i] = ulPadLen;
        }
        ulInputKeyLen = ulKeyLen + ulPadLen;
    } else {
        ulResultLen = ulKeyLen - 8;
        pInputKey = (Uint8 *) malloc(ulKeyLen);
        if (pInputKey == NULL) {
            printf(" malloc failure \n");
            return ulRet;
        }
        memcpy(pInputKey, pKey, ulKeyLen);
        ulInputKeyLen = ulKeyLen;
    }

    /* Allocate Result buffer */
    pResultData = (Uint8 *) malloc(ulResultLen);
    if (pResultData == NULL) {
        printf("malloc failure \n");
        return ulRet;
    }

    if ((pResultData) &&
        (ulRet = Cfm2AesWrapUnwrapBuffer(session_handle,
                         ulWrappingKey64,
                         pInputKey,
                         ulInputKeyLen,
                         *(Uint64 *) pIV,
                         pResultData, ulMode)) == 0) {
        int i = 0;
        /* strip the padding bytes if mode is unwrap */
        if (ulMode == 0) {
            Uint8 pad_value = 0;
            pad_value = pResultData[ulResultLen - 1];
            ulResultLen = ulResultLen - pad_value;
        }
        printf("result data:");
        for (i = 0; i < ulResultLen; i++) {
            if ((i % 8) == 0)
                printf("\n");
            printf("%02X ", (Uint8) pResultData[i]);
        }

        key_file = (ulMode) ? "wrapped_key" : "unwrapped_key";
        // write to a file
        if (WriteBinaryFile(key_file, (char *)pResultData, ulResultLen))
            printf("\n\nresult written to file %s \n", key_file);
        else {
            ulRet = ERR_WRITE_OUTPUT_FILE;
            printf("\n\nFailed to write result into a file.\n");
        }
    }
    printf("\n\tCfm2AesWrapUnwrapBuffer returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    if (pInputKey)
        free(pInputKey);
    if (pResultData)
        free(pResultData);
    return ulRet;
}

#ifdef ENABLE_EAP_PAC
/****************************************************************************
 *
 * FUNCTION     : eapPacHs
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 eapPacHs(int argc, char **argv)
{

    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint64 ulWrapperKey64 = 0;
    Uint8 bWrapperKey64 = FALSE;

    Uint8 *pPacOpaque = NULL;
    Uint8 bPacOpaque = FALSE;
    Uint32 ulPacOpaqueLen = 0;

    Uint64 ulContextPtr = 0ull;
    Uint8 bContextPtr = FALSE;

    Uint8 bIV = FALSE;
    Uint8 *pIV = NULL;

    Uint8 pSessionKeySeed[40] = { };

    Uint8 pClientRandom[32] =
    { 0x3F, 0xFB, 0x11, 0xC4, 0x6C, 0xBF, 0xA5, 0x7A,
        0x54, 0x40, 0xDA, 0xE8, 0x22, 0xD3, 0x11, 0xD3,
        0xF7, 0x6D, 0xE4, 0x1D, 0xD9, 0x33, 0xE5, 0x93,
        0x70, 0x97, 0xEB, 0xA9, 0xB3, 0x66, 0xF4, 0x2A
    };

    Uint8 pServerRandom[48] =
    { 0x00, 0x00, 0x00, 0x02, 0x6A, 0x66, 0x43, 0x2A,
        0x8D, 0x14, 0x43, 0x2C, 0xEC, 0x58, 0x2D, 0x2F,
        0xC7, 0x9C, 0x33, 0x64, 0xBA, 0x04, 0xAD, 0x3A,
        0x52, 0x54, 0xD6, 0xA5, 0x79, 0xAD, 0x1E, 0x00
    };

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // CurveId
        else if ((!bContextPtr) && (strcmp(argv[i], "-c") == 0)
             && (argc > i + 1)) {
            sscanf(argv[i + 1], "%llu", &ulContextPtr);
            bContextPtr = TRUE;
        }
        // Key handle
        else if ((!bWrapperKey64) && (strcmp(argv[i], "-k") == 0)
             && (argc > i + 1))
            bWrapperKey64 =
                readIntegerArg(argv[i + 1], (Uint32 *) & ulWrapperKey64);

        //Pac
        else if ((!bPacOpaque) && (strcmp(argv[i], "-p") == 0)
             && (argc > i + 1)) {
            bPacOpaque =
                readStringArg(argv[i + 1], (char **)&pPacOpaque,
                          &ulPacOpaqueLen, 1, "Pac Opaque");
        }
        // wrapping iv
        else if ((!bIV) && (strcmp(argv[i], "-i") == 0) && (argc > i + 1)) {
            Uint32 ulIVLen = 0;
            BIGNUM *n = NULL;
            bIV =
                readStringArg(argv[i + 1], (char **)&pIV, &ulIVLen, 0, "IV");
            n = BN_new();
            BN_hex2bn(&n, (char *)pIV);
            ulIVLen = BN_num_bytes(n);
            BN_bn2bin(n, pIV);
            BN_free(n);
            if (ulIVLen != 8) {
                printf("\nError: Invalid IV length %d\n", ulIVLen);
                bHelp = TRUE;
            }
        } else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bPacOpaque) {
        printf("\n\tError: pac opaque file (-p) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bWrapperKey64) {
        printf("\n\tError: Unwrapping Key handle (-k) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bIV) {
        printf("\n\tError: Initial Value (-i) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nEap Fast: Pac based authentication");
        printf("\n");
        printf("\nSyntax: eapPacHs -h [-c <Context id>] -p <Pac Opaque file> "
               "-k <key handle> -i <iv> \n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -c  specifies the Context id <optional>");
        printf
            ("\n       -p  Pac opqaue file should contain aes wrapped pac data  of >= 48 bytes");
        printf("\n       -k  AES key handle to unwrap Pac opaque");
        printf("\n       -i  iv value");
        printf("\n");
        return ulRet;
    }

    if (!bContextPtr) {
        ulRet = Cfm2AllocContext(session_handle,
                     OP_BLOCKING, &ulContextPtr, NULL);

        if (ulRet != 0) {
            printf("\n\tAllocContext failed ret %d  %s\n", ulRet,
                   Cfm2ResultAsString(ulRet));
            return ulRet;
        }
    }

    ulRet = Cfm2EapFastPacHs(session_handle, pPacOpaque, ulPacOpaqueLen, 0, 48, /* masterkey len */
                 104, /*Key Material Size */
                 *(Uint64 *) pIV,
                 pClientRandom,
                 pServerRandom,
                 pSessionKeySeed, ulWrapperKey64, ulContextPtr);
    printf("\nSKS (session key seed)");
    if (ulRet == 0) {
        int i = 0;
        for (; i < 40; i++) {
            if (i % 8 == 0)
                printf("\n");
            printf("%02x ", pSessionKeySeed[i]);
        }
        printf("\n");
    }

    printf("\n\tCfm2EapFastPacHs returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    return ulRet;
}
#endif

/****************************************************************************
 *
 * FUNCTION     : genRSAKeyPair
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 genRSAKeyPair(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint64 ulRSAPublicKey64 = 0;
    Uint64 ulRSAPrivateKey64 = 0;
    Uint32 ulRSAPublicKey32 = 0;
    Uint32 ulRSAPrivateKey32 = 0;

    Uint8 bMod = FALSE;
    Uint32 ulModLen = 0;

    Uint8 bID = FALSE;
    char *pID = 0;
    Uint32 ulIDLen = 0;

    Uint8 bLabel = FALSE;
    char *pLabel = 0;
    Uint32 ulLabelLen = 0;
    Uint8 ucKeyLocation = STORAGE_FLASH;

    Uint8 bExp = FALSE;
    Uint32 ulPubExp = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // exp
        else if ((!bExp) && (strcmp(argv[i], "-e") == 0) && (argc > i + 1))
            bExp = readIntegerArg(argv[i + 1], &ulPubExp);

        // Mod
        else if ((!bMod) && (strcmp(argv[i], "-m") == 0) && (argc > i + 1))
            bMod = readIntegerArg(argv[i + 1], &ulModLen);

        // Label
        else if ((!bLabel) && (strcmp(argv[i], "-l") == 0) && (argc > i + 1))
            bLabel = readArgAsString(argv[i + 1], &pLabel, &ulLabelLen);

        // Key ID
        else if ((!bID) && (strcmp(argv[i], "-id") == 0) && (argc > i + 1))
            bID = readArgAsString(argv[i + 1], &pID, &ulIDLen);

        else if (strcmp(argv[i], "-sess") == 0) {
            ucKeyLocation = STORAGE_RAM;
            i--;                //This for loops skips i by 2. so go with it.
        }

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bMod) {
        printf("\n\tError: Modulus size (-m) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bExp) {
        printf("\n\tError: Public exponant (-e) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bLabel) {
        printf("\n\tError: Key label (-l) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bLabel && validateLabel(pLabel, ulLabelLen))
        bHelp = TRUE;

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf
            ("\nGenerate RSA key pair specifying modulus length, public exponent");
        printf("\nand key label.");
        printf("\n");
        printf
            ("\nSyntax: genRSAKeyPair -h -m <modulus length> -e <public exponent> -l <label> [-id <key ID>]\n");
        printf("\n");
        printf("\nWhere: -h    displays this information");
        printf("\n       -m    specifies the modulus length: eg. 2048");
        printf
            ("\n       -e    specifies the public exponent: any odd number typically >= 65537");
        printf("\n       -l    specifies the key label");
        printf("\n       -sess specifies key as session key");
        printf("\n       -id   specifies key ID");
        printf("\n");
        return ulRet;
    }
    ulRet = Cfm2GenerateKeyPair(session_handle,
                    KEY_TYPE_RSA,
                    ulModLen, ulPubExp, 0,
                    0, NULL, 0, NULL,
                    (Uint8 *) pID, ulIDLen,
                    (Uint8 *) pLabel, ulLabelLen,
                    (Uint8 *) pLabel, ulLabelLen,
                    ucKeyLocation,
                    (Uint64 *) & ulRSAPublicKey64,
                    (Uint64 *) & ulRSAPrivateKey64);
    if (ulRet == 0) {
        ulRSAPublicKey32 = (Uint32) ulRSAPublicKey64;
        ulRSAPrivateKey32 = (Uint32) ulRSAPrivateKey64;
        printf
            ("\n\tCfm2RSAGenKeyPair:  public key handle: %d  private key handle: %d\n",
             ulRSAPublicKey32, ulRSAPrivateKey32);
    }
    printf("\n\tCfm2RSAGenKeyPair returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : createPublicKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 createPublicKey(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint64 ulRSAPublicKey64 = 0;
    Uint32 ulRSAPublicKey32 = 0;

    Uint8 bMod = FALSE;
    Uint8 *pModulus = 0;
    Uint32 ulModLen = 0;

    Uint8 bID = FALSE;
    char *pID = 0;
    Uint32 ulIDLen = 0;

    Uint8 bLabel = FALSE;
    char *pLabel = 0;
    Uint32 ulLabelLen = 0;
    Uint8 ucKeyLocation = STORAGE_FLASH;

    Uint8 bExp = FALSE;
    Uint32 ulPubExp = 0;

    Uint8 bName = FALSE;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // exp
        else if ((!bExp) && (strcmp(argv[i], "-e") == 0) && (argc > i + 1))
            bExp = readIntegerArg(argv[i + 1], &ulPubExp);

        // Mod
        else if ((!bMod) && (strcmp(argv[i], "-m") == 0) && (argc > i + 1)) {
            BIGNUM *n = NULL;
            bMod =
                readArgAsString(argv[i + 1], (char **)&pModulus, &ulModLen);
            n = BN_new();
            BN_hex2bn(&n, (char *)pModulus);
            ulModLen = BN_num_bytes(n);
            BN_bn2bin(n, pModulus);
            BN_free(n);
        } else if ((!bName) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1)) {
            bName =
                readStringArg(argv[i + 1], (char **)&pModulus, &ulModLen, 1,
                          "modulus");
        }
        // Label
        else if ((!bLabel) && (strcmp(argv[i], "-l") == 0) && (argc > i + 1))
            bLabel = readArgAsString(argv[i + 1], &pLabel, &ulLabelLen);

        // Key ID
        else if ((!bID) && (strcmp(argv[i], "-id") == 0) && (argc > i + 1))
            bID = readArgAsString(argv[i + 1], &pID, &ulIDLen);

        else if (strcmp(argv[i], "-sess") == 0) {
            ucKeyLocation = STORAGE_RAM;
            i--;                //This for loops skips i by 2. so go with it.
        }

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (bMod && bName) {
        printf
            ("\n\tError: Only Either Modulus (or) Modulus file name is allowed at a time.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !(bMod || bName)) {
        printf("\n\tError: Modulus (-m)/(-f) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bExp) {
        printf("\n\tError: Public exponant (-e) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bLabel) {
        printf("\n\tError: Key label (-l) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bLabel && validateLabel(pLabel, ulLabelLen))
        bHelp = TRUE;

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nCreates RSA public key.");
        printf("\n");
        printf
            ("\nSyntax: createPublicKey -h {-m <modulus> | -f <filename>} -e <exponent> -l <label> [-sess] [-id <key ID>]\n");
        printf("\n");
        printf("\nWhere: -h    displays this information");
        printf("\n       -m    specifies the modulus in hex format");
        printf
            ("\n             ex: when modulus is extracted using \"openssl rsa -in <key file> -modulus\"");
        printf
            ("\n       -f    specifies the file containing modulus in binary");
        printf
            ("\n             ex: when modulus is extracted using \"getAttribute -o <obj handle> -a 288\" of Cfm2Util");
        printf("\n       -e    specifies the exponent: eg. 3");
        printf("\n       -l    specifies the label");
        printf("\n       -sess specifies key as session key");
        printf("\n       -id   specifies key ID");
        printf("\n             Note: For more details on the usage of this");
        printf("\n             command, please refer Cfm2Util example");
        printf("\n             documentation");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2CreatePublicKey(session_handle,
                    KEY_TYPE_RSA,
                    ulModLen,
                    ulPubExp,
                    0, 0, NULL, 0, NULL,
                    pModulus, ulModLen,
                    (Uint8 *) pID, ulIDLen,
                    (Uint8 *) pLabel, ulLabelLen,
                    ucKeyLocation, &ulRSAPublicKey64);

    ulRSAPublicKey32 = (Uint32) ulRSAPublicKey64;

    if (ulRet == 0)
        printf("\n\tPublic Key Created.  Key Handle: %d\n", ulRSAPublicKey32);
    printf("\n\tCfm2CreateRSAPublicKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : importPublicKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 importPublicKey(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint64 ulHandle = 0;

    Uint8 bFile = FALSE;
    char *pFile = 0;
    Uint32 ulFileLen = 0;

    Uint8 bID = FALSE;
    char *pID = 0;
    Uint32 ulIDLen = 0;

    Uint8 bLabel = FALSE;
    char *pLabel = 0;
    Uint32 ulLabelLen = 0;
    Uint8 ucKeyLocation = STORAGE_FLASH;
    Uint32 ulKeyType = 0;

    FILE *fp = NULL;
    char *keyfile = NULL;

    Uint8 *pKeyData = NULL;
    Uint32 ulKeyDataLen = 0;
    Uint8 *pPrime = NULL;
    Uint32 ulPrimeLen = 0;
    Uint8 *pBase = NULL;
    Uint32 ulBaseLen = 0;
    Uint32 ulModLen = 0;
    Uint32 ulPubExp = 0;
    Uint32 ulCurveId = 0;
    const EC_GROUP *group = NULL;
    RSA *rsa = NULL;
    DSA *dsa = NULL;
    EC_KEY *ec_key = NULL;
    EVP_PKEY *pkey = NULL;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0) {
            bHelp = TRUE;
            break;
        }
        // file
        else if ((!bFile) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1)) {
            bFile =
                readStringArg(argv[i + 1], &pFile, &ulFileLen, 1,
                          "public key");
            keyfile = argv[i + 1];

        }
        // Label
        else if ((!bLabel) && (strcmp(argv[i], "-l") == 0) && (argc > i + 1))
            bLabel = readArgAsString(argv[i + 1], &pLabel, &ulLabelLen);

        // Key ID
        else if ((!bID) && (strcmp(argv[i], "-id") == 0) && (argc > i + 1))
            bID = readArgAsString(argv[i + 1], &pID, &ulIDLen);

        else if (strcmp(argv[i], "-sess") == 0) {
            ucKeyLocation = STORAGE_RAM;
            i--;                //This for loops skips i by 2. so go with it.
        }

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bFile) {
        printf("\n\tError: File name (-f) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bLabel) {
        printf("\n\tError: Key label (-l) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bLabel && validateLabel(pLabel, ulLabelLen))
        bHelp = TRUE;

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");

        printf("\nImport a PEM encoded public key onto HSM.");
        printf("\n");
        printf("\nSyntax: importPubKey -h -l <label> -f <filename>  [-sess] [-id <key ID>]\n");
        printf("\n");
        printf("\nWhere: -h    displays this information");
        printf("\n       -l    label for the new key");
        printf("\n       -f    file containing the PEM encoded public key");
        printf("\n       -sess specifies key as session key");
        printf("\n       -id   specifies key ID");
        printf("\n");
        return ulRet;
    }

    fp = fopen(keyfile, "rb");
    if (!fp) {
        ulRet = ERR_INVALID_INPUT;
        printf("unable to openfile\n");
        return ulRet;
    }

    pkey = PEM_read_PUBKEY(fp, NULL, NULL, NULL);
    if (pkey == NULL) {
        ulRet = ERR_INVALID_INPUT;
        printf("\n\t Failed to read Public Key from file %s\n", keyfile);
        goto end;
    }

    ulKeyType = get_key_type(pkey->type);
    if (ulKeyType < 0 || ulKeyType > 3) {
        printf("Error: Invalid Key Type %d\n", ulKeyType);
        ulRet = ERR_INVALID_INPUT;
        goto end;
    }

    switch (ulKeyType) {
    case KEY_TYPE_RSA:
        {
            char *pub_exp = NULL;
            rsa = EVP_PKEY_get1_RSA(pkey);
            if (rsa == NULL) {
                ulRet = ERR_INVALID_INPUT;
                printf("\n\t Failed to get RSA key from input file\n");
                ERR_print_errors_fp(stderr);
                goto end;
            }

            ulKeyDataLen = ulModLen = BN_num_bytes(rsa->n);

            pKeyData = (Uint8 *) malloc(ulKeyDataLen);
            if (pKeyData == NULL)
                goto end;
            memset(pKeyData, 0x00, ulKeyDataLen);

            BN_bn2bin(rsa->n, (Uint8 *) pKeyData);
            pub_exp = BN_bn2dec(rsa->e);
            ulPubExp = atoi(pub_exp);
            if (pub_exp) {
                free(pub_exp);
                pub_exp = NULL;
            }
        }
        break;
    case KEY_TYPE_DSA:
        {
            dsa = EVP_PKEY_get1_DSA(pkey);
            if (dsa == NULL) {
                ulRet = ERR_INVALID_INPUT;
                printf("\n\t Failed to get DSA key from input file\n");
                ERR_print_errors_fp(stderr);
                goto end;
            }
            ulModLen = BN_num_bytes(dsa->p);
            ulKeyDataLen = i2d_DSA_PUBKEY(dsa, &pKeyData);
        }
        break;
    case KEY_TYPE_ECDSA:
        {
            ec_key = EVP_PKEY_get1_EC_KEY(pkey);
            if (ec_key == NULL) {
                ulRet = ERR_INVALID_INPUT;
                printf("\n\t failed to get EC_KEY from input file\n");
                ERR_print_errors_fp(stderr);
                goto end;
            }

            group = EC_KEY_get0_group(ec_key);
            ulCurveId = EC_GROUP_get_curve_name(group);

            ulKeyDataLen =
                EC_POINT_point2oct(group, EC_KEY_get0_public_key(ec_key),
                           POINT_CONVERSION_UNCOMPRESSED, NULL, 0,
                           NULL);
            pKeyData = (Uint8 *) malloc(ulKeyDataLen);
            if (pKeyData == NULL) {
                printf("malloc failure \n");
                goto end;
            }
            EC_POINT_point2oct(group, EC_KEY_get0_public_key(ec_key),
                       POINT_CONVERSION_UNCOMPRESSED,
                       pKeyData, ulKeyDataLen, NULL);
        }
        break;
    default:
        printf("Error: Invalid Key Type %d\n", ulKeyType);
        ulRet = ERR_INVALID_INPUT;
        goto end;
        break;
    }

    ulRet = Cfm2CreatePublicKey(session_handle,
                    ulKeyType, ulModLen,
                    ulPubExp, ulCurveId,
                    ulPrimeLen, pPrime,
                    ulBaseLen, pBase,
                    pKeyData, ulKeyDataLen,
                    (Uint8 *) pID, ulIDLen,
                    (Uint8 *) pLabel, ulLabelLen,
                    ucKeyLocation, &ulHandle);
    if (ulRet == 0) {
        printf("\nPublic Key Handle: %llu \n", ulHandle);
    }

end:
    printf("\n\tCfm2CreatePublicKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    if (fp)
        fclose(fp);
    if (rsa)
        RSA_free(rsa);
    if (dsa)
        DSA_free(dsa);
    if (ec_key)
        EC_KEY_free(ec_key);
    if (pKeyData)
        free(pKeyData);
    if (pkey)
        EVP_PKEY_free(pkey);
    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : exportPublicKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 exportPublicKey(int argc, char **argv)
{
    Uint32 ulRet = -1;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bKey = FALSE;
    Uint32 ulKeyHandle = 0;

    Uint8 bFile = FALSE;
    char *KeyFile = NULL;

    Uint32 ulModLen = 0;
    Uint32 ulKeyType = 0;
    Uint8 *pbKeyBuf = NULL;
    Uint32 ulDataLen = 0;
    FILE *fd = NULL;
    RSA *rsa = NULL;
    DSA *dsa = NULL;
    EC_KEY *eckey = NULL;
    EC_GROUP *group = NULL;
    EC_POINT *pub_key = NULL;
    Uint32 ulCurveId = 0;
    Uint32 ulAttrLen = 0;
    Uint8 pAttr[4] = { 0 };

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // Key Handle
        else if ((!bKey) && (strcmp(argv[i], "-k") == 0) && (argc > i + 1))
            bKey = readIntegerArg(argv[i + 1], &ulKeyHandle);

        else if ((!bFile) && (strcmp(argv[i], "-out") == 0) && (argc > i + 1)) {
            KeyFile = argv[i+1];
            bFile = 1;
        }
        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bKey) {
        printf("\n\tError: Key handle (-k) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bFile) {
        printf("\n\tError: Key File (-out) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nExport a public key in PEM encoded format.");
        printf("\n");
        printf("\nSyntax: exportPubKey -h -k <key handle> -out <key file>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -k  specifies the public key handle");
        printf("\n       -out  specifies the file to write the exported public key");
        printf("\n");
        return ulRet;
    }

    ulDataLen = 1024;
    pbKeyBuf = (Uint8 *) calloc(ulDataLen, 1);
    ulRet = Cfm2ExportPublicKey(session_handle,
                    ulKeyHandle, pbKeyBuf, &ulDataLen);
    if (ulRet == RET_RESULT_SIZE) {
        printf("RET_RESULT_SIZE\n");
        pbKeyBuf = (Uint8 *) realloc(pbKeyBuf, ulDataLen);
        if (pbKeyBuf) {
            ulRet = Cfm2ExportPublicKey(session_handle,
                            ulKeyHandle, pbKeyBuf, &ulDataLen);
        } else {
            printf("\n\tError: Memory allocation errror.\n");
            ulRet = ERR_MEMORY_ALLOC_FAILURE;
        }
    }
    if (ulRet == RET_OK) {
        fd = fopen(KeyFile, "w");
        if (!fd){
	    printf("\n\tFailed to open or create output file %s\n",KeyFile);
	    ulRet = ERR_OPEN_FILE;
	    goto err;
        }
        ulAttrLen = sizeof(pAttr);
        ulRet = Cfm2GetAttribute_new(session_handle,
                     ulKeyHandle,
                     OBJ_ATTR_KEY_TYPE, pAttr, &ulAttrLen);
        if (ulRet) {
            printf("Cfm2GetAttribute to get key type failed %d : %s \n",
                   ulRet, Cfm2ResultAsString(ulRet));
            goto err;
        }
        ulKeyType = atoi((Int8 *) pAttr);

        switch (ulKeyType) {
        case KEY_TYPE_RSA:
            {
                ulModLen = ulDataLen / 2;
                rsa = RSA_new();
                if (rsa == NULL) {
                    printf("Failed to create RSA Key \n");
                    goto err;
                }

                if (!rsa->n && ((rsa->n = BN_new()) == NULL))
                    goto err;
                if (!rsa->e && ((rsa->e = BN_new()) == NULL))
                    goto err;

                /* Modulus */
                if (!BN_bin2bn(pbKeyBuf, ulModLen, rsa->n))
                    goto err;

                /* Public  Exponent */
                if (!BN_bin2bn(pbKeyBuf + (ulDataLen - 4), 4, rsa->e))
                    goto err;

                if (!PEM_write_RSA_PUBKEY(fd, rsa))
                    goto err;
            }
            break;
        case KEY_TYPE_DSA:
            {
                const unsigned char *key_buf = pbKeyBuf;

                dsa = d2i_DSA_PUBKEY(&dsa, &key_buf, ulDataLen);
                if (dsa == NULL) {
                    printf
                        ("Failed to convert into dsa: d2i_DSA_PUBKEY failed \n");
                    goto err;
                }

                if (!PEM_write_DSA_PUBKEY(fd, dsa))
                    goto err;
            }
            break;
        case KEY_TYPE_ECDSA:
            {
                const unsigned char *key_buf = pbKeyBuf;

                ulAttrLen = sizeof(pAttr);
                ulRet = Cfm2GetAttribute_new(session_handle,
                             ulKeyHandle,
                             OBJ_ATTR_MODULUS_BITS,
                             pAttr, &ulAttrLen);
                if (ulRet) {
                    printf("Cfm2GetAttribute failed %d : %s \n", ulRet,
                           Cfm2ResultAsString(ulRet));
                    goto err;
                }
                ulCurveId = atoi((Int8 *) pAttr);
                group = EC_GROUP_new_by_curve_name(ulCurveId);
                if (group == NULL) {
                    printf("unable to create curve id %d\n", ulCurveId);
                    goto err;
                }

                EC_GROUP_set_asn1_flag(group, OPENSSL_EC_NAMED_CURVE);
                EC_GROUP_set_point_conversion_form(group,
                                   POINT_CONVERSION_UNCOMPRESSED);

                eckey = EC_KEY_new();
                if (eckey == NULL) {
                    printf("Failed to create ec key\n");
                    goto err;
                }

                if (EC_KEY_set_group(eckey, group) == 0) {
                    printf("unable to set the group (%d)\n", ulCurveId);
                    goto err;
                }
                pub_key = EC_POINT_new(EC_KEY_get0_group(eckey));
                if (pub_key == NULL)
                    goto err;

                EC_POINT_oct2point(group, pub_key, key_buf, ulDataLen, NULL);
                EC_KEY_set_public_key(eckey, (const EC_POINT *)pub_key);

                if (!PEM_write_EC_PUBKEY(fd, eckey))
                    goto err;
            }
            break;
        default:
            printf("\n\tError: Invalid Key Type: 0x%02x\n", ulKeyType);
            ulRet = ERR_INVALID_INPUT;
            goto err;
        }
        printf("\nPEM formatted public key is written to %s\n", KeyFile);
    }

err:
    printf("\n\tCfm2ExportPubKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    if (fd)
        fclose(fd);
    if (rsa)
        RSA_free(rsa);
    if (dsa)
        DSA_free(dsa);
    if (eckey)
        EC_KEY_free(eckey);
    if (group)
        EC_GROUP_free(group);
    if (pub_key)
        EC_POINT_free(pub_key);
    if (pbKeyBuf)
        free(pbKeyBuf);
    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : GeneratePEK
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 generatePEK(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint8 mk[64] = { };

    HSMInfo hsm_info;

    Uint8 nonce_in[HOST_CLONE_NONCE_LEN] = { 0 };
    Uint8 nonce_out[512] = { 0 };
    Uint32 nonce_out_len = 512;
    EVP_PKEY  *pri_key  = NULL;
    Uint8     pmk[512] = {0};
    int       pmk_len   = 512;
    char file[256] = { };
    char priv_key_file[256] = { };
    char pub_key_file[256] = { };

    if (argc > 2) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\ngeneratePEK.");
        printf("\n");
        printf("\nSyntax: generatePEK -h \n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2GetHSMInfo2(session_handle, &hsm_info);
    if (ulRet) {
        printf("\n\t Error: Cfm2GetHSMInfo2 return %d %s \n",
               ulRet, Cfm2ResultAsString(ulRet));
        return ulRet;
    }

    /* generate nonce(N) */
    if (RAND_bytes(nonce_in, HOST_CLONE_NONCE_LEN) <= 0) {
        printf("\n\tError: generating rand bytes %d failed.\n",
               HOST_CLONE_NONCE_LEN);
        return ulRet;
    }

    sprintf(priv_key_file, "%s/nfbe%d/%s_%d", BUILD_DIR, dev_id,
        PEK_PRIVATE_KEY, PARTITION_INDEX(session_handle));
    sprintf(pub_key_file, "%s/nfbe%d/%s_%d", BUILD_DIR, dev_id,
        PEK_PUBLIC_KEY, PARTITION_INDEX(session_handle));

    ulRet = generate_kek_rsa_key_pair(RSA_MOD_SIZE_FOR_PEK,
                      priv_key_file, pub_key_file);

    if (ulRet != RET_OK) {
        printf("\n\t Error: generate rsa key pair failed \n");
        return ulRet;
    }

    ulRet = Cfm2GenPswdEncKey(session_handle,
                  nonce_in,
                  &nonce_out_len, (Uint8 *) nonce_out);

    printf("\n\tCfm2GenPswdEncKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    if (ulRet == 0) {
        /* compute master key */
        /* read the private key from the file */
        pri_key = load_pkey(priv_key_file,
                    KEY_FORMAT_PEM,
                    PRIVATE_KEY);
        if (pri_key == NULL)
        {
            printf("Failed to get PEK private key from file %s \n",priv_key_file);
            return ulRet;
        }

        pmk_len = RSA_private_decrypt(nonce_out_len,
                          nonce_out,
                          pmk,
                          pri_key->pkey.rsa,
                          RSA_NO_PADDING);
        if (pmk_len < 0) /* returns -1 on error */
        {
            printf("Failed to decrypt HSM nonce using PEK private key \n");
            EVP_PKEY_free(pri_key);
            return ulRet;
        }
        EVP_PKEY_free(pri_key);
        /* do KDF with the nonce and hsm nonce */
        memcpy(nonce_out, pmk, pmk_len);

        KDF(pmk, pmk_len, (Uint8 *) hsm_info.label, HOST_CLONE_NONCE_LEN, nonce_in,
            pmk_len, nonce_out, NULL, NULL, (Uint8 *)hsm_info.serialNumber, mk);

        sprintf(file, "%s/nfbe%d/%s_%d", BUILD_DIR, dev_id, PEK_FILE,
            PARTITION_INDEX(session_handle));
        ulRet = write_file(file, mk, 32);
        if(ulRet == 0)
            printf("\n\n\tPEK is written to the file: %s\n", file);
        else
            printf("\n\n\tFailed to write PEK to the file: %s\n", file);
    }

    unlink(pub_key_file);
    unlink(priv_key_file);
#ifdef CLOUD_HSM_CLIENT
    if(NULL != argv)
    {
        uint32_t msg_type = SEND_PEK_TO_SERVER;
        if(RET_OK != cvm_cloudhsm_cli_send_daemon(NULL, HSM_CAV_SERVER_OPERATION, OP_BLOCKING, 0, NULL, (void*)&msg_type))
        {
            printf("Could not sent the new PEK to server\n");
        }
    }
#endif

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : GenerateKEK
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 generateKEK(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 kek_method = 0;
    Uint8 mk[64] = { };

    HSMInfo hsm_info;

    Uint8 nonce_in[HOST_CLONE_NONCE_LEN] = { 0 };
    Uint8 nonce_out[512] = { 0 };
    Uint32 nonce_out_len = 512;
    char file[256] = { };
    char priv_key_file[256] = { };
    char pub_key_file[256] = { };
    char hsm_pub_key_file[256] = { };

    if (argc > 2) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\ngenerateKEK.");
        printf("\n");
        printf("\nSyntax: generateKEK -h \n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2GetHSMInfo2(session_handle, &hsm_info);
    if (ulRet) {
        printf("\n\t Error: Cfm2GetHSMInfo2 return %d %s \n",
               ulRet, Cfm2ResultAsString(ulRet));
        return ulRet;
    }

    kek_method = betoh32(hsm_info.uiKekMethod);

    if ((kek_method != KEK_TYPE_ECC) && (kek_method != KEK_TYPE_RSA)) {
        printf("\n\tError: KEK generation method is invalid %d.\n",
               kek_method);
        return ulRet;
    }

    /* generate nonce(N) */
    if (RAND_bytes(nonce_in, HOST_CLONE_NONCE_LEN) <= 0) {
        printf("\n\tError: generating rand bytes %d failed.\n",
               HOST_CLONE_NONCE_LEN);
        return ulRet;
    }

    printf("\n\tGenerating Kek using %s\n", (kek_method ? "RSA" : "ECC"));

    sprintf(hsm_pub_key_file, "%s/nfbe%d/%s_%d", BUILD_DIR, dev_id,
        KEK_HSM_PUBLIC_KEY, PARTITION_INDEX(session_handle));
    sprintf(priv_key_file, "%s/nfbe%d/%s_%d", BUILD_DIR, dev_id,
        KEK_PRIVATE_KEY, PARTITION_INDEX(session_handle));
    sprintf(pub_key_file, "%s/nfbe%d/%s_%d", BUILD_DIR, dev_id,
        KEK_PUBLIC_KEY, PARTITION_INDEX(session_handle));
    if (kek_method == KEK_TYPE_ECC) {
        ulRet = generate_kek_ecc_key_pair(NIST_ECC_CURVE_NAME,
                          priv_key_file, pub_key_file);
    } else {
        ulRet = generate_kek_rsa_key_pair(RSA_MOD_SIZE_FOR_KEK,
                          priv_key_file, pub_key_file);
    }

    if (ulRet != RET_OK) {
        printf("\n\t Error: generate %s key pair failed \n",
               kek_method ? "rsa" : "ecc");
        return ulRet;
    }

    ulRet = Cfm2GenKeyEncKeyNew(session_handle,
                    kek_method,
                    nonce_in,
                    &nonce_out_len, (Uint8 *) nonce_out);

    printf("\n\tCfm2GenKeyEncKeyNew returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    if (ulRet == 0) {
        /* compute master key */
        ulRet = compute_master_key(dev_id,
                       session_handle,
                       kek_method,
                       nonce_in,
                       nonce_out,
                       nonce_out_len,
                       (Uint8 *) hsm_info.label, (Uint8 *)hsm_info.serialNumber, mk);
        if (ulRet == RET_OK) {
            sprintf(file, "%s/nfbe%d/%s_%d", BUILD_DIR, dev_id, KEK_FILE,
                PARTITION_INDEX(session_handle));
            ulRet = write_file(file, mk, 32);
            printf("\n\n\tKEK is written to the file: %s\n", file);
#ifdef CLOUD_HSM_CLIENT
            {
                uint32_t msg_type = SEND_KEK_TO_SERVER;

                if (RET_OK != cvm_cloudhsm_cli_send_daemon(NULL, HSM_CAV_SERVER_OPERATION, OP_BLOCKING, 0, NULL, (void*)&msg_type))
                {
                    printf("Could not sent the new KEK to server\n");
                }
            }
#endif
        } else
            print_error("Failed to compute master key\n");
    }
    unlink(pub_key_file);
    unlink(priv_key_file);
    unlink(hsm_pub_key_file);

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : exportPrivateKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 exportPrivateKey(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bWrappingKey = FALSE;
    Uint32 ulWrappingKey = 0;

    Uint8 bKey = FALSE;
    Uint32 ulKey = 0;

    Uint8 bFile = FALSE;
    char *KeyFile = NULL;

    Uint32 ulMech = CRYPTO_MECH_AES_KEY_WRAP;

    Uint8 randomIV[16] = {};
    Uint8 *pIV = &randomIV[0];

    Uint8 *pKey = NULL;
    Uint32 ulKeyLen = 0;
    Uint32 attr_len = 0;
    Uint32 ulKeyClass = OBJ_CLASS_PRIVATE_KEY;
    BIO *mem_bio = BIO_new(BIO_s_mem());
    BIO *file_bio = BIO_new(BIO_s_file());
    EVP_PKEY *pkey = NULL;
    PKCS8_PRIV_KEY_INFO *p8inf = NULL;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // wrapping key
        else if ((!bWrappingKey) && (strcmp(argv[i], "-w") == 0)
             && (argc > i + 1))
            bWrappingKey = readIntegerArg(argv[i + 1], &ulWrappingKey);

        //  key
        else if ((!bKey) && (strcmp(argv[i], "-k") == 0) && (argc > i + 1))
            bKey = readIntegerArg(argv[i + 1], &ulKey);
        else if ((strcmp(argv[i], "-m") == 0) && (argc > i + 1)) {
            readIntegerArg(argv[i + 1], (Uint32 *) & ulMech);
            ulMech = get_wrap_mechanism(ulMech);
        }
        else if ((!bFile) && (strcmp(argv[i], "-out") == 0) && (argc > i + 1)) {
            KeyFile = argv[i+1];
            bFile = 1;
        }
        else
            bHelp = TRUE;

    }

    // ensure that we have all the required args
    if (!bHelp && !bKey) {
        printf("\n\tError: Key handle (-k) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bWrappingKey) {
        printf("\n\tError: Wrapping key handle (-w) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bFile) {
        printf("\n\tError: Key File (-out) is missing.\n");
        bHelp = TRUE;
    }
    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nExport a private key.");
        printf("\n");
        printf
            ("\nSyntax: exportPrivateKey -h -k <key handle> -w <wrapping key handle> -out <key file>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -k  specifies the private key handle to export");
        printf("\n       -w  specifies the wrapping key handle");
        printf("\n       -m  specifies the wrapping mechanism");
        printf("\n               NIST_AES_WRAP - 4");
        printf("\n       -out  specifies the file to write the exported private key");
        printf("\n");
        return ulRet;
    }

    attr_len = 4;
    if ((ulRet = Cfm2GetAttribute_new(session_handle,
                      ulKey,
                      OBJ_ATTR_CLASS,
                      (Uint8 *) & ulKeyClass, &attr_len)) == 0) {
        ulKeyClass = atoi((Int8 *) & ulKeyClass);
        if (ulKeyClass != OBJ_CLASS_PRIVATE_KEY) {
            printf("\n\t%d is not a private key\n", (int)ulKey);
            ulRet = ERR_INVALID_INPUT;
            goto err;
        }
    }

    RAND_bytes(pIV, 16);

    // Retrieve Size of BER Encoded Public Key Blob
    if ((ulRet = Cfm2ExportKey(session_handle,
                   pIV, ulMech,
                   ulWrappingKey, ulKey, NULL, &ulKeyLen)) == 0) {
        // Allocate BER Encoded Public Key Blob
        pKey = (Uint8 *) calloc(ulKeyLen, 1);
        if (pKey) {
            // Export private Key
            if ((ulRet = Cfm2ExportKey(session_handle,
                           pIV, ulMech,
                           ulWrappingKey, ulKey,
                           pKey, &ulKeyLen)) == 0) {
                if (CRYPTO_MECH_AES_KEY_WRAP != ulMech) {
                    printf("Writing the key of length %d in wrapped form\n",
                           ulKeyLen);
                    if (WriteBinaryFile(KeyFile, (char *)pKey, ulKeyLen))
                        printf
                            ("\n\nWrapped Symmetric Key written to file \"%s\"\n", KeyFile);
                    else
                        ulRet = ERR_WRITE_OUTPUT_FILE;
                } else {
                    // write to a file
                    if (BIO_write(mem_bio, (char *)pKey, ulKeyLen) <= 0) {
                        printf("Couldn't write to mem_bio\n");
                        ulRet = ERR_GENERAL_ERROR;
                        goto err;
                    }

                    p8inf = d2i_PKCS8_PRIV_KEY_INFO_bio(mem_bio, NULL);
                    if (p8inf == 0) {
                        printf("failed to convert to pkcs8 format\n\n");
                        ulRet = ERR_GENERAL_ERROR;
                        goto err;
                    }

                    pkey = EVP_PKCS82PKEY(p8inf);
                    if (p8inf == 0) {
                        printf("failed to convert to pkcs8 format\n\n");
                        ulRet = ERR_GENERAL_ERROR;
                        goto err;
                    }

                    if (BIO_write_filename(file_bio, KeyFile) <= 0) {
                        printf("failed to open file bio\n\n");
                        goto err;
                    }

                    if (!PEM_write_bio_PrivateKey
                        (file_bio, pkey, NULL, NULL, 0, NULL, NULL)) {
                        printf("failed to write to file\n\n");
                        ulRet = ERR_GENERAL_ERROR;
                    }
                }
            }
        } else {
            printf("\n\tError: Memory allocation errror.\n");
            ulRet = ERR_MEMORY_ALLOC_FAILURE;
        }
    }

err:
    printf("\n\tCfm2ExportKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    if (pKey)
        free(pKey);
    if (pkey)
        EVP_PKEY_free(pkey);
    if (mem_bio)
        BIO_free(mem_bio);
    if (file_bio)
        BIO_free(file_bio);

    return ulRet;
}

int get_kek_from_file(char *file, Uint8* kek)
{
    FILE *fptr = NULL;
    fptr = fopen(file, "r");
    if (fptr == NULL)
    {
        printf("Unable to open file %s. Returning..\n", KEK_FILE);
        return -1;
    }
    if (fread(kek, 32, 1, fptr) == 0)
    {
        printf("Failed to read from file: %s\n", KEK_FILE);
        return -1;
    }
    fclose(fptr);
    return 0;
}

Uint8 wrap_with_kek(Uint8 *pKey,Uint32 ulKeyLen,Uint8 *pIV,Uint8 *wrappedKey,Uint32 *wrappedKeyLen)
{
    char file[200];
    Uint8 kek[32];
    Uint8 cond_code = 0;
    sprintf(file, "%s/nfbe%d/%s_%d", BUILD_DIR, dev_id, KEK_FILE,
        PARTITION_INDEX(session_handle));

    cond_code = get_kek_from_file(file,kek);
    if(cond_code != 0)
        return cond_code;

    cond_code = KEK_key_wrap(kek,pKey,ulKeyLen,pIV,wrappedKey,wrappedKeyLen);
    if(cond_code)
        printf("kek wrap key failed\n");
    else
        printf("kek wrap key success\n");

    return cond_code;


}
// It fetch the kek file, extracts key, unwrap the input with kek

Uint8 unwrap_with_kek(Uint8 *wrappedKey,Uint32 wrappedKeyLen,Uint8 *pIV,Uint8 *unwrappedKey,Uint32 *unwrappedKeyLen)
{
    char file[200];
    Uint8 kek[32];
    Uint8 cond_code = 0;
    sprintf(file, "%s/nfbe%d/%s_%d", BUILD_DIR, dev_id, KEK_FILE,
        PARTITION_INDEX(session_handle));

    cond_code = get_kek_from_file(file,kek);
    if(cond_code != 0)
        return cond_code;
    cond_code = KEK_key_unwrap(kek,wrappedKey,wrappedKeyLen,pIV,unwrappedKey,unwrappedKeyLen);
    if(cond_code)
        printf("kek unwrap key failed\n");
    else
        printf("kek unwrap key success\n");

    return cond_code;
}


/****************************************************************************
 *
 * FUNCTION     : importPrivateKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 importPrivateKey(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;
    Uint32 ulModLen = 0;
    Uint32 ulCrtLen = 0;

    Uint8 bWrappingKeyHandle = FALSE;
    Uint32 ulWrappingKeyHandle = 0;

    Uint8 bFile = FALSE;
    char *pFile = 0;

    Uint8 bID = FALSE;
    char *pID = 0;
    Uint32 ulIDLen = 0;

    Uint8 bLabel = FALSE;
    char *pLabel = 0;
    Uint32 ulLabelLen = 0;
    Uint8 ucKeyLocation = STORAGE_FLASH;

    Uint64 ulNewKey64 = 0;
    Uint32 ulNewKey32 = 0;

    Uint32 ulKeyType = 0;

    FILE *fp = NULL;
    EVP_PKEY *pkey = NULL;
    BIO *mem_bio = BIO_new(BIO_s_mem());
    Uint8 *pKey = NULL;
    Uint32 ulKeyLen = 0;
    Uint32 ulPubExp = 0;
    Uint32 ulCurveId = 0;
    Uint8 randomIV[16] = {};
    Uint8 *pIV = &randomIV[0];

    Uint32 ulMech = CRYPTO_MECH_AES_KEY_WRAP;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // wrapping key handle
        else if ((strcmp(argv[i], "-w") == 0) && (argc > i + 1))
            bWrappingKeyHandle =
                readIntegerArg(argv[i + 1], &ulWrappingKeyHandle);
        // private key file
        else if ((!bFile) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1)) {
            pFile = argv[i + 1];
            bFile = TRUE;
        }
        // Label
        else if ((!bLabel) && (strcmp(argv[i], "-l") == 0) && (argc > i + 1))
            bLabel = readArgAsString(argv[i + 1], &pLabel, &ulLabelLen);

        // Key ID
        else if ((!bID) && (strcmp(argv[i], "-id") == 0) && (argc > i + 1))
            bID = readArgAsString(argv[i + 1], &pID, &ulIDLen);

        else if (strcmp(argv[i], "-sess") == 0) {
            ucKeyLocation = STORAGE_RAM;
            i--;                //This for loops skips i by 2. so go with it.
        }

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bWrappingKeyHandle) {
        printf("\n\tError: wrapping Key handle (-w) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bFile) {
        printf("\n\tError: File name (-f) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bLabel) {
        printf("\n\tError: Key label (-l) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bLabel && validateLabel(pLabel, ulLabelLen))
        bHelp = TRUE;

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nImports RSA/DSA/EC Private Key.");
        printf("\n");
        printf
            ("\nSyntax: importPrivateKey -h -l <label> -f <private key file name> -w <wrapper key handle> [-sess] [-id <key ID>]\n");
        printf("\n");
        printf("\nWhere: -h    displays this information");
        printf("\n       -l    specifies the private key label");
        printf
            ("\n       -f    specifies the filename containing the key to import");
        printf
            ("\n       -w    specifies the wrapping key handle (KEK handle - 4)");
        printf("\n       -sess specifies key as session key");
        printf("\n       -id   specifies key ID");
        printf("\n");
        return ulRet;
    }

    fp = fopen(pFile, "rb");
    if (!fp) {
        printf("failed to open file  %s : %s\n", pFile, strerror(errno));
        ulRet = ERR_OPEN_FILE;
        goto err;
    }

    pkey = PEM_read_PrivateKey(fp, NULL, NULL, NULL);
    if (pkey == NULL) {
        printf("failed to read private key from file\n\n");
        ulRet = ERR_INVALID_INPUT;
        goto err;
    }

    ulKeyType = get_key_type(pkey->type);
    if (ulKeyType < 0 || ulKeyType > 3) {
        printf("Error: Invalid Key Type %d\n", ulKeyType);
        ulRet = ERR_INVALID_INPUT;
        goto err;
    }

    ulRet = i2d_PKCS8PrivateKeyInfo_bio(mem_bio, pkey);
    if (ulRet == 0) {
        printf("failed to convert to pkcs8 format\n\n");
        ulRet = ERR_INVALID_USER_INPUT;
        goto err;
    }

    pKey = (Uint8 *) malloc(BUFSIZE);
    if (pKey == NULL) {
        printf("malloc failure \n");
        ulRet = ERR_MEMORY_ALLOC_FAILURE;
        goto err;
    }

    ulKeyLen = BIO_read(mem_bio, (char *)pKey, BUFSIZE);
    if (ulKeyLen == 0) {
        printf("Couldn't read from the mem_bio\n");
        ulRet = ERR_INVALID_USER_INPUT;
        goto err;
    }

    switch (ulKeyType) {
    case KEY_TYPE_RSA:
        {
            RSA *rsa = NULL;
            char *pub_exp = NULL;
            Uint8 *priv_exp = NULL;
            rsa = EVP_PKEY_get1_RSA(pkey);
            if (rsa == NULL) {
                printf("failed to read rsa private key from file\n\n");
                ulRet = ERR_INVALID_USER_INPUT;
                goto err;
            }
            priv_exp = (Uint8 *) OPENSSL_malloc(BN_num_bytes(rsa->d));
            if (priv_exp == NULL) {
                printf(" memory alloc failure \n");
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                goto err;
            }

            BN_bn2bin(rsa->d, priv_exp);

            if ((*(Uint32 *) priv_exp == CAV_SIG_IMPORTED_KEY) ||
                (*(Uint32 *) priv_exp == CAV_SIG_HSM_KEY)) {
                printf("\nThis key is already imported with handle %lld\n\n", *(Uint64 *)&priv_exp[8]);
                ulRet = ERR_INVALID_USER_INPUT;
                if (priv_exp) {
                    free(priv_exp);
                    priv_exp = NULL;
                }
                goto err;
            }
            if (priv_exp) {
                free(priv_exp);
                priv_exp = NULL;
            }

            pub_exp = BN_bn2dec(rsa->e);
            ulPubExp = atoi(pub_exp);
            if (pub_exp) {
                free(pub_exp);
                pub_exp = NULL;
            }
            ulCrtLen = (BN_num_bytes(rsa->n) * 5) / 2;
            ulModLen = BN_num_bytes(rsa->n);
        }
        break;
    case KEY_TYPE_DSA:
        {
            DSA *dsa = NULL;
            dsa = EVP_PKEY_get1_DSA(pkey);
            if (dsa == NULL) {
                printf("failed to read dsa private key from file\n\n");
                ulRet = ERR_INVALID_USER_INPUT;
                goto err;
            }
            ulModLen = BN_num_bytes(dsa->p);
        }
        break;
    case KEY_TYPE_ECDSA:
        {
            EC_KEY *ec_key = NULL;
            ec_key = EVP_PKEY_get1_EC_KEY(pkey);
            if (!ec_key) {
                printf("\n \tfailed to read ecdsa private key from file\n");
                ulRet = ERR_INVALID_USER_INPUT;
                goto err;
            }
            ulCurveId = EC_GROUP_get_curve_name(EC_KEY_get0_group(ec_key));
        }
        break;
    default:
        ulRet = ERR_INVALID_USER_INPUT;
        goto err;
    }
    printf("BER encoded key length is %d\n", ulKeyLen);

    RAND_bytes(pIV, 16);
    if(ulWrappingKeyHandle == 4)
    {
        int ret = 0;
        Uint32 wrappedKeyLen;
        Uint8 * wrappedKey;
        Uint8 IV[8] = {0xa6,0xa6,0xa6,0xa6,0xa6,0xa6,0xa6,0xa6};
        wrappedKeyLen = ulKeyLen + 32 ;
        wrappedKey = calloc(wrappedKeyLen, 1);

        memcpy(pIV,IV,8);

        ret = wrap_with_kek(pKey,ulKeyLen,IV, wrappedKey, &wrappedKeyLen);
        if(ret != 0)
        {
            if(wrappedKey)
                free(wrappedKey);
            goto err;
        }
        ulRet = Cfm2ImportKey3(session_handle,
                       ulKeyType,
                       PLAIN,
                       pKey,
                       ulKeyLen,
                       ulCrtLen,
                       ulPubExp,
                       ulModLen * 8,
                       ulCurveId,
                       (Uint8 *) pID, ulIDLen,
                       (Uint8 *) pLabel, ulLabelLen,
                       pIV,
                       ulMech,
                       ucKeyLocation, ulWrappingKeyHandle, &ulNewKey64,wrappedKey,wrappedKeyLen);

        ulNewKey32 = (Uint32) ulNewKey64;
        if (ulRet == 0) {
            printf("\n\tPrivate Key Unwrapped.  Key Handle: %d \n", ulNewKey32);
        }
        goto err;
    }


    ulRet = Cfm2ImportKey(session_handle,
                  ulKeyType,
                  PLAIN,
                  pKey,
                  ulKeyLen,
                  ulCrtLen,
                  ulPubExp,
                  ulModLen * 8,
                  ulCurveId,
                  (Uint8 *) pID, ulIDLen,
                  (Uint8 *) pLabel, ulLabelLen,
                  pIV,
                  ulMech,
                  ucKeyLocation, ulWrappingKeyHandle, &ulNewKey64);

    ulNewKey32 = (Uint32) ulNewKey64;
    if (ulRet == 0) {
        printf("\n\tPrivate Key Unwrapped.  Key Handle: %d \n", ulNewKey32);
    }
err:
    printf("\n\tCfm2ImportKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    if (fp)
        fclose(fp);
    if (pKey)
        free(pKey);
    if (pkey)
        EVP_PKEY_free(pkey);
    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : importRawRSAPrivateKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 importRawRSAPrivateKey(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;
    Uint32 ulModLen = 0;
    Uint32 ulCrtLen = 0;
    Uint32 ulHalfModLen = 0;

    Uint8 bWrappingKeyHandle = FALSE;
    Uint32 ulWrappingKeyHandle = 0;

    Uint8 bFile = FALSE;
    char *pFile = 0;

    Uint8 bID = FALSE;
    char *pID = 0;
    Uint32 ulIDLen = 0;

    Uint8 bLabel = FALSE;
    char *pLabel = 0;
    Uint32 ulLabelLen = 0;
    Uint8 ucKeyLocation = STORAGE_FLASH;

    Uint64 ulNewKey64 = 0;
    Uint32 ulNewKey32 = 0;

    Uint32 ulKeyType = 0;

    FILE *fp = NULL;
    EVP_PKEY *pkey = NULL;
    RSA *rsa = NULL;
    char *pub_exp = NULL;

    Uint8 *pKey = NULL;
    Uint32 ulKeyLen = 0;
    Uint32 ulPubExp = 0;

    Uint32 ulCurveId = 0;
    Uint8 randomIV[16] = {};
    Uint8 *pIV = &randomIV[0];

    Uint32 ulMech = CRYPTO_MECH_AES_KEY_WRAP;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // wrapping key handle
        else if ((strcmp(argv[i], "-w") == 0) && (argc > i + 1))
            bWrappingKeyHandle =
                readIntegerArg(argv[i + 1], &ulWrappingKeyHandle);
        // private key file
        else if ((!bFile) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1)) {
            pFile = argv[i + 1];
            bFile = TRUE;
        }
        // Label
        else if ((!bLabel) && (strcmp(argv[i], "-l") == 0) && (argc > i + 1))
            bLabel = readArgAsString(argv[i + 1], &pLabel, &ulLabelLen);

        // Key ID
        else if ((!bID) && (strcmp(argv[i], "-id") == 0) && (argc > i + 1))
            bID = readArgAsString(argv[i + 1], &pID, &ulIDLen);

        else if (strcmp(argv[i], "-sess") == 0) {
            ucKeyLocation = STORAGE_RAM;
            i--;                //This for loops skips i by 2. so go with it.
        }

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bWrappingKeyHandle) {
        printf("\n\tError: wrapping Key handle (-w) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bFile) {
        printf("\n\tError: File name (-f) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bLabel) {
        printf("\n\tError: Key label (-l) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bLabel && validateLabel(pLabel, ulLabelLen))
        bHelp = TRUE;

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nImports RSA Private Key.");
        printf("\n");
        printf
            ("\nSyntax: importRawRSAPrivateKey -h -l <label> -f <private key file name> -w <wrapper key handle> [-sess] [-id <key ID>]\n");
        printf("\n");
        printf("\nWhere: -h    displays this information");
        printf("\n       -l    specifies the private key label");
        printf
            ("\n       -f    specifies the filename containing the key to import");
        printf
            ("\n       -w    specifies the wrapping key handle (KEK handle - 4)");
        printf("\n       -sess specifies key as session key");
        printf("\n       -id   specifies key ID");
        printf("\n");
        return ulRet;
    }

    fp = fopen(pFile, "rb");
    if (!fp) {
        printf("failed to open file  %s : %s\n", pFile, strerror(errno));
        ulRet = ERR_OPEN_FILE;
        goto err;
    }

    pkey = PEM_read_PrivateKey(fp, NULL, NULL, NULL);
    if (pkey == NULL) {
        printf("failed to read private key from file\n\n");
        ulRet = ERR_INVALID_INPUT;
        goto err;
    }

    ulKeyType = get_key_type(pkey->type);
    if (ulKeyType != KEY_TYPE_RSA) {
        printf("Error: Invalid Key Type %d\n", ulKeyType);
        ulRet = ERR_INVALID_INPUT;
        goto err;
    }

    pKey = (Uint8 *) malloc(BUFSIZE);
    if (pKey == NULL) {
        printf("malloc failure \n");
        ulRet = ERR_MEMORY_ALLOC_FAILURE;
        goto err;
    }

    rsa = EVP_PKEY_get1_RSA(pkey);
    if (rsa == NULL) {
        printf("failed to read rsa private key from file\n\n");
        ulRet = ERR_INVALID_USER_INPUT;
        goto err;
    }
    pub_exp = BN_bn2dec(rsa->e);
    ulPubExp = atoi(pub_exp);
    if (pub_exp) {
        free(pub_exp);
        pub_exp = NULL;
    }
    ulCrtLen = (BN_num_bytes(rsa->n) * 5) / 2;
    ulModLen = BN_num_bytes(rsa->n);
    ulHalfModLen = ulModLen/2;
    BN_bn2bin(rsa->n, pKey);
    BN_bn2bin(rsa->d, pKey+(2*ulModLen)-BN_num_bytes(rsa->d));
    BN_bn2bin(rsa->q, pKey+(2*ulModLen)+(1*ulHalfModLen)-BN_num_bytes(rsa->q));
    BN_bn2bin(rsa->dmq1, pKey+(2*ulModLen)+(2*ulHalfModLen)-BN_num_bytes(rsa->dmq1));
    BN_bn2bin(rsa->p, pKey+(2*ulModLen)+(3*ulHalfModLen)-BN_num_bytes(rsa->p));
    BN_bn2bin(rsa->dmp1, pKey+(2*ulModLen)+(4*ulHalfModLen)-BN_num_bytes(rsa->dmp1));
    BN_bn2bin(rsa->iqmp, pKey+(2*ulModLen)+(5*ulHalfModLen)-BN_num_bytes(rsa->iqmp));
    ulKeyLen = (2*ulModLen) + (5*ulHalfModLen);
    RAND_bytes(pIV, 16);

    ulRet = Cfm2ImportKey(session_handle,
                  ulKeyType,
                  PLAIN_RAW,
                  pKey,
                  ulKeyLen,
                  ulCrtLen,
                  ulPubExp,
                  ulModLen * 8,
                  ulCurveId,
                  (Uint8 *) pID, ulIDLen,
                  (Uint8 *) pLabel, ulLabelLen,
                  pIV,
                  ulMech,
                  ucKeyLocation, ulWrappingKeyHandle, &ulNewKey64);

    ulNewKey32 = (Uint32) ulNewKey64;
    if (ulRet == 0) {
        printf("\n\tPrivate Key Unwrapped.  Key Handle: %d \n", ulNewKey32);
    }
err:
    printf("\n\tCfm2ImportKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    if (pub_exp)
        free(pub_exp);
    if (fp)
        fclose(fp);
    if (pKey)
        free(pKey);
    if (pkey)
        EVP_PKEY_free(pkey);
    return ulRet;
}


/****************************************************************************
 *
 * FUNCTION     : wrapKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 wrapKey(int argc, char **argv)
{
    Uint32 i = 0;
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;

    Uint8 bWrappingKeyHandle = FALSE;
    Uint32 ulWrappingKeyHandle = 0;

    Uint8 bKeyHandle = FALSE;
    Uint32 ulKeyHandle = 0;

    Uint8 bFile = FALSE;
    char *KeyFile = NULL;

    Uint8 *pBuf = NULL;
    Uint8 *pData = NULL;
    Uint32 ulDataLen = 4096;

    Uint8 *pIV = 0;
    Uint32 ulIVLen = 8;

    Uint32 ulMech = CRYPTO_MECH_AES_KEY_WRAP;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        // wrapping key handle
        else if ((strcmp(argv[i], "-w") == 0) && (argc > i + 1))
            bWrappingKeyHandle =
                readIntegerArg(argv[i + 1], &ulWrappingKeyHandle);
        // key to be wrapped
        else if ((!bKeyHandle) && (strcmp(argv[i], "-k") == 0)
             && (argc > i + 1))
            bKeyHandle = readIntegerArg(argv[i + 1], &ulKeyHandle);
        else if ((!bFile) && (strcmp(argv[i], "-out") == 0)
             && (argc > i + 1)) {
            KeyFile = argv[i+1];
            bFile = 1;
        }
        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bWrappingKeyHandle) {
        printf("\n\tError: wrapping Key handle (-w) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bKeyHandle) {
        printf("\n\tError: key to be wrapped (-k) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bFile) {
        printf("\n\tError: Wrapped Key File (-out) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nWraps sensitive keys from HSM to host.");
        printf("\n");
        printf
            ("\nSyntax: wrapKey -h -k <key to be wrapped> -w <wrapping key handle> -out <wrapped key file>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -k  handle of the key to be wrapped");
        printf
            ("\n       -w  specifies the wrapping key handle (KEK handle - 4)");
        printf("\n       -out  specifies the file to write the wrapped key data");
        printf("\n");
        return ulRet;
    }

    pBuf = (Uint8 *) calloc(sizeof(Uint64) + ulIVLen + ulDataLen, 1);
    if (pBuf == NULL) {
        printf("Cannot allocate memory for Wrapped Key data\n");
        return -1;
    }

    /* Store Wrapping Key Handle */
    *(Uint64 *)pBuf = ulWrappingKeyHandle;

    /* Take Random IV */
    pIV = pBuf + sizeof(Uint64);
    RAND_bytes(pIV, ulIVLen);

    /* Point pData to correct offset */
    pData = pBuf + sizeof(Uint64) + ulIVLen;

    ulRet = Cfm2WrapKey3(session_handle,
                 ulWrappingKeyHandle,
                 ulKeyHandle, pIV, ulMech, 0, pData, &ulDataLen);

    if (ulRet == 0) {
        printf("\n\tKey Wrapped.\n");
        // write to a file
        if (WriteBinaryFile(KeyFile, (char *)pBuf, sizeof(Uint64) + ulIVLen + ulDataLen)) {
            printf("\n\tWrapped Key written to file \"%s\"\n", KeyFile);
        } else {
            ulRet = ERR_WRITE_OUTPUT_FILE;
            printf("\n\tFailed to write Wrapped Key to a file.\n");
        }
    }

    if (pBuf)
        free(pBuf);
    printf("\n\tCfm2WrapKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : unWrapKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 unWrapKey(int argc, char **argv)
{
    Uint32 i = 0;
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;

    Uint8 bWrappedKey = FALSE;
    Uint8 *pBuf = NULL;
    Uint8 *pWrappedKey = NULL;
    Uint32 ulWrappedKeyLen = 0;

    Uint64 ulNewKey64 = 0;
    Uint8 ucKeyLocation = STORAGE_FLASH;

    Uint32 ulUnWrappingKeyHandle = 0;
    Uint8 *pIV = 0;
    Uint32 ulIVLen = 8;

    Uint32 ulMech = CRYPTO_MECH_AES_KEY_WRAP;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        // wrapped key file
        else if ((!bWrappedKey) && (strcmp(argv[i], "-f") == 0)
             && (argc > i + 1)) {
            bWrappedKey =
                readStringArgByMap(argv[i + 1], (char **)&pBuf,
                           &ulWrappedKeyLen, 1, "wrapped key");
        } else if (strcmp(argv[i], "-sess") == 0) {
            ucKeyLocation = STORAGE_RAM;
            i--;                //This for loops skips i by 2. so go with it.
        }

        else
            bHelp = TRUE;
    }

    if (!bHelp && !bWrappedKey) {
        printf("\n\tError: File name (-f) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nUnwraps sensitive keys onto HSM.");
        printf("\n");
        printf
            ("\nSyntax: unWrapKey -h -f <wrapped key file name> \n");
        printf("\n");
        printf("\nWhere: -h   displays this information");
        printf
            ("\n       -f   specifies the filename containing the key to import");
        printf("\n       -sess specifies key as session key");
        printf("\n");
        return ulRet;
    }

    ulUnWrappingKeyHandle = *(Uint64 *)pBuf;
    pIV = pBuf + sizeof(Uint64);
    pWrappedKey = pBuf + sizeof(Uint64) + ulIVLen;
    ulWrappedKeyLen -= (sizeof(Uint64) + ulIVLen);

    ulRet = Cfm2UnWrapKey3(session_handle,
                   ulUnWrappingKeyHandle,
                   (Uint8 *) pWrappedKey,
                   ulWrappedKeyLen,
                   pIV, ulMech, 0, ucKeyLocation, &ulNewKey64);

    if (ulRet == 0) {
        printf("\n\tKey Unwrapped.  Key Handle: %lld \n", ulNewKey64);
    }
    printf("\n\tCfm2UnWrapKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : convert2CaviumPrivKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 convert2CaviumPrivKey(int argc, char **argv)
{
    Uint32 ulRet = -1;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;
    Uint32 ulModLen = 0;
    Uint32 ulCrtLen = 0;

    Uint8 bWrappingKeyHandle = FALSE;
    Uint32 ulWrappingKeyHandle = 0;

    Uint8 bFile = FALSE;
    char *pFile = 0;

    Uint8 bOutFile = FALSE;
    char *KeyFile = 0;

    Uint8 bID = FALSE;
    char *pID = 0;
    Uint32 ulIDLen = 0;

    Uint8 bLabel = FALSE;
    char *pLabel = 0;
    Uint32 ulLabelLen = 0;
    Uint8 ucKeyLocation = STORAGE_FLASH;

    Uint64 ulNewKey64 = 0;
    Uint32 ulNewKey32 = 0;

    Uint8 *privexp = NULL;
    Uint32 *temp = NULL;
    Uint32 ulPubExp = 0;

    Uint8 randomIV[16] = {};
    Uint8 *pIV = &randomIV[0];

    Uint32 ulMech = CRYPTO_MECH_AES_KEY_WRAP;

    FILE *fp = NULL;
    EVP_PKEY *pkey = NULL;
    Uint8 *pKey = NULL;
    Uint32 ulKeyLen = 0;
    RSA *rsa = NULL;
    char *pub_exp = NULL;
    BIO *mem_bio = BIO_new(BIO_s_mem());

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        // private key file
        else if ((!bFile) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1)) {
            pFile = argv[i + 1];
            bFile = TRUE;
        }
        else if ((!bOutFile) && (strcmp(argv[i], "-out") == 0) && (argc > i + 1)) {
            KeyFile = argv[i + 1];
            bOutFile = TRUE;
        }
        // Label
        else if ((!bLabel) && (strcmp(argv[i], "-l") == 0) && (argc > i + 1))
            bLabel = readArgAsString(argv[i + 1], &pLabel, &ulLabelLen);
        // Key ID
        else if ((!bID) && (strcmp(argv[i], "-id") == 0) && (argc > i + 1))
            bID = readArgAsString(argv[i + 1], &pID, &ulIDLen);

        // wrapping key handle
        else if ((!bWrappingKeyHandle) && (strcmp(argv[i], "-w") == 0)
             && (argc > i + 1))
            bWrappingKeyHandle =
                readIntegerArg(argv[i + 1], &ulWrappingKeyHandle);
        else if (strcmp(argv[i], "-sess") == 0) {
            ucKeyLocation = STORAGE_RAM;
            i--;                //This for loops skips i by 2. so go with it.
        }

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bWrappingKeyHandle) {
        printf("\n\tError: wrapping key handle (-w) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bFile) {
        printf("\n\tError: File name (-f) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bLabel) {
        printf("\n\tError: Key label (-l) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bLabel && validateLabel(pLabel, ulLabelLen))
        bHelp = TRUE;
    if (!bHelp && !bOutFile) {
        printf("\n\tError: Fake Key File (-out) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf
            ("\nImports an RSA Private Key and saves key_handle in Cavium's fake PEM format");
        printf("\n");
        printf
            ("\nSyntax: convert2CaviumPrivKey -h -l <label> -f <private key file name> -w  <wrapping key handle> -out <fake-pem-key-file> [-sess] [-id <key ID>]\n");
        printf("\n");
        printf("\nWhere: -h    displays this information");
        printf("\n       -l    specifies the private key label");
        printf
            ("\n       -f    specifies the filename containing the key to import");
        printf("\n       -w    specifies the wrapping key handle, 4 for KEK");
        printf("\n       -out specifies the file to write the Private Key in Fake PEM format");
        printf("\n       -sess specifies key as session key");
        printf("\n       -id   specifies key ID");
        printf("\n");
        return ulRet;
    }

    fp = fopen(pFile, "rb");
    if (!fp) {
        printf("failed to open file  %s : %s\n", pFile, strerror(errno));
        ulRet = ERR_FILE_READ;
        goto err;
    }

    pkey = PEM_read_PrivateKey(fp, NULL, NULL, NULL);
    if (pkey == NULL) {
        printf("failed to read private key from file\n\n");
        ulRet = ERR_INVALID_INPUT;
        goto err;
    }

    rsa = EVP_PKEY_get1_RSA(pkey);
    if (rsa == NULL) {
        printf("failed to read rsa private key from file\n\n");
        ulRet = ERR_INVALID_INPUT;
        goto err;
    }

    pub_exp = BN_bn2dec(rsa->e);
    ulPubExp = atoi(pub_exp);
    if (pub_exp) {
        free(pub_exp);
        pub_exp = NULL;
    }
    ulModLen = BN_num_bytes(rsa->n);
    ulCrtLen = (ulModLen * 5) / 2;
    ulRet = i2d_PKCS8PrivateKeyInfo_bio(mem_bio, pkey);
    if (ulRet == 0) {
        printf("failed to convert to pkcs8 format\n\n");
        goto err;
    }

    pKey = (Uint8 *) malloc(BUFSIZE);
    if (pKey == NULL) {
        printf("malloc failure \n");
        goto err;
    }

    ulKeyLen = BIO_read(mem_bio, (char *)pKey, BUFSIZE);
    if (ulKeyLen == 0) {
        printf("Couldn't read from the mem_bio\n");
        goto err;
    }

    printf("Key length is %d\n", ulKeyLen);

    RAND_bytes(pIV, 16);

    ulRet = Cfm2ImportKey(session_handle,
                  KEY_TYPE_RSA,
                  0,
                  pKey,
                  ulKeyLen,
                  ulCrtLen,
                  ulPubExp,
                  0,
                  0,
                  (Uint8 *) pID, ulIDLen,
                  (Uint8 *) pLabel, ulLabelLen,
                  pIV,
                  ulMech,
                  ucKeyLocation, ulWrappingKeyHandle, &ulNewKey64);

    ulNewKey32 = (Uint32) ulNewKey64;
    if (ulRet)
        goto err;
    printf("\n\tPrivate Key Imported.  Key Handle: %d \n", ulNewKey32);

    privexp = OPENSSL_malloc(ulModLen);
    if (privexp == NULL) {
        printf("\n\n\t OPENSSL_malloc failure \n");
        goto err;
    }
    temp = (Uint32 *) privexp;
    for (i = 0; i < ulModLen / 4; i++, temp++)
        *temp = CAV_SIG_HSM_KEY;

    memset(&(privexp[8]), 0, 8);
    memcpy(&(privexp[8]), (Uint8 *) & ulNewKey64, 8);

    /* Private Exponent */
    BN_bin2bn(privexp, ulModLen, rsa->d);
    BN_zero(rsa->p);
    BN_zero(rsa->q);
    BN_zero(rsa->dmp1);
    BN_zero(rsa->dmq1);
    BN_zero(rsa->iqmp);

    /* Turn off delete key handle flag in RSA flags,
     * so RSA_free does not delete the key handle from card */
    rsa->flags &= 0xbfff;

    fclose(fp);
    fp = fopen(KeyFile, "w");
    if (!fp) {
	printf("failed to open file  %s : %s\n", KeyFile, strerror(errno));
	ulRet = ERR_OPEN_FILE;
	goto err;
    }
    if (!PEM_write_RSAPrivateKey(fp, rsa, NULL, NULL, 0, NULL, NULL)) {
        printf("failed to write rsa private key to file\n\n");
        goto err;
    }
    printf
        ("\n\nPrivate Key Handle is written to %s in fake PEM format\n", KeyFile);
    ulRet = 0;

err:
    if (pub_exp)
        free(pub_exp);
    if (privexp)
        OPENSSL_free(privexp);
    if (pkey)
        EVP_PKEY_free(pkey);
    if (rsa)
        RSA_free(rsa);
    if (fp)
        fclose(fp);
    if (mem_bio)
        BIO_free(mem_bio);
    printf("\n\tconvert2CaviumPrivKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : getFWVer
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 getFWVer(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint8 ver[50];

    if (argc > 2) {
        printf
            ("\n\tThis command doesn't expect any arguments and allowed only on initialized HSM\n");
        printf("\nDescription:");
        printf
            ("\n\tgetFWVersion returns current firmware version loaded onto the HSM\n");
        return ulRet;
    }

    ulRet = Cfm2FWVersion(session_handle, ver);
    if (ulRet == 0) {
        /* +9 to skip "Version: " from Firmware version string */
        printf("\n\tFirmware Info:\t%s\n", ver + 9);
    }
    printf("\n\tCfm2FWVersion returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : getLoginFailCount
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 getLoginFailCount(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bUserType = FALSE;
    char *pUserType = 0;
    Uint32 ulUserType = 0;
    Uint32 ulUserTypeLen = 0;

    Uint8 bUserName = FALSE;
    char *userName = 0;
    Uint32 ulNameLen = 0;

    Uint32 fail_cnt = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        //  user type
        else if ((!bUserType) && (strcmp(argv[i], "-u") == 0)
             && (argc > i + 1))
            bUserType =
                readArgAsString(argv[i + 1], &pUserType, &ulUserTypeLen);

        // userName
        else if ((!bUserName) && (strcmp(argv[i], "-n") == 0)
             && (argc > i + 1))
            bUserName =
                readArgAsString(argv[i + 1], &userName, &ulNameLen);
        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bUserType) {
        printf("\n\tError: User type (-u) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bUserType) {
        if ((strcmp(pUserType, "CO") == 0))
            ulUserType = CN_CRYPTO_OFFICER;
        else if ((strcmp(pUserType, "CU") == 0))
            ulUserType = CN_CRYPTO_USER;
        else if ((strcmp(pUserType, "AU") == 0))
            ulUserType = CN_APPLIANCE_USER;
        else {
            printf("\n\tError: Invalid user type specified.\n");
            bHelp = TRUE;
        }
    }

    if (!bHelp && !bUserName) {
        printf("\n\tError: UserName (-n) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nGets current login failure count of a particular user.");
        printf("\n");
        printf
            ("\nSyntax: getLoginFailCount -h -u <CO/CU/AU> -n <user name>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -u  specifies the user type as \"CO\" or \"CU\" or \"AU\"");
        printf("\n       -n  specifies the user name");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2GetLoginFailureCount(session_handle, ulUserType, userName, ulNameLen, &fail_cnt);
    if (ulRet == 0) {
        printf("\n\t Login Failure Count of %s is %d\n", userName, fail_cnt);
    }
    printf("\n\tCfm2GetLoginFailureCount returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : genPBEKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 genPBEKey(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint32 ulKey32 = 0;
    Uint64 ulKey64 = 0;

    Uint8 bCount = FALSE;
    Uint32 ulCount = 0;

    Uint8 bSalt = FALSE;
    char *pSalt = 0;
    Uint32 ulSaltLen = 0;

    Uint8 bPassword = FALSE;
    char *pPassword = 0;
    Uint32 ulPasswordLen = 0;

    Uint8 bID = FALSE;
    char *pID = 0;
    Uint32 ulIDLen = 0;

    Uint8 bLabel = FALSE;
    char *pLabel = 0;
    Uint32 ulLabelLen = 0;

    Uint8 IV[8];
    Uint8 ucKeyLocation = STORAGE_FLASH;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // count
        else if ((!bCount) && (strcmp(argv[i], "-c") == 0) && (argc > i + 1))
            bCount = readIntegerArg(argv[i + 1], &ulCount);

        // password
        else if ((!bPassword) && (strcmp(argv[i], "-p") == 0)
             && (argc > i + 1))
            bPassword =
                readArgAsString(argv[i + 1], &pPassword, &ulPasswordLen);

        // salt
        else if ((!bSalt) && (strcmp(argv[i], "-s") == 0) && (argc > i + 1))
            bSalt = readArgAsString(argv[i + 1], &pSalt, &ulSaltLen);

        // label
        else if ((!bLabel) && (strcmp(argv[i], "-l") == 0) && (argc > i + 1))
            bLabel = readArgAsString(argv[i + 1], &pLabel, &ulLabelLen);

        // Key ID
        else if ((!bID) && (strcmp(argv[i], "-id") == 0) && (argc > i + 1))
            bID = readArgAsString(argv[i + 1], &pID, &ulIDLen);

        else if (strcmp(argv[i], "-sess") == 0) {
            ucKeyLocation = STORAGE_RAM;
            i--;                //This for loops skips i by 2. so go with it.
        }

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bCount) {
        printf("\n\tError: Iteration count (-c) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bPassword) {
        printf("\n\tError: Password (-p) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bSalt) {
        printf("\n\tError: Salt value (-s) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bLabel) {
        printf("\n\tError: Key label (-l) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bLabel && validateLabel(pLabel, ulLabelLen))
        bHelp = TRUE;

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nGenerates a PBE DES3 key.");
        printf("\n");
        printf
            ("\nSyntax: genPBEKey -h -l <label > -p <password> -s <salt> -c <iteration count> [-sess] [-id <key ID>]\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -l  specifies the key label");
        printf("\n       -p  specifies the password");
        printf("\n       -s  specifies the salt value: eg. name");
        printf("\n       -c  specifies the iteration count <= 10000: eg. 10");
        printf("\n       -sess specifies key as session key");
        printf("\n       -id   specifies key ID");
        printf("\n");
        return ulRet;
    }

    ulKey64 = 0;
    ulRet = Cfm2GeneratePBEKey(session_handle,
                   (Uint8 *) pID, ulIDLen,
                   (Uint8 *) pLabel, ulLabelLen,
                   (Uint8 *) pPassword, ulPasswordLen,
                   (Uint8 *) pSalt, ulSaltLen,
                   ulCount, IV, ucKeyLocation, 0, &ulKey64);
    if (ulRet == 0) {
        ulKey32 = (Uint32) ulKey64;
        printf("\n\tSymetric Key Created.  Key Handle: %d\n", ulKey32);
    }
    printf("\n\tCfm2GeneratePBEKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : genSymKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 genSymKey(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint32 ulKey32 = 0;
    Uint64 ulKey64 = 0;

    Uint8 bKeyType = FALSE;
    Uint32 ulType = 0;

    Uint8 bSize = FALSE;
    Uint32 ulSize = 0;

    Uint8 bID = FALSE;
    char *pID = 0;
    Uint32 ulIDLen = 0;

    Uint8 bLabel = FALSE;
    char *pLabel = 0;
    Uint32 ulLabelLen = 0;
    Uint8 ucKeyLocation = STORAGE_FLASH;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // type
        else if ((!bKeyType) && (strcmp(argv[i], "-t") == 0)
             && (argc > i + 1))
            bKeyType = readIntegerArg(argv[i + 1], &ulType);

        // size
        else if ((!bSize) && (strcmp(argv[i], "-s") == 0) && (argc > i + 1))
            bSize = readIntegerArg(argv[i + 1], &ulSize);

        // label
        else if ((!bLabel) && (strcmp(argv[i], "-l") == 0) && (argc > i + 1))
            bLabel = readArgAsString(argv[i + 1], &pLabel, &ulLabelLen);

        // Key ID
        else if ((!bID) && (strcmp(argv[i], "-id") == 0) && (argc > i + 1))
            bID = readArgAsString(argv[i + 1], &pID, &ulIDLen);

        else if (strcmp(argv[i], "-sess") == 0) {
            ucKeyLocation = STORAGE_RAM;
            i--;                //This for loops skips i by 2. so go with it.
        }

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bKeyType) {
        printf("\n\tError: Key type (-t) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bSize) {
        printf("\n\tError: Key size (-s) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bLabel) {
        printf("\n\tError: Key label (-l) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bLabel && validateLabel(pLabel, ulLabelLen))
        bHelp = TRUE;

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nGenerates a Symmetric  keys.");
        printf("\n");
        printf
            ("\nSyntax: genSymKey -h -l <label > -t <key type> -s <key size> [-sess] [-id <key ID>]\n");
        printf("\n");
        printf("\nWhere: -h    displays this information");
        printf("\n       -l    specifies the Key Label");
        printf("\n       -t    specifies the key type");
        printf("\n             (16 = GENERIC_SECRET, 18 = RC4, 21 = DES3, 31 = AES)");
        printf("\n       -s    specifies the key size in bytes");
        printf("\n             for AES: 16, 24, 32  3DES: 24  RC4: <256, GENERIC_SECRET: <= 3584");
        printf("\n       -sess specifies key as session key");
        printf("\n       -id   specifies key ID");
        printf("\n");
        return ulRet;
    }

    if ((fipsState >= 2) && (ulType == KEY_TYPE_RC4)) {
        printf("\n RC4 is not allowed in FIPS mode\n");
        printf("\n Current FIPS mode: %d\n", fipsState);
        return -1;
    }

    ulKey64 = 0;
    ulRet = Cfm2GenerateSymmetricKey(session_handle,
                     ulType, ulSize,
                     (Uint8 *) pID, ulIDLen,
                     (Uint8 *) pLabel, ulLabelLen,
                     ucKeyLocation, &ulKey64);
    if (ulRet == 0) {
        ulKey32 = (Uint32) ulKey64;
        printf("\n\tSymmetric Key Created.  Key Handle: %d\n", ulKey32);
    }
    printf("\n\tCfm2GenerateSymmetricKey3 returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

Uint32 exSymKey(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bKey = FALSE;
    Uint64 ulKey64 = 0;

    Uint8 bFile = FALSE;
    char *KeyFile = NULL;

    Uint8 bWrappingKey = FALSE;
    Uint64 ulWrappingKey64 = 0;

    Uint8 randomIV[16] = {};
    Uint8 *pIV = &randomIV[0];

    Uint32 ulMech = CRYPTO_MECH_AES_KEY_WRAP;

    Uint8 *pKey = NULL;
    Uint32 ulKeyLen = 0;
    Uint32 ulTemp = 0;
    Uint32 ulKeyClass = OBJ_CLASS_SECRET_KEY;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        // key to wrap
        else if ((!bKey) && (strcmp(argv[i], "-k") == 0) && (argc > i + 1)) {
            bKey = readIntegerArg(argv[i + 1], &ulTemp);
            ulKey64 = ulTemp;
        }
        // wrapping key
        else if ((!bWrappingKey) && (strcmp(argv[i], "-w") == 0)
             && (argc > i + 1)) {
            bWrappingKey = readIntegerArg(argv[i + 1], &ulTemp);
            ulWrappingKey64 = ulTemp;
        } else if ((strcmp(argv[i], "-m") == 0) && (argc > i + 1)) {
            readIntegerArg(argv[i + 1], (Uint32 *) & ulMech);
            ulMech = get_wrap_mechanism(ulMech);
        } else if (!bFile && (strcmp(argv[i], "-out") == 0) && (argc > i + 1)) {
            KeyFile = argv[i+1];
            bFile = 1;
        }
        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bKey) {
        printf("\n\tError: Handle of key to wrap (-k) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bWrappingKey) {
        printf("\n\tError: Handle of wrapping Key (-w) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bFile) {
        printf("\n\tError: Key File (-out) is missing.\n");
        bHelp = TRUE;
    }
    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nWraps a Symmetric key with another Symmetric key.");
        printf("\n");
        printf
            ("\nSyntax: exSymKey -h -w <wrapping key> -k <key to wrap> -out <key file> \n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -w  specifies the handle of the wrapping key");
        printf
            ("\n       -k  specifies the handle of the key to wrap:3DES/AES/RC4 key handle");
        printf("\n       -m  specifies the wrapping mechanismi (Optional)");
        printf("\n               NIST_AES_WRAP - 4");
        printf("\n       -out  specifies the file to write the exported key");
        printf("\n");

        printf("\n");
        return ulRet;
    }

    ulTemp = 4;
    if ((ulRet = Cfm2GetAttribute_new(session_handle,
                      ulKey64,
                      OBJ_ATTR_CLASS,
                      (Uint8 *) & ulKeyClass, &ulTemp)) == 0) {
        ulKeyClass = atoi((Int8 *) & ulKeyClass);
        if (ulKeyClass != OBJ_CLASS_SECRET_KEY) {
            printf("\n\t%d is not a symmetric key\n", (int)ulKey64);
            ulRet = ERR_INVALID_INPUT;
            goto err;
        }
    }

    RAND_bytes(pIV, 16);

    if ((ulRet = Cfm2ExportKey(session_handle,
                   pIV, ulMech,
                   ulWrappingKey64,
                   ulKey64, NULL, &ulKeyLen)) == 0) {
        /* Allocate Buffer for wrapped key data */
        pKey = (Uint8 *) calloc(ulKeyLen, 1);
        if (!pKey) {
            ulRet = ERR_MEMORY_ALLOC_FAILURE;
            goto err;
        }
        if ((ulRet = Cfm2ExportKey(session_handle,
                       pIV, ulMech,
                       ulWrappingKey64,
                       ulKey64, pKey, &ulKeyLen)) == 0) {
            printf("Key length is %d\n", ulKeyLen);

            if (CRYPTO_MECH_AES_KEY_WRAP != ulMech)
                printf("Writing the key of length %d in wrapped form\n",
                       ulKeyLen);

            // write to a file
            if (WriteBinaryFile(KeyFile, (char *)pKey, ulKeyLen))
                printf
                    ("\n\nWrapped Symmetric Key written to file \"%s\"\n", KeyFile);
            else
                ulRet = ERR_WRITE_OUTPUT_FILE;
        }
    }

err:
    printf("\n\tCfm2ExportKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    if (pKey)
        free(pKey);
    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : imSymKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 imSymKey(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint64 ulNewKey64 = 0;
    Uint32 ulTemp = 0;

    Uint8 randomIV[16] = {};
    Uint8 *pIV = &randomIV[0];

    Uint8 bWrappingKeyHandle = FALSE;
    Uint32 ulWrappingKeyHandle = 0;

    Uint8 bKeyType = FALSE;
    Uint32 ulKeyType = 0;

    Uint8 bID = FALSE;
    char *pID = 0;
    Uint32 ulIDLen = 0;

    Uint8 bLabel = FALSE;
    char *pLabel = 0;
    Uint32 ulLabelLen = 0;
    Uint8 ucKeyLocation = STORAGE_FLASH;

    Uint8 bKey = FALSE;
    char *pTemp = NULL;

    Uint8 pKey[4096] = { 0 };
    Uint32 ulKeyLen = 0;

    Uint32 ulMech = CRYPTO_MECH_AES_KEY_WRAP;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        // type
        else if ((!bKeyType) && (strcmp(argv[i], "-t") == 0)
             && (argc > i + 1)) {
            bKeyType = readIntegerArg(argv[i + 1], &ulKeyType);
        }
        // wrapping key handle
        else if ((strcmp(argv[i], "-w") == 0) && (argc > i + 1))
            bWrappingKeyHandle =
                readIntegerArg(argv[i + 1], &ulWrappingKeyHandle);
        // label
        else if ((!bLabel) && (strcmp(argv[i], "-l") == 0) && (argc > i + 1))
            bLabel = readArgAsString(argv[i + 1], &pLabel, &ulLabelLen);
        // Key ID
        else if ((!bID) && (strcmp(argv[i], "-id") == 0) && (argc > i + 1))
            bID = readArgAsString(argv[i + 1], &pID, &ulIDLen);

        // wrapped key
        else if ((!bKey) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1)) {
            bKey =
                readStringArgByMap(argv[i + 1], &pTemp, &ulKeyLen, 1,
                           "symmetric key");
            if (bKey != TRUE || ulKeyLen > sizeof(pKey)) {
                printf("\n\tError: Invalid file passed\n");
                bKey = FALSE;
                continue;
            }
            memcpy(&pKey[0], pTemp, ulKeyLen);
        }
        else if (strcmp(argv[i], "-sess") == 0) {
            ucKeyLocation = STORAGE_RAM;
            i--;                //This for loops skips i by 2. so go with it.
        }

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bKeyType) {
        printf("\n\tError: Key type (-t) is missing.\n");
        bHelp = TRUE;
    }
    // ensure that we have all the required args
    if (!bHelp && !bWrappingKeyHandle) {
        printf("\n\tError: wrapping key handle (-w) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bLabel) {
        printf("\n\tError: Key label (-l) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bKey) {
        printf("\n\tError: Key filename (-f) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bLabel && validateLabel(pLabel, ulLabelLen))
        bHelp = TRUE;

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nImports a symmetric key.");
        printf("\n");
        printf
            ("\nSyntax: imSymKey -h -l <label> -t <key type> -f <key file name> -w <wrapper key handle> [-sess] [-id <key ID>]\n");
        printf("\n");
        printf("\nWhere: -h    displays this information");
        printf("\n       -l    specifies the new key's Label");
        printf("\n       -t    specifies the key type");
        printf("\n             (16 = GENERIC_SECRET, 18 = RC4, 21 = DES3/DES, 31 = AES)");
        printf
            ("\n       -f    specifies the filename containing the key to import");
        printf("\n             For RC4,  file size <= 256 bytes\n");
        printf("               DES,  file size = 8 bytes (non-FIPS mode) \n");
        printf("               DES3, file size = 24 bytes\n");
        printf("               AES,  file size = 16, 24 and 32 bytes\n");
        printf("\n       -w    specifies the wrapper key handle, 4 for KEK");
        printf("\n       -sess specifies key as session key");
        printf("\n       -id   specifies key ID");
        printf("\n");
        return ulRet;
    }

    if (ulKeyType == 21)
        /*DES*/ {
            if ((fipsState < 2) && (ulKeyLen == 8)) {
                memcpy(pKey + 8, pKey, 8);
                memcpy(pKey + 16, pKey, 8);
                ulKeyLen = 24;
            }
        }

    RAND_bytes(pIV, 16);

    ulRet = Cfm2ImportKey(session_handle,
                  ulKeyType,
                  PLAIN,
                  pKey,
                  ulKeyLen,
                  0, 0, 0, 0,
                  (Uint8 *) pID, ulIDLen,
                  (Uint8 *) pLabel, ulLabelLen,
                  pIV,
                  ulMech,
                  ucKeyLocation, ulWrappingKeyHandle, &ulNewKey64);
    if (ulRet == 0) {
        ulTemp = (Uint32) ulNewKey64;
        printf("\n\nSymmetric Key Imported.  Key Handle: %d \n", ulTemp);
    }
    printf("\n\tCfm2ImportKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : deleteKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 deleteKey(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bKey = FALSE;
    Uint32 ulKey32 = 0;
    Uint64 ulKey64 = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // key handle
        else if ((!bKey) && (strcmp(argv[i], "-k") == 0) && (argc > i + 1))
            bKey = readIntegerArg(argv[i + 1], &ulKey32);

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bKey) {
        printf("\n\tError: Key handle (-k) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nDelete a key specifying a key handle.");
        printf("\n");
        printf("\nSyntax: deleteKey -h -k <key handle>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -k  specifies the key handle to delete");
        printf("\n");
        return ulRet;
    }

    ulKey64 = ulKey32;
    ulRet = Cfm2DeleteKey(session_handle, ulKey64);
    printf("\n\tCfm2DeleteKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : Error2String
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 Error2String(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bResponseCode = FALSE;
    Uint32 ulResponseCode = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // Response Code
        else if ((!bResponseCode) && (strcmp(argv[i], "-r") == 0)
             && (argc > i + 1)) {
            bResponseCode = TRUE;
            ulResponseCode = strtoul(argv[i + 1], NULL, 0);
        } else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bResponseCode) {
        printf("\n\tError: Response code (-r) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nconvert a response code to Error String.");
        printf("\n");
        printf("\nSyntax: Error2String -h -r <response code> \n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -r  specifies response code to be converted");
        printf("\n");
        return ulRet;
    }

    printf("\n\tError Code %x maps to %s\n", ulResponseCode,
           Cfm2ResultAsString(ulResponseCode));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : getAuditLogs
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 getAuditLogs(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bOldLogs = FALSE;
    Uint32 ulOldLogs = -1;
    Uint8 bSign = FALSE;
    Uint32 ulSign = -1;

    Uint32 numLogs = 0;
    Uint32 remLogs = 0;
    Uint32 curIndex = 0;

    Uint64 sign[32];

    FILE * signFile;
    FILE * cmbLogFile;

    char cmbLogFilePath[100] = {};
    char signFilePath[100] = {};

    cmbLogEntry *cmbLogs = NULL;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        else if ((!bOldLogs) && (strcmp(argv[i], "-a") == 0)
             && (argc > i + 1))
            bOldLogs = readIntegerArg(argv[i + 1], &ulOldLogs);

        else if ((!bSign) && (strcmp(argv[i], "-s") == 0)
             && (argc > i + 1))
            bSign = readIntegerArg(argv[i + 1], &ulSign);

        else
            bHelp = TRUE;
    }

    if((ulOldLogs != 0) && (ulOldLogs != 1))
    {
        printf("\n\tError: Get all logs (-a) value must be 0 or 1.\n");
        bHelp = TRUE;
    }

    if(bSign) {
        if((ulSign != 0) && (ulSign != 1) && (ulSign != 2))
        {
            printf("\n\tError: Get sign (-s) value must be 0 or 1 or 2.\n");
            bHelp = TRUE;
        }
    } else {
        ulSign = 0;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf
            ("\nPrints Audit Logs.");
        printf("\n");
        printf
            ("\nSyntax: getAuditLogs -a <old logs> [-s <hash/rsa signature>]\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -a  specifies whether to display all logs or logs since last print");
        printf("\n           0 to print only new logs");
        printf("\n           1 to print all logs");
        printf("\n       -s  specifies whether to get hash or RSA signature of logs (optional)");
        printf("\n           0 no hash or RSA signature (default)");
        printf("\n           1 to get RSA signature along with the logs");
        printf("\n           2 to get SHA256 hash along with the logs");
        printf("\n\n");
        return ulRet;
    }

    ulRet = Cfm2GetAuditLogInfo(session_handle, ulOldLogs, &numLogs);
    if (ulRet == 0) {
        printf("Number of Logs found %d\n", numLogs-1); /* -1 for LOG_DETAILS_LOG log entry */
    } else {
        goto end;
    }

    if(numLogs > 0) {
        cmbLogs = (cmbLogEntry *) malloc (sizeof(cmbLogEntry) * numLogs);
    }

    if(!cmbLogs) {
        printf("\nFailed to allocated memory for combined logs.\n");
        goto end;
    }

    memset(cmbLogs, 0 ,(numLogs * sizeof(cmbLogEntry)));

    remLogs = numLogs;
    curIndex = 0;

    while(TRUE) {
        if(remLogs > 0) {
            if(remLogs > MAX_NUM_LOGS) {
                ulRet = Cfm2GetAuditLogs(session_handle, MAX_NUM_LOGS, curIndex, cmbLogs + curIndex);
                if(ulRet != 0)
                    goto end;
                remLogs = remLogs - MAX_NUM_LOGS;
                curIndex = curIndex + MAX_NUM_LOGS;
            } else {
                ulRet = Cfm2GetAuditLogs(session_handle, remLogs, curIndex, cmbLogs + curIndex);
                if(ulRet != 0)
                    goto end;
                remLogs = 0;
            }
        } else {
            break;
        }
    }

    ulRet = Cfm2GetAuditLogSign(session_handle, ulSign, sign);
    if(ulRet != 0)
        goto end;

    memset(cmbLogFilePath, 0, sizeof(cmbLogFilePath));
    sprintf(cmbLogFilePath,"cmb.log");
    if ((cmbLogFile = fopen(cmbLogFilePath, "wb")) == NULL)
    {
        printf("\nFailed to create combined log entry.\n");
        ulRet = ERR_UNKNOWN_ERROR;
        goto end;
    }

    fwrite(cmbLogs, sizeof(cmbLogEntry), numLogs, cmbLogFile);
    fclose(cmbLogFile);

    printf("\nLog binary at cmb.log\n");

    if(ulSign == 1) {
        memset(signFilePath, 0, sizeof(signFilePath));
        sprintf(signFilePath,"cmb.sign");
        if ((signFile = fopen(signFilePath, "wb")) == NULL)
        {
            printf("\nFailed to create sign file.\n");
            ulRet = ERR_UNKNOWN_ERROR;
            goto end;
        }
        fwrite(sign, 1, 256, signFile);
        fclose(signFile);

        printf("\nLog sign at cmb.sign\n");
    }

    if(ulSign == 2) {
        memset(signFilePath, 0, sizeof(signFilePath));
        sprintf(signFilePath,"cmb.hash");
        if ((signFile = fopen(signFilePath, "wb")) == NULL)
        {
            printf("\nFailed to create hash file.\n");
            ulRet = ERR_UNKNOWN_ERROR;
            goto end;
        }
        fwrite(sign, 1, 32, signFile);
        fclose(signFile);

        printf("\nLog hash at cmb.hash\n");
    }

end:
    printf("\n\tCfm2GetAuditLogInfo returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    if(cmbLogs)
        free(cmbLogs);

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : findKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 findKey(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bKeyClass = FALSE;
    Uint8 bKeyClassIn = FALSE;
    Uint32 ulKeyClass = -1;

    Uint8 bKeyType = FALSE;
    Uint32 ulKeyType = -1;

    Uint8 bKeyLocation = FALSE;
    Uint32 ulKeyLocation = -1;

    Uint8 bID = FALSE;
    char *pID = 0;
    Uint32 ulIDLen = 0;

    Uint8 bLabel = FALSE;
    char *pLabel = 0;
    Uint32 ulLabelLen = 0;

    Uint8 bModulus = FALSE;
    char *pModulus = 0;
    Uint32 ulModLen = 0;

    Uint64 *pulKeyArray = 0;
    Uint32 ulKeyArrayLen = 0;
    Uint32 total_keys = 0;
    int j = 0, found = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // class
        else if ((!bKeyClass) && (strcmp(argv[i], "-c") == 0)
             && (argc > i + 1)){

            bKeyClass = readIntegerArg(argv[i + 1], &ulKeyClass);
            bKeyClassIn = TRUE;
        }

        // type
        else if ((!bKeyType) && (strcmp(argv[i], "-t") == 0)
             && (argc > i + 1))
            bKeyType = readIntegerArg(argv[i + 1], &ulKeyType);

        // location
        else if ((!bKeyLocation) && (strcmp(argv[i], "-s") == 0)
             && (argc > i + 1))
            bKeyLocation = readIntegerArg(argv[i + 1], &ulKeyLocation);

        // label
        else if ((!bLabel) && (strcmp(argv[i], "-l") == 0) && (argc > i + 1))
            bLabel = readArgAsString(argv[i + 1], &pLabel, &ulLabelLen);

        // Key ID
        else if ((!bID) && (strcmp(argv[i], "-id") == 0) && (argc > i + 1))
            bID = readArgAsString(argv[i + 1], &pID, &ulIDLen);

        else if ((!bModulus) && (strcmp(argv[i], "-m") == 0)
             && (argc > i + 1))
            bModulus =
                readStringArg(argv[i + 1], &pModulus, &ulModLen, 1,
                          "modulus");
        else
            bHelp = TRUE;
    }

    if (!bHelp && bLabel && validateLabel(pLabel, ulLabelLen))
        bHelp = TRUE;

    if(((bKeyClassIn && !bKeyClass) ||
        ( bKeyClass && 2 != ulKeyClass && 3 != ulKeyClass && 4 != ulKeyClass)))
        bHelp = TRUE;

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf
            ("\nFind keys optionally matching the specified key class, key label and modulus.");
        printf("\n");
        printf
            ("\nSyntax: findKey -h [-c <key class>] [-l <key label>] [-m <modulus>] [-sess] [-id <key ID>]\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -c  specifies the key class to find (optional)");
        printf("\n           (4 = secret, 2 = public, 3 = private)");
        printf("\n       -l  specifies the key label to find (optional)");
        printf
            ("\n       -m  specifies the file containing RSA modulus to match with (optional)");
        printf("\n       -id   specifies key ID");
        printf("\n");
        return ulRet;
    }
    // Determine Key Handle(s) Found and Place in Key Handle Array
    ulRet = Cfm2FindKey(session_handle, 0, ulKeyClass,
                (Uint8 *) pID, ulIDLen+1,
                (Uint8 *) pLabel, ulLabelLen+1,
                (Uint8 *) pModulus, ulModLen,
                NULL, &total_keys);
    if (ulRet == 0 || ulRet == RET_RESULT_SIZE) {
        printf("Number of Keys found %d\n", total_keys);
    } else {
        goto end;
    }

    if (!total_keys) {
        printf("\n\tNo keys are found \n");
    }
    // Allocate Key Handle(s) Array
    pulKeyArray = (Uint64 *) calloc(MAX_SEARCH_OBJECTS, sizeof(Uint64));
    if (pulKeyArray == 0) {
        ulRet = ERR_MEMORY_ALLOC_FAILURE;
        goto end;
    }

    while (found < total_keys) {
        ulKeyArrayLen = MAX_SEARCH_OBJECTS;
        // Find All Keys of Key Class, Type and Label
        ulRet = Cfm2FindKeyFromIndex(session_handle, 0, j, ulKeyClass,
                         (Uint8 *) pID, ulIDLen+1,
                         (Uint8 *) pLabel, ulLabelLen+1,
                         (Uint8 *) pModulus, ulModLen,
                         pulKeyArray, &ulKeyArrayLen);
        // Print Key Handle Array
        //HexPrint((Uint8*)pKeyArray, ulKeyArrayLen*8);
        if (ulRet == 0 || ulRet == RET_RESULT_SIZE) {
            if (ulKeyArrayLen != 0) {
                Uint32 ulValue = 0;
                printf("number of keys matched from start index %d::%d\n", j,
                       ulKeyArrayLen);
                for (i = 0; i < ulKeyArrayLen - 1; i++) {
                    ulValue = (Uint32) (pulKeyArray[i]);
                    printf("%d, ", ulValue);
                }
                ulValue = (Uint32) (pulKeyArray[i]);
                printf("%d\n", ulValue);
                found += ulKeyArrayLen;
                /* next start index will be the last key found in
                 * the previous iteration. mask off session bit as
                 * last key found could be a session key. */
#ifndef CLOUD_HSM_CLIENT
                j = (ulValue & 0x1FFFF) + 1;
#else
                /* required this for EKP */
                j = ulValue + 1;
#endif

            } else {
                printf("\n\tNo more keys are found\n");
                break;
            }
        } else {
            printf("\n\tFailed to fetch the keys with error %x : %s\n", ulRet, Cfm2ResultAsString(ulRet));
            break;
        }

    }
end:
    if (pulKeyArray)
        free(pulKeyArray);
    printf("\n\tCfm2FindKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : getAttribute
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 getAttribute(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bKey = FALSE;
    Uint32 ulKey32 = 0;
    Uint64 ulKey64 = 0;

    Uint8 bFile = FALSE;
    char *AttrFile = NULL;

    Uint8 bAttribute = FALSE;
    Uint32 ulAttribute = 0;

    Uint8 *pAttr = 0;
    Uint32 ulAttrLen = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // key
        else if ((!bKey) && (strcmp(argv[i], "-o") == 0) && (argc > i + 1)) {
            bKey = readIntegerArg(argv[i + 1], &ulKey32);
            ulKey64 = (Uint64) ulKey32;
        }
        // attribute
        else if ((!bAttribute) && (strcmp(argv[i], "-a") == 0)
             && (argc > i + 1))
            bAttribute = readIntegerArg(argv[i + 1], &ulAttribute);

        else if ((!bFile) && (strcmp(argv[i], "-out") == 0)
             && (argc > i + 1)) {
            AttrFile = argv[i+1];
            bFile = 1;
        }
        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bKey) {
        printf("\n\tError: Object handle (-o) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bAttribute) {
        printf("\n\tError: Attribute ID (-a) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bFile) {
        printf("\n\tError: Attribute File (-out) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nGet an attribute from an object.");
        printf("\n");
        printf("\nSyntax: getAttribute -h -o <object Handle> -a <attribute> -out <attribute file>");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -o  specifies the object handle");
        printf("\n       -a  specifies the attribute to read");
        printf("\n       -out  specifies the file to write the attribute value");
        printf
            ("\n       (run listAttributes to see a list of all possible attribute values)");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2GetAttribute_new(session_handle,
                 ulKey64, ulAttribute, 0, &ulAttrLen);
    if (!ulRet && (ulAttrLen > 0)) {
        pAttr = (Uint8 *) malloc(ulAttrLen);
        if (pAttr == 0)
            ulRet = ERR_MEMORY_ALLOC_FAILURE;
        else {
            ulRet = Cfm2GetAttribute_new(session_handle,
                         ulKey64, ulAttribute, pAttr, &ulAttrLen);
            if (ulRet == 0) {
                // write to a file
                if (WriteBinaryFile
                    (AttrFile, (char *)pAttr, ulAttrLen)) {
                    printf
                        ("\n\tAttribute was written to the file \"%s\"\n", AttrFile);
                    printf("\nAttribute: \n");
                    HexPrint(pAttr, ulAttrLen);
                } else {
                    ulRet = ERR_WRITE_OUTPUT_FILE;
                    printf("\n\tFailed to write Attribute to a file.\n");
                }
            }
        }
    }
    printf("\n\tCfm2GetAttribute returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    if (pAttr)
        free(pAttr);

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : setAttribute
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 setAttribute(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bKey = FALSE;
    Uint32 ulKey32 = 0;
    Uint64 ulKey64 = 0;

    Uint8 bAttribute = FALSE;
    Uint32 ulAttribute = 0;

    Uint8 *pAttribute = 0;
    Uint32 ulAttributeLen = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // key
        else if ((!bKey) && (strcmp(argv[i], "-o") == 0) && (argc > i + 1)) {
            bKey = readIntegerArg(argv[i + 1], &ulKey32);
            ulKey64 = (Uint64) ulKey32;
        }
        // attribute
        else if ((!bAttribute) && (strcmp(argv[i], "-a") == 0)
             && (argc > i + 1))
            bAttribute = readIntegerArg(argv[i + 1], &ulAttribute);

        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bKey) {
        printf("\n\tError: Object handle (-o) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bAttribute) {
        printf("\n\tError: Attribute ID (-a) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nSet an attribute value for an object.");
        printf("\n");
        printf
            ("\nSyntax: setAttribute -h -o <object handle> -a <attribute> \n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -o  specifies the Object Handle");
        printf("\n       -a  specifies the Attribute to set");
        printf
            ("\n       (only the following attribute's value can be modified \n");
        printf("\n        OBJ_ATTR_LABEL                  = %d", 0x00000003);
        printf("\n        OBJ_ATTR_ENCRYPT                = %d", 0x00000104);
        printf("\n        OBJ_ATTR_DECRYPT                = %d", 0x00000105);
        printf("\n        OBJ_ATTR_WRAP                   = %d", 0x00000106);
        printf("\n        OBJ_ATTR_UNWRAP                 = %d", 0x00000107);
        printf("\n");
        return ulRet;
    }

    ulRet = getAttributeValue(ulAttribute, &pAttribute, &ulAttributeLen);
    if (ulRet == 0)
        ulRet = Cfm2SetAttribute(session_handle, ulKey64,
                     ulAttribute, pAttribute, ulAttributeLen);
    printf("\n\tCfm2SetAttribute returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : listAttributes
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 listAttributes(int argc, char **argv)
{
    printf("\n");
    printf("\nDescription");
    printf("\n===========");
    printf
        ("\nThe following are all of the possible attribute values for getAttributes.");
    printf("\n");
    printf("\n      OBJ_ATTR_CLASS                  = %d", 0x00000000);
    printf("\n      OBJ_ATTR_TOKEN                  = %d", 0x00000001);
    printf("\n      OBJ_ATTR_PRIVATE                = %d", 0x00000002);
    printf("\n      OBJ_ATTR_LABEL                  = %d", 0x00000003);
    printf("\n      OBJ_ATTR_KEY_TYPE               = %d", 0x00000100);
    //printf("\n      OBJ_ATTR_SENSITIVE              = %d",0x00000103);
    printf("\n      OBJ_ATTR_ENCRYPT                = %d", 0x00000104);
    printf("\n      OBJ_ATTR_DECRYPT                = %d", 0x00000105);
    printf("\n      OBJ_ATTR_WRAP                   = %d", 0x00000106);
    printf("\n      OBJ_ATTR_UNWRAP                 = %d", 0x00000107);
    printf("\n      OBJ_ATTR_SIGN                   = %d", 0x00000108);
    printf("\n      OBJ_ATTR_VERIFY                 = %d", 0x0000010A);
    printf("\n      OBJ_ATTR_DERIVE                 = %d", 0x0000010C);
    printf("\n      OBJ_ATTR_MODULUS                = %d", 0x00000120);
    printf("\n      OBJ_ATTR_MODULUS_BITS           = %d", 0x00000121);
    printf("\n      OBJ_ATTR_PUBLIC_EXPONENT        = %d", 0x00000122);
    //printf("\n      OBJ_ATTR_COEFFICIENT            = %d",0x00000128);
    //printf("\n      OBJ_ATTR_VALUE_BITS             = %d",0x00000160);
    printf("\n      OBJ_ATTR_VALUE_LEN              = %d", 0x00000161);
    printf("\n      OBJ_ATTR_EXTRACTABLE            = %d", 0x00000162);
    //printf("\n      OBJ_ATTR_LOCAL                  = %d",0x00000163);
    //printf("\n      OBJ_ATTR_NEVER_EXTRACTABLE      = %d",0x00000164);
    //printf("\n      OBJ_ATTR_ALWAYS_SENSITIVE       = %d",0x00000165);
    //printf("\n      OBJ_ATTR_MODIFIABLE             = %d",0x00000170);
    printf("\n\n");
    return 0;
}

/****************************************************************************
 *
 * FUNCTION     : insertMaskedObject
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 insertMaskedObject(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bFile = FALSE;
    char *pFile = 0;
    Uint32 ulFileLen = 0;

    Uint8 bKey = FALSE;
    Uint64 ulKey64 = 0;
    Uint32 ulKey32 = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // file
        else if ((!bFile) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1))
            bFile =
                readStringArg(argv[i + 1], &pFile, &ulFileLen, 1,
                          "masked object");
        else if ((!bKey) && (strcmp(argv[i], "-o") == 0) && (argc > i + 1)) {
            bKey = readIntegerArg(argv[i + 1], &ulKey32);
            ulKey64 = ulKey32;
        }
        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bFile) {
        printf("\n\tError: File name (-f) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nInserts a masked object.");
        printf("\n");
        printf
            ("\nSyntax: insertMaskedObject -h -f <filename>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -f  specifies the file containing the masked key");
        printf("\n           type 0 = Key/Data object");
        printf("\n       -o  request object handle (Optional)");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2InsertMaskedObject(session_handle,
                       &ulKey64, (Uint8 *) pFile, ulFileLen);
    if (ulRet == 0) {
        ulKey32 = (Uint32) ulKey64;
        printf("\tNew Key Handle: %d \n", ulKey32);
    }

    printf("\n\tCfm2InsertMaskedObject returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : extractMaskedObject
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 extractMaskedObject(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 bKey = FALSE;
    Uint64 ulKey64 = 0;
    Uint32 ulKey32 = 0;

    Uint8 bFile = FALSE;
    char *ObjFile = NULL;

    Uint8 DataBuffer[5000];
    Uint8 *pData = (Uint8 *) DataBuffer;
    Uint32 ulDataLen = 5000;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // object
        else if ((!bKey) && (strcmp(argv[i], "-o") == 0) && (argc > i + 1)) {
            bKey = readIntegerArg(argv[i + 1], &ulKey32);
            ulKey64 = ulKey32;
        } else if ((!bFile) && (strcmp(argv[i], "-out") == 0) && (argc > i + 1)) {
            ObjFile = argv[i+1];
            bFile = TRUE;
        } else
            bHelp = TRUE;

    }

    // ensure that we have all the required args
    if (!bHelp && !bKey) {
        printf("\n\tError: Object handle (-o) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bFile) {
        printf("\n\tError: Masked Object File (-out) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nExtracts a masked object.");
        printf("\n");
        printf
            ("\nSyntax: extractMaskedObject -h -o <object handle> -out <masked object file>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -o  specifies the object handle to mask");
        printf("\n       -out  specifies the file to write the masked object");
        printf("\n");

        return ulRet;
    }

    ulRet = Cfm2ExtractMaskedObject(session_handle,
                    ulKey64, (Uint8 *) pData, &ulDataLen);
    if (ulRet == 0) {
        if (WriteBinaryFile(ObjFile, (char *)pData, ulDataLen))
            printf
                ("\n\tObject was masked and written to file \"%s\" \n", ObjFile);
        else {
            ulRet = ERR_WRITE_OUTPUT_FILE;
            printf("\n\tFailed to write masked object to a file. \n");
        }
    }
    printf("\n\tCfm2ExtractMaskedObject returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : dumpItUtil
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 dumpItUtil(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;
    int i = 0;
    char *file_name = NULL;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        // output file
        else if ((file_name == NULL) && (strcmp(argv[i], "-o") == 0)
             && (argc > i + 1))
            file_name = argv[i + 1];
        else
            bHelp = TRUE;
    }

    if (bHelp) {
        printf("\nDescription:");
        printf("\n\tDumps nitrox debug information");
        printf("\n");
        printf("\nSyntax: dumpItUtil [-o <output_file>] -h");
        printf("\n");
        printf("\n\tWhere: -h  displays this information");
        printf("\n\t       -o <output_file> writes debug info to this file");
        printf
            ("\n\t If -o option is not specified, debug info will be written to ./dump.txt");
        printf("\n\n");

        return ulRet;
    }

    if (file_name == NULL)
        file_name = "./dump.txt";
    ulRet = Cfm2DumpIt(session_handle, file_name);

    printf("\n\tCfm2DumpIt returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    if (ulRet == 0)
        printf("\n\tNitrox debug data is written to %s\n", file_name);

    return ulRet;

}

/****************************************************************************
 *
 * FUNCTION     : cloneSourceStart
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 cloneSourceStart(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 part[2048] = { };
    Uint32 len = 2048;

    char *out_file = NULL;
    Uint8 bOutFile = FALSE;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        else if ((!bOutFile) && (strcmp(argv[i], "-of") == 0) && (argc > i + 1)) {
            if (argv[i + 1] != NULL) {
                out_file = argv[i + 1];
                bOutFile = TRUE;
            }
        }
        else
            bHelp = TRUE;

    }

    if (!bHelp && !bOutFile) {
        printf("\n\tError: out File name is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nFetch value for Clone Target Init");
        printf("\n");
        printf("\nSyntax: cloneSourceStart -h -of <Output_file_for_target>");
        printf("\n");
        printf("\nWhere: -h     displays this information");
        printf("\n       -of    Filename to get values from Clone Source");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2CloneSourceInit(session_handle, part, &len);
    printf("\n\tCfm2CloneSourceInit returned 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    if (ulRet == 0) {
        ulRet = write_file(out_file,part, len);
        printf("\n\tSourceInit Data is written to file %s\n", out_file);
    }
    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : cloneSourceEnd
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 cloneSourceEnd(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;

    Uint8 part1[2048] = { }, part2[2048] = { };
    Uint32 len1 = 0, len2 = 2048;
    int i = 0;

    char *in_file = NULL;
    Uint8 bInFile = FALSE;

    char *out_file = NULL;
    Uint8 bOutFile = FALSE;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        else if ((!bInFile) && (strcmp(argv[i], "-if") == 0) && (argc > i + 1)) {
            if (argv[i + 1] != NULL) {
                in_file = argv[i + 1];
                bInFile = TRUE;
            }
        } else if ((!bOutFile) && (strcmp(argv[i], "-of") == 0) && (argc > i + 1)) {
            if (argv[i + 1] != NULL) {
                out_file = argv[i + 1];
                bOutFile = TRUE;
            }
        } else
            bHelp = TRUE;
    }

    if (!bHelp && !bInFile) {
        printf("\n\tError: Input File (-if) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bOutFile) {
        printf("\n\tError: Output File (-of) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nPush Clone Target output into Clone Source");
        printf("\n");
        printf("\nSyntax: cloneSourceEnd -h -if <Input_file>  -of <Output_file>");
        printf("\n");
        printf("\nWhere: -h      displays this information");
        printf("\n       -if <file1> File from Clone Target for Clone Source");
        printf("\n       -of <file2> File from Clone Source for Clone Target");
        printf("\n");
        return ulRet;
    }

    ulRet = read_file(in_file, part1, &len1);
    if ((ulRet != 0) || (len1 == 0)) {
        printf("\n\tError: Empty file: %s\n", argv[2]);
        goto err;
    }

    ulRet = Cfm2CloneSourceStage1(session_handle, part1, len1, part2, &len2);
    printf("\n\tCfm2CloneSourceStage returned 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    if (ulRet == 0) {
        ulRet = write_file(out_file, part2, len2);
        printf("\n\tSourceStage1 Data is written to file %s\n", out_file);
    }

err:
    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : cloneTargetStart
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 cloneTargetStart(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;

    Uint8 part1[2048] = { }, part2[2048] = {
    };
    Uint32 len1 = 0, len2 = 2048;

    char *out_file = NULL;
    Uint8 bOutFile = FALSE;

    char *in_file = NULL;
    Uint8 bInFile = FALSE;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        else if ((!bInFile) && (strcmp(argv[i], "-if") == 0) && (argc > i + 1)) {
            if (argv[i + 1] != NULL) {
                in_file = argv[i + 1];
                bInFile = TRUE;
            }
        }
        else if ((!bOutFile) && (strcmp(argv[i], "-of") == 0) && (argc > i + 1)) {
            if (argv[i + 1] != NULL) {
                out_file = argv[i + 1];
                bOutFile = TRUE;
            }
        }
        else
            bHelp = TRUE;
    }

    if (!bHelp && !bInFile) {
        printf("\n\tError: In File name is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bOutFile) {
        printf("\n\tError: out File name is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nPush Clone Source output into Clone Target");
        printf("\n");
        printf("\nSyntax: cloneTargetStart -h -if <Input_file> -of <Output_file>");
        printf("\n");
        printf("\nWhere: -h     displays this information");
        printf("\n       -if    File from Clone Source for Clone Target");
        printf("\n       -of    File from Clone Target for Clone Source");
        printf("\n");
        return ulRet;
    }

    ulRet = read_file(in_file, part1, &len1);
    if ((ulRet != 0) || (len1 == 0)) {
        printf("\n\tError: Empty file: %s\n", in_file);
        goto err;
    }

    ulRet = Cfm2CloneTargetInit(session_handle, part1, len1, part2, &len2);
    printf("\n\tCfm2CloneTargetInit returned 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    if (ulRet == 0) {
        ulRet = write_file(out_file, part2, len2);
        printf("\n\tTargetInit Data is written to file %s\n", out_file);
    }

err:
    return ulRet;
}

/****************************************************************************
 *
 * FUNCTION     : cloneTargetEnd
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 cloneTargetEnd(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;

    Uint8 part[2048] = { };
    Uint32 len = 0;
    int i = 0;
    Uint8 ptr[] = "dummy ptr";
    Uint32 dummy_len = 0;

    char *in_file = NULL;
    Uint8 bInFile = FALSE;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        else if ((!bInFile) && (strcmp(argv[i], "-if") == 0) && (argc > i + 1)) {
            if (argv[i + 1] != NULL) {
                in_file = argv[i + 1];
                bInFile = TRUE;
            }
        } else
            bHelp = TRUE;
    }

    if(!bHelp && !bInFile){
        printf("\n\tError: input file name is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nFetch value for Clone Target End");
        printf("\n");
        printf("\nSyntax: cloneTargetEnd -h -if <Input_file_for_target>");
        printf("\n");
        printf("\nWhere: -h     displays this information");
        printf("\n       -if <file> Filename to get values from Clone Source");
        printf("\n");
        return ulRet;
    }

    ulRet = read_file(in_file, part, &len);
    if ((ulRet != 0) || (len == 0)) {
        printf("\n\tError: Empty file: %s\n", in_file);
        goto err;
    }

    ulRet = Cfm2CloneTargetStage1(session_handle, part, len, ptr, &dummy_len);
    printf("\n\tCfm2CloneTargetStage returned 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));

err:
    return ulRet;
}

#ifdef NIC_ENABLE
/****************************************************************************
 * *
 * * FUNCTION     : setNetConfig
 * *
 * * DESCRIPTION  :
 * *
 * * PARAMETERS   :
 * *
 * *****************************************************************************/
Uint32 setNetConfig(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;

    Uint8 bHelp = FALSE;

    Uint8 bIPAddr = FALSE;
    char *pIPAddress = 0;
    Uint32 ulIPAddress = 0;

    Uint8 bNetMask = FALSE;
    char *pNetMask = 0;
    Uint32 ulNetMask = 0;

    Uint8 bGateway = FALSE;
    char *pGateway = 0;
    Uint32 ulGateway = 0;

    Uint8 bInterface = FALSE;
    char *pInterface = 0;
    Uint32 ulInterface = 0;
    int valInterface = -1;

    for (i = 2; i < argc; i = i + 2) {
        // Help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // IP Address
        else if ((!bIPAddr) && (strcmp(argv[i], "-i") == 0) && (argc > i + 1))
            bIPAddr =
                readArgAsString(argv[i + 1], &pIPAddress, &ulIPAddress);

        // NetMask
        else if ((!bNetMask) && (strcmp(argv[i], "-n") == 0)
             && (argc > i + 1))
            bNetMask =
                readArgAsString(argv[i + 1], &pNetMask, &ulNetMask);

        // Gateway
        else if ((!bGateway) && (strcmp(argv[i], "-g") == 0)
             && (argc > i + 1))
            bGateway =
                readArgAsString(argv[i + 1], &pGateway, &ulGateway);

        // Interface
        else if ((!bInterface) && (strcmp(argv[i], "-e") == 0)
             && (argc > i + 1))
            bInterface =
                readArgAsString(argv[i + 1], &pInterface, &ulInterface);
        else
            bHelp = TRUE;

        if (bInterface) {
            if (!strcmp(pInterface, "0"))
                valInterface = 0;
            else if (!strcmp(pInterface, "1"))
                valInterface = 1;
            else
                bInterface = FALSE;
        }
    }

    if (!bHelp && !bIPAddr) {
        printf("\n\tError: IP Address not provided.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bNetMask) {
        printf("\n\tError: NetMask is not provided.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bGateway) {
        printf("\n\tError: Gateway is not provided.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bInterface) {
        printf("\n\tError: Interface is not provided.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf
            ("\nSet the Network interface details for the HSM NIC interface.");
        printf("\n");
        printf
            ("\nSyntax: setNetConfig -h -i <IP Address> -n <NetMask> -g <Gateway> -e <Interface 0/1>");
        printf("\n");
        printf("\nWhere: -h   displays this information");
        printf("\n       -i   specifies the IP Address");
        printf("\n       -n   specifies the NetMask");
        printf("\n       -g   specifies the Gateway");
        printf("\n       -e   specifies the Interface (eth0 OR eth1)");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2SetNetConfig(session_handle,
                 (Uint8 *) pIPAddress,
                 (Uint8 *) pNetMask,
                 (Uint8 *) pGateway, valInterface);
    if (ulRet) {
        printf("\n\tSetting Network details failed: 0x%02x : %s\n",
               ulRet, Cfm2ResultAsString(ulRet));
        return -1;
    }
    printf("\n\tSetting Network details returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
    return ulRet;
}
#endif



/****************************************************************************
 *
 * FUNCTION     : storeUserFixedKey
 *
 * DESCRIPTION  :
 *
 * PARAMETERS   :
 *
 *****************************************************************************/
Uint32 storeUserFixedKey(int argc, char** argv)
{
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;
    int i = 0;

    Uint8* fixed_key = NULL;
    Uint32 fixed_key_len = 0;
    Uint8* enc_key = NULL;
    Uint32 enc_key_len = 0;

    Int8  *fixed_key_file= NULL;
    Uint8  bFile = FALSE;

    EVP_PKEY *pkey = NULL;
    EVP_PKEY *pub_key = NULL;

    Uint8   cert_buf[4096] = {};
    Uint32  cert_len = 4096;
    CertSubject cert_subject = HSM_CERT;

    BIO   *bio_cert = BIO_new(BIO_s_mem());
    X509  *hsm_cert = NULL;
    Uint32 key_type = KEY_TYPE_AES;


    for (i = 2; i < argc; i=i+2)
    {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // private key
        else if ((!bFile ) && (strcmp(argv[i],"-f")==0) && (argc > i+1))
        {
            fixed_key_file = argv[i+1];
            bFile = TRUE;
        }
        else
            bHelp = TRUE;
    }

    // ensure that we have all the required args
    if (!bHelp && !bFile )
    {
        printf("\n\tError: User fixed key file is missing. \n");
        bHelp = TRUE;
    }


    if (bHelp)
    {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nStores User fixed key");
        printf("\n");
        printf("\nSyntax: storeUserFixedKey -f <file having fixed key >");
        printf("\n");
        printf("\nWhere: -h    displays this information");
        printf("\n       -f    specifies the fixed key file ");
        printf("\n");
        return ulRet;
    }

    if(bFile && fixed_key_file)
    {
        struct stat fileStat;
        if (stat( fixed_key_file, &fileStat ))
        {
            printf("File %s not found\n", fixed_key_file);
            goto err;
        }

        fixed_key_len = fileStat.st_size;
        if(fixed_key_len != 32) {
            printf("\n\tError: File: %s is not of 32 bytes.\n", fixed_key_file);
            printf("\n\tError: Fixed key file should be exactly 32 bytes.\n");
            ulRet = ERR_FILE_WRONG_SIZE;
            goto err;
        }
        fixed_key = (Uint8*) calloc(fixed_key_len,1);
        if(!fixed_key){
            printf("\n\tMemory alloc failure : \n");
            ulRet = ERR_MEMORY_ALLOC_FAILURE;
            goto err;
        }

        ulRet = read_file(fixed_key_file, fixed_key, &fixed_key_len);
        if ((ulRet != 0) || (fixed_key_len== 0)) {
            printf("\n\tError: Empty file: %s\n", fixed_key_file);
            ulRet = ERR_OPEN_FILE;
            goto err;
        }
    }

    /* Get the HSM certificate, encyrpt with the certificate pub key */
    ulRet = Cfm2GetCert(session_handle, cert_subject, cert_buf, &cert_len);

    if(RET_OK != ulRet)
    {
        print_error("Cfm2GetCert failed with error 0x%02x: %s\n", ulRet, Cfm2ResultAsString(ulRet));
        goto err;
    }

    BIO_write(bio_cert, (const char*) cert_buf, cert_len);

    hsm_cert = PEM_read_bio_X509(bio_cert, NULL, 0, NULL);
    pub_key = X509_get_pubkey(hsm_cert);

    if (pub_key) {
        /* Do encrypt the Cavium fixed key with the same and then sign it */
        RSA *rsa =  EVP_PKEY_get1_RSA(pub_key);

        enc_key_len = RSA_size(rsa);
        enc_key = (Uint8*) calloc(enc_key_len,1);
        if(!enc_key){
            printf("\n\tMemory alloc failure : \n");
            ulRet = ERR_MEMORY_ALLOC_FAILURE;
            goto err;
        }

        enc_key_len = RSA_public_encrypt(fixed_key_len, fixed_key, enc_key, rsa, RSA_PKCS1_PADDING);

    }

    ulRet = Cfm2StoreKBKShare(session_handle, key_type, fixed_key_len,
                  3, NULL, 0, enc_key, enc_key_len, NULL, 0);

    if (ulRet != RET_OK) {
        printf("\n\t Cfm2StoreCaviumFixedKey failed with return code 0x%02x : %s\n", ulRet, Cfm2ResultAsString(ulRet));
    }
    printf("\n\tCfm2StoreCaviumFixedKey returned: 0x%02x : %s\n",
           ulRet, Cfm2ResultAsString(ulRet));
err:
    if (pkey) EVP_PKEY_free(pkey);
    if (pub_key) EVP_PKEY_free(pub_key);
    if (hsm_cert) X509_free(hsm_cert);

    return ulRet;
}


Uint32 backupPartition(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;
    MxNAuth *pAuth = NULL;
    Uint32 ulMValue = 1;

    Uint8 bMech = FALSE;
    char ulMech = -1;

    Uint8 bPath = FALSE;
    char *pPath = NULL;

    Uint32 config_len = 0;
    Uint8 config[8000] = { };

    Uint32 no_of_users = 0;
    Uint32 enc_user_len = 0;
    Uint8 enc_user_info[8000] = { };

    Uint32 no_of_officers = 0;
    Uint32 enc_officer_len = 0;
    Uint8 enc_officer_info[4096] = { };
    PartitionInfo info = { };

    Uint32 key_info_len = 0;
    Uint8 key_info[4096] = { };

    Uint64 *pulKeyArray = 0;
    Uint32 ulKeyArrayLen = 0;
    Uint32 total_keys = 0;
    int j = 0, found = 0;

#ifdef CLOUD_HSM_CLIENT
#if defined(BACKUP_WITH_SMARTCARD) || defined(USE_SMARTCARD)
    Uint8  bDualFactorHost   = FALSE;
    char*  pDualFactorHost   = 0;
    Uint32 ulDualFactorHost  = 0;

    Uint8  bDualFactorPort   = FALSE;
    Uint32 pDualFactorPort   = 0;

    Uint8  bDualFactorCert   = FALSE;
    char*  pDualFactorCert   = 0;

    Uint8  bDualFactorSlot   = FALSE;
    Uint32 pDualFactorSlot   = 0;

    Uint8  bDualFactorPin   = FALSE;
    char*  pDualFactorPin   = 0;
    Uint32 ulDualFactorPin  = 0;

    Uint8  bDualFactorToken   = FALSE;
    char*  pDualFactorToken   = 0;
    Uint32 ulDualFactorToken  = 0;

    Uint8  bDualFactorUserPin   = FALSE;
    char*  pDualFactorUserPin   = 0;
    Uint32 ulDualFactorUserPin  = 0;

    SSL_CTX *ctx = NULL;
    int server = 0;
    SSL *ssl = NULL;
    df_response resp_data;
    df_request req_data;
#endif
#endif

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        // storage directory path for backup data
        else if ((!bPath) && (strcmp(argv[i], "-d") == 0) && (argc > i + 1)) {
            pPath = argv[i + 1];
            bPath = TRUE;
        } else if ((strcmp(argv[i], "-m") == 0) && (argc > i + 1)) {
            bMech = TRUE;
            ulMech = atoi(argv[i + 1]);
        }
#ifdef CLOUD_HSM_CLIENT
#if defined(BACKUP_WITH_SMARTCARD) || defined(USE_SMARTCARD)
        //  dual factor authentication hostname
        else if ((!bDualFactorHost ) && (strcmp(argv[i],"-dh")==0) && (argc > i+1))
            bDualFactorHost = readArgAsString(argv[i+1], &pDualFactorHost, &ulDualFactorHost);

        //  dual factor authentication port
        else if ((!bDualFactorPort ) && (strcmp(argv[i],"-dp")==0) && (argc > i+1))
            bDualFactorPort = readIntegerArg(argv[i+1], &pDualFactorPort);

        //  dual factor authentication server certificate path
        else if ((!bDualFactorCert ) && (strcmp(argv[i],"-dc")==0) && (argc > i+1)) {
            pDualFactorCert = argv[i+1];
            bDualFactorCert = TRUE;
        }
        //  dual factor authentication token slot
        else if ((!bDualFactorSlot ) && (strcmp(argv[i],"-dn")==0) && (argc > i+1))
        {
            bDualFactorSlot = readIntegerArg(argv[i+1], &pDualFactorSlot);
        }
        //  dual factor authentication SO pin
        else if ((!bDualFactorPin ) && (strcmp(argv[i],"-ds")==0) && (argc > i+1))
        {
            bDualFactorPin = readArgAsString(argv[i+1], &pDualFactorPin, &ulDualFactorPin);
        }
        //  dual factor authentication token label
        else if ((!bDualFactorToken ) && (strcmp(argv[i],"-dt")==0) && (argc > i+1))
        {
            bDualFactorToken = readArgAsString(argv[i+1], &pDualFactorToken, &ulDualFactorToken);
        }
        //  dual factor authentication user pin
        else if ((!bDualFactorUserPin ) && (strcmp(argv[i],"-du")==0) && (argc > i+1))
        {
            bDualFactorUserPin = readArgAsString(argv[i+1], &pDualFactorUserPin, &ulDualFactorUserPin);
        }
#endif
#endif
        else
            bHelp = TRUE;
    }

    if (!bHelp && !pPath) {
        printf("\n\tError: Backup Directory path (-d) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bMech) {
        printf("\n\tError: Invaid KBK wrap mechanism.\n");
        bHelp = TRUE;
    }

#ifdef CLOUD_HSM_CLIENT
#if defined(BACKUP_WITH_SMARTCARD) || defined(USE_SMARTCARD)
    if (bDualFactorHost || bDualFactorPort ||  bDualFactorCert || bDualFactorSlot || bDualFactorPin || bDualFactorToken || bDualFactorUserPin) {
        if ( ! (bDualFactorHost && bDualFactorPort &&  bDualFactorCert && bDualFactorSlot &&
            bDualFactorPin && bDualFactorToken && bDualFactorUserPin)) {
            printf("\n\tError: For network dual factor authentication -dh,-dp,-dc,-dn,-ds,-dt and -du options must be specified.\n");
            bHelp = TRUE;
        }
    }
#endif
#endif

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\n");
        printf("\nBackup the Partition's configuration and user details ");
        printf("\n");
        printf("\nSyntax:backupPartition -d <dir>");
#ifdef CLOUD_HSM_CLIENT
        printf(" [-dh <host> -dp <port> -dc <cert> -dn <slot> -ds <so pin> -dt <token label> du <user pin>]");
#endif

        printf("\n");
        printf("\nWhere: -h  displays this information    \n ");
        printf("\n       -d  Absolute directory to store backup files");
        printf("\n       -m  KBK wrap mechanism ");
        printf("\n            KBK_WRAP_WITH_RSA - 0 ");
        printf("\n            KBK_WRAP_WITH_KEK - 1 ");
        printf("\n            KBK_USING_PRE_SHARED_KEYS - 2 ");
        printf("\n            KBK_WRAP_WITH_CERT_AUTH_DERIVED_KEY - 3 ");
#ifdef CLOUD_HSM_CLIENT
        printf("\n       -dh specifies the hostname for network dual factor authentication server");
        printf("\n       -dp specifies the port used by network dual factor authentication server");
        printf("\n       -dc specifies the certificate used by network dual factor authentication server");
        printf("\n       -dn specifies the slot number for SmartCard in network dual factor authentication server");
        printf("\n       -ds specifies the so pin for SmartCard in network dual factor authentication server");
        printf("\n       -dt specifies the token label for SmartCard in network dual factor authentication server");
        printf("\n       -du specifies the user pin for SmartCard in network dual factor authentication server");
#endif

        printf("\n");
        return ulRet;
    }
    ulMValue = 1;
    printf("\n\tCurrent M Value is %u\n", ulMValue);
    if ((ulMValue <= 0)) {
        printf(" Obtained Invalid  M value \n");
        return -1;
    } else if (ulMValue > 1) {
        ulRet = prepare_mxn_user_details(session_handle, ulMValue, &pAuth);
        if (ulRet != 0) {
            printf("Failed to encrypt MxN Auth details\n");
            return ulRet;
        }
    }

#if defined(BACKUP_WITH_SMARTCARD) || defined(USE_SMARTCARD)
    if(KBK_WRAP_WITH_RSA == ulMech)
    {
#ifndef CLOUD_HSM_CLIENT
        ulRet = initSmartCard(1);
        if (ulRet != RET_OK) {
            printf("\n\tFailed to initialize Smart Card\n");
            shutdownSmartCard();
            return ulRet;
        }
#else
        if(bDualFactorHost) {
            printf("\n\tPlease ensure dual factor authentication server is running\n");

            SSL_library_init();
            ctx = InitSSL_CTX();

            if(!SSL_CTX_load_verify_locations(ctx, pDualFactorCert, NULL)){
                ulRet = -1;
                printf("\n\tCould not load the certificate trust chain\n");
                if(ssl) SSL_free(ssl);
                if(server) close(server);
                if(ctx) SSL_CTX_free(ctx);
                return ulRet;
            }

            SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);
            server = OpenSocketConnection(pDualFactorHost, pDualFactorPort);
            ssl = SSL_new(ctx);
            SSL_set_fd(ssl, server);

            ulRet =  SSL_connect(ssl);
            if ( ulRet == -1 ) {
                printf("\n\nSSL connect failed\n");
                if(ssl) SSL_free(ssl);
                if(server) close(server);
                if(ctx) SSL_CTX_free(ctx);
                return ulRet;
            }

            req_data.command_type = DF_INITIALIZE;
            req_data.init_data.reinitialize = 1;
            req_data.init_data.slot = pDualFactorSlot;
            memcpy(req_data.init_data.SOPin,pDualFactorPin,ulDualFactorPin+1);
            memcpy(req_data.init_data.tokenLabel,pDualFactorToken,ulDualFactorToken+1);
            memcpy(req_data.init_data.userPin,pDualFactorUserPin,ulDualFactorUserPin+1);

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code != 0) {
                    printf("\n\tFailed to get initialize SmartCard over network\n");
                    ulRet = -1;
                    memset(&req_data,0,sizeof(req_data));
                    req_data.command_type = DF_END;
                    SSL_write(ssl, &req_data, sizeof(req_data));
                    if(ssl) SSL_free(ssl);
                    if(server) close(server);
                    if(ctx) SSL_CTX_free(ctx);
                    return ulRet;
                }
            } else {
                printf("\n\tSSL_read failed\n");
                ulRet = -1;
                memset(&req_data,0,sizeof(req_data));
                req_data.command_type = DF_END;
                SSL_write(ssl, &req_data, sizeof(req_data));
                if(ssl) SSL_free(ssl);
                if(server) close(server);
                if(ctx) SSL_CTX_free(ctx);
                return ulRet;
            }
        } else {
            ulRet = initSmartCard(1);
            if (ulRet != RET_OK) {
                printf("\n\tFailed to initialize Smart Card\n");
                shutdownSmartCard();
                return ulRet;
            }
        }
#endif
    }
#endif

    ulRet =
        Cfm2GetPartitionInfo(session_handle, (Uint8 *) partition_name,
                     strlen(partition_name), &info);
    if (ulRet != 0) {
        printf("\n\tCfm2GetPartitionInfo returned: 0x%02x : %s\n",
               ulRet, Cfm2ResultAsString(ulRet));
        return ulRet;
    }

    if(KBK_USING_PRE_SHARED_KEYS == ulMech)
        ulRet = Cfm2BackupBegin(session_handle, 0, 0, NULL, 0, 1, pAuth);
    else
        ulRet = Cfm2BackupBegin(session_handle, 0, 0, NULL, 0, 0, pAuth);

    if (ulRet) {
        printf("\n\tCfm2BackupBegin failed with error: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        printf("\n\tPlease ensure required fixed keys are inserted.\n");
        return ulRet;
    }

    if (KBK_WRAP_WITH_RSA == ulMech) {
#ifndef CLOUD_HSM_CLIENT
        ulRet = ImportKBKtoSC(PARTITION_INDEX(session_handle), partition_name);
#else
#if defined(BACKUP_WITH_SMARTCARD) || defined(USE_SMARTCARD)
        if(bDualFactorHost)
            ulRet = ImportKBKtoSC2(PARTITION_INDEX(session_handle), partition_name, pDualFactorUserPin, ssl);
        else
#endif
            ulRet = ImportKBKtoSC(PARTITION_INDEX(session_handle), partition_name);
#endif
    } else if((KBK_WRAP_WITH_KEK == ulMech) || (KBK_WRAP_WITH_CERT_AUTH_DERIVED_KEY == ulMech)) {
        ulRet = ImportKBKtoHost(pPath, PARTITION_INDEX(session_handle), ulMech);
    }

    if (ulRet) {
        printf("\n\tImporting KBK onto SC returned: 0x%02x : %s\n",
               ulRet, Cfm2ResultAsString(ulRet));
        goto error;
    }

    /* Backing Up the Current Partition Configuration */
    ulRet = Cfm2BackupConfig(session_handle, 0, config, &config_len);
    if (ulRet != 0) {
        printf("\n\tBackup Partition configuration Failure: 0x%02x : %s\n",
               ulRet, Cfm2ResultAsString(ulRet));
        goto error;
    }
    printf("\n\tCfm2BackupConfig returned: 0x%02x : %s\n", ulRet,
           Cfm2ResultAsString(ulRet));

    ulRet = SaveConfig(config, config_len,
               0, pPath, PARTITION_INDEX(session_handle),
               betoh32(info.MaxUserKeys),
               betoh32(info.MaxSSLContexts), info.MaxAcclrDevs, NULL);
    if (ulRet) {
        printf("\n\tFailed to Save the Partition configuration\n");
        goto error;
    }

    /* Backing Up the Partition Crypto Officers */
    enc_officer_len = sizeof(enc_officer_info);
    ulRet = Cfm2BackupUsers(session_handle,
                0,
                enc_officer_info, &enc_officer_len,
                &no_of_officers, CN_CRYPTO_OFFICER);
    if (ulRet == RET_NO_USERS_TO_BACKUP) {
        printf("\n\tCfm2BackupUsers for COs returns:  0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
    } else if (ulRet != 0) {
        printf("\n\tCfm2BackupUsers for COs failure :  0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        goto error;
    } else
        printf("\n\tCfm2BackupUsers for COs returned: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));

    ulRet = SaveUserInfo(enc_officer_info, enc_officer_len,
                 no_of_officers, 0, CN_CRYPTO_OFFICER, 0, pPath, 0);
    if (ulRet)
        printf("\n\tFailed to Save the Partition Crypto Officers \n");

    /* Backing Up the Partition Crypto Users */
    enc_user_len = sizeof(enc_user_info);
    ulRet = Cfm2BackupUsers(session_handle,
                0,
                enc_user_info, &enc_user_len,
                &no_of_users, CN_CRYPTO_USER);
    if (ulRet == RET_NO_USERS_TO_BACKUP) {
        printf("\n\tCfm2BackupUsers for CUs returns:  0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
    } else if (RET_OK == ulRet) {
        printf("\n\tCfm2BackupUsers for CUs returned: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        ulRet = SaveUserInfo(enc_user_info, enc_user_len,
                     no_of_users, 0, CN_CRYPTO_USER, 0, pPath, 0);
        if (ulRet)
            printf("\n\tFailed to Save the Partition Crypto Users \n");
    } else if (RET_RESULT_SIZE == ulRet) {
        /* Handle the case of backup users where the response fits in more than
         * one fragment */
        Uint32  user_idx = 0;/*user index starts from 0 */
        int    current_user_idx = user_idx;
        int    iter = 0;

        while(1) {
            current_user_idx = user_idx;
            enc_user_len = sizeof(enc_user_info);
            memset(enc_user_info, 0, enc_user_len);
            ulRet = Cfm2BackupUsersFromIndex(session_handle,
                             0,
                             &user_idx,
                             enc_user_info, &enc_user_len,
                             &no_of_users,
                             CN_CRYPTO_USER);

            if (RET_OK == ulRet) {
                printf("Backing up users from %d to %d\n", current_user_idx, user_idx);
                ulRet = SaveUserInfo(enc_user_info, enc_user_len,
                             no_of_users, current_user_idx,
                             CN_CRYPTO_USER, 0, pPath, 0);
                if (ulRet)
                    printf("\n\tFailed to Save the Partition CUs \n");
            }
            else
                break;

            iter++;
            if (!user_idx)
                break;
        }

        if (RET_OK != ulRet) {
            printf("Cfm2BackupHSMUsersFromIndex has failed \n");
            goto error;
        }
    } else if (ulRet != 0) {
        printf("\n\tCfm2BackupUsers for CUs failure :  0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        goto error;
    } else
        printf("\n\tCfm2BackupUsers for CUs returned: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));

    /* Backing Up the Partition's Appliance Users */
    enc_user_len = sizeof(enc_user_info);
    ulRet = Cfm2BackupUsers(session_handle,
                0,
                enc_user_info, &enc_user_len,
                &no_of_users, CN_APPLIANCE_USER);
    if (ulRet == RET_NO_USERS_TO_BACKUP) {
        printf("\n\tCfm2BackupUsers for AUs returns:  0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
    } else if (RET_OK == ulRet) {
        printf("\n\tCfm2BackupUsers for AUs returned: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        ulRet = SaveUserInfo(enc_user_info, enc_user_len,
                     no_of_users, 0, CN_APPLIANCE_USER, 0, pPath, 0);
        if (ulRet)
            printf("\n\tFailed to Save the Partition Crypto Users \n");
    } else if (RET_RESULT_SIZE == ulRet) {
        /* Handle the case of backup users where the response fits in more than
         * one fragment */
        Uint32  user_idx = 0;/*user index starts from 0 */
        int    current_user_idx = user_idx;
        int    iter = 0;

        while(1) {
            current_user_idx = user_idx;
            enc_user_len = sizeof(enc_user_info);
            memset(enc_user_info, 0, enc_user_len);
            ulRet = Cfm2BackupUsersFromIndex(session_handle,
                             0,
                             &user_idx,
                             enc_user_info, &enc_user_len,
                             &no_of_users,
                             CN_APPLIANCE_USER);

            if (RET_OK == ulRet) {
                printf("Backing up users from %d to %d\n", current_user_idx, user_idx);
                ulRet = SaveUserInfo(enc_user_info, enc_user_len,
                             no_of_users, current_user_idx,
                             CN_APPLIANCE_USER, 0, pPath, 0);
                if (ulRet)
                    printf("\n\tFailed to Save the Partition AUs \n");
            }
            else
                break;

            iter++;
            if (!user_idx)
                break;
        }
        if (RET_OK != ulRet) {
            printf("Cfm2BackupUsersFromIndex has failed \n");
            goto error;
        }
    } else if (ulRet != 0) {
        printf("\n\tWarn!! Cfm2BackupUsers for AUs failure :  0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        //goto error;
    } else
        printf("\n\tCfm2BackupUsers for AUs returned: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));

    /* Now backup all keys from the partition before doing backup end */

    /* Find the keys belonging to the partition */
    ulRet = Cfm2FindKey(session_handle, 0,
                -1, NULL, 0, NULL, 0, NULL, 0, NULL, &total_keys);

    if (ulRet == 0 || ulRet == RET_RESULT_SIZE) {
        printf("Number of Keys found %d\n", total_keys);
    } else {
        goto error;
    }

    if (!total_keys) {
        printf("\n\tNo keys are found \n");
    }
    // Allocate Key Handle(s) Array
    pulKeyArray = (Uint64 *) calloc(MAX_SEARCH_OBJECTS, sizeof(Uint64));
    if (pulKeyArray == 0) {
        ulRet = ERR_MEMORY_ALLOC_FAILURE;
        goto error;
    }

    while (found < total_keys) {
        ulKeyArrayLen = MAX_SEARCH_OBJECTS;
        // Find All Keys of Key Class, Type and Label
        ulRet = Cfm2FindKeyFromIndex(session_handle,
                         0,
                         j,
                         -1,
                         NULL,
                         0,
                         NULL,
                         0, NULL, 0, pulKeyArray, &ulKeyArrayLen);
        // Print Key Handle Array
        //HexPrint((Uint8*)pKeyArray, ulKeyArrayLen*8);
        if (ulRet == 0 || ulRet == RET_RESULT_SIZE) {
            if (ulKeyArrayLen != 0) {
                Uint32 ulValue = 0;
                printf("number of keys matched from start index %d::%d\n", j,
                       ulKeyArrayLen);
                for (i = 0; i < ulKeyArrayLen - 1; i++) {
                    ulValue = (Uint32) (pulKeyArray[i]);
                    printf("%d, ", ulValue);
                }
                ulValue = (Uint32) (pulKeyArray[i]);
                printf("%d\n", ulValue);
                found += ulKeyArrayLen;
                j = ulValue;    /* next start index will be the last key found in the previous iteration */

                if(found != total_keys)
                    found--;

                for ( i=0; i<ulKeyArrayLen-1; i++)
                {
                    memset(key_info, 0, 4096);
                    ulRet = Cfm2BackupKey(session_handle,0,
                                  pulKeyArray[i],
                                  key_info, &key_info_len);
                    printf("\n\tCfm2BackupKeyStore returned: 0x%02x : %s\n", ulRet, Cfm2ResultAsString(ulRet));

                    if(RET_OK == ulRet)
                        ulRet = SaveKeyInfo(pPath, key_info, key_info_len, pulKeyArray[i],
                                    PARTITION_INDEX(session_handle), 0);
                    if(ulRet)
                        printf("\n\tFailed to Save Key Data \n");
                }

                /* In the search the last key can not repeat, so backup the last key*/
                if(found == total_keys)
                {
                    memset(key_info, 0, 4096);
                    ulRet = Cfm2BackupKey(session_handle,0,
                                  pulKeyArray[i],
                                  key_info, &key_info_len);
                    printf("\n\tCfm2BackupKeyStore returned: 0x%02x : %s\n", ulRet, Cfm2ResultAsString(ulRet));

                    if(RET_OK == ulRet)
                        ulRet = SaveKeyInfo(pPath, key_info, key_info_len, pulKeyArray[i],
                                    PARTITION_INDEX(session_handle), 0);
                    if(ulRet)
                        printf("\n\tFailed to Save Key Data \n");
                }

            }
            else
            {
                printf("\n\tNo more keys are found\n");
                break;
            }
        } else {
            printf("\n\tFailed to fetch the keys \n");
            break;
        }

    }

    if (ulRet != RET_OK) {
        printf("\nCfm2FindKey3 failed with error:%x", ulRet);
        goto error;
    }

error:
    ulRet = Cfm2BackupEnd(session_handle, 0, 0);
    if (ulRet) {
        printf("\n\tCfm2BackupEnd failed: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
    }

#if defined(BACKUP_WITH_SMARTCARD) || defined(USE_SMARTCARD)
    if(KBK_WRAP_WITH_RSA == ulMech) {
#ifndef CLOUD_HSM_CLIENT
        shutdownSmartCard();
#else
        if(!bDualFactorHost) {
            shutdownSmartCard();
        } else {
            memset(&req_data,0,sizeof(req_data));
            req_data.command_type = DF_END;
            SSL_write(ssl, &req_data, sizeof(req_data));
            if(ssl) SSL_free(ssl);
            if(server) close(server);
            if(ctx) SSL_CTX_free(ctx);
        }
#endif
    }
#endif

    if(pulKeyArray) free(pulKeyArray);
    return ulRet;
}


Uint32 restorePartition(int argc, char **argv)
{
    Uint32 ulRet = 0;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;
    MxNAuth *pAuth = NULL;

    Uint8 bPath = FALSE;
    char *pPath = NULL;

    Uint8 bFile = FALSE;
    char *pData = NULL;
    Uint32 ulDataLen = -1;

    Uint8 bFlag = FALSE;
    Uint32 ulFlag = 0;

    Uint8 bLabel = FALSE;
    Uint32 ulLabelLen = 0;
    char *pLabel = NULL;

    Uint8 bName = FALSE;
    Uint32 ulNameLen = 0;
    char *pName = NULL;

    Uint8 bMech = FALSE;
    char ulMech = -1;

    Uint32 config_len = 0;
    Uint8 config[8000] = { };

    Uint32 no_of_users = 0;
    Uint32 enc_user_len = 0;
    Uint8 enc_user_info[4096] = { };

    Uint32 no_of_officers = 0;
    Uint32 enc_officer_len = 0;
    Uint8 enc_officer_info[4096] = { };

    DIR *dir = NULL;
    struct dirent entry = { };
    struct dirent *result = NULL;

    char  file_name[255  + 1] = { };
    Uint32  file_name_common_len = 0;

    Uint32 key_info_len = 0;
    Uint8 key_info[4096] = { };
    Uint64  KeyHandle       = 0;

    Uint32  user_index = 0;
    Uint32  key_index = 0;
    Uint32  max_keys = 0;
    char    **key_files = NULL;

#ifdef CLOUD_HSM_CLIENT
#if defined(BACKUP_WITH_SMARTCARD) || defined(USE_SMARTCARD)
    Uint8  bDualFactorHost   = FALSE;
    char*  pDualFactorHost   = 0;
    Uint32 ulDualFactorHost  = 0;

    Uint8  bDualFactorPort   = FALSE;
    Uint32 pDualFactorPort   = 0;

    Uint8  bDualFactorCert   = FALSE;
    char*  pDualFactorCert   = 0;

    Uint8  bDualFactorSlot   = FALSE;
    Uint32 pDualFactorSlot   = 0;

    Uint8  bDualFactorUserPin   = FALSE;
    char*  pDualFactorUserPin   = 0;
    Uint32 ulDualFactorUserPin  = 0;

    SSL_CTX *ctx = NULL;
    int server = 0;
    SSL *ssl = NULL;
    df_response resp_data;
    df_request req_data;
#endif
#endif

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        // storage directory path for backup data
        else if ((!bPath) && (strcmp(argv[i], "-d") == 0) && (argc > i + 1)) {
            pPath = argv[i + 1];
            bPath = TRUE;
        } else if ((!bFile) && (strcmp(argv[i], "-kf") == 0)
               && (argc > i + 1)) {
            bFile =
                readStringArg(argv[i + 1], (char **)&pData, &ulDataLen, 1,
                          "");
        } else if ((strcmp(argv[i], "-m") == 0) && (argc > i + 1)) {
            bMech = TRUE;
            ulMech = atoi(argv[i + 1]);
        } else if ((!bLabel) && (strcmp(argv[i], "-l") == 0)
               && (argc > i + 1))
            bLabel = readArgAsString(argv[i + 1], &pLabel, &ulLabelLen);
        else if ((!bName) && (strcmp(argv[i], "-n") == 0)
             && (argc > i + 1))
            bName = readArgAsString(argv[i + 1], &pName, &ulNameLen);
        else if ((!bFlag) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1))
            bFlag = readIntegerArg(argv[i + 1], &ulFlag);
#ifdef CLOUD_HSM_CLIENT
#if defined(BACKUP_WITH_SMARTCARD) || defined(USE_SMARTCARD)
        //  dual factor authentication hostname
        else if ((!bDualFactorHost ) && (strcmp(argv[i],"-dh")==0) && (argc > i+1))
            bDualFactorHost = readArgAsString(argv[i+1], &pDualFactorHost, &ulDualFactorHost);

        //  dual factor authentication port
        else if ((!bDualFactorPort ) && (strcmp(argv[i],"-dp")==0) && (argc > i+1))
            bDualFactorPort = readIntegerArg(argv[i+1], &pDualFactorPort);

        //  dual factor authentication server certificate path
        else if ((!bDualFactorCert ) && (strcmp(argv[i],"-dc")==0) && (argc > i+1)) {
            pDualFactorCert = argv[i+1];
            bDualFactorCert = TRUE;
        }
        //  dual factor authentication token slot
        else if ((!bDualFactorSlot ) && (strcmp(argv[i],"-dn")==0) && (argc > i+1))
        {
            bDualFactorSlot = readIntegerArg(argv[i+1], &pDualFactorSlot);
        }
        //  dual factor authentication user pin
        else if ((!bDualFactorUserPin ) && (strcmp(argv[i],"-du")==0) && (argc > i+1))
        {
            bDualFactorUserPin = readArgAsString(argv[i+1], &pDualFactorUserPin, &ulDualFactorUserPin);
        }
#endif
#endif
        else
            bHelp = TRUE;
    }

    if (!bHelp && !pPath) {
        printf("\n\tError: Backup Directory path (-d) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bMech) {
        printf("\n\tError: Invaid KBK wrap mechanism.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bFlag) {
        printf("\n\tError: Flag (-f) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bLabel && (KBK_WRAP_WITH_RSA == ulMech)) {
        printf("\n\tError: Label (-l) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bLabel && validateLabel(pLabel, ulLabelLen))
        bHelp = TRUE;


    if (!bHelp && !bName && (KBK_WRAP_WITH_RSA == ulMech)) {
        printf("\n\tError: Label (-n) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && bName && (KBK_WRAP_WITH_RSA != ulMech)) {
        printf("\n\tError: Use -n option only for KBK_WRAP_WITH_RSA \n");
        bHelp = TRUE;
    }

    if (!bHelp && !bFile && ((KBK_WRAP_WITH_KEK == ulMech) || (KBK_WRAP_WITH_CERT_AUTH_DERIVED_KEY == ulMech))) {
        printf("\n\tError: File (-kf) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && ((ulFlag != 0) && (ulFlag != 1) && (ulFlag != 2))) {
        printf("\n\tError: Flag (-f) value must be 0/1/2.\n");
        bHelp = TRUE;
    }

#ifdef CLOUD_HSM_CLIENT
#if defined(BACKUP_WITH_SMARTCARD) || defined(USE_SMARTCARD)
    if (bDualFactorHost || bDualFactorPort ||  bDualFactorCert || bDualFactorSlot || bDualFactorUserPin) {
        if ( ! (bDualFactorHost && bDualFactorPort &&  bDualFactorCert && bDualFactorSlot && bDualFactorUserPin)) {
            printf("\n\tError: For network dual factor authentication -dh,-dp,-dc,-dn and -du options must be specified.\n");
            bHelp = TRUE;
        }
    }
#endif
#endif

    if (bHelp) {
        printf("\n");
        printf("\nDescription     ");
        printf("\n===========     ");
        printf("\n");
        printf("\nRestore the Partition's configuration and user details ");
        printf("\n");
        printf
            ("\nSyntax:restorePartition -d <dir> -f <config only> -m <mechanism> [-l <HSM label>] [-kf <kbk file>]");
#ifdef CLOUD_HSM_CLIENT
        printf(" [-dh <host> -dp <port> -dc <cert> -dn <slot> du <user pin>]");
#endif
        printf("\n");
        printf("\nWhere: -h  displays this information    \n ");
        printf("\n       -d  Absolute directory containing backed-up files");
        printf("\n       -f  flag to indicate type of restoration.");
        printf("\n            0 -- only Configuration will be restored ");
        printf("\n            1 -- Both Configuration and Users will be restored ");
        printf("\n            2 -- Ful restore config, keys and Users will be restored ");
        printf("\n       -l  partition label (Optional, valid only when using KBK_WRAP_WITH_RSA)");
        printf("\n       -n  name of backed up partition (Valid only when using KBK_WRAP_WITH_RSA)");
        printf("\n       -m  mechanism ");
        printf("\n              KBK_WRAP_WITH_RSA - 0 ");
        printf("\n              KBK_WRAP_WITH_KEK - 1 ");
        printf("\n              KBK_USING_PRE_SHARED_KEYS - 2 ");
        printf("\n              KBK_WRAP_WITH_CERT_AUTH_DERIVED_KEY - 3 ");
        printf("\n       -kf  File containing the KBK in plain (Optional, valid only when ");
        printf("\n                      using KBK_WRAP_WITH_KEK or KBK_WRAP_WITH_CERT_AUTH_DERIVED_KEY) ");
#ifdef CLOUD_HSM_CLIENT
        printf("\n       -dh specifies the hostname for network dual factor authentication server");
        printf("\n       -dp specifies the port used by network dual factor authentication server");
        printf("\n       -dc specifies the certificate used by network dual factor authentication server");
        printf("\n       -dn specifies the slot number for SmartCard in network dual factor authentication server");
        printf("\n       -du specifies the user pin for SmartCard in network dual factor authentication server");
#endif

        printf("\n");
        return ulRet;
    }

#if defined(BACKUP_WITH_SMARTCARD) || defined(USE_SMARTCARD)
    if(KBK_WRAP_WITH_RSA == ulMech)
    {
#ifndef CLOUD_HSM_CLIENT
        ulRet = initSmartCard(0);
        if (ulRet != RET_OK) {
            printf("\n\tFailed to initialize Smart Card\n");
            shutdownSmartCard();
            return ulRet;
        }
#else
        if(bDualFactorHost) {
            printf("\n\tPlease ensure dual factor authentication server is running\n");

            SSL_library_init();
            ctx = InitSSL_CTX();

            if(!SSL_CTX_load_verify_locations(ctx, pDualFactorCert, NULL)){
                ulRet = -1;
                printf("\n\tCould not load the certificate trust chain\n");
                if(ssl) SSL_free(ssl);
                if(server) close(server);
                if(ctx) SSL_CTX_free(ctx);
                return ulRet;
            }

            SSL_CTX_set_verify(ctx, SSL_VERIFY_PEER | SSL_VERIFY_FAIL_IF_NO_PEER_CERT, NULL);
            server = OpenSocketConnection(pDualFactorHost, pDualFactorPort);
            ssl = SSL_new(ctx);
            SSL_set_fd(ssl, server);

            ulRet =  SSL_connect(ssl);
            if ( ulRet == -1 ) {
                printf("\n\nSSL connect failed\n");
                if(ssl) SSL_free(ssl);
                if(server) close(server);
                if(ctx) SSL_CTX_free(ctx);
                return ulRet;
            }

            req_data.command_type = DF_INITIALIZE;
            req_data.init_data.reinitialize = 0;
            req_data.init_data.slot = pDualFactorSlot;

            SSL_write(ssl, &req_data, sizeof(req_data));   /* encrypt & send message */
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code != 0) {
                    printf("\n\tFailed to get initialize SmartCard over network\n");
                    ulRet = -1;
                    memset(&req_data,0,sizeof(req_data));
                    req_data.command_type = DF_END;
                    SSL_write(ssl, &req_data, sizeof(req_data));
                    if(ssl) SSL_free(ssl);
                    if(server) close(server);
                    if(ctx) SSL_CTX_free(ctx);
                    return ulRet;
                }
            } else {
                printf("\n\tSSL_read failed\n");
                ulRet = -1;
                memset(&req_data,0,sizeof(req_data));
                req_data.command_type = DF_END;
                SSL_write(ssl, &req_data, sizeof(req_data));
                if(ssl) SSL_free(ssl);
                if(server) close(server);
                if(ctx) SSL_CTX_free(ctx);
                return ulRet;
            }
        } else {
            ulRet = initSmartCard(0);
            if (ulRet != RET_OK) {
                printf("\n\tFailed to initialize Smart Card\n");
                shutdownSmartCard();
                return ulRet;
            }
        }
#endif
    }
#endif

    if(KBK_USING_PRE_SHARED_KEYS == ulMech)
        ulRet = Cfm2RestoreBegin(session_handle, 0, 0, NULL, 0, 1, pAuth);
    else
        ulRet = Cfm2RestoreBegin(session_handle, 0, 0, NULL, 0, 0, pAuth);
    if (ulRet) {
        printf("\n\tCfm2RestoreBegin failed with error: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
        printf("\n\tPlease ensure required fixed keys are inserted and");
        printf("\n\tpartition is intialized with only Crypto Officer\n");
        return ulRet;
    }

    if (KBK_WRAP_WITH_RSA == ulMech) {
#ifndef CLOUD_HSM_CLIENT
        ulRet = ExportKBKfromSC(PARTITION_INDEX(session_handle), pLabel, pName);
#else
#if defined(BACKUP_WITH_SMARTCARD) || defined(USE_SMARTCARD)
        if(bDualFactorHost)
            ulRet = ExportKBKfromSC2(PARTITION_INDEX(session_handle), pLabel, pName, pDualFactorUserPin, ssl);
        else
#endif
            ulRet = ExportKBKfromSC(PARTITION_INDEX(session_handle), pLabel, pName);
#endif
    }else if((KBK_WRAP_WITH_KEK == ulMech) || (KBK_WRAP_WITH_CERT_AUTH_DERIVED_KEY == ulMech))
        ulRet = ExportKBKfromHost(PARTITION_INDEX(session_handle), (Uint8 *) pData, ulDataLen, ulMech);

    if (ulRet) {
        printf("\tExporting KBK from SC failed with error 0x%02x\n", ulRet);
        goto error;
    }

    ulRet = ReadConfig(pPath, config, &config_len, 0, 0, NULL);
    if (ulRet) {
        printf("\n\tFailed to read the Partition configuration\n");
        goto error;
    }

    /* Backing Up the Current Partition Configuration */
    ulRet = Cfm2RestoreConfig(session_handle, 0, config, config_len, pAuth);
    if (ulRet != 0) {
        printf("\n\tRestore Partition configuration Failure: 0x%02x : %s\n",
               ulRet, Cfm2ResultAsString(ulRet));
        goto error;
    }
    printf("\n\tCfm2RestoreConfig returned: 0x%02x : %s\n", ulRet,
           Cfm2ResultAsString(ulRet));

    if (ulFlag) {
        user_index = 0;
        ulRet = ReadUserInfo(CN_CRYPTO_OFFICER, &no_of_officers,
                     sizeof(enc_officer_info), enc_officer_info, &user_index, &enc_officer_len, 0, pPath,
                     0);
        if (ulRet)
            printf("\n\tFailed to Read the Partition Crypto Officers \n");

        /* Restoring the Partition Crypto Officers */
        ulRet = Cfm2RestoreUsers(session_handle, 0,
                     enc_officer_info, enc_officer_len,
                     no_of_officers, CN_CRYPTO_OFFICER);
        if (ulRet != 0) {
            printf
                ("\n\tCfm2RestoreUsers for Partition COs failure :  0x%02x : %s\n",
                 ulRet, Cfm2ResultAsString(ulRet));
            goto error;
        }
        printf("\n\tCfm2RestoreUsers for COs returned: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));

        user_index = 0;
        while(1) {
            Uint32 restored_index = 0;

            enc_user_len = 0;
            memset(enc_user_info, 0, sizeof(enc_user_info));
            ulRet = ReadUserInfo(CN_CRYPTO_USER, &no_of_users,
                         sizeof(enc_user_info), enc_user_info, &user_index, &enc_user_len, 0, pPath,
                         0);

            if (!enc_user_len){
                printf("\n\tNo Users exists for restore \n");
                break;
            }

            if (ulRet)
                printf("\n\tFailed to Save the Partition Crypto Users \n");

            /* Restoring Partition Crypto Users */
            ulRet = Cfm2RestoreUsersFromIndex(session_handle, 0,
                              enc_user_info, enc_user_len,&restored_index,
                              no_of_users, CN_CRYPTO_USER);
            if (ulRet != 0) {
                printf
                    ("\n\tCfm2RestoreUsers for Partition CUs failure :  0x%02x : %s\n",
                     ulRet, Cfm2ResultAsString(ulRet));
                goto error;
            }
            printf("\n\tCfm2RestoreUsers for CUs returned: 0x%02x : %s\n", ulRet,
                   Cfm2ResultAsString(ulRet));
            printf("Restored user index is %d, current user index %d \n", restored_index, user_index);
            no_of_users = 0;

            if (!user_index)
                break;
        }
        user_index = 0;
        while(1) {
            Uint32 restored_index = 0;

            enc_user_len = 0;
            memset(enc_user_info, 0, sizeof(enc_user_info));
            ulRet = ReadUserInfo(CN_APPLIANCE_USER, &no_of_users,
                         sizeof(enc_user_info), enc_user_info, &user_index, &enc_user_len, 0, pPath,
                         0);

            if (!enc_user_len){
                printf("\n\tNo AU Users exists for restore \n");
                break;
            }

            if (ulRet)
                printf("\n\tFailed to Save the Partition Appliance Users \n");

            /* Restoring Partition Crypto Users */
            ulRet = Cfm2RestoreUsersFromIndex(session_handle, 0,
                              enc_user_info, enc_user_len,&restored_index,
                              no_of_users, CN_APPLIANCE_USER);
            if (ulRet != 0) {
                printf
                    ("\n\tWarn!! Cfm2RestoreUsers for Partition AUs failure :  0x%02x : %s\n",
                     ulRet, Cfm2ResultAsString(ulRet));
                //goto error;
            }
            printf("\n\tCfm2RestoreUsers for AUs returned: 0x%02x : %s\n", ulRet,
                   Cfm2ResultAsString(ulRet));
            printf("Restored user index is %d, current user index %d \n", restored_index, user_index);
            no_of_users = 0;

            if (!user_index)
                break;
        }
    }

    if(2 == ulFlag){
        /* Now restore the key parition key store */
        strncpy(file_name, pPath, strlen(pPath));
        strcat(file_name, "/");
        file_name_common_len = strlen(file_name);

        /* Find the number of keys in the directory */
        if (NULL != (dir = opendir(pPath))) {
            while (1) {
                readdir_r(dir, &entry, &result);

                if (NULL == result)
                    break;

                if (!strcmp(".", entry.d_name) || !strcmp("..", entry.d_name)
                    || !strcmp(KBK_UNWRAPPED_WIH_KEK_FILE, entry.d_name)
                    || strncmp("key_", entry.d_name,4))
                    continue;
                max_keys ++;
            }
        }

        /* Allocate memory to sort the */
        key_files = (char **) cn_alloc(sizeof(char*) * max_keys);
        if(!key_files){
            ulRet = ERR_MEMORY_ALLOC_FAILURE;
            goto error;
        }
        memset(key_files, 0, max_keys*sizeof(char*));

        for(i=0; i < max_keys; i++){
            key_files[i] = (char*)cn_alloc(sizeof(char) * MAX_FILENAME_SIZE);
            if(!key_files[i]){
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                goto error;
            }
        }
        max_keys = 0;
        if (NULL != (dir = opendir(pPath))) {
            while (1) {
                readdir_r(dir, &entry, &result);

                if (NULL == result)
                    break;
                if (!strcmp(".", entry.d_name) || !strcmp("..", entry.d_name)
                    || !strcmp(KBK_UNWRAPPED_WIH_KEK_FILE, entry.d_name)
                    || strncmp("key_", entry.d_name,4))
                    continue;

                strcpy(file_name + file_name_common_len, entry.d_name);
                strcpy(key_files[max_keys], file_name);
                memset(file_name + file_name_common_len, 0, strlen(entry.d_name));
                max_keys++;
            }
        }

        /* Sort the key files */

        qsort(key_files, max_keys, sizeof(char*), cmpstringp);

        key_index = 0;
        while (key_index <  max_keys) {
            struct stat fstat;

            if(stat(key_files[key_index], &fstat))
            {
                printf("Key file %s does not exist\n", key_files[key_index]);
                key_index++;
            }

            ulRet = ReadKeyInfo(key_files[key_index], key_info, &key_info_len);
            if (ulRet) {
                printf("\n\tFailed to read the Partition key\n");
                goto error;
            }

            /* Restoring the Current Partition Configuration */
            ulRet =
                Cfm2RestoreKey(session_handle, 0, 1, key_info, key_info_len,
                           &KeyHandle);
            if (ulRet != 0) {
                printf("\n\tRestoreKeyStore Failure: 0x%02x : %s\n", ulRet,
                       Cfm2ResultAsString(ulRet));
            }
            printf("\n\tCfm2RestoreKeyStore returned: 0x%02x : %s\n", ulRet,
                   Cfm2ResultAsString(ulRet));
            printf("\n\t Handle of the restored key is %lld for key file: %s\n",
                   KeyHandle, key_files[key_index]);
            memset(key_info, 0, sizeof(key_info));
            key_info_len = 0;
            KeyHandle = 0;
            key_index++;
        }
    }

error:
    ulRet = Cfm2RestoreEnd(session_handle, 0, 0);
    if (ulRet) {
        printf("\n\tCfm2RestoreEnd failed: 0x%02x : %s\n", ulRet,
               Cfm2ResultAsString(ulRet));
    }
    if(key_files){
        for(i=0; i<max_keys; i++)
        {
            if(key_files[i])  free(key_files[i]);
        }

        free(key_files);
    }

#if defined(BACKUP_WITH_SMARTCARD) || defined(USE_SMARTCARD)
    if(KBK_WRAP_WITH_RSA == ulMech) {
#ifndef CLOUD_HSM_CLIENT
        shutdownSmartCard();
#else
        if(!bDualFactorHost) {
            shutdownSmartCard();
        } else {
            memset(&req_data,0,sizeof(req_data));
            req_data.command_type = DF_END;
            SSL_write(ssl, &req_data, sizeof(req_data));
            if(ssl) SSL_free(ssl);
            if(server) close(server);
            if(ctx) SSL_CTX_free(ctx);
        }
#endif
    }
#endif

    return ulRet;
}


/*****************************************************************************
 *
 * FUNCTION     : getCaviumPrivKey
 *
 * DESCRIPTION  : Returns private key file in PEM format for given private key handle
 *
 * PARAMETERS   : input:   Keyhandle
 *                         file name to which private key is to be written
 *
 *                output:  Returns  non-zero on Error
 *                                  0 on Success
 *
 ******************************************************************************/
Uint32 getCaviumPrivKey(int argc, char **argv)
{
    Uint32 ulRet = -1;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;
    RSA *rsa = NULL;

    Uint8 bKey = FALSE;
    Uint64 ulKey64 = 0;

    Uint8 bFile = FALSE;
    char *KeyFile = NULL;

    Uint32 pubExpVal = 0;
    Uint32 ulpubExpValLen = 0;

    Uint32 ulModLen;
    Uint32 *temp = NULL;
    Uint8 *privexp = NULL;
    Uint8 *pModulus = NULL;
    Uint32 ulKeyType = 0;
    Uint32 key_class = 0;
    Uint32 attr_len = 4;
    FILE *fd = NULL;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        else if ((!bKey) && (strcmp(argv[i], "-k") == 0) && (argc > i + 1)) {
            bKey = TRUE;
            ulKey64 = strtoul(argv[i + 1], NULL, 0);
        }

        else if ((!bFile) && (strcmp(argv[i], "-out") == 0) && (argc > i + 1)) {
            KeyFile = argv[i+1];
            bFile = TRUE;
        }

        else
            bHelp = TRUE;

    }

    // ensure that we have all the required args
    if (!bHelp && !bKey) {
        printf("\n\tError: Key handle (-k) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bFile) {
        printf("\n\tError: Key File (-out) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf
            ("\nCreates PrivateKey file for specified RSA private key handle.");
        printf("\n");
        printf("\nSyntax:  getCaviumPrivKey -h -k <key handle> -out <key file>");
        printf("\n\n");
        printf("\nWhere: -h    displays this information");
        printf("\n       -k    specifies the RSA private key handle");
        printf("\n       -out  specifies the file to write fake RSA private key");
        printf("\n");
        return ulRet;
    }

    /* get key type from key handle */
    if ((ulRet = Cfm2GetAttribute_new(session_handle,
                      ulKey64,
                      OBJ_ATTR_KEY_TYPE,
                      (Uint8 *) & ulKeyType, &attr_len)))
        goto err;

    attr_len = 4;

    /* get private attribute from key handle */
    if ((ulRet = Cfm2GetAttribute_new(session_handle,
                      ulKey64,
                      OBJ_ATTR_CLASS,
                      (Uint8 *) & key_class, &attr_len)))
        goto err;

    if (atoi((char *)&ulKeyType) != KEY_TYPE_RSA ||
        atoi((char *)&key_class) != OBJ_CLASS_PRIVATE_KEY) {
        printf("\n\tinput key handle should be RSA private key handle\n");
        ulRet = ERR_KEY_HANDLE_INVALID;
        goto err;
    }

    /* get modulus length from private key */
    if ((ulRet = Cfm2GetAttribute_new(session_handle,
                      ulKey64,
                      OBJ_ATTR_MODULUS, NULL, &ulModLen)))
        goto err;

    pModulus = malloc(ulModLen);
    ulpubExpValLen = 4;

    /* get public exponent from private key */
    if ((ulRet = Cfm2GetAttribute_new(session_handle,
                      ulKey64,
                      OBJ_ATTR_PUBLIC_EXPONENT,
                      (Uint8 *) & pubExpVal, &ulpubExpValLen)))
        goto err;

    /* get modulus from private key */
    if ((ulRet = Cfm2GetAttribute_new(session_handle,
                      ulKey64,
                      OBJ_ATTR_MODULUS, pModulus, &ulModLen)))
        goto err;

    rsa = RSA_new();

    if (!rsa->n && ((rsa->n = BN_new()) == NULL))
        goto err;
    if (!rsa->d && ((rsa->d = BN_new()) == NULL))
        goto err;
    if (!rsa->e && ((rsa->e = BN_new()) == NULL))
        goto err;
    if (!rsa->p && ((rsa->p = BN_new()) == NULL))
        goto err;
    if (!rsa->q && ((rsa->q = BN_new()) == NULL))
        goto err;
    if (!rsa->dmp1 && ((rsa->dmp1 = BN_new()) == NULL))
        goto err;
    if (!rsa->dmq1 && ((rsa->dmq1 = BN_new()) == NULL))
        goto err;
    if (!rsa->iqmp && ((rsa->iqmp = BN_new()) == NULL))
        goto err;

    /* Modulus */
    if (!BN_bin2bn(pModulus, ulModLen, rsa->n))
        goto err;

    /* Public  Exponent */
    if (!BN_bin2bn((Uint8 *) & pubExpVal, ulpubExpValLen, rsa->e))
        goto err;

    privexp = OPENSSL_malloc(ulModLen);
    temp = (Uint32 *) privexp;
    for (i = 0; i < ulModLen / 4; i++, temp++)
        *temp = CAV_SIG_HSM_KEY;

    memset(&(privexp[8]), 0, 8);
    memcpy(&(privexp[8]), (Uint8 *) & ulKey64, 8);

    /* Private Exponent */
    BN_bin2bn(privexp, ulModLen, rsa->d);

    /* Turn off delete key handle flag in RSA flags,
     * so RSA_free does not delete the key handle from card */
    rsa->flags &= 0xbfff;

    fd = fopen(KeyFile, "w");
    if (!fd){
	printf("\n\tFailed to open or create file %s",KeyFile);
	ulRet = ERR_OPEN_FILE;
	goto err;
    }
    if (!PEM_write_RSAPrivateKey(fd, rsa, NULL, NULL, 0, NULL, NULL))
        goto err;
    printf
        ("\n\nPrivate Key Handle is written to %s in fake PEM format\n", KeyFile);
    ulRet = 0;

err:
    if (fd)
        fclose(fd);
    if (privexp)
        OPENSSL_free(privexp);
    if (rsa)
        RSA_free(rsa);
    if (pModulus)
        free(pModulus);

    if (ulRet == -1)
        printf("\n:Error in writing private key file 0x%02x : ", ulRet);
    else
        printf("\n\tgetCaviumPrivKey returned: 0x%02x : %s\n",
               ulRet, Cfm2ResultAsString(ulRet));

    return ulRet;
}

/************************************************************************************
 *
 * FUNCTION     : IsValidKeyHandlefile
 *
 * DESCRIPTION  : checks input key file has a valid private key handle.
 *
 * PARAMETERS   : input:  PrivateKey file name
 *
 *                output:  Returns  non-zero on Error
 *                                  0 on Success
 *
 *******************************************************************************/

Uint32 IsValidKeyHandlefile(int argc, char **argv)
{

    Uint32 ulRet = -1;
    Uint32 i = 0;
    Uint8 bHelp = FALSE;
    RSA *rsa = NULL;

    Uint8 *privexp = NULL;
    Uint8 bFile = FALSE;
    FILE *fd = NULL;
    char *key_file = NULL;
    Uint32 ulKeyHandle = 0;

    Uint64 *pulKeyArray = 0;
    Uint32 ulKeyArrayLen = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;

        else if ((!bFile) && (strcmp(argv[i], "-f") == 0) && (argc > i + 1)) {
            if (argv[i + 1] != NULL) {
                key_file = argv[i + 1];
                bFile = TRUE;
            }
        }

        else
            bHelp = TRUE;

    }

    // ensure that we have all the required args
    if (!bHelp && !bFile) {
        printf("\n\tError: Key File (-f) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf
            ("\nChecks given key file has key handle or real key \n(supported for RSA keys only)");
        printf("\n");
        printf("\nSyntax:  IsValidKeyHandlefile -h -f <key file>");
        printf("\n\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -f  specifies the RSA private key file name");
        printf("\n");
        return ulRet;
    }

    rsa = RSA_new();

    if (!rsa->n && ((rsa->n = BN_new()) == NULL))
        goto ret;
    if (!rsa->d && ((rsa->d = BN_new()) == NULL))
        goto ret;
    if (!rsa->e && ((rsa->e = BN_new()) == NULL))
        goto ret;
    if (!rsa->p && ((rsa->p = BN_new()) == NULL))
        goto ret;
    if (!rsa->q && ((rsa->q = BN_new()) == NULL))
        goto ret;
    if (!rsa->dmp1 && ((rsa->dmp1 = BN_new()) == NULL))
        goto ret;
    if (!rsa->dmq1 && ((rsa->dmq1 = BN_new()) == NULL))
        goto ret;
    if (!rsa->iqmp && ((rsa->iqmp = BN_new()) == NULL))
        goto ret;

    fd = fopen(key_file, "r");
    if (!fd) {
        printf("\n\n \t error in opening file %s \n\n", key_file);
        ulRet = 0;
        goto ret;
    }

    if (!PEM_read_RSAPrivateKey(fd, &rsa, NULL, NULL))
        goto ret;

    privexp = OPENSSL_malloc(BN_num_bytes(rsa->d));
    BN_bn2bin(rsa->d, privexp);

    if ((*(Uint32 *) privexp == CAV_SIG_IMPORTED_KEY) ||
        (*(Uint32 *) privexp == CAV_SIG_HSM_KEY)) {
        ulKeyHandle = *(Uint32 *) & (privexp[8]);
        /* Determine Key Handle(s) Found and Place in Key Handle Array */
        ulRet = Cfm2FindKey(session_handle, 0, 3,
                    NULL, 0, NULL, 0, NULL, 0,
                    NULL, &ulKeyArrayLen);
        if (ulKeyArrayLen) {
            /* Allocate Key Handle(s) Array */
            pulKeyArray = (Uint64 *) calloc(ulKeyArrayLen, sizeof(Uint64));
            if (pulKeyArray == 0) {
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
                goto ret;
            } else {
                // Find All Keys of Key Type and Key Label
                ulRet = Cfm2FindKey(session_handle, 0, 3,
                            NULL, 0, NULL, 0, NULL, 0,
                            pulKeyArray, &ulKeyArrayLen);
                if (ulRet)
                    goto ret;
                // Print Key Handle Array
                //HexPrint((Uint8*)pKeyArray, ulKeyArrayLen*8);
                if (ulKeyArrayLen) {
                    for (i = 0; i <= ulKeyArrayLen - 1; i++) {
                        if ((Uint32) (pulKeyArray[i]) == ulKeyHandle) {
                            printf
                                ("\n\n \tInput file has key handle: %d \n\n",
                                 ulKeyHandle);
                            ulRet = 0;
                            goto ret;
                        }
                    }
                    ulRet = 0;
                    printf("\n\n \tInput file has invalid key handle: %d \n",
                           ulKeyHandle);
                    goto ret;
                } else {
                    printf("\n\n \tInput file has invalid key handle: %d \n",
                           ulKeyHandle);
                    ulRet = 0;
                    goto ret;
                }
            }
        } else {
            printf("\n\n \tInput file has invalid key handle: %d \n",
                   ulKeyHandle);
            ulRet = 0;
            goto ret;
        }
    } else {
        printf("\n\n \tInput key file has real private key \n\n");
        ulRet = 0;
    }

ret:

    if (ulRet == -1)
        printf("\n\n: \terror in Reading private key file %s \n\n", key_file);
    else if (ulRet)
        printf("\n\n \t IsValidKeyHandlefile return  0x%02x  %s\n\n",
               ulRet, Cfm2ResultAsString(ulRet));

    if (fd)
        fclose(fd);
    if (privexp)
        OPENSSL_free(privexp);
    if (rsa)
        RSA_free(rsa);
    if (pulKeyArray)
        free(pulKeyArray);

    return ulRet;
}

Uint32 storeCert(int argc, char **argv)
{
    Uint32 i = 0;
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;
    Uint8 bCertFile = FALSE;
    Uint8 bCertSubject = FALSE;
    Uint8   cert_buf[4096] = {};
    Uint32  cert_len = 0;
    char   *cert_file = NULL;
    FILE   *fd;
    char   *cmd = NULL;
    char   *err = NULL;
    int BUFLEN = 100;
    char buf[BUFLEN];
    CertSubject cert_subject = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        else if ((!bCertFile) && (strcmp(argv[i],"-f")==0) && (argc > i+1))
        {
            cert_file = argv[i+1];
            bCertFile = TRUE;
        }
        // cert subject
        else if ((!bCertSubject) && (strcmp(argv[i], "-s") == 0)
             && (argc > i + 1))
            bCertSubject = readIntegerArg(argv[i + 1], &cert_subject);
        else
            bHelp = TRUE;
    }

    if(!bHelp && !bCertFile)
    {
        printf("\n\tCertificate file is missing \n");
        bHelp = TRUE;
    }

    if(!bHelp && !bCertSubject)
    {
        printf("\n\tCertificate Subject is missing \n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nStores Certificate in the HSM");
        printf("\n");
        printf("\nSyntax: storeCert -f <cert-file> -s <cert-subject>");
        printf("\n");
        printf("\nWhere: -h     displays this information");
        printf("\n       -f     specifies the certificate file name");
        printf("\n       -s     specifies owner of the certificate");
        printf("\n       (ex: \n\t PARTITION_OWNER - 4,\n\t PARTITION -8");
        printf("\n");
        return ulRet;
    }

    cmd = (char *)malloc(strlen(cert_file) + 100);
    sprintf(cmd,"openssl x509 -in %s -noout > /tmp/cmdout 2>&1",cert_file);
    system(cmd);
    fd  = fopen("/tmp/cmdout", "rb");
    if (!fd) {
        ulRet = ERR_INVALID_INPUT;
        printf("unable to open file\n");
        return ulRet;
    }
    fread(buf, sizeof(char), BUFLEN, fd);
    sprintf(cmd,"rm -rf /tmp/cmdout");
    system(cmd);
    free(cmd);
    err = strstr(buf, "error");
    if(err)
    {
        memset(buf,0,BUFLEN);
        printf("\n\tCertificate file %s is either not valid or does not exist\n", cert_file);
        ulRet = ERR_INVALID_INPUT;
        goto end;
    }

    /* read the pem formatted cert file */
    ulRet = read_file(cert_file, cert_buf, &cert_len);
    if ((ulRet != 0) || (cert_len == 0)) {
        printf("\n\tError: Empty file: %s\n", cert_file);
        goto end;
    }

    ulRet = Cfm2StoreCert(session_handle, cert_subject, cert_buf, cert_len);

    if(RET_OK == ulRet)
        printf("\n\tCfm2StoreCert() returned %d :%s\n", ulRet, Cfm2ResultAsString(ulRet));
    else
    {
        printf("\n\tCfm2StoreCert() failed with erro code %d :%s\n", ulRet, Cfm2ResultAsString(ulRet));
        return 0;
    }

end:
    if (fd)
        fclose(fd);

    return ulRet;
}

Uint32 getCertReq(int argc, char **argv)
{
    Uint32 i = 0;
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;
    Uint8 bCertReqFile = FALSE;
    Uint8   cert_req_buf[4096] = {};
    Uint32  cert_req_len = 4096;
    char   *cert_req_file = NULL;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        else if ((!bCertReqFile) && (strcmp(argv[i],"-f")==0) && (argc > i+1))
        {
            cert_req_file = argv[i+1];
            bCertReqFile = TRUE;
        }
        else
            bHelp = TRUE;
    }

    if(!bHelp && !bCertReqFile)
    {
        printf("\n\tCertificate Request file is missing \n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nGets Partition's Certificate Request from the HSM");
        printf("\n");
        printf("\nSyntax: getCertReq -f <cert-req-file>");
        printf("\n");
        printf("\nWhere: -h     displays this information");
        printf("\n       -f     specifies the certificate req file name");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2GetCertReq(session_handle, PARTITION_CERT, cert_req_buf, &cert_req_len);

    if(RET_OK == ulRet) {
        printf("\n\tCfm2GetCertReq() returned %d :%s\n", ulRet, Cfm2ResultAsString(ulRet));
        /* write the pem formatted cert file */
        ulRet = write_file(cert_req_file, cert_req_buf, cert_req_len);
        if (ulRet != 0) {
            printf("\n\tError: Empty file: %s\n", cert_req_file);
            return -1;
        }
    }
    else
    {
        printf("\n\tCfm2GetCertReq() failed with erro code %d :%s\n", ulRet, Cfm2ResultAsString(ulRet));
        return 0;
    }

    return ulRet;
}


Uint32 getCert(int argc, char **argv)
{
    Uint32 i = 0;
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;
    Uint8 bCertFile = FALSE;
    Uint8 bCertSubject = FALSE;
    Uint8   cert_buf[4096] = {};
    Uint32  cert_len = 4096;
    char   *cert_file = NULL;
    CertSubject cert_subject = 0;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        else if ((!bCertFile) && (strcmp(argv[i],"-f")==0) && (argc > i+1))
        {
            cert_file = argv[i+1];
            bCertFile = TRUE;
        }
        // cert subject
        else if ((!bCertSubject) && (strcmp(argv[i], "-s") == 0)
             && (argc > i + 1))
            bCertSubject = readIntegerArg(argv[i + 1], &cert_subject);
        else
            bHelp = TRUE;
    }

    if(!bHelp && !bCertFile)
    {
        printf("\n\tCertificate file is missing \n");
        bHelp = TRUE;
    }

    if(!bHelp && !bCertSubject)
    {
        printf("\n\tCertificate Subject is missing \n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nGets Certificate from the HSM");
        printf("\n");
        printf("\nSyntax: getCert -f <cert-file> -s <cert-subject>");
        printf("\n");
        printf("\nWhere: -h     displays this information");
        printf("\n       -f     specifies the certificate file name");
        printf("\n       -s     specifies owner of the certificate");
        printf("\n       (ex: CAVIUM - 1,\n HSM - 2,\n PARTITION_OWNER - 4,\n PARTITION - 8,\n PARTITION_CERT_ISSUED_BY_HSM - 16");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2GetCert(session_handle, cert_subject, cert_buf, &cert_len);

    if(RET_OK == ulRet) {
        printf("\n\tCfm2GetCert() returned %d :%s\n", ulRet, Cfm2ResultAsString(ulRet));
        /* write the pem formatted cert file */
        ulRet = write_file(cert_file, cert_buf, cert_len);
        if (ulRet != 0) {
            printf("\n\tError: Empty file: %s\n", cert_file);
            return -1;
        }
    }
    else
    {
        printf("\n\tCfm2GetCert() failed with erro code %d :%s\n", ulRet, Cfm2ResultAsString(ulRet));
        return 0;
    }

    return ulRet;
}

Uint32 getSourceRandom(int argc, char **argv)
{
    Uint32 i = 0;
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;
    Uint8 bSRFile = FALSE;
    Uint8 source_random[4096] = {};
    char   *source_random_file = NULL;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        else if ((!bSRFile) && (strcmp(argv[i],"-f")==0) && (argc > i+1))
        {
            source_random_file = argv[i+1];
            bSRFile = TRUE;
        }
        else
            bHelp = TRUE;
    }

    if(!bHelp && !bSRFile)
    {
        printf("\n\tSource Random file is missing \n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nGets Source Random required for mutual trust protocol");
        printf("\n");
        printf("\nSyntax: getSourceRandom -f <source-random-file>");
        printf("\n");
        printf("\nWhere: -h     displays this information");
        printf("\n       -f     specifies the file to which source random has to be written");
        printf("\n");
        return ulRet;
    }

    ulRet = Cfm2GetSourceRandom(session_handle, 0, source_random);

    if(RET_OK == ulRet) {
        printf("\n\tCfm2GetSourceRandom() returned %d :%s\n", ulRet, Cfm2ResultAsString(ulRet));

        printf("\n\n After receiving Target Random and Target Key Exchange from Peer,");
        printf("\n generate Source Key Exchange Message and validate Target Key Exchange Message:\n");
        printf("\n using the following command\n");
        printf("\n\t $validateCert -f <peer-cert-file> -trf <target-random-file> -tkef <target-key-exchange-file> -skef <source-key-exchange-file>");
        printf("\n\n Any Other Certificate command will report an error RET_INVALID_CERT_AUTH_STATE");
        /* write the pem formatted cert file */
        ulRet = write_file(source_random_file, source_random, CERT_AUTH_NONCE_LENGTH);
        if (ulRet != 0) {
            printf("\n\tError: Empty file: %s\n", source_random_file);
            return -1;
        }

    }
    else
    {
        printf("\n\tCfm2GetSourceRandom() failed with erro code %d :%s\n", ulRet, Cfm2ResultAsString(ulRet));
        return 0;
    }

    return ulRet;
}

Uint32 validateCert(int argc, char **argv)
{
    int i = 0, tmp_len = 0, ske_len = 0, tke_len = 0;
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;

    Uint8 bCertFile = FALSE;
    Uint8   cert_buf[MAX_DATA_LENGTH] = {};
    Uint32  cert_len = 0;
    char   *cert_file = NULL;

    Uint8 bTRFile = FALSE;
    Uint8   target_random[CERT_AUTH_NONCE_LENGTH] = {};
    char   *target_random_file = NULL;

    Uint8 bTKEFile = FALSE;
    Uint8   target_key_exchange[4096] = {};
    char   *target_key_exchange_file = NULL;

    Uint8 bSRFile = FALSE;
    Uint8 source_random[4096] = {};
    char   *source_random_file = NULL;

    Uint8 bSKEFile = FALSE;
    Uint8   source_key_exchange[4096] = {};
    char   *source_key_exchange_file = NULL;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        else if ((!bCertFile) && (strcmp(argv[i],"-f")==0) && (argc > i+1))
        {
            cert_file = argv[i+1];
            bCertFile = TRUE;
        }
        else if ((!bTRFile) && (strcmp(argv[i],"-trf")==0) && (argc > i+1))
        {
            target_random_file = argv[i+1];
            bTRFile = TRUE;
        }
        else if ((!bSKEFile) && (strcmp(argv[i],"-tkef")==0) && (argc > i+1))
        {
            target_key_exchange_file = argv[i+1];
            bTKEFile = TRUE;
        }
        else if ((!bSRFile) && (strcmp(argv[i],"-srf")==0) && (argc > i+1))
        {
            source_random_file = argv[i+1];
            bSRFile = TRUE;
        }
        else if ((!bSKEFile) && (strcmp(argv[i],"-skef")==0) && (argc > i+1))
        {
            source_key_exchange_file = argv[i+1];
            bSKEFile = TRUE;
        }
        else
            bHelp = TRUE;
    }

    if(!bHelp && !bCertFile)
    {
        printf("\n\tCertificate file is missing \n");
        bHelp = TRUE;
    }
    if(!bHelp && !bTRFile && !bSKEFile)
    {
        printf("\n\tTarget Random file is missing \n");
        printf("\n\tOnly Certificate validation will be done \n");
    } else {
        if (!bHelp) {
            if ((!bTRFile) && (bSKEFile)) {
                printf("\n\tTarget Random file is missing \n");
                bHelp = TRUE;
            }
        }
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nValidates Certificate");
        printf("\n");
        printf("\nSyntax: validateCert -f <cert-file> [-trf <target-random-file>] [-tkef <target-key-exchange-file>]");
        printf("\n                                    [-srf <source-random-file>] [-skef <source-key-exchange-file>]");
        printf("\n");
        printf("\nWhere: -h     displays this information");
        printf("\n       -f     specifies the file containing peer's certificate");
        printf("\n       -trf   specifies the file containing peer's random");
        printf("\n       -tkef  specifies the file containing peer's key exchagne data");
        printf("\n       -srf   specifies the file to write source random number");
        printf("\n       -skef  specifies the file to write source key exchagne data");
        printf("\n");
        printf("\n NOTE: At a time, only 3 of the optional arguments can be chosen for the following reason");

        printf("\n\n If you are the initiator of the handshake, Source Random has to be generated and ");
        printf("\n be shared with the Peer to get the Target Key Exchange");
        printf("\n So initiator should do the handshake in two steps as below.");
        printf("\n Generate Source Random:\n");
        printf("\n\t $getSourceRandom -f <source-random-file>");
        printf("\n\n After receiving Target Random and Target Key Exchange from Peer,");
        printf("\n generate Source Key Exchange Message and validate Target Key Exchange Message:\n");
        printf("\n\t $validateCert -f <peer-cert-file> -trf <target-random-file> -tkef <target-key-exchange-file> -skef <source-key-exchange-file>");
        printf("\n\n If Peer initiates the handshake by submitting it's Random and Certificate");
        printf("\n do the handshake in two steps as below.");
        printf("\n After receiving Target's Random,");
        printf("\n generate Source Random and Source Key Exchange Message:\n");
        printf("\n\t $validateCert -f <peer-cert-file> -trf <target-random-file> -srf <source-random-file> -skef <source-key-exchange-file>");
        printf("\n\n After receiving Target's Key Exchange Message, validate:\n");
        printf("\n\t $targetKeyExchange -f <target-key-exchange-file>");
        printf("\n\nSuccessfully authenticated session will be alive for 20 minutes, if not closed\n");

        printf("\n");
        return ulRet;
    }

    /* read the pem formatted cert file */
    ulRet = read_file(cert_file, cert_buf, &cert_len);
    if ((ulRet != 0) || (cert_len == 0)) {
        printf("\n\tError: Empty file: %s\n", cert_file);
        return -1;
    }

    if (bTRFile != FALSE) {
        /* read the target random file */
        ulRet = read_file(target_random_file, target_random, (uint32_t *)&tmp_len);
        if ((ulRet != 0) || (tmp_len != CERT_AUTH_NONCE_LENGTH)) {
            printf("\n\tError: error reading file: %s\n", target_random_file);
            return -1;
        }
    }

    if (bTKEFile != FALSE) {
        /* read the target key exchange file */
        ulRet = read_file(target_key_exchange_file, target_key_exchange, (uint32_t *)&tke_len);
        if ((ulRet != 0) || (tke_len > 4096)) {
            printf("\n\tError: error reading file: %s\n", target_key_exchange_file);
            return -1;
        }
    }

    ske_len = 4096;
    ulRet = Cfm2ValidateCert(session_handle, 0,
                 cert_buf, cert_len,
                 (bTRFile)?target_random:NULL,
                 (bTKEFile)?target_key_exchange:NULL, tke_len,
                 (bSRFile)?source_random:NULL,
                 (bSKEFile)?source_key_exchange:NULL,
                 (uint32_t *)&ske_len);


    if(RET_OK == ulRet) {
        printf("\n\tCfm2ValidateCert() returned %d :%s\n", ulRet, Cfm2ResultAsString(ulRet));
        if (bSRFile != FALSE) {
            ulRet = write_file(source_random_file, source_random, CERT_AUTH_NONCE_LENGTH);
            if (ulRet != 0) {
                printf("\n\tError: Empty file: %s\n", source_random_file);
                return -1;
            }
        }
        if (bSKEFile != FALSE) {
            ulRet = write_file(source_key_exchange_file, source_key_exchange, ske_len);
            if (ulRet != 0) {
                printf("\n\tError: Empty file: %s\n", source_key_exchange_file);
                return -1;
            }
        }
    }
    else
    {
        printf("\n\tCfm2ValidateCert() failed with erro code %d :%s\n", ulRet, Cfm2ResultAsString(ulRet));
        return 0;
    }

    return ulRet;
}

Uint32 sourceKeyExchange(int argc, char **argv)
{
    int i = 0, tmp_len = 0;
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;
    Uint8 bSKEFile = FALSE;
    Uint8   source_key_exchange[4096] = {};
    char *source_key_exchange_file = NULL;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        else if ((!bSKEFile) && (strcmp(argv[i],"-f")==0) && (argc > i+1))
        {
            source_key_exchange_file = argv[i+1];
            bSKEFile = TRUE;
        }
        else
            bHelp = TRUE;
    }

    if(!bHelp && !bSKEFile)
    {
        printf("\n\tSource Key Exchange file is missing \n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nGet's Key Exchange Message from HSM");
        printf("\n");
        printf("\nSyntax: sourceKeyExchange -f <source-key-exchange-file> ");
        printf("\n");
        printf("\nWhere: -h     displays this information");
        printf("\n       -f     specifies the file to write source key exchange message");
        printf("\n");
        return ulRet;
    }

    tmp_len = 4096;
    ulRet = Cfm2SourceKeyExchange(session_handle, 0,
                      source_key_exchange,
                      (uint32_t *)&tmp_len);

    if(RET_OK == ulRet) {
        printf("\n\tCfm2SourceKeyExchange() returned %d :%s\n", ulRet, Cfm2ResultAsString(ulRet));
        /* write the source random file */
        if (bSKEFile != FALSE) {
            ulRet = write_file(source_key_exchange_file, source_key_exchange, tmp_len);
            if (ulRet != 0) {
                printf("\n\tError: Empty file: %s\n", source_key_exchange_file);
                return -1;
            }
        }
    }
    else
    {
        printf("\n\tCfm2SourceKeyExchange() failed with erro code %d :%s\n", ulRet, Cfm2ResultAsString(ulRet));
        return 0;
    }

    return ulRet;
}

Uint32 targetKeyExchange(int argc, char **argv)
{
    int i = 0, tmp_len = 0;
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;
    Uint8 bTKEFile = FALSE;
    Uint8 target_key_exchange[4096] = {};
    char *target_key_exchange_file = NULL;

    for (i = 2; i < argc; i = i + 2) {
        // help
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        else if ((!bTKEFile) && (strcmp(argv[i],"-f")==0) && (argc > i+1))
        {
            target_key_exchange_file = argv[i+1];
            bTKEFile = TRUE;
        }
        else
            bHelp = TRUE;
    }

    if(!bHelp && !bTKEFile)
    {
        printf("\n\tTarget Key Exchange file is missing \n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nValidates's Key Exchange Message from Peer");
        printf("\n");
        printf("\nSyntax: targetKeyExchange -f <target-key-exchange-file> ");
        printf("\n");
        printf("\nWhere: -h     displays this information");
        printf("\n       -f     specifies the file containing target key exchange message");
        printf("\n");
        return ulRet;
    }

    /* read from target key exchange file */
    if (bTKEFile != FALSE) {
        ulRet = read_file(target_key_exchange_file, target_key_exchange, (uint32_t *)&tmp_len);
        if (ulRet != 0) {
            printf("\n\tError: Empty file: %s\n", target_key_exchange_file);
            return -1;
        }
    }

    ulRet = Cfm2TargetKeyExchange(session_handle, 0,
                      target_key_exchange,
                      tmp_len);

    if(RET_OK == ulRet) {
        printf("\n\tCfm2TargetKeyExchange() returned %d :%s\n", ulRet, Cfm2ResultAsString(ulRet));
    }
    else
    {
        printf("\n\tCfm2TargetKeyExchange() failed with erro code %d :%s\n", ulRet, Cfm2ResultAsString(ulRet));
        return 0;
    }

    return ulRet;
}


/****************************************************************************\
 *
 * FUNCTION     : CfmUtil_main
 *
 * DESCRIPTION  : The "main" function of the Cfm2Util Utility to allow command
 *                line arguments to be used to make Cavium Shim API calls.
 *                This function is called each time a command is entered.
 *
 * PARAMETERS   : argc, **argv
 *
 * RETURN VALUE : int
 *
 \****************************************************************************/
int CfmUtil_main(int argc, char **argv)
{

    Uint32 ulRet = 0;
    char *pszArg = "help";

    if(argc > 1)
        pszArg = argv[1];

    if (strcmp(pszArg, "getHSMInfo") == 0)
        ulRet = getHSMInfo(argc, argv);
    else if (strcmp(pszArg, "getPartitionInfo") == 0)
        ulRet = getPartitionInfo(argc, argv);
#ifndef CLOUD_HSM_CLIENT
    else if (strcmp(pszArg, "closeAllSessions") == 0)
        ulRet = closeAllSessions(argc, argv);
    else if (strcmp(pszArg, "closePartitionSessions") == 0)
        ulRet = closePartitionSessions(argc, argv);
#endif
    else if (strcmp(pszArg, "loginStatus") == 0)
        ulRet = loginStatus(argc, argv);
    else if (strcmp(pszArg, "createUser") == 0)
        ulRet = createUser(argc, argv);
    else if (strcmp(pszArg, "deleteUser") == 0)
        ulRet = deleteUser(argc, argv);
    else if (strcmp(pszArg, "listUsers") == 0)
        ulRet = listUsers(argc, argv);
    else if ((strcmp(pszArg, "changeUserPswd") == 0) ||
         (strcmp(pszArg, "changePswd") == 0) ||
         (strcmp(pszArg, "changeUserPin") == 0))
        ulRet = changePswd(argc, argv);
    else if (strcmp(pszArg, "loginHSM") == 0)
        ulRet = login(argc, argv);
    else if (strcmp(pszArg, "logoutHSM") == 0)
        ulRet = logout(argc, argv);
    else if (strcmp(pszArg, "zeroizeHSM") == 0)
        ulRet = zeroize(argc, argv);
    else if (strcmp(pszArg, "initHSM") == 0)
        ulRet = initializeHSM(argc, argv);
    else if ((strcmp(pszArg, "rsaGenKeyPair") == 0) ||
         (strcmp(pszArg, "genRSAKeyPair") == 0))
        ulRet = genRSAKeyPair(argc, argv);
    else if ((strcmp(pszArg, "dsaGenKeyPair") == 0) ||
         (strcmp(pszArg, "genDSAKeyPair") == 0))
        ulRet = genDSAKeyPair(argc, argv);
    else if (strcmp(pszArg, "genECCKeyPair") == 0)
        ulRet = genECCKeyPair(argc, argv);
    else if (strcmp(pszArg, "createPublicKey") == 0)
        ulRet = createPublicKey(argc, argv);
    else if (strcmp(pszArg, "importPubKey") == 0)
        ulRet = importPublicKey(argc, argv);
    else if (strcmp(pszArg, "generateKEK") == 0)
        ulRet = generateKEK(argc, argv);
    else if (strcmp(pszArg, "exportPubKey") == 0)
        ulRet = exportPublicKey(argc, argv);
    else if (strcmp(pszArg, "importPrivateKey") == 0)
        ulRet = importPrivateKey(argc, argv);
    else if (strcmp(pszArg, "importRawRSAPrivateKey") == 0)
        ulRet = importRawRSAPrivateKey(argc, argv);
    else if ((strcmp(pszArg, "Convert2CaviumPrivKey") == 0) ||
         (strcmp(pszArg, "convert2CaviumPrivKey") == 0))
        ulRet = convert2CaviumPrivKey(argc, argv);
    else if (strcmp(pszArg, "exportPrivateKey") == 0)
        ulRet = exportPrivateKey(argc, argv);
    else if (strcmp(pszArg, "unWrapKey") == 0)
        ulRet = unWrapKey(argc, argv);
    else if (strcmp(pszArg, "wrapKey") == 0)
        ulRet = wrapKey(argc, argv);
    else if (strcmp(pszArg, "getFWVersion") == 0)
        ulRet = getFWVer(argc, argv);
    else if (strcmp(pszArg, "getLoginFailCount") == 0)
        ulRet = getLoginFailCount(argc, argv);
    else if (strcmp(pszArg, "genPBEKey") == 0)
        ulRet = genPBEKey(argc, argv);
    else if (strcmp(pszArg, "genSymKey") == 0)
        ulRet = genSymKey(argc, argv);
    else if (strcmp(pszArg, "imSymKey") == 0)
        ulRet = imSymKey(argc, argv);
    else if (strcmp(pszArg, "exSymKey") == 0)
        ulRet = exSymKey(argc, argv);
    else if (strcmp(pszArg, "deleteKey") == 0)
        ulRet = deleteKey(argc, argv);
    else if (strcmp(pszArg, "getAuditLogs") == 0)
        ulRet = getAuditLogs(argc, argv);
    else if (strcmp(pszArg, "findKey") == 0)
        ulRet = findKey(argc, argv);
    else if (strcmp(pszArg, "getAttribute") == 0)
        ulRet = getAttribute(argc, argv);
    else if (strcmp(pszArg, "setAttribute") == 0)
        ulRet = setAttribute(argc, argv);
    else if (strcmp(pszArg, "listAttributes") == 0)
        ulRet = listAttributes(argc, argv);
    else if (strcmp(pszArg, "insertMaskedObject") == 0)
        ulRet = insertMaskedObject(argc, argv);
    else if (strcmp(pszArg, "extractMaskedObject") == 0)
        ulRet = extractMaskedObject(argc, argv);
    else if (strcmp(pszArg, "listECCCurveIds") == 0)
        ulRet = listECCCurveIds(argc, argv);
    else if (strcmp(pszArg, "aesWrapUnwrap") == 0)
        ulRet = aesWrapUnwrap(argc, argv);
    else if (strcmp(pszArg, "eccSign") == 0)
        ulRet = eccSign(argc, argv);
    else if (strcmp(pszArg, "eccVerify") == 0)
        ulRet = eccVerify(argc, argv);
#ifdef ENABLE_EAP_PAC
    else if (strcmp(pszArg, "eapPacHs") == 0)
        ulRet = eapPacHs(argc, argv);
#endif
    else if (strcmp(pszArg, "rsaSign") == 0)
        ulRet = rsaSign(argc, argv);
    else if (strcmp(pszArg, "rsaVerify") == 0)
        ulRet = rsaVerify(argc, argv);
    else if (strcmp(pszArg, "dsaSign") == 0)
        ulRet = dsaSign(argc, argv);
    else if (strcmp(pszArg, "dsaVerify") == 0)
        ulRet = dsaVerify(argc, argv);
    else if (strcmp(pszArg, "sign") == 0)
        ulRet = sign(argc, argv);
    else if (strcmp(pszArg, "verify") == 0)
        ulRet = verify(argc, argv);
    else if (strcmp(pszArg, "dumpItUtil") == 0)
        ulRet = dumpItUtil(argc, argv);
    else if (strcmp(pszArg, "cloneSourceStart") == 0)
        ulRet = cloneSourceStart(argc, argv);
    else if (strcmp(pszArg, "cloneSourceEnd") == 0)
        ulRet = cloneSourceEnd(argc, argv);
    else if (strcmp(pszArg, "cloneTargetStart") == 0)
        ulRet = cloneTargetStart(argc, argv);
    else if (strcmp(pszArg, "cloneTargetEnd") == 0)
        ulRet = cloneTargetEnd(argc, argv);
    else if ( strcmp( pszArg,"storeUserFixedKey" )   ==0 )
        ulRet = storeUserFixedKey(argc, argv);
    else if (strcmp(pszArg, "backupPartition") == 0)
        ulRet = backupPartition(argc, argv);
    else if (strcmp(pszArg, "restorePartition") == 0)
        ulRet = restorePartition(argc, argv);
    else if ( strcmp( pszArg,"storeCert")==0 )
        ulRet = storeCert(argc, argv);
    else if ( strcmp( pszArg,"getCertReq")==0 )
        ulRet = getCertReq(argc, argv);
    else if ( strcmp( pszArg,"getCert")==0 )
        ulRet = getCert(argc, argv);
    else if ( strcmp( pszArg,"getSourceRandom")==0 )
        ulRet = getSourceRandom(argc, argv);
    else if ( strcmp( pszArg,"validateCert")==0 )
        ulRet = validateCert(argc, argv);
    else if ( strcmp( pszArg,"sourceKeyExchange")==0 )
        ulRet = sourceKeyExchange(argc, argv);
    else if ( strcmp( pszArg,"targetKeyExchange")==0 )
        ulRet = targetKeyExchange(argc, argv);
#ifdef CERT_OPS
    /* CloudHSM certificate related*/

    else if (strcmp(pszArg,"createNode")== 0)
        ulRet = createNode(argc, argv);
    else if ( strcmp( pszArg,"exportPartnCSR")==0 )
        ulRet = exportPartnCSR(argc, argv);
    else if ( strcmp( pszArg,"importPartnCert")==0 )
        ulRet = importPartnCert(argc, argv);
    else if ( strcmp( pszArg,"exportPartnCert")==0 )
        ulRet = exportPartnCert(argc, argv);
    else if ( strcmp( pszArg,"exportAppCert")==0 )
        ulRet = exportAppCert(argc, argv);


    else if ( strcmp( pszArg,"importCavClientCert")==0 )
        ulRet = importCavClientCert(argc, argv);
    else if ( strcmp( pszArg,"exportCavClientCert")==0 )
        ulRet = exportCavClientCert(argc, argv);
    else if ( strcmp( pszArg,"deleteCavClientCert")==0 )
        ulRet = deleteCavClientCert(argc, argv);
    else if ( strcmp( pszArg,"listCavClientCert")==0 )
        ulRet = listCavClientCert(argc, argv);
#endif
#ifdef NIC_ENABLE
    else if (strcmp(pszArg, "setNetConfig") == 0)
        ulRet = setNetConfig(argc, argv);
#endif
    else if (strcmp(pszArg, "Error2String") == 0)
        ulRet = Error2String(argc, argv);
    else if (strcmp(pszArg, "generatePEK") == 0)
        ulRet = generatePEK(argc, argv);
    else if ((strcmp(pszArg, "getPrivKeyfile") == 0) ||
         (strcmp(pszArg, "getCaviumPrivKey") == 0))
        ulRet = getCaviumPrivKey(argc, argv);
    else if (strcmp(pszArg, "IsValidKeyHandlefile") == 0)
        ulRet = IsValidKeyHandlefile(argc, argv);
    else if (strcmp(pszArg, "exit") == 0)
        return ERR_EXIT_CFM1UTIL; // force an exit
    else {
        // "help" or an unknown command
        //
#ifdef SINGLE_VM
        ulRet = checkSingleVmCmd(pszArg, argc, argv);
        if(ulRet == RET_INVALID_INPUT )
#endif

            Help_AllCommands(vector[0]);
    }
#ifdef CLOUD_HSM_CLIENT
    if (ulRet != CLOUDHSM_SOCKET_SND_ERROR)
#endif
        ulRet = 0;

    return ulRet;
}

/****************************************************************************\
 *
 * FUNCTION     : HexPrint
 *
 * DESCRIPTION  : Displays Data of a Given Length in Hexidecimal Format
 *
 * PARAMETERS   : data, len
 *
 * RETURN VALUE : none
 *
 \****************************************************************************/
void HexPrint(Uint8 * data, Uint32 len)
{
    Uint32 i;
    //                  No Data or No Length Specified
    if (!data || !len)
        return;
    //                           Display Data
    for (i = 1; i <= len; i++) {
        //printf( "%02X ", data[i] );
        printf("%02X ", data[i - 1]);
        if ((i % 16) == 0)
            printf("\n");
    }
    if ((i % 16) != 0)
        printf("\n");

    printf("\n\n");
}

/****************************************************************************\
 *
 * FUNCTION     : Help_AllCommands
 *
 * DESCRIPTION  : Displays Help for All Commands Available
 *
 * PARAMETERS   : pAppName
 *
 * RETURN VALUE : none
 *
 \****************************************************************************/
void Help_AllCommands(char *pAppName)
{
    printf("\n");
    printf("\nHelp Commands Available:");
    printf("\n");
    printf("\nSyntax: <command> -h                                     \n");
    printf("\n");
    printf("\n   Command               Description");
    printf("\n   =======               ===========");
    printf("\n");
    printf("\n   exit                   Exits this application");
    printf("\n   help                   Displays this information");

    printf("\n\n\tConfiguration and Admin Commands");
    printf("\n   getHSMInfo             Gets the HSM Information");
    printf("\n   getPartitionInfo       Gets the Partition Information");
    printf("\n   zeroizeHSM             Init HSM to factory default state");

    printf("\n   initHSM                Initializes HSM");
    printf("\n   generatePEK            Generates  password encryption key");

    printf("\n   createUser             Creates a new CU or CO           ");
    printf("\n   deleteUser             Deletes an existing CU or CO     ");
    printf("\n   listUsers              Lists all users of a partition   ");

    printf("\n   changePswd             Changes CO/CU/AU password/challenge");

    printf("\n   loginStatus            Gets the Login Information");
    printf("\n   loginHSM               Login to the HSM");
    printf("\n   logoutHSM              Logout from the HSM");

    printf("\n   generateKEK            Generates Key Encryption Key");
    printf("\n   getFWVersion           Gets the firmware version");
    printf("\n   getLoginFailCount      Gets the login failure count of an user");
#ifdef NIC_ENABLE
    printf
        ("\n   setNetConfig           Configures the HSM Network interface");
#endif

#ifndef CLOUD_HSM_CLIENT
    printf("\n   closeAllSessions       closes all the open sessions");
    printf
        ("\n   closePartitionSessions closes all the open sessions of partition");
#endif

    printf("\n\n\tKey Generation Commands");
    printf("\n\n\tAsymmetric Keys:");
    printf("\n   genRSAKeyPair          Generates an RSA Key Pair");
    printf("\n   genDSAKeyPair          Generates a DSA Key Pair");
    printf("\n   genECCKeyPair          Generates an ECC Key Pair");
    printf("\n\n\tSymmetric Keys:");
    printf("\n   genPBEKey              Generates a PBE DES3 key");
    printf("\n   genSymKey              Generates a Symmetric keys");

    printf("\n\n\tKey Import/Export Commands");
    printf("\n   createPublicKey        Creates an RSA public key");
    printf("\n   importPubKey           Imports RSA/DSA/EC Public key");
    printf("\n   exportPubKey           Exports RSA/DSA/EC Public key");
    printf("\n   importPrivateKey       Imports RSA/DSA/EC private key");
    printf("\n   importRawRSAPrivateKey Imports RSA private key in cavium custom format");
    printf("\n   exportPrivateKey       Exports RSA/DSA/EC private key");
    printf("\n   imSymKey               Imports a Symmetric key");
    printf("\n   exSymKey               Exports a Symmetric key");
    printf("\n   wrapKey                Wraps a key with an AES key");
    printf("\n                          existing on HSM or KEK");
    printf("\n   unWrapKey              UnWraps a key with an AES key");
    printf("\n                          existing on HSM or KEK");

    printf("\n\n\tKey Management Commands");
    printf("\n   deleteKey              Delete Key");
    printf("\n   findKey                Find Key");
    printf("\n   getAttribute           Reads an attribute from an object");
    printf("\n   setAttribute           Sets an attribute of an object");

    printf("\n\n\tCertificate Setup Commands");
    printf("\n   getCertReq             Gets Partition's Certificate Signing Request");
    printf("\n   storeCert              Stores Partition Owner's Certificate or");
    printf("\n                          Partition's Certificate Signed by Partition Owner");
    printf("\n   getCert                Gets Partition Certificates stored on HSM");

    printf("\n\n\tCertificate based mutual authentication Commands");
    printf("\n   getSourceRandom        Gets Random Number chosen by Source");
    printf("\n   validateCert           Validates Target's Certificate and optionally stores Targets Random Number");
    printf("\n   sourceKeyExchange      Generate Source Key Exchange Message");
    printf("\n   targetKeyExchange      Validate Target Key Exchange Message");

    printf("\n\n\tHSM Cloning Commands");
    printf
        ("\n   cloneSourceStart       Cloning: Fetch the values for Target");
    printf
        ("\n   cloneSourceEnd         Cloning: Pass the values from Target");
    printf
        ("\n   cloneTargetStart       Cloning: Pass the values from Source");
    printf
        ("\n   cloneTargetEnd         Cloning: Fetch the values for Source");

    printf("\n\n\tKey Transfer Commands");
    printf("\n   insertMaskedObject     Inserts a masked object");
    printf("\n   extractMaskedObject    Extracts a masked object");

    printf("\n\n\tManagement Crypto Commands");
    printf("\n   sign                   Generates a signature");
    printf("\n   verify                 Verifies a signature");
    printf("\n   aesWrapUnwrap          Does NIST AES Wrap/Unwrap");

#ifdef ENABLE_EAP_PAC
    printf("\n   eapPacHs               Does PAC based handshake EAP-FAST");
#endif

    printf("\n\n\tBackup/Restore Commands");
    printf("\n   storeUserFixedKey      Stores Users fixed pre shared key ");
    printf
        ("\n    backupPartition        Backup partition's Configuration and Users ");
    //printf("\n    backupHSM              Backup HSM's Configuration and Users ");
    printf
        ("\n    restorePartition       Restore Partition's Configuration and Users(optional)");
    //printf("\n    restoreHSM             Restore HSM's Configuration and Users ");
#ifdef CERT_OPS
    printf("\n    createNode         Prepares the partition to become the node");
    printf("\n\n\tCertificate Commands");
    printf("\n    exportPartnCSR         Export appliance and HSM Partition CSR ");
    printf("\n    importPartnCert        Import Certificates signed by PO ");
    printf("\n    exportPartnCert        Export all Partition certificates ");
    printf("\n    exportAppCert          Export all appliance key signed certificates, Cavium signed appliance certificate and Cavium certificate ");

    printf("\n    importCavClientCert    Import Certificates for cav-client connections ");
    printf("\n    exportCavClientCert    Export Cav-Client certificates that are imported ");
    printf("\n    deleteCavClientCert    Delete a Cav-Client certificate ");
    printf("\n    listCavClientCert      List all the Cav-Client certificates ");
#endif
    printf("\n\n\tHelper Commands");
    printf("\n   Error2String           Converts Error codes to Strings");
    printf("\n   convert2CaviumPrivKey  Imports an RSA private key and");
    printf("\n                          save key handle in fake PEM format");
    printf("\n   getCaviumPrivKey       Saves an RSA private key handle");
    printf("\n                          in fake PEM format");
    printf("\n   IsValidKeyHandlefile   Checks if private key file has");
    printf("\n                          an HSM key handle or a real key");
    printf
        ("\n   listAttributes         List all attributes for getAttributes");
    printf("\n   listECCCurveIds        List HSM supported ECC CurveIds");
    printf("\n\n\tAudit Log Commands");
    printf("\n   getAuditLogs           List Audit Logs");

#ifdef SINGLE_VM
    printf("\n   getRecurrentAuditLogs     \n\t\t\t\tGet audit logs repeatedly");
    printf("\n   stopRecurrentAuditLogs    \n\t\t\t\tStop getting  audit logs ");
    printf("\n   getImmediateAuditLogs     \n\t\t\t\tGet audit logs immediately");

    printf("\n   setNetworkStats        Set Network Stats");

#endif
    printf("\n\n");
}

/****************************************************************************\
 *
 * FUNCTION     : ReadBinaryFile
 *
 * DESCRIPTION  : Reads a binary file with the input file, allocates memory
 *                to read it and returns the content using the input pointers.
 *                Returns 1 if successful.
 *
 * PARAMETERS   : char *pbFileName
 *                char **ppMemBlock
 *                unsigned long *pulMemSize
 *
 * RETURN VALUE : int
 *
 \****************************************************************************/
int ReadBinaryFile(char *pbFileName, char **ppMemBlock,
           unsigned int *pulMemSize)
{
    int isOK = 1;               // Return Code
    int fileHandle;             // File Handle
    int isFileOpen = 0;         // File Open Flag

    //      Verify Pointers
    if (!pbFileName || !ppMemBlock || !pulMemSize) {
        isOK = 0;
    }
    //      Open File
    if (isOK) {
        fileHandle = open(pbFileName, O_RDONLY);
        if (fileHandle == -1) {
            isOK = 0;
        } else {
            isFileOpen = 1;
        }
    }
    //      Get File Size
    if (isOK) {
        struct stat fileStat;
        if (fstat(fileHandle, &fileStat)) {
            isOK = 0;
        }
        *pulMemSize = fileStat.st_size;
    }
    if (isOK) {
        // Allocate Memory to Read File
        *ppMemBlock = malloc(*pulMemSize);
        if (!*ppMemBlock) {
            isOK = 0;
        } else {
            memset(*ppMemBlock, 0, *pulMemSize);
        }
    }
    //   Read File
    if (isOK) {
        unsigned int bytesSupplied = (unsigned int)*pulMemSize;
        unsigned int bytesRead;

        bytesRead = read(fileHandle, *ppMemBlock, bytesSupplied);
        if (bytesRead <= 0) {
            //  Error While Reading
            isOK = 0;
        }
    }
    //    Close File Handle
    if (isFileOpen) {
        close(fileHandle);
    }
    //    Free Allocated Memory
    return isOK;
}

int map_file(char *inPathName, void **outDataPtr, size_t *outDataLength)
{
    int outError = 0;
    int fileDescriptor;
    struct stat statInfo;
    // Return safe values on error.
    *outDataPtr = NULL;
    *outDataLength = 0;

    // Open the file.
    fileDescriptor = open(inPathName, O_RDONLY, 0);

    if (fileDescriptor < 0) {
        outError = errno;
        print_debug("map_file: open failed\n");
    } else {
        // We now know the file exists. Retrieve the file size.
        if (fstat(fileDescriptor, &statInfo) != 0) {
            outError = errno;
            print_debug("map_file: fstat failed\n");
        } else {
            usleep(1000);
            // Map the file into a read-only memory region.
            *outDataPtr = mmap(0, statInfo.st_size, PROT_READ, MAP_SHARED,
                       fileDescriptor, 0);
            if (*outDataPtr == MAP_FAILED) {
                outError = errno;
                print_debug("map_file: mmap failed\n");
            } else {
                *outDataLength = statInfo.st_size;
            }
        }
        close(fileDescriptor);
    }
    return outError;
}

int ReadFileByMap(char *pbFileName, char **ppMemBlock,
          unsigned int *pulMemSize)
{
    int isOK = 1;               // Return Code
    size_t len1;
    void *data1;

    //    Verify Pointers
    if (!pbFileName || !ppMemBlock || !pulMemSize) {
        isOK = 0;
        return isOK;
    }
    //    Open File
    if (map_file(pbFileName, &data1, &len1)) {
        print_debug("Could not map file %s\n", pbFileName);
        isOK = 0;
        return isOK;
    }

    *pulMemSize = len1;
    *ppMemBlock = malloc(*pulMemSize);
    if (!*ppMemBlock) {
        isOK = 0;
    } else {
        memset(*ppMemBlock, 0, *pulMemSize);
        memcpy(*ppMemBlock, data1, len1);
        munmap(data1, len1);
    }

    return isOK;

}

//******************************************************************8
//
//                        write bin file
//
//*********************************************************************
int WriteBinaryFile(char *pbFileName, char *pMemBlock,
            unsigned long ulMemSize)
{
    int isOK = 1;               // Return Code
    int fileHandle;             // File Handle
    int isFileOpen = 0;         // File Open Flag

    //       Verify Pointers
    if (!pbFileName || !pMemBlock || (ulMemSize <= 0)) {
        isOK = 0;
    }
    //       Open File
    if (isOK) {
        fileHandle =
            open(pbFileName, O_WRONLY | O_CREAT | O_TRUNC,
                 S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
        if (fileHandle == -1) {
            isOK = 0;
        } else {
            isFileOpen = 1;
        }
    }
    // Write file
    if (isOK) {
        unsigned int bytesRead;
        bytesRead = write(fileHandle, pMemBlock, ulMemSize);
        if (bytesRead <= 0) {
            isOK = 0;
        }
    }
    //    Close File Handle
    if (isFileOpen) {
        close(fileHandle);
    }
    return isOK;
}

// reads a line of input and NULL terminates it, returning len without NULL
unsigned int GetConsoleString(char *pPrompt,
                  char *pBuffer, unsigned int nBufferSize)
{
    unsigned int nCharsRead = 0;
    int ch;
    if (pPrompt) {
        printf("%s ", pPrompt);
    }
    ch = getchar();
    while ((ch != EOF) && (ch != '\n')) {
        if ((ch != '\r') && ((nCharsRead + 1) < nBufferSize)) {
            pBuffer[nCharsRead++] = ch;
        }
        ch = getchar();
    }
    // Null terminate the string before returning
    pBuffer[nCharsRead] = 0;
    return (nCharsRead);
}

int getAttributeValue(Uint32 ulAttribute,
              Uint8 ** pAttribute, Uint32 * ulAttributeLen)
{
    Uint32 ulRet = 0;

    switch (ulAttribute) {

        // Boolean Attributes
    case OBJ_ATTR_ENCRYPT:
    case OBJ_ATTR_DECRYPT:
    case OBJ_ATTR_WRAP:
    case OBJ_ATTR_UNWRAP:
        {
            Uint32 ulVal = 0;
            Uint8 bValue = 0;
            printf("\t\tThis attribute is defined as a boolean value.\n");
            printf("\t\tEnter the boolean attribute value (0 or 1):");
            scanf("%d", &ulVal);
            if (ulVal)
                bValue = '1';
            else if (ulVal == 0)
                bValue = '0';
            //bValue = (Uint8)ulVal;
            *pAttribute = (Uint8 *) malloc(2);

            print_debug("\t\tSending the attribute value %d:\n", bValue);
            if (pAttribute == 0) {
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
            } else {
                memcpy(*pAttribute, &bValue, sizeof(bValue));
                (*pAttribute)[1] = '\0';
                *ulAttributeLen = sizeof(bValue) + 1;
            }
            print_debug("\t\tSending the attribute of size %d:\n",
                    *ulAttributeLen);
        }
        break;

        // string attributes
    case OBJ_ATTR_LABEL:
        {
            Uint8 Buffer[128];
            Uint8 *pBuffer = Buffer;
            printf("\t\tThis attribute is defined as a string.\n");
            printf
                ("\t\tEnter the string attribute value (no spaces, max 128 characters):");
            scanf("%s", pBuffer);
            *pAttribute = (Uint8 *) malloc(strlen((char *)pBuffer));
            if (pAttribute == 0) {
                ulRet = ERR_MEMORY_ALLOC_FAILURE;
            } else {
                memcpy(*pAttribute, pBuffer, strlen((char *)pBuffer));
                *ulAttributeLen = strlen((char *)pBuffer);
            }
        }
        break;

    default:
        printf("\n\tThis attribute is either not modifiable or unknown");
        ulRet = ERR_ARGUMENTS_BAD;
        break;
    }

    return ulRet;
}

/*
 * getPswdString()
 * ==============
 * This function retrieves a pin string from the user.
 * It modifies the console mode before starting so that the
 * characters the user types are not echoed, and a '*'
 * character is displayed for each typed character instead.
 *
 * Backspace is supported, but we don't get any fancier than that.
 *
 * If -1 is passed in for ulMinLen, the size constraints are not checked.
 *
 * return codes
 *       0 == OK
 *       1 == problem with len constraints
 *       2 == problem with input params
 *       3+ == problems manipulating the console
 */

Uint32 getPswdString(char *pw, int *pulLen, int ulMinLen, int ulMaxLen)
{
    char *pBuffer = pw;
    int len = 0;
    char c = 0;
    int retVal = 0;

    // Unfortunately, the method of turning off character echo is different for
    // Windows and Unix platforms.  So we have to conditionally compile the appropriate
    // section.  Even the basic password retrieval is slightly different, since
    // Windows and Unix use different character codes for the return key.


    struct termios tio;
    int fd;
    int rc;
    cc_t old_min, old_time;
    char termbuff[200];

    if ((pw == 0) || (ulMinLen == 0) || (ulMaxLen < ulMinLen))
        return 2;

    *pulLen = 0;
    *pw = '\0';

    // Before we change mode on the terminal, we need to flush stdout (else
    // our prompt doesn't get shown until the user starts typing).
    fflush(stdout);

    fd = open(ctermid(termbuff), O_RDONLY);
    if (fd == -1) {
        return 3;
    }

    rc = tcgetattr(fd, &tio);
    if (rc == -1) {
        close(fd);
        return 3;
    }

    /* turn off canonical mode & echo */
    old_min = tio.c_cc[VMIN];
    old_time = tio.c_cc[VTIME];
    tio.c_lflag = tio.c_lflag & ~ICANON & ~ECHO;
    tio.c_cc[VMIN] = 1;
    tio.c_cc[VTIME] = 0;

    rc = tcsetattr(fd, TCSADRAIN, &tio);
    if (rc == -1) {
        close(fd);
        return 3;
    }

    while ((c != '\n') && (retVal == 0)) {
        rc = read(fd, &c, 1);
        if (rc != 0) {
            if (c != '\n') {
                // check for backspace
                if (c != '\b' && c != 127) { // BS can be either 0x8(\b) or 0x127 on unix
                    // neither CR nor BS -- add it to the password string
                    if (len >= ulMaxLen) {
                        printf("\b \b");
                        printf("*");
                        fflush(stdout);
                        *pw = c;
                    } else {
                        printf("*");
                        fflush(stdout);
                        *pw++ = c;
                        len++;
                    }
                } else {
                    // handle backspace -- delete the last character & erase it from the screen
                    if (len > 0) {
                        pw--;
                        len--;
                        printf("\b \b");
                        fflush(stdout);
                    }
                }
            }
        } else {
            close(fd);
            return 3;
        }
    }
    *pw++ = '\0';
    printf("\n");

    /* return terminal to its original state */
    tio.c_lflag = tio.c_lflag | ICANON | ECHO;
    tio.c_cc[VMIN] = old_min;
    tio.c_cc[VTIME] = old_time;

    rc = tcsetattr(fd, TCSADRAIN, &tio);
    if (rc == -1) {
        close(fd);
        return 3;
    }

    close(fd);

    // check len restrictions here
    *pulLen = strlen(pBuffer);
    if (ulMinLen != -1)
        if ((*pulLen < ulMinLen) || (*pulLen > ulMaxLen))
            return 1;

    return 0;
}

/********************************************************
 *
 * GetPassword()
 *
 *  returns size of password read, also asigne new len to pulBufferLen.
 ********************************************************/
int GetPassword(char *pPrompt, char *pPrompt2, char *pBuffer,
        int *pulBufferLen)
{
    int retVal = -1;
    int ulMinLen = -1;
    int ulMaxLen = -1;

    char pBuffer2[256] = { };   //HSM's defined max pin len
    int ulBufferLen2 = 256;

    char *prompt = pPrompt;
    char *pBuff = pBuffer;
    int *pulBuffLen = pulBufferLen;

    ulMinLen = 1;
    ulMaxLen = 255;

    while (retVal == -1) {
        printf("\n\t%s: ", prompt);
        switch (getPswdString(pBuff, pulBuffLen, ulMinLen, ulMaxLen)) {
        case 0:
            /* ok. good to go. Either confirm the string, or return
               the string. */
            if (prompt == pPrompt2) {
                // compare new and old
                if (strcmp(pBuffer, pBuffer2) == 0)
                    retVal = *pulBufferLen;
                else {
                    printf("\n\tThe passwords are not the same.\n");
                    // reset everything so that the first password is requested again.
                    prompt = pPrompt;
                    pBuff = pBuffer;
                    pulBuffLen = pulBufferLen;
                }
            } else if (pPrompt2) {
                prompt = pPrompt2;
                pBuff = pBuffer2;
                pulBuffLen = &ulBufferLen2;
            } else
                retVal = *pulBufferLen;
            break;

        case 1:
            // we don't meet our length constraints.  loop again until we get it
            printf
                ("\n\tThe password must be between 1 and 255 characters long.\n");
            break;

        case 2:
            // this is probably a programming error
            retVal = 0;
            break;

        default:
            // 3 or higher is a problem manipulating the console
            retVal = 0;
            break;
        };
    }
    return *pulBufferLen;
}
