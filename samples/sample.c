#include"Cfm2Util.h"
int pkcsWrapKey(int argc, char ** argv)
{
    Uint32 i = 0;
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;

    Uint8 bWrappingKeyHandle = FALSE;
    Uint32 ulWrappingKeyHandle = 0;

    Uint8 bKeyHandle = FALSE;
    Uint32 ulKeyHandle = 0;

    Uint8 bFile = FALSE;
    char *KeyFile = NULL;

    Uint8 *pBuf = NULL;
    Uint8 *pData = NULL;
    Uint32 ulDataLen = 4096;

    Uint8 *pIV = 0;
    Uint32 ulIVLen = 8;

    Uint32 ulMech = CRYPTO_MECH_AES_KEY_WRAP;
    CK_ULONG ulWrappedKeyLen;
    CK_RV rv;
    CK_BYTE wrappedKey[ulDataLen];
    CK_MECHANISM mechanism = {
    	CKM_DES3_ECB, NULL_PTR, 0
	};

    for (i = 2; i < argc; i = i + 2) {
        
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        
        else if ((strcmp(argv[i], "-w") == 0) && (argc > i + 1))
            bWrappingKeyHandle =
                readIntegerArg(argv[i + 1], &ulWrappingKeyHandle);
        
        else if ((!bKeyHandle) && (strcmp(argv[i], "-k") == 0)
             && (argc > i + 1))
            bKeyHandle = readIntegerArg(argv[i + 1], &ulKeyHandle);
        else if ((!bFile) && (strcmp(argv[i], "-out") == 0)
             && (argc > i + 1)) {
            KeyFile = argv[i+1];
            bFile = 1;
        }
        else
            bHelp = TRUE;
    }

   
    if (!bHelp && !bWrappingKeyHandle) {
        printf("\n\tError: wrapping Key handle (-w) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bKeyHandle) {
        printf("\n\tError: key to be wrapped (-k) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bFile) {
        printf("\n\tError: Wrapped Key File (-out) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nWraps sensitive keys from HSM to host.");
        printf("\n");
        printf
            ("\nSyntax: wrapKey -h -k <key to be wrapped> -w <wrapping key handle> -out <wrapped key file>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -k  handle of the key to be wrapped");
        printf
            ("\n       -w  specifies the wrapping key handle (KEK handle - 4)");
        printf("\n       -out  specifies the file to write the wrapped key data");
        printf("\n");
        return ulRet;
    }

     printf("\nSession Handle: %d\n", session_handle);
     printf("\nulWrappingKeyHandle: %d, ulKeyHandle: %d\n", 
		ulWrappingKeyHandle, ulKeyHandle);
     rv = C_WrapKey(session_handle, &mechanism,
               ulWrappingKeyHandle, ulKeyHandle, wrappedKey, &ulWrappedKeyLen);

    if(rv != CKR_OK)
	printf("\nError %s\n", perror);
     else
	printf("\nWrapped Key %s\n", pData);
}
int pkcsGetHSMInfo()
{
        CK_SESSION_HANDLE session;
	CK_ULONG ulCount;
	CK_SLOT_ID_PTR pSlotList;
	CK_SLOT_INFO slotInfo;
	CK_TOKEN_INFO pInfo;
	CK_RV rv;

       rv = (C_Initialize) (NULL);
	if(CKR_OK != rv )
	{
		printf("\nC_Initialize() failed with %08lx\n", rv);
		return -1;
	}
	printf("\nInitialization return value is %d",rv);




	rv = C_GetSlotList(CK_FALSE, NULL_PTR, &ulCount);
	if ((rv == CKR_OK) && (ulCount > 0)) {
    		pSlotList = (CK_SLOT_ID_PTR) malloc(ulCount * sizeof(CK_SLOT_ID));
    		rv = C_GetSlotList(CK_FALSE, pSlotList, &ulCount);
    		if(rv != CKR_OK)
		{
			printf(" Get Slotlist Failed");
			return rv;
		}
		/* Get slot information for first slot */
    		rv = C_GetSlotInfo(pSlotList[0], &slotInfo);
    		if(rv != CKR_OK)
		{
			printf("\nGet SlotInfo for %d Failed\n", pSlotList[0]);
			return rv;
		}

	}	

	printf("\nCount of the Slots %d\n", ulCount);

	rv = C_GetTokenInfo(pSlotList[0], &pInfo);
  	printf("\nLabel: %s",pInfo.label);
  	printf("\nManufacture ID: %s",pInfo.manufacturerID);
  	printf("\nModel: %s",pInfo.model);
  	printf("\nSerial Number: %x",pInfo.serialNumber);
  	printf("\nFlags: %02x",pInfo.flags);
  	printf("\nMax Session count: %d",pInfo.ulMaxSessionCount);
  	printf("\nOpen Sessions: %d",pInfo.ulSessionCount);
  	printf("\nMax R/W Sessions: %d",pInfo.ulMaxRwSessionCount);
  	printf("\nOpen R/W Sessions: %d",pInfo.ulRwSessionCount);
  	printf("\nMax PIN Length: %d",pInfo.ulMaxPinLen);
  	printf("\nMin PIN Length: %d",pInfo.ulMinPinLen);
  	printf("\nTotal Public Memory: %d",pInfo.ulTotalPublicMemory);
  	printf("\nFree Public Memory: %d",pInfo.ulFreePublicMemory);
  	printf("\nTotal Privavte Memory: %d",pInfo.ulTotalPrivateMemory);
  	printf("\nFree Private Memory: %d",pInfo.ulFreePrivateMemory);
  	printf("\nHardware Minor Version: %d",pInfo.hardwareVersion.minor);
  	printf("\nHardware Major Version: %d",pInfo.hardwareVersion.major);
  	printf("\nFirmware Minor Version: %d",pInfo.firmwareVersion.minor);
  	printf("\nFirmware Major Version: %d\n",pInfo.firmwareVersion.major);
}
int main(int argc, char ** argv)
{
	//pkcsGetHSMInfo();
	login(8, argv);
	pkcsWrapKey(argc - 6, argv+6);
}
