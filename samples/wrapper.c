#include"Cfm2Util.h"


int main(int argc, char **argv)
{
	printf("\nNumber of Agruments: %d", argc);
	printf("\nCommand Being Executed: %s", argv[1]);
	
	
	/* Command Related calls */
	if(strcmp(argv[1], "loginHSM") == 0)
	{
		login(argc, argv);
	}
	else if(strcmp(argv[1], "initHSM") == 0)
	{
		login(8, argv);
		initializeHSM((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "zeroize") == 0)
	{
		login(8, argv);
		zeroize((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "generatePEK") == 0)
	{
		login(8, argv);
		generatePEK((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "getHSMInfo") == 0)
	{
		login(8, argv);
		getHSMInfo((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "createUser") == 0)
	{
		login(8, argv);
		createUser((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "deleteUser") == 0)
	{
		login(8, argv);
		deleteUser((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "listUsers") == 0)
	{
		login(8, argv);
		listUsers((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "changePswd") == 0)
	{
		login(8, argv);
		changePswd((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "loginStatus") == 0)
	{
		login(8, argv);
		loginStatus((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "logoutHSM") == 0)
	{
		login(8, argv);
		logout((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "generateKEK") == 0)
	{
		login(8, argv);
		generateKEK((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "getFWVersion") == 0)
	{
		login(8, argv);
		getFWVer((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "getLoginFailCount") == 0)
	{
		login(8, argv);
		getLoginFailCount((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "closeAllSessions") == 0)
	{
		login(8, argv);
		closeAllSessions((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "closePartitionSessions") == 0)
	{
		login(8, argv);
		closePartitionSessions((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "genRSAKeyPair") == 0)
	{
		login(8, argv);
		genRSAKeyPair((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "genDSAKeyPair") == 0)
	{
		login(8, argv);
		genDSAKeyPair((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "genECCKeyPair") == 0)
	{
		login(8, argv);
		genECCKeyPair((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "genPBEKey") == 0)
	{
		login(8, argv);
		genPBEKey((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "genSymKey") == 0)
	{
		login(8, argv);
		genSymKey((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "createPublicKey") == 0)
	{
		login(8, argv);
		createPublicKey((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "importPubKey") == 0)
	{
		login(8, argv);
		importPublicKey((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "importPrivateKey") == 0)
	{
		login(8, argv);
		importPrivateKey((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "importRawRSAPrivateKey") == 0)
	{
		login(8, argv);
		importRawRSAPrivateKey((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "exportPrivateKey") == 0)
	{
		login(8, argv);
		exportPrivateKey((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "imSymKey") == 0)
	{
		login(8, argv);
		imSymKey((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "wrapKey") == 0)
	{
		login(8, argv);
		wrapKey((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "unWrapKey") == 0)
	{
		login(8, argv);
		unWrapKey((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "findKey") == 0)
	{
		login(8, argv);
		findKey((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "getAttribute") == 0)
	{
		login(8, argv);
		getAttribute((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "setAttribute") == 0)
	{
		login(8, argv);
		setAttribute((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "getCertReq") == 0)
	{
		login(8, argv);
		getCertReq((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "storeCert") == 0)
	{
		login(8, argv);
		storeCert((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "getCert") == 0)
	{
		login(8, argv);
		getCert((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "getSourceRandom") == 0)
	{
		login(8, argv);
		getSourceRandom((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "validateCert") == 0)
	{
		login(8, argv);
		validateCert((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "sourceKeyExchange") == 0)
	{
		login(8, argv);
		sourceKeyExchange((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "targetKeyExchange") == 0)
	{
		login(8, argv);
		targetKeyExchange((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "cloneSourceStart") == 0)
	{
		login(8, argv);
		cloneSourceStart((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "cloneSourceEnd") == 0)
	{
		login(8, argv);
		cloneSourceEnd((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "cloneTargetStart") == 0)
	{
		login(8, argv);
		cloneTargetStart((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "cloneTargetEnd") == 0)
	{
		login(8, argv);
		cloneTargetEnd((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "insertMaskedObject") == 0)
	{
		login(8, argv);
		insertMaskedObject((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "extractMaskedObject") == 0)
	{
		login(8, argv);
		extractMaskedObject((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "aesWrapUnwrap") == 0)
	{
		login(8, argv);
		aesWrapUnwrap((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "storeUserFixedKey") == 0)
	{
		login(8, argv);
		storeUserFixedKey((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "backupPartition") == 0)
	{
		login(8, argv);
		backupPartition((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "restorePartition") == 0)
	{
		login(8, argv);
		restorePartition((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "convert2CaviumPrivKey") == 0)
	{
		login(8, argv);
		convert2CaviumPrivKey((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "getCaviumPrivKey") == 0)
	{
		login(8, argv);
		getCaviumPrivKey((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "IsValidKeyHandlefile") == 0)
	{
		login(8, argv);
		IsValidKeyHandlefile((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "listAttributes") == 0)
	{
		listAttributes((argc),(argv));
	}
	else if(strcmp(argv[1], "listECCCurveIds") == 0)
	{
		listECCCurveIds((argc),(argv));
	}
	else if(strcmp(argv[1], "getAuditLogs") == 0)
	{
		login(8, argv);
		getAuditLogs((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "sign") == 0)
	{
		login(8, argv);
		sign((argc - 6),(argv + 6));
	}
	else if(strcmp(argv[1], "verify") == 0)
	{
		login(8, argv);
		verify((argc - 6),(argv + 6));
	}
	else
	{
		printf("\nCommand Mismatch :%s\n", argv[1]);
	}
}
