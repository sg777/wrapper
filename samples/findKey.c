#include"Cfm2Util.h"
void
show_key_info(CK_SESSION_HANDLE session, CK_OBJECT_HANDLE key)
{
     CK_RV rv;
     CK_UTF8CHAR *label = (CK_UTF8CHAR *) malloc(80);
     CK_BYTE *id = (CK_BYTE *) malloc(10);
     size_t label_len;
     char *label_str;

     memset(id, 0, 10);

     CK_ATTRIBUTE template[] = {
          {CKA_LABEL, label, 80},
          {CKA_ID, id, 1}
     };

	printf("\nKey Handle: %d\n", key);
     rv = C_GetAttributeValue(session, key, template, 2);
     check_return_value(rv, "get attribute value");

     fprintf(stdout, "Found a key:\n");
     label_len = template[0].ulValueLen;
     if (label_len > 0) {
          label_str = malloc(label_len + 1);
          memcpy(label_str, label, label_len);
          label_str[label_len] = '\0';
          fprintf(stdout, "\tKey label: %s\n", label_str);
          free(label_str);
     } else {
          fprintf(stdout, "\tKey label too large, or not found\n");
     }
     if (template[1].ulValueLen > 0) {
          fprintf(stdout, "\tKey ID: %02x\n", id[0]);
     } else {
          fprintf(stdout, "\tKey id too large, or not found\n");
     }

     free(label);
     free(id);
}
void
check_return_value(CK_RV rv, const char *message)
{
        if (rv != CKR_OK) {
                fprintf(stderr, "Error at %s: %u\n",
                        message, (unsigned int)rv);
                exit(EXIT_FAILURE);
        }
	else
		printf("\nSuccessfull: %s\n", message);
}
CK_RV
initialize()
{
        return C_Initialize(NULL);
}


CK_SLOT_ID
get_slot()
{
     CK_RV rv;
     CK_SLOT_ID slotId;
     CK_ULONG slotCount = 10;
     CK_SLOT_ID *slotIds = malloc(sizeof(CK_SLOT_ID) * slotCount);

     rv = C_GetSlotList(CK_TRUE, slotIds, &slotCount);
     check_return_value(rv, "get slot list");

     if (slotCount < 1) {
          fprintf(stderr, "Error; could not find any slots\n");
          exit(1);
     }

     slotId = slotIds[0];
     free(slotIds);
     printf("slot count: %d\n", (int)slotCount);
     return slotId;
}


CK_SESSION_HANDLE
start_session(CK_SLOT_ID slotId)
{
        CK_RV rv;
        CK_SESSION_HANDLE session;
        rv = C_OpenSession(slotId,
                           CKF_SERIAL_SESSION,
                           NULL,
                           NULL,
                           &session);
        check_return_value(rv, "open session");
        return session;
}



void
login_new(CK_SESSION_HANDLE session, CK_BYTE *pin)
{
     CK_RV rv;
     if (pin) {
          rv = C_Login(session, CKU_USER, pin, strlen((char *)pin));
          check_return_value(rv, "log in");
     }
}


void
logout_new(CK_SESSION_HANDLE session)
{
     CK_RV rv;
     rv = C_Logout(session);
     if (rv != CKR_USER_NOT_LOGGED_IN) {
          check_return_value(rv, "log out");
     }
}



void
end_session(CK_SESSION_HANDLE session)
{
        CK_RV rv;
        rv = C_CloseSession(session);
        check_return_value(rv, "close session");
}
void
finalize()
{
        C_Finalize(NULL);
}
void
read_private_keys(session)
{
     CK_RV rv;
     CK_OBJECT_CLASS keyClass = CKO_PUBLIC_KEY;
     CK_ATTRIBUTE template[] = {
          { CKA_CLASS, &keyClass, sizeof(keyClass) }
     };
     CK_ULONG objectCount;
     CK_OBJECT_HANDLE object;

     rv = C_FindObjectsInit(session, template, 1);
     check_return_value(rv, "Find objects init");

     rv = C_FindObjects(session, &object, 1, &objectCount);
     check_return_value(rv, "Find first object");

     while (objectCount > 0) {
        //  show_key_info(session, object);
	printf("\nKey : %d\n", object);
          rv = C_FindObjects(session, &object, 1, &objectCount);
          check_return_value(rv, "Find other objects");
     }

     rv = C_FindObjectsFinal(session);
     check_return_value(rv, "Find objects final");
}


int
main(int argc, char **argv)
{
     CK_SLOT_ID slot;
     CK_SESSION_HANDLE session;
     CK_BYTE *userPin = "testuser:password";
     CK_RV rv;

     if (argc > 1) {
          if (strcmp(argv[1], "null") == 0) {
               userPin = NULL;
          } else {
               userPin = (CK_BYTE *) argv[1];
          }
     }

     rv = initialize();
     check_return_value(rv, "initialize");
     slot = get_slot();
     session = start_session(slot);
     login_new(session, userPin);
     read_private_keys(session);
     logout_new(session);
     end_session(session);
     finalize();
     return EXIT_SUCCESS;
}
