#include"Cfm2Util.h"

int pkcsGetHSMInfo()
{
        CK_SESSION_HANDLE session;
	CK_ULONG ulCount;
	CK_SLOT_ID_PTR pSlotList;
	CK_SLOT_INFO slotInfo;
	CK_TOKEN_INFO pInfo;
	CK_RV rv;

       rv = (C_Initialize) (NULL);
	if(CKR_OK != rv )
	{
		printf("\nC_Initialize() failed with %08lx\n", rv);
		return -1;
	}
	printf("\nInitialization return value is %d",rv);




	rv = C_GetSlotList(CK_FALSE, NULL_PTR, &ulCount);
	if ((rv == CKR_OK) && (ulCount > 0)) {
    		pSlotList = (CK_SLOT_ID_PTR) malloc(ulCount * sizeof(CK_SLOT_ID));
    		rv = C_GetSlotList(CK_FALSE, pSlotList, &ulCount);
    		if(rv != CKR_OK)
		{
			printf(" Get Slotlist Failed");
			return rv;
		}
		/* Get slot information for first slot */
    		rv = C_GetSlotInfo(pSlotList[0], &slotInfo);
    		if(rv != CKR_OK)
		{
			printf("\nGet SlotInfo for %d Failed\n", pSlotList[0]);
			return rv;
		}

	}	

	printf("\nCount of the Slots %d\n", ulCount);

	rv = C_GetTokenInfo(pSlotList[0], &pInfo);
  	printf("\nLabel: %s",pInfo.label);
  	printf("\nManufacture ID: %s",pInfo.manufacturerID);
  	printf("\nModel: %s",pInfo.model);
  	printf("\nSerial Number: %x",pInfo.serialNumber);
  	printf("\nFlags: %02x",pInfo.flags);
  	printf("\nMax Session count: %d",pInfo.ulMaxSessionCount);
  	printf("\nOpen Sessions: %d",pInfo.ulSessionCount);
  	printf("\nMax R/W Sessions: %d",pInfo.ulMaxRwSessionCount);
  	printf("\nOpen R/W Sessions: %d",pInfo.ulRwSessionCount);
  	printf("\nMax PIN Length: %d",pInfo.ulMaxPinLen);
  	printf("\nMin PIN Length: %d",pInfo.ulMinPinLen);
  	printf("\nTotal Public Memory: %d",pInfo.ulTotalPublicMemory);
  	printf("\nFree Public Memory: %d",pInfo.ulFreePublicMemory);
  	printf("\nTotal Privavte Memory: %d",pInfo.ulTotalPrivateMemory);
  	printf("\nFree Private Memory: %d",pInfo.ulFreePrivateMemory);
  	printf("\nHardware Minor Version: %d",pInfo.hardwareVersion.minor);
  	printf("\nHardware Major Version: %d",pInfo.hardwareVersion.major);
  	printf("\nFirmware Minor Version: %d",pInfo.firmwareVersion.minor);
  	printf("\nFirmware Major Version: %d\n",pInfo.firmwareVersion.major);
}
pkcsWrapKey(int argc, char **argv)
{
    Uint32 i = 0;
    Uint32 ulRet = 0;
    Uint8 bHelp = FALSE;

    Uint8 bWrappingKeyHandle = FALSE;
    Uint32 ulWrappingKeyHandle = 0;

    Uint8 bKeyHandle = FALSE;
    Uint32 ulKeyHandle = 0;

    Uint8 bFile = FALSE;
    char *KeyFile = NULL;

    Uint8 *pBuf = NULL;
    Uint8 *pData = NULL;
    Uint32 ulDataLen = 4096;

    Uint8 *pIV = 0;
    Uint32 ulIVLen = 8;

    Uint32 ulMech = CRYPTO_MECH_AES_KEY_WRAP;

    for (i = 2; i < argc; i = i + 2) {
        
        if (strcmp(argv[i], "-h") == 0)
            bHelp = TRUE;
        
        else if ((strcmp(argv[i], "-w") == 0) && (argc > i + 1))
            bWrappingKeyHandle =
                readIntegerArg(argv[i + 1], &ulWrappingKeyHandle);
        
        else if ((!bKeyHandle) && (strcmp(argv[i], "-k") == 0)
             && (argc > i + 1))
            bKeyHandle = readIntegerArg(argv[i + 1], &ulKeyHandle);
        else if ((!bFile) && (strcmp(argv[i], "-out") == 0)
             && (argc > i + 1)) {
            KeyFile = argv[i+1];
            bFile = 1;
        }
        else
            bHelp = TRUE;
    }

   
    if (!bHelp && !bWrappingKeyHandle) {
        printf("\n\tError: wrapping Key handle (-w) is missing.\n");
        bHelp = TRUE;
    }
    if (!bHelp && !bKeyHandle) {
        printf("\n\tError: key to be wrapped (-k) is missing.\n");
        bHelp = TRUE;
    }

    if (!bHelp && !bFile) {
        printf("\n\tError: Wrapped Key File (-out) is missing.\n");
        bHelp = TRUE;
    }

    if (bHelp) {
        printf("\n");
        printf("\nDescription");
        printf("\n===========");
        printf("\nWraps sensitive keys from HSM to host.");
        printf("\n");
        printf
            ("\nSyntax: wrapKey -h -k <key to be wrapped> -w <wrapping key handle> -out <wrapped key file>\n");
        printf("\n");
        printf("\nWhere: -h  displays this information");
        printf("\n       -k  handle of the key to be wrapped");
        printf
            ("\n       -w  specifies the wrapping key handle (KEK handle - 4)");
        printf("\n       -out  specifies the file to write the wrapped key data");
        printf("\n");
        return ulRet;
    }

    pBuf = (Uint8 *) calloc(sizeof(Uint64) + ulIVLen + ulDataLen, 1);
    if (pBuf == NULL) {
        printf("Cannot allocate memory for Wrapped Key data\n");
        return -1;
    }

    /* Store Wrapping Key Handle */
    *(Uint64 *)pBuf = ulWrappingKeyHandle;

    /* Take Random IV */
    pIV = pBuf + sizeof(Uint64);
    RAND_bytes(pIV, ulIVLen);

    /* Point pData to correct offset */
    pData = pBuf + sizeof(Uint64) + ulIVLen;
}
int main(int argc, char **argv)
{
	if(argc > 1)
	{
		printf("\nNumber of Agruments: %d", argc);
		printf("\nCommand Being Executed: %s\n", argv[1]);
	}
	else
	{	
		printf("\nPlease provide a command to execute\n");
		return -1;
	}	
	
	/* Command Related calls */
	if(strcmp(argv[1], "loginHSM") == 0)
	{
		login(argc, argv);
	}
	else if((strcmp(argv[1], "initHSM") == 0) && (argc >= 8))
	{
		login(8, argv);
		initializeHSM((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "zeroize") == 0) && (argc >= 8))
	{
		login(8, argv);
		zeroize((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "generatePEK") == 0) && (argc >= 8))
	{
		login(8, argv);
		generatePEK((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "getHSMInfo") == 0) && (argc >= 2))
	{
		//login(8, argv);
		//getHSMInfo((argc - 6),(argv + 6));
		pkcsGetHSMInfo();
	}
	else if((strcmp(argv[1], "createUser") == 0) && (argc >= 8))
	{
		login(8, argv);
		createUser((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "deleteUser") == 0) && (argc >= 8))
	{
		login(8, argv);
		deleteUser((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "listUsers") == 0) && (argc >= 8))
	{
		login(8, argv);
		listUsers((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "changePswd") == 0) && (argc >= 8))
	{
		login(8, argv);
		changePswd((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "loginStatus") == 0) && (argc >= 8))
	{
		login(8, argv);
		loginStatus((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "logoutHSM") == 0) && (argc >= 8))
	{
		login(8, argv);
		logout((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "generateKEK") == 0) && (argc >= 8))
	{
		login(8, argv);
		generateKEK((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "getFWVersion") == 0) && (argc >= 8))
	{
		login(8, argv);
		getFWVer((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "getLoginFailCount") == 0) && (argc >= 8))
	{
		login(8, argv);
		getLoginFailCount((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "closeAllSessions") == 0) && (argc >= 8))
	{
		login(8, argv);
		closeAllSessions((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "closePartitionSessions") == 0) && (argc >= 8))
	{
		login(8, argv);
		closePartitionSessions((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "genRSAKeyPair") == 0) && (argc >= 8))
	{
		login(8, argv);
		genRSAKeyPair((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "genDSAKeyPair") == 0) && (argc >= 8))
	{
		login(8, argv);
		genDSAKeyPair((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "genECCKeyPair") == 0) && (argc >= 8))
	{
		login(8, argv);
		genECCKeyPair((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "genPBEKey") == 0) && (argc >= 8))
	{
		login(8, argv);
		genPBEKey((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "genSymKey") == 0) && (argc >= 8))
	{
		login(8, argv);
		genSymKey((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "createPublicKey") == 0) && (argc >= 8))
	{
		login(8, argv);
		createPublicKey((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "importPubKey") == 0) && (argc >= 8))
	{
		login(8, argv);
		importPublicKey((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "importPrivateKey") == 0) && (argc >= 8))
	{
		login(8, argv);
		importPrivateKey((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "importRawRSAPrivateKey") == 0) && (argc >= 8))
	{
		login(8, argv);
		importRawRSAPrivateKey((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "exportPrivateKey") == 0) && (argc >= 8))
	{
		login(8, argv);
		exportPrivateKey((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "imSymKey") == 0) && (argc >= 8))
	{
		login(8, argv);
		imSymKey((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "wrapKey") == 0) && (argc >= 8))
	{
		login(8, argv);
		wrapKey((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "unWrapKey") == 0) && (argc >= 8))
	{
		login(8, argv);
		unWrapKey((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "findKey") == 0) && (argc >= 8))
	{
		login(8, argv);
		findKey((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "getAttribute") == 0) && (argc >= 8))
	{
		login(8, argv);
		getAttribute((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "setAttribute") == 0) && (argc >= 8))
	{
		login(8, argv);
		setAttribute((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "getCertReq") == 0) && (argc >= 8))
	{
		login(8, argv);
		getCertReq((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "storeCert") == 0) && (argc >= 8))
	{
		login(8, argv);
		storeCert((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "getCert") == 0) && (argc >= 8))
	{
		login(8, argv);
		getCert((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "getSourceRandom") == 0) && (argc >= 8))
	{
		login(8, argv);
		getSourceRandom((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "validateCert") == 0) && (argc >= 8))
	{
		login(8, argv);
		validateCert((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "sourceKeyExchange") == 0) && (argc >= 8))
	{
		login(8, argv);
		sourceKeyExchange((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "targetKeyExchange") == 0) && (argc >= 8))
	{
		login(8, argv);
		targetKeyExchange((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "cloneSourceStart") == 0) && (argc >= 8))
	{
		login(8, argv);
		cloneSourceStart((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "cloneSourceEnd") == 0) && (argc >= 8))
	{
		login(8, argv);
		cloneSourceEnd((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "cloneTargetStart") == 0) && (argc >= 8))
	{
		login(8, argv);
		cloneTargetStart((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "cloneTargetEnd") == 0) && (argc >= 8))
	{
		login(8, argv);
		cloneTargetEnd((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "insertMaskedObject") == 0) && (argc >= 8))
	{
		login(8, argv);
		insertMaskedObject((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "extractMaskedObject") == 0) && (argc >= 8))
	{
		login(8, argv);
		extractMaskedObject((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "aesWrapUnwrap") == 0) && (argc >= 8))
	{
		login(8, argv);
		aesWrapUnwrap((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "storeUserFixedKey") == 0) && (argc >= 8))
	{
		login(8, argv);
		storeUserFixedKey((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "backupPartition") == 0) && (argc >= 8))
	{
		login(8, argv);
		backupPartition((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "restorePartition") == 0) && (argc >= 8))
	{
		login(8, argv);
		restorePartition((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "convert2CaviumPrivKey") == 0) && (argc >= 8))
	{
		login(8, argv);
		convert2CaviumPrivKey((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "getCaviumPrivKey") == 0) && (argc >= 8))
	{
		login(8, argv);
		getCaviumPrivKey((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "IsValidKeyHandlefile") == 0) && (argc >= 8))
	{
		login(8, argv);
		IsValidKeyHandlefile((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "listAttributes") == 0) && (argc >= 8))
	{
		listAttributes((argc),(argv));
	}
	else if((strcmp(argv[1], "listECCCurveIds") == 0) && (argc >= 8))
	{
		listECCCurveIds((argc),(argv));
	}
	else if((strcmp(argv[1], "getAuditLogs") == 0) && (argc >= 8))
	{
		login(8, argv);
		getAuditLogs((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "sign") == 0) && (argc >= 8))
	{
		login(8, argv);
		sign((argc - 6),(argv + 6));
	}
	else if((strcmp(argv[1], "verify") == 0) && (argc >= 8))
	{
		login(8, argv);
		verify((argc - 6),(argv + 6));
	}
	else
	{
		printf("\nCommand Mismatch :%s\n", argv[1]);
	}
}
