/*
 * Copyright (c) 2003-2017 Cavium Networks (support@cavium.com). All rights 
 * reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 * 
 * 3. All manuals,brochures,user guides mentioning features or use of this software 
 *    must display the following acknowledgement:
 * 
 *   This product includes software developed by Cavium Networks
 * 
 * 4. Cavium Networks' name may not be used to endorse or promote products 
 *    derived from this software without specific prior written permission.
 * 
 * 5. User agrees to enable and utilize only the features and performance 
 *    purchased on the target hardware.
 * 
 * This Software,including technical data,may be subject to U.S. export control 
 * laws, including the U.S. Export Administration Act and its associated 
 * regulations, and may be subject to export or import regulations in other 
 * countries.You warrant that You will comply strictly in all respects with all 
 * such regulations and acknowledge that you have the responsibility to obtain 
 * licenses to export, re-export or import the Software.
 
 * TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS" AND 
 * WITH ALL FAULTS AND CAVIUM MAKES NO PROMISES, REPRESENTATIONS OR WARRANTIES, 
 * EITHER EXPRESS,IMPLIED,STATUTORY, OR OTHERWISE, WITH RESPECT TO THE SOFTWARE,
 * INCLUDING ITS CONDITION,ITS CONFORMITY TO ANY REPRESENTATION OR DESCRIPTION, 
 * OR THE EXISTENCE OF ANY LATENT OR PATENT DEFECTS, AND CAVIUM SPECIFICALLY 
 * DISCLAIMS ALL IMPLIED (IF ANY) WARRANTIES OF TITLE, MERCHANTABILITY, 
 * NONINFRINGEMENT,FITNESS FOR A PARTICULAR PURPOSE,LACK OF VIRUSES, ACCURACY OR
 * COMPLETENESS, QUIET ENJOYMENT, QUIET POSSESSION OR CORRESPONDENCE TO 
 * DESCRIPTION. THE ENTIRE RISK ARISING OUT OF USE OR PERFORMANCE OF THE 
 * SOFTWARE LIES WITH YOU.
 *
 */

typedef char* CHAR_PTR;

// Windows 32 Directives

#include <termios.h>
#include <ctype.h>
#include <unistd.h>
#define _WITH_GETLINE

// Standard "C" Directives
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <ctype.h>
#include <assert.h>
#include <sys/mman.h>
#include <unistd.h>
#include <errno.h>
#include <arpa/inet.h>
#include <netinet/tcp.h>
#include <dlfcn.h>
#include <netdb.h>
#include <openssl/ssl.h>

#include "cavium_defines.h"
#include "cavium_mgmt.h"
#include "cavium_crypto.h"
#include "openssl_util.h"

#include "openssl/pem.h"
#include "openssl/x509.h"
#include "openssl/bn.h"
#include "openssl/rsa.h"
#include "openssl/rand.h"
#include "openssl/err.h"
#include "openssl/ec.h"
#include "openssl/aes.h"
#include "openssl/des.h"

#ifdef BACKUP_WITH_SMARTCARD
#include "opensc_util.h"
#include "eTPkcs11.h"
#include "pkcs11t.h"
#endif
#include "Cfm2Helper.h"

extern unsigned int session_handle;


#ifdef BACKUP_WITH_SMARTCARD
extern CK_FUNCTION_LIST_PTR   pFunctionList;
extern CK_SESSION_HANDLE      sc_session;
extern CK_SLOT_ID*            slots;
extern int                    slot_index;
extern CK_ULONG               slot_count;

extern CK_BBOOL true;
extern CK_BBOOL false;
extern CK_OBJECT_CLASS secret;
extern CK_OBJECT_CLASS public;
extern CK_OBJECT_CLASS private;
extern CK_KEY_TYPE rsa;
extern CK_KEY_TYPE aes;
extern CK_ULONG kbk_rsa_len_in_bits;


extern CK_C_GetFunctionList   pGFL;
extern Uint8                  wasInit;
//char readerName[MAX_NAME_LEN]        = "AKS ifdh 00 00";
extern char user_password[MAX_PSWD_LENGTH] ;
extern char formatPassword[MAX_PSWD_LENGTH];
extern CK_CHAR token_label[MAX_LABEL_LENGTH]; 
#endif

int OpenSocketConnection(const char *hostname, int port)
{   int sd;
    struct hostent *host;
    struct sockaddr_in addr;
 
    if ( (host = gethostbyname(hostname)) == NULL )
    {
        perror(hostname);
        abort();
    }
    sd = socket(PF_INET, SOCK_STREAM, 0);
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = *(long*)(host->h_addr);
    if ( connect(sd, (struct sockaddr*)&addr, sizeof(addr)) != 0 )
    {
        close(sd);
        perror(hostname);
        return -1;
    }
    return sd;
}
 
SSL_CTX* InitSSL_CTX(void)
{   const SSL_METHOD *method;
    SSL_CTX *ctx;
 
    OpenSSL_add_all_algorithms(); 
    SSL_load_error_strings();   

    method = TLSv1_2_client_method();
    ctx = SSL_CTX_new(method);   
    if ( ctx == NULL )
    {
        ERR_print_errors_fp(stderr);
        return NULL;
    }
    SSL_CTX_set_cipher_list(ctx, "AES128-GCM-SHA256"); 
   
    return ctx;
}

Uint32 ImportKBKtoHost(char* pPath, Uint8 p_no, Uint32 ulMech)
{
    Uint32             ulRet      = 0;
    char file[256] = {};

    Uint64 pIV  = 0xA6A6A6A6A6A6A6A6ull;
    //Uint8 pIV[] = {0xA6, 0xA6, 0xA6, 0xA6, 0xA6, 0xA6, 0xA6, 0xA6}  ;


    Uint32  ulWrappedKeyLen_1 = 4096, ulWrappedKeyLen_2 = 4096, ulWrappedKeyLen_3 = 4096 ;
    Uint8   pWrappedKey_1[4096] = {};
    Uint8   pWrappedKey_2[4096] = {};
    Uint8   pWrappedKey_3[4096] = {};

    Uint32 wrapped_keyLen = 0;
    Uint32 keyLen = 0;
    Uint8 *pWKey = NULL;
    Uint8 pKey[40] = {};


    /* Export the KBK from HSM onto the host by wrapping it with the imported
     * public Key. Import this key onto the SC by Unwrapping it with
     * the RSA Priv Key on SC*/
    ulRet = Cfm2ExportKBK(session_handle,
            pWrappedKey_1, pWrappedKey_2, pWrappedKey_3,
            &ulWrappedKeyLen_1, &ulWrappedKeyLen_2, &ulWrappedKeyLen_3, p_no, ulMech);
    if (ulRet != 0)
    {
        printf("\n\tCfm2ExportKBK Failure: 0x%02x : %s\n",
                ulRet, Cfm2ResultAsString(ulRet));
        goto error;
    }

    /* Unwrap the KBK and write it in plain in a file */
    printf("Wrapped KBK size %d \n", ulWrappedKeyLen_1);
    wrapped_keyLen = ulWrappedKeyLen_1;
    pWKey = pWrappedKey_1;

    if (ulMech == KBK_WRAP_WITH_CERT_AUTH_DERIVED_KEY) {
        memcpy(pKey, pWKey, wrapped_keyLen);
        keyLen = wrapped_keyLen;
    } else {
        if (pWKey && 0 < wrapped_keyLen)
        {
            ulRet =  Cfm2AesWrapUnwrapBuffer(
                    session_handle,
                    KEK_HSM_HANDLE,
                    pWKey,
                    wrapped_keyLen,
                    pIV,
                    pKey,
                    0);

            if(RET_OK != ulRet )
            {
                printf("\n\tCfm2AesWrapUnwrapBuffer Failure: 0x%02x : %s\n",
                        ulRet, Cfm2ResultAsString(ulRet));
                goto error;
            }

            keyLen = ulWrappedKeyLen_1 - 8;
            printf("Unwrapped key size %d \n", keyLen);
        }
        else
        {
            print_error("Invalid wrapped key size %d \n", ulWrappedKeyLen_1);
            ulRet = ERR_BAD_LENGTH;
        }

        if(RET_OK != ulRet )
        {
            print_error("Failed to un wrap the key with return code %02x: %s\n", ulRet, Cfm2ResultAsString(ulRet));
            return ulRet;
        }
    }

    /* Write the encrypted KBK with KEK on to the host in a file */
    sprintf(file, "%s/"KBK_UNWRAPPED_WIH_KEK_FILE, pPath);
    ulRet = write_file(file, pKey, keyLen);
    printf("Wrote KBK wrapped with in the file %s\n", file);
error:
    return ulRet;
}

Uint32 ImportKBKtoSC2(Uint8 p_no, char *pname, char * user_password, SSL *ssl)
{
#ifdef BACKUP_WITH_SMARTCARD
    Uint32 ulRet  = 0;
    Uint8* pModulus = (CK_BYTE_PTR)malloc(KBK_IMPORT_MODULUS_SIZE);
    Uint8 *pExponent = (CK_BYTE_PTR)malloc(KBK_IMPORT_EXPONENT_SIZE);
    Uint32 pModLen = 0;
    CK_OBJECT_HANDLE priv_key = 0;

    Uint64  hsm_public_key = 0;
    char  label[MAX_LABEL_LENGTH] = {};
    CK_OBJECT_HANDLE privkey = 0;

    ulRet = Cfm2GetHSMLabel(session_handle, (Uint8 *)label);
    if (ulRet != 0)
    {
        printf("\n\tCfm2GetHSMLabel Failure: 0x%02x : %s\n",
                ulRet, Cfm2ResultAsString(ulRet));
        goto error;
    }

    Uint32  ulWrappedKeyLen_1 = 4096, ulWrappedKeyLen_2 = 4096, ulWrappedKeyLen_3 = 4096 ;
    Uint8   pWrappedKey_1[4096] = {};
    Uint8   pWrappedKey_2[4096] = {};
    Uint8   pWrappedKey_3[4096] = {};

    memset(pModulus, 0, KBK_IMPORT_MODULUS_SIZE);
    memset(pExponent, 0, KBK_IMPORT_EXPONENT_SIZE);

    if(ssl == NULL) {
        ulRet = get_public_mod_exp_from_eToken_for_backup(user_password,pModulus,&pModLen,pExponent,&priv_key);
        if(ulRet)
        {
            printf("\n\tFailed to get public key details from smartCard\n");
            goto error;
        }
    } else {
        df_request req_data;
        df_response resp_data;

        memset(&req_data,0,sizeof(req_data));
        memset(&resp_data,0,sizeof(resp_data));

        req_data.command_type = SC_BACKUP_GET_KEY;
        memcpy(req_data.backup_get_key_data.userPin,user_password,strlen(user_password));

        SSL_write(ssl, &req_data, sizeof(req_data));  
        if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
            if(resp_data.response_code != 0) {
                printf("\n\tFailed to get backup key details from  SmartCard over network\n");
                ulRet = -1;
                goto error;
            }
        } else {
            printf("\n\tSSL_read failed\n");
            ulRet = -1;
            goto error;
        }

        pModLen = resp_data.backup_get_key_resp.pModLen;
        memcpy(pModulus, resp_data.backup_get_key_resp.pModulus, pModLen);
        memcpy(pExponent, resp_data.backup_get_key_resp.pExponent, KBK_IMPORT_EXPONENT_SIZE);
        priv_key = resp_data.backup_get_key_resp.priv_key;
    }

    privkey = priv_key;

    ulRet = Cfm2CreatePublicKey2(session_handle,
            KEY_TYPE_RSA,
            pModLen,
            *(Uint32 *)pExponent,
            0,0,NULL,0,NULL,
            pModulus,
            pModLen,
            NULL,0,
            (Uint8 *)"KBK public key",
            strlen("KBK public key"),
            STORAGE_FLASH,
            p_no,
            &hsm_public_key);
    if(ulRet)
    {
        printf("\n\tCfm2CreatePublicKey returned: 0x%02x : %s\n", ulRet, Cfm2ResultAsString(ulRet));
        goto error;
    }

    /* Export the KBK from HSM onto the host by wrapping it with the imported
     * public Key. Import this key onto the SC by Unwrapping it with
     * the RSA Priv Key on SC*/
    ulRet = Cfm2ExportKBK(session_handle,
            pWrappedKey_1, pWrappedKey_2, pWrappedKey_3,
            &ulWrappedKeyLen_1, &ulWrappedKeyLen_2, &ulWrappedKeyLen_3, p_no, KBK_WRAP_WITH_RSA);
    if (ulRet != 0)
    {
        printf("\n\tCfm2ExportKBK Failure: 0x%02x : %s\n",
                ulRet, Cfm2ResultAsString(ulRet));
        goto error;
    }
   
    if(ssl == NULL) { 
        ulRet = unwrap_kbk_to_etoken_for_backup(pname, label, pWrappedKey_1, ulWrappedKeyLen_1, privkey, 1);
        if(ulRet)
        {
            printf("\n\tFailed to unwrap KBK_1 to smartCard\n");
            goto error;
        }

        ulRet = unwrap_kbk_to_etoken_for_backup(pname, label, pWrappedKey_2, ulWrappedKeyLen_2, privkey, 2);
        if(ulRet)
        {
            printf("\n\tFailed to unwrap KBK_2 to smartCard\n");
            goto error;
        }

        ulRet = unwrap_kbk_to_etoken_for_backup(pname, label, pWrappedKey_3, ulWrappedKeyLen_3, privkey, 3);
        if(ulRet)
        {
            printf("\n\tFailed to unwrap KBK_3 to smartCard\n");
            goto error;
        }
    } else {
        df_request req_data;
        df_response resp_data;

        memset(&req_data,0,sizeof(req_data));
        memset(&resp_data,0,sizeof(resp_data));

        req_data.command_type = SC_BACKUP_UNWRAP_KBK;
        memcpy(req_data.backup_unwrap_kbk_data.pname,pname,strlen(pname));
        memcpy(req_data.backup_unwrap_kbk_data.label,label,strlen(label));
        memcpy(req_data.backup_unwrap_kbk_data.wrappedkey,pWrappedKey_1,ulWrappedKeyLen_1);
        req_data.backup_unwrap_kbk_data.wrappedkey_len = ulWrappedKeyLen_1;
        req_data.backup_unwrap_kbk_data.privkey = privkey;
        req_data.backup_unwrap_kbk_data.wrappedkey_num = 1;

        SSL_write(ssl, &req_data, sizeof(req_data));  
        if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
            if(resp_data.response_code != 0) {
                printf("\n\tFailed to get unwrap KBK_1 to SmartCard over network\n");
                ulRet = -1;
                goto error;
            }
        } else {
            printf("\n\tSSL_read failed\n");
            ulRet = -1;
            goto error;
        }

        memset(&req_data,0,sizeof(req_data));
        memset(&resp_data,0,sizeof(resp_data));

        req_data.command_type = SC_BACKUP_UNWRAP_KBK;
        memcpy(req_data.backup_unwrap_kbk_data.pname,pname,strlen(pname));
        memcpy(req_data.backup_unwrap_kbk_data.label,label,strlen(label));
        memcpy(req_data.backup_unwrap_kbk_data.wrappedkey,pWrappedKey_2,ulWrappedKeyLen_2);
        req_data.backup_unwrap_kbk_data.wrappedkey_len = ulWrappedKeyLen_2;
        req_data.backup_unwrap_kbk_data.privkey = privkey;
        req_data.backup_unwrap_kbk_data.wrappedkey_num = 2;

        SSL_write(ssl, &req_data, sizeof(req_data));  
        if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
            if(resp_data.response_code != 0) {
                printf("\n\tFailed to get unwrap KBK_2 to SmartCard over network\n");
                ulRet = -1;
                goto error;
            }
        } else {
            printf("\n\tSSL_read failed\n");
            ulRet = -1;
            goto error;
        }

        memset(&req_data,0,sizeof(req_data));
        memset(&resp_data,0,sizeof(resp_data));

        req_data.command_type = SC_BACKUP_UNWRAP_KBK;
        memcpy(req_data.backup_unwrap_kbk_data.pname,pname,strlen(pname));
        memcpy(req_data.backup_unwrap_kbk_data.label,label,strlen(label));
        memcpy(req_data.backup_unwrap_kbk_data.wrappedkey,pWrappedKey_3,ulWrappedKeyLen_3);
        req_data.backup_unwrap_kbk_data.wrappedkey_len = ulWrappedKeyLen_3;
        req_data.backup_unwrap_kbk_data.privkey = privkey;
        req_data.backup_unwrap_kbk_data.wrappedkey_num = 3;

        SSL_write(ssl, &req_data, sizeof(req_data));  
        if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
            if(resp_data.response_code != 0) {
                printf("\n\tFailed to get unwrap KBK_3 to SmartCard over network\n");
                ulRet = -1;
                goto error;
            }
        } else {
            printf("\n\tSSL_read failed\n");
            ulRet = -1;
            goto error;
        }

    }

error:
    if(pModulus) free(pModulus);
    if(pExponent) free(pExponent);

    return ulRet;
#else
    return RET_SMART_CARD_FAILURE;
#endif
}


Uint32 ImportKBKtoSC(Uint8 p_no, char *pname)
{
#ifdef BACKUP_WITH_SMARTCARD
    Uint32 ulRet  = 0, ulRet1 = 0;
    Uint8* pModulus = 0, *pExponent = 0;
    CK_ULONG  keylen_1 = 16, keylen_2 = 16, keylen_3 = 16;
    Uint64  hsm_public_key = 0;
    char  label[MAX_LABEL_LENGTH] = {},
          label_1[MAX_LABEL_LENGTH] = {},
          label_2[MAX_LABEL_LENGTH] = {},
          label_3[MAX_LABEL_LENGTH] = {};
    CK_OBJECT_HANDLE  hkbk_1 = 0, hkbk_2 = 0, hkbk_3 = 0;
    CK_OBJECT_HANDLE  pubkey  = 0, privkey = 0;
    char user_password[MAX_PSWD_LENGTH] ;

    printf("\nPlease enter User PIN, which is programmed while initializing the SC:\n");
    scanf("%s", user_password);

    ulRet = pFunctionList->C_Login (sc_session, CKU_USER, (CK_CHAR_PTR)user_password,strlen(user_password));
    if (CKR_OK != ulRet)
    {
        printf("\tC_Login failed with error 0x%02x\n", ulRet);
        goto error;
    }

    ulRet = Cfm2GetHSMLabel(session_handle, (Uint8 *)label);
    if (ulRet != 0)
    {
        printf("\n\tCfm2GetHSMLabel Failure: 0x%02x : %s\n",
                ulRet, Cfm2ResultAsString(ulRet));
        goto error;
    }

    sprintf(label_1,"KBK_AES_%s_1_%s", pname, label);
    sprintf(label_2,"KBK_AES_%s_2_%s", pname, label);
    sprintf(label_3,"KBK_AES_%s_3_%s", pname, label);

    CK_ATTRIBUTE kbk_template_1[] = {
        {CKA_LABEL,      label_1,   sizeof(label_1)},
        {CKA_CLASS,      &secret,   sizeof(CK_OBJECT_CLASS)},
        {CKA_KEY_TYPE,   &aes,      sizeof(CK_KEY_TYPE)},
        {CKA_ENCRYPT,    &true,     sizeof(CK_BBOOL)},
        {CKA_DECRYPT,    &true,     sizeof(CK_BBOOL)},
        {CKA_TOKEN,      &true,     sizeof(CK_BBOOL)},
        {CKA_VALUE_LEN,  &keylen_1, sizeof(keylen_1)},
    };

    CK_ATTRIBUTE kbk_template_2[] = {
        {CKA_LABEL,     label_2,    sizeof(label_2)},
        {CKA_CLASS,     &secret,    sizeof(CK_OBJECT_CLASS)},
        {CKA_KEY_TYPE,  &aes,       sizeof(CK_KEY_TYPE)},
        {CKA_ENCRYPT,   &true,      sizeof(CK_BBOOL)},
        {CKA_DECRYPT,   &true,      sizeof(CK_BBOOL)},
        {CKA_TOKEN,     &true,      sizeof(CK_BBOOL)},
        {CKA_VALUE_LEN, &keylen_2,  sizeof(keylen_2)},
    };

    CK_ATTRIBUTE kbk_template_3[] = {
        {CKA_LABEL,     label_3,    sizeof(label_3)},
        {CKA_CLASS,     &secret,    sizeof(CK_OBJECT_CLASS)},
        {CKA_KEY_TYPE,  &aes,       sizeof(CK_KEY_TYPE)},
        {CKA_ENCRYPT,   &true,      sizeof(CK_BBOOL)},
        {CKA_DECRYPT,   &true,      sizeof(CK_BBOOL)},
        {CKA_TOKEN,     &true,      sizeof(CK_BBOOL)},
        {CKA_VALUE_LEN, &keylen_3,  sizeof(keylen_3)},
    };
    Uint32  ulWrappedKeyLen_1 = 4096, ulWrappedKeyLen_2 = 4096, ulWrappedKeyLen_3 = 4096 ;
    Uint8   pWrappedKey_1[4096] = {};
    Uint8   pWrappedKey_2[4096] = {};
    Uint8   pWrappedKey_3[4096] = {};

    /* Setup public key attributes */
    CK_BYTE          public_exponent[] = { 0x01, 0x00, 0x01 };

    CK_ATTRIBUTE public_key_template[] =
    {
        { CKA_CLASS,           &public,   sizeof (CK_OBJECT_CLASS) },
        { CKA_LABEL,           "KBK public key", strlen ("KBK public key") },
        { CKA_KEY_TYPE,        &rsa,      sizeof (CK_KEY_TYPE) },
        { CKA_TOKEN,           &true,     sizeof (CK_BBOOL) },
        { CKA_PRIVATE,         &false,    sizeof (CK_BBOOL) },
        { CKA_MODULUS_BITS,    &kbk_rsa_len_in_bits, sizeof (CK_ULONG) },
        { CKA_PUBLIC_EXPONENT, &public_exponent, sizeof (public_exponent) },
    };

    /* Setup private key attributes.*/
    CK_ATTRIBUTE private_key_template[] =
    {
        { CKA_CLASS,      &private,         sizeof (CK_OBJECT_CLASS) },
        { CKA_LABEL,      "KBK private key",  strlen ("KBK private key") },
        { CKA_KEY_TYPE,   &rsa,             sizeof (CK_KEY_TYPE) },
        { CKA_TOKEN,      &true,            sizeof (CK_BBOOL) },
        { CKA_PRIVATE,    &true,            sizeof (CK_BBOOL) },
    };

    /* Setup mechanism to generate RSA key pair or to find the Key handles */
    CK_MECHANISM keygen_mechanism = {
        CKM_RSA_PKCS_KEY_PAIR_GEN, NULL_PTR, 0
    };

    CK_MECHANISM unwrap_mechanism = {
        CKM_RSA_PKCS, NULL_PTR, 0
    };

    /*public key template to export*/
    CK_ATTRIBUTE pub_template[] = {
        { CKA_MODULUS,         NULL_PTR, 0 },
        { CKA_PUBLIC_EXPONENT, NULL_PTR, 0},
    };

    /*Generate 2048 bit RSA Key pair on the SC*/
    privkey =  find_object (sc_session, private_key_template, 1);
    pubkey  =  find_object (sc_session, public_key_template, 1);

    if (!(pubkey && privkey))
    {
        ulRet = pFunctionList->C_GenerateKeyPair (sc_session,&keygen_mechanism,
                public_key_template, 7,
                private_key_template,5,
                &pubkey,&privkey);
    }
    if (SUCCESS != ulRet)
    {
        printf("C_GenerateKeyPair failed with error 0x%02x\n", ulRet);
        goto error;
    }

    /*Export Public key from SC onto the host*/
    ulRet = pFunctionList->C_GetAttributeValue(sc_session, pubkey, pub_template, 2) ;
    if(CKR_OK == ulRet)
    {
        pModulus = (CK_BYTE_PTR)malloc(pub_template[0].ulValueLen);
        pub_template[0].pValue = pModulus;

        pExponent = (CK_BYTE_PTR)malloc(pub_template[1].ulValueLen);
        pub_template[1].pValue = pExponent;

        ulRet = pFunctionList->C_GetAttributeValue(sc_session, pubkey, pub_template, 2);
        if( ulRet != CKR_OK )
        {
            if(pModulus) free(pModulus);
            if(pExponent) free(pExponent);
            printf("C_GetAttributeValue failed with error 0x%02x\n", ulRet);
            goto error;
        }
    }
    else
    {
        printf("C_GetAttributeValue failed with error 0x%02x\n", ulRet);
        goto error;
    }

    ulRet = Cfm2CreatePublicKey2(session_handle,
            KEY_TYPE_RSA,
            pub_template[0].ulValueLen,
            *(Uint32 *)pExponent,
            0,0,NULL,0,NULL,
            pModulus,
            pub_template[0].ulValueLen,
            NULL,0,
            (Uint8 *)"KBK public key",
            strlen("KBK public key"),
            STORAGE_FLASH,
            p_no,
            &hsm_public_key);
    if(ulRet)
    {
        printf("\n\tCfm2CreatePublicKey returned: 0x%02x : %s\n", ulRet, Cfm2ResultAsString(ulRet));
        goto error;
    }

    /* Export the KBK from HSM onto the host by wrapping it with the imported
     * public Key. Import this key onto the SC by Unwrapping it with
     * the RSA Priv Key on SC*/
    ulRet = Cfm2ExportKBK(session_handle,
            pWrappedKey_1, pWrappedKey_2, pWrappedKey_3,
            &ulWrappedKeyLen_1, &ulWrappedKeyLen_2, &ulWrappedKeyLen_3, p_no, KBK_WRAP_WITH_RSA);
    if (ulRet != 0)
    {
        printf("\n\tCfm2ExportKBK Failure: 0x%02x : %s\n",
                ulRet, Cfm2ResultAsString(ulRet));
        goto error;
    }

    /*Unwrap the KBK part 1 and store it on the SC*/
    ulRet = pFunctionList->C_UnwrapKey(sc_session, &unwrap_mechanism, privkey,
            pWrappedKey_1,
            ulWrappedKeyLen_1,
            kbk_template_1 ,7,
            &hkbk_1);
    if(ulRet != CKR_OK)
    {
        printf("C_UnwrapKey(KBK_1) failed with error 0x%02x\n\n", ulRet);
        goto error;
    }
    print_debug("%s: imported key into SC with label %s\n", __func__, label_1);

    /*Unwrap the KBK part 2 and store it on the SC*/
    ulRet = pFunctionList->C_UnwrapKey(sc_session, &unwrap_mechanism, privkey,
            pWrappedKey_2, ulWrappedKeyLen_2,
            kbk_template_2 ,7,
            &hkbk_2);
    if(ulRet != CKR_OK)
    {
        printf("C_UnwrapKey(KBK_2) failed with error 0x%02x\n\n", ulRet);
        goto error;
    }
    print_debug("%s: imported key into SC with label %s\n", __func__, label_2);

    /*Unwrap the KBK part 3 and store it on the SC*/
    ulRet = pFunctionList->C_UnwrapKey(sc_session, &unwrap_mechanism, privkey,
            pWrappedKey_3, ulWrappedKeyLen_3,
            kbk_template_3 ,7,
            &hkbk_3);
    if(ulRet != CKR_OK)
    {
        printf("C_UnwrapKey(KBK_3) failed with error 0x%02x\n\n", ulRet);
        goto error;
    }
    print_debug("%s: imported key into SC with label %s\n", __func__, label_3);

error:
    ulRet1 = pFunctionList->C_Logout (sc_session);
    if(ulRet1 != CKR_OK)
    {
        printf("\tC_Logout failed with error 0x%02x\n", ulRet1);
        return ulRet1;
    }
    return ulRet;
#else
    return RET_SMART_CARD_FAILURE;
#endif
}

Uint32 ExportKBKfromSC2(int partition_index, char *label, char* pname, char* user_password, SSL *ssl)
{
#ifdef BACKUP_WITH_SMARTCARD
    Uint32 ulRet  = 0;
    Uint32  ulModLen = 2048, ulPubExp = 65537;
    Uint64  ulRSAPublicKey64  = 0;
    Uint64  ulRSAPrivateKey64 = 0;
    Uint32  ulModAttrLen = 0;
    Uint8*  pModAttr     = NULL;
    CK_OBJECT_HANDLE  pubkey  = 0;

    CK_ULONG ulWrappedKeyLen_1 = 256, ulWrappedKeyLen_2 = 256, ulWrappedKeyLen_3 = 256;
    CK_BYTE  *pWrappedKey_1 = NULL, *pWrappedKey_2 = NULL, *pWrappedKey_3 = NULL;
    
    ulRet = Cfm2GenerateKeyPair2(session_handle,
            KEY_TYPE_RSA,
            ulModLen,ulPubExp, 0,
            0,NULL,0,NULL,
            NULL,0,
            (Uint8 *)"KBK public key",
            strlen((Int8 *)"KBK public key"),
            (Uint8 *)"KBK private key",
            strlen((Int8 *)"KBK private key"),
            STORAGE_FLASH,
            partition_index,
            (Uint64 *)&ulRSAPublicKey64,
            (Uint64 *)&ulRSAPrivateKey64);
    if (ulRet == 0) {
        printf("\n\tCfm2RSAGenKeyPair:  public key handle: %lld  private key handle: %lld\n", ulRSAPublicKey64, ulRSAPrivateKey64);
    }
    else
    {
        printf("\n\tCfm2RSAGenKeyPair returned: 0x%02x : %s\n",
                ulRet, Cfm2ResultAsString(ulRet));
        goto error;
    }

    /* Get RSA Public Key Modulus */
    ulRet = Cfm2GetAttribute(session_handle, ulRSAPublicKey64, OBJ_ATTR_MODULUS, 0, &ulModAttrLen);
    if (!ulRet && (ulModAttrLen > 0))
    {
        pModAttr = (Uint8*)calloc(ulModAttrLen,1);

        if (pModAttr == 0)
            ulRet = ERR_MEMORY_ALLOC_FAILURE;
        else
            ulRet = Cfm2GetAttribute(session_handle, ulRSAPublicKey64,OBJ_ATTR_MODULUS, pModAttr, &ulModAttrLen);

        if(ulRet != 0)
        {
            printf("\tCfm2GetAttribute Failure: 0x%02x : %s\n", ulRet, Cfm2ResultAsString(ulRet));
            goto error;
        }
    }
    else
    {
        printf("\tCfm2GetAttribute returned: 0x%02x : %s\n", ulRet, Cfm2ResultAsString(ulRet));
        goto error;
    }

    if(ssl == NULL) {
        ulRet = create_object_in_etoken_for_restore(user_password, pModAttr, ulModAttrLen, &pubkey);
        if(ulRet != 0)
        {
            printf("\tFailed to create object in smartCard: 0x%02x\n", ulRet);
            goto error;
        }
    } else {
            df_request req_data;
            df_response resp_data;

            memset(&req_data,0,sizeof(req_data));
            memset(&resp_data,0,sizeof(resp_data));

            req_data.command_type = SC_RESTORE_CREATE_OBJECT;
            memcpy(req_data.restore_create_object_data.userPin,user_password,strlen(user_password));
            memcpy(req_data.restore_create_object_data.pModulus,pModAttr,ulModAttrLen);
            req_data.restore_create_object_data.pModLen = ulModAttrLen;

            SSL_write(ssl, &req_data, sizeof(req_data));  
            if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
                if(resp_data.response_code != 0) {
                    printf("\n\tFailed to create object in SmartCard over network\n");
                    ulRet = -1;
                    goto error;
                }
            } else {
                printf("\n\tSSL_read failed\n");
                ulRet = -1;
                goto error;
            }

            pubkey = resp_data.restore_create_object_resp.pubkey;
    }

    if(pModAttr)    free(pModAttr);

    /*Wrapping the first part of KBK on SC */
    pWrappedKey_1 = (CK_BYTE_PTR)malloc(ulWrappedKeyLen_1);
    if(!pWrappedKey_1)
    {
        printf("\tMemory allocation failure for pWrappedKey_1\n");
        ulRet = -1;
        goto error ;
    }
    memset(pWrappedKey_1,0,ulWrappedKeyLen_1);

    /*Wrapping the second part of KBK on SC */
    pWrappedKey_2 = (CK_BYTE_PTR)malloc(ulWrappedKeyLen_2);
    if(!pWrappedKey_2)
    {
        printf("\tMemory allocation failure for pWrappedKey_1\n");
        ulRet = -1;
        goto error ;
    }
    memset(pWrappedKey_2,0,ulWrappedKeyLen_2);

    /*Wrapping the third part of KBK on SC */
    pWrappedKey_3 = (CK_BYTE_PTR)malloc(ulWrappedKeyLen_3);
    if(!pWrappedKey_3)
    {
        printf("\tMemory allocation failure for pWrappedKey_3\n");
        ulRet = -1;
        goto error ;
    }
    memset(pWrappedKey_3,0,ulWrappedKeyLen_3);

    if(ssl == NULL) {
        ulRet = wrap_kbk_to_etoken_for_restore(pname, label, pWrappedKey_1, &ulWrappedKeyLen_1, pubkey, 1);
        if(ulRet != 0)
        {
            printf("\tFailed to wrap kbk to smartCard: 0x%02x\n", ulRet);
            goto error;
        }

        ulRet = wrap_kbk_to_etoken_for_restore(pname, label, pWrappedKey_2, &ulWrappedKeyLen_2, pubkey, 2);
        if(ulRet != 0)
        {
            printf("\tFailed to wrap kbk to smartCard: 0x%02x\n", ulRet);
            goto error;
        }

        ulRet = wrap_kbk_to_etoken_for_restore(pname, label, pWrappedKey_3, &ulWrappedKeyLen_3, pubkey, 3);
        if(ulRet != 0)
        {
            printf("\tFailed to wrap kbk to smartCard: 0x%02x\n", ulRet);
            goto error;
        }
    } else {
        df_request req_data;
        df_response resp_data;

        memset(&req_data,0,sizeof(req_data));
        memset(&resp_data,0,sizeof(resp_data));

        req_data.command_type = SC_RESTORE_WRAP_KBK;
        memcpy(req_data.restore_wrap_kbk_data.pname,pname,strlen(pname));
        memcpy(req_data.restore_wrap_kbk_data.label,label,strlen(label));
        req_data.restore_wrap_kbk_data.pubkey = pubkey;
        req_data.restore_wrap_kbk_data.wrappedkey_num = 1;

        SSL_write(ssl, &req_data, sizeof(req_data));  
        if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
            if(resp_data.response_code != 0) {
                printf("\n\tFailed to get wrap KBK_1 to SmartCard over network\n");
                ulRet = -1;
                goto error;
            }
        } else {
            printf("\n\tSSL_read failed\n");
            ulRet = -1;
            goto error;
        }
        
        ulWrappedKeyLen_1 = resp_data.restore_wrap_kbk_resp.wrappedkey_len;
        memcpy(pWrappedKey_1, resp_data.restore_wrap_kbk_resp.wrappedkey, ulWrappedKeyLen_1); 

        memset(&req_data,0,sizeof(req_data));
        memset(&resp_data,0,sizeof(resp_data));

        req_data.command_type = SC_RESTORE_WRAP_KBK;
        memcpy(req_data.restore_wrap_kbk_data.pname,pname,strlen(pname));
        memcpy(req_data.restore_wrap_kbk_data.label,label,strlen(label));
        req_data.restore_wrap_kbk_data.pubkey = pubkey;
        req_data.restore_wrap_kbk_data.wrappedkey_num = 2;

        SSL_write(ssl, &req_data, sizeof(req_data));  
        if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
            if(resp_data.response_code != 0) {
                printf("\n\tFailed to get wrap KBK_1 to SmartCard over network\n");
                ulRet = -1;
                goto error;
            }
        } else {
            printf("\n\tSSL_read failed\n");
            ulRet = -1;
            goto error;
        }
        
        ulWrappedKeyLen_2 = resp_data.restore_wrap_kbk_resp.wrappedkey_len;
        memcpy(pWrappedKey_2, resp_data.restore_wrap_kbk_resp.wrappedkey, ulWrappedKeyLen_1); 

        memset(&req_data,0,sizeof(req_data));
        memset(&resp_data,0,sizeof(resp_data));

        req_data.command_type = SC_RESTORE_WRAP_KBK;
        memcpy(req_data.restore_wrap_kbk_data.pname,pname,strlen(pname));
        memcpy(req_data.restore_wrap_kbk_data.label,label,strlen(label));
        req_data.restore_wrap_kbk_data.pubkey = pubkey;
        req_data.restore_wrap_kbk_data.wrappedkey_num = 3;

        SSL_write(ssl, &req_data, sizeof(req_data));  
        if(SSL_read(ssl, &resp_data, sizeof(resp_data))) {
            if(resp_data.response_code != 0) {
                printf("\n\tFailed to get wrap KBK_1 to SmartCard over network\n");
                ulRet = -1;
                goto error;
            }
        } else {
            printf("\n\tSSL_read failed\n");
            ulRet = -1;
            goto error;
        }
        
        ulWrappedKeyLen_3 = resp_data.restore_wrap_kbk_resp.wrappedkey_len;
        memcpy(pWrappedKey_3, resp_data.restore_wrap_kbk_resp.wrappedkey, ulWrappedKeyLen_1); 

    }

    /* Unwrap the KBK's on the HSM/Partition.
     * concatenate the 3 parts of KBK and form 256-bit AES KBK */
    ulRet = Cfm2ImportKBK(session_handle,
            pWrappedKey_1,
            pWrappedKey_2,
            pWrappedKey_3,
            ulWrappedKeyLen_1,
            ulWrappedKeyLen_2,
            ulWrappedKeyLen_3,
            0,
            KBK_WRAP_WITH_RSA);
    if(ulRet)
    {
        printf("\tCfm2ImportKBK failed with error 0x%02x\n", ulRet);
        goto error;
    }

error:
    if(pWrappedKey_1)        free(pWrappedKey_1);
    if(pWrappedKey_2)        free(pWrappedKey_2);
    if(pWrappedKey_3)        free(pWrappedKey_3);

    return ulRet;
#else
    return RET_SMART_CARD_FAILURE;
#endif
}


Uint32 ExportKBKfromSC(int partition_index, char *label, char* pname)
{
#ifdef BACKUP_WITH_SMARTCARD
    Uint32 ulRet  = 0, ulRet1 = 0;
    Uint32  ulModLen = 2048, ulPubExp = 65537;
    Uint64  ulRSAPublicKey64  = 0;
    Uint64  ulRSAPrivateKey64 = 0;
    Uint32  ulModAttrLen = 0;
    Uint8*  pModAttr     = NULL;
    CK_OBJECT_HANDLE  hkbk_1 = 0, hkbk_2 = 0, hkbk_3 = 0;
    CK_OBJECT_HANDLE  pubkey  = 0;

    CK_ULONG ulWrappedKeyLen_1 = 256, ulWrappedKeyLen_2 = 256, ulWrappedKeyLen_3 = 256;
    CK_BYTE  *pWrappedKey_1 = NULL, *pWrappedKey_2 = NULL, *pWrappedKey_3 = NULL;
    /*template for the KBK keys*/
    char  label_1[MAX_LABEL_LENGTH] = {}, label_2[MAX_LABEL_LENGTH]   = {}, label_3[MAX_LABEL_LENGTH]   = {};
    char user_password[MAX_PSWD_LENGTH] ;

    sprintf(label_1,"KBK_AES_%s_1_%s",pname,label);
    sprintf(label_2,"KBK_AES_%s_2_%s",pname,label);
    sprintf(label_3,"KBK_AES_%s_3_%s",pname,label);

    CK_ATTRIBUTE kbk_template_1[] = {
        {CKA_LABEL, label_1, sizeof(label_1)},
        {CKA_KEY_TYPE, &aes, sizeof(CK_KEY_TYPE)},
    };

    CK_ATTRIBUTE kbk_template_2[] = {
        {CKA_LABEL, label_2, sizeof(label_2)},
        {CKA_KEY_TYPE, &aes, sizeof(CK_KEY_TYPE)},
    };

    CK_ATTRIBUTE kbk_template_3[] = {
        {CKA_LABEL, label_3, sizeof(label_3)},
        {CKA_KEY_TYPE, &aes, sizeof(CK_KEY_TYPE)},
    };

    printf("\nPlease enter User PIN, which is programmed while initializing the SC:\n");
    scanf("%s", user_password);

    ulRet = pFunctionList->C_Login (sc_session, CKU_USER, (CK_CHAR_PTR)user_password,strlen(user_password));
    if (CKR_OK != ulRet)
    {
        printf("\tC_Login failed with error 0x%02x\n", ulRet);
        goto error;
    }
    ulRet = Cfm2GenerateKeyPair2(session_handle,
            KEY_TYPE_RSA,
            ulModLen,ulPubExp, 0,
            0,NULL,0,NULL,
            NULL,0,
            (Uint8 *)"KBK public key",
            strlen((Int8 *)"KBK public key"),
            (Uint8 *)"KBK private key",
            strlen((Int8 *)"KBK private key"),
            STORAGE_FLASH,
            partition_index,
            (Uint64 *)&ulRSAPublicKey64,
            (Uint64 *)&ulRSAPrivateKey64);
    if (ulRet == 0) {
        printf("\n\tCfm2RSAGenKeyPair:  public key handle: %lld  private key handle: %lld\n", ulRSAPublicKey64, ulRSAPrivateKey64);
    }
    else
    {
        printf("\n\tCfm2RSAGenKeyPair returned: 0x%02x : %s\n",
                ulRet, Cfm2ResultAsString(ulRet));
        goto error;
    }

    /* Get RSA Public Key Modulus */
    ulRet = Cfm2GetAttribute(session_handle, ulRSAPublicKey64, OBJ_ATTR_MODULUS, 0, &ulModAttrLen);
    if (!ulRet && (ulModAttrLen > 0))
    {
        pModAttr = (Uint8*)calloc(ulModAttrLen,1);

        if (pModAttr == 0)
            ulRet = ERR_MEMORY_ALLOC_FAILURE;
        else
            ulRet = Cfm2GetAttribute(session_handle, ulRSAPublicKey64,OBJ_ATTR_MODULUS, pModAttr, &ulModAttrLen);

        if(ulRet != 0)
        {
            printf("\tCfm2GetAttribute Failure: 0x%02x : %s\n", ulRet, Cfm2ResultAsString(ulRet));
            goto error;
        }
    }
    else
    {
        printf("\tCfm2GetAttribute returned: 0x%02x : %s\n", ulRet, Cfm2ResultAsString(ulRet));
        goto error;
    }


    /* With the above obtained Attributes Generate RSA Public Key on SC */
    CK_BYTE          pPubAttr[] = { 0x01, 0x00, 0x01 };

    CK_ATTRIBUTE public_key_template[] =
    {
        { CKA_CLASS,           &public,     sizeof (CK_OBJECT_CLASS) },
        { CKA_LABEL,           "KBK public key",  strlen ("KBK public key") },
        { CKA_KEY_TYPE,        &rsa,        sizeof (CK_KEY_TYPE) },
        { CKA_TOKEN,           &true,       sizeof (CK_BBOOL) },
        { CKA_WRAP,            &true,       sizeof (CK_BBOOL) },
        { CKA_ENCRYPT,         &true,       sizeof (CK_BBOOL)},
        { CKA_MODULUS,         pModAttr,    ulModAttrLen},
        { CKA_PUBLIC_EXPONENT, pPubAttr,    sizeof (pPubAttr)}
    };

    ulRet = pFunctionList->C_CreateObject(sc_session,public_key_template,8,&pubkey) ;
    if(ulRet != CKR_OK)
    {
        printf("\tC_CreateObject failed with error 0x%02x \n", ulRet);
        goto error;
    }
    if(pModAttr)    free(pModAttr);

    print_debug("%s: Find from SC with label %s\n", __func__, label_1);
    hkbk_1 = find_object (sc_session, kbk_template_1, 2);
    if(!hkbk_1)
    {
        printf("\tFinding KBK_1 handle failed \n");
        ulRet = -1;
        goto error;
    }

    print_debug("%s: Find from SC with label %s\n", __func__, label_2);
    hkbk_2 = find_object (sc_session, kbk_template_2, 2);
    if(!hkbk_2)
    {
        printf("\tFinding KBK_2 handle failed \n");
        ulRet = -1;
        goto error;
    }

    print_debug("%s: Find from SC with label %s\n", __func__, label_3);
    hkbk_3 = find_object (sc_session, kbk_template_3, 2);
    if(!hkbk_3)
    {
        printf("\tFinding KBK_3 handle failed \n");
        ulRet = -1;
        goto error;
    }

    CK_MECHANISM wrap_mechanism = {
        CKM_RSA_PKCS, NULL_PTR, 0
    };

    /*Wrapping the first part of KBK on SC */
    pWrappedKey_1 = (CK_BYTE_PTR)malloc(ulWrappedKeyLen_1);
    if(!pWrappedKey_1)
    {
        printf("\tMemory allocation failure for pWrappedKey_1\n");
        ulRet = -1;
        goto error ;
    }
    memset(pWrappedKey_1,0,ulWrappedKeyLen_1);
    ulRet = pFunctionList->C_WrapKey(sc_session,&wrap_mechanism, pubkey,
            hkbk_1,pWrappedKey_1,&ulWrappedKeyLen_1);
    if((ulRet != CKR_OK) || (ulWrappedKeyLen_1 > 256))
    {
        printf("\tC_WrapKey(KBK_1) failed with error 0x%02x \n", ulRet);
        goto error;
    }

    /*Wrapping the second part of KBK on SC */
    pWrappedKey_2 = (CK_BYTE_PTR)malloc(ulWrappedKeyLen_2);
    if(!pWrappedKey_2)
    {
        printf("\tMemory allocation failure for pWrappedKey_1\n");
        ulRet = -1;
        goto error ;
    }
    memset(pWrappedKey_2,0,ulWrappedKeyLen_2);
    ulRet = pFunctionList->C_WrapKey(sc_session,&wrap_mechanism, pubkey,
            hkbk_2,pWrappedKey_2,&ulWrappedKeyLen_2);
    if((ulRet != CKR_OK) || (ulWrappedKeyLen_2 > 256))
    {
        printf("\tC_WrapKey(KBK_2) failed with error 0x%02x \n", ulRet);
        goto error;
    }

    /*Wrapping the third part of KBK on SC */
    pWrappedKey_3 = (CK_BYTE_PTR)malloc(ulWrappedKeyLen_3);
    if(!pWrappedKey_3)
    {
        printf("\tMemory allocation failure for pWrappedKey_3\n");
        ulRet = -1;
        goto error ;
    }
    memset(pWrappedKey_3,0,ulWrappedKeyLen_3);
    ulRet = pFunctionList->C_WrapKey(sc_session,&wrap_mechanism, pubkey,
            hkbk_3,pWrappedKey_3,&ulWrappedKeyLen_3);
    if((ulRet != CKR_OK) || (ulWrappedKeyLen_3 > 256))
    {
        printf("\tC_WrapKey(KBK_3) failed with error 0x%02x \n", ulRet);
        goto error;
    }

    /* Unwrap the KBK's on the HSM/Partition.
     * concatenate the 3 parts of KBK and form 256-bit AES KBK */
    ulRet = Cfm2ImportKBK(session_handle,
            pWrappedKey_1,
            pWrappedKey_2,
            pWrappedKey_3,
            ulWrappedKeyLen_1,
            ulWrappedKeyLen_2,
            ulWrappedKeyLen_3,
            0,
            KBK_WRAP_WITH_RSA);
    if(ulRet)
    {
        printf("\tCfm2ImportKBK failed with error 0x%02x\n", ulRet);
        goto error;
    }

error:
    if(pWrappedKey_1)        free(pWrappedKey_1);
    if(pWrappedKey_2)        free(pWrappedKey_2);
    if(pWrappedKey_3)        free(pWrappedKey_3);

    ulRet1= pFunctionList->C_Logout (sc_session);
    if(ulRet1 != CKR_OK)
    {
        printf("\tC_Logout failed with error 0x%02x\n", ulRet1);
        return ulRet1;
    }

    return ulRet;
#else
    return RET_SMART_CARD_FAILURE;
#endif
}



Uint32 ExportKBKfromHost(int partition_index, Uint8* pData, Uint32 ulDataLen, Uint32 ulMech )
{
    Uint32 ulRet  = RET_ERROR;
    Uint64 pIV  = 0xA6A6A6A6A6A6A6A6ull;

    Uint32 ulWrappedKeyLen = 0;
    Uint8* pWrappedKey = NULL;
    Uint8* pPlainKey = NULL;

    /* Wrap the KBK in plain on host with KEK and then import it*/
    if(!pData || !ulDataLen)
    {
        print_error("Null KBK received !! \n");
        goto error;
    }


    if (ulMech == KBK_WRAP_WITH_CERT_AUTH_DERIVED_KEY) {
        pWrappedKey = pData;
        ulWrappedKeyLen = ulDataLen;
    } else {
        // Assuming wrapped data cannot be more than this.
        ulWrappedKeyLen = ulDataLen + 32;
        pWrappedKey = calloc(ulWrappedKeyLen, 1);
        pPlainKey = calloc(ulWrappedKeyLen, 1);
        if ((pWrappedKey == NULL) || (pPlainKey == NULL))
        {
            print_debug("Failed to allocate memory\n");
            ulRet = ERR_MEMORY_ALLOC_FAILURE;
            goto error;
        }
        memset(pPlainKey, 0, ulWrappedKeyLen);
        memcpy(pPlainKey, pData, ulDataLen);


        ulRet =  Cfm2AesWrapUnwrapBuffer(
                session_handle,
                KEK_HSM_HANDLE,
                pPlainKey,
                ulDataLen,
                pIV,
                pWrappedKey,
                1);

        ulWrappedKeyLen = ulDataLen + 8;
        if (ulRet)
        {
            print_error("WrapHostKey failed with error %x \n", ulRet);
            goto error;
        }
    }


    /* Unwrap the KBK's on the HSM/Partition.
     * concatenate the 3 parts of KBK and form 256-bit AES KBK */
    ulRet = Cfm2ImportKBK(session_handle,
            pWrappedKey,
            NULL,
            NULL,
            ulWrappedKeyLen,
            0,
            0,
            partition_index,
            ulMech);
    if(ulRet)
    {
        printf("\tCfm2ImportKBK failed with error 0x%02x\n", ulRet);
        goto error;
    }

error:
    return ulRet;
}

/*Reads Cos/CUs info from the files to Restore*/
Uint32 ReadUserInfo(Uint8   user_type,
        Uint32 *no_of_users,
        Uint32   data_buf_len,
        Uint8  *data,
        Uint32 *user_index,
        Uint32 *user_len,
        Uint8   isHSM,
        char   *dir,
        Uint8   part_index)
{
    struct stat status;
    FILE *fp = NULL;
    char file[100] = {},path[100] = {};
    Uint8 *enc_data = NULL, *data_ptr = NULL;
    Uint32  i = 0, size = 0, users = 0;
    int curr_buf_len = 0;

    data_ptr = data;

    if(isHSM)
    {
        sprintf(path,"%s/partition_%d",dir,part_index);
    }
    else
    {
        sprintf(path,"%s",dir);
    }

    if(CN_CRYPTO_OFFICER == user_type)
        users = isHSM? 1:MAX_OFFICERS_IN_PARTITION;
    else if (CN_CRYPTO_USER == user_type)
        users = MAX_USERS_IN_PARTITION;
    else if (CN_APPLIANCE_USER == user_type)
        users = MAX_APPLIANCE_USERS_IN_PARTITION;

    for (i= *user_index; i< users ; i++)
    {
        if(user_type == CN_CRYPTO_OFFICER)
            sprintf(file,"%s/Crypto_officer_%d",path,i+1);
        else if(user_type == CN_CRYPTO_USER)
            sprintf(file,"%s/Crypto_user_%d",path,i+1);
        else if(user_type == CN_APPLIANCE_USER)
            sprintf(file,"%s/Appliance_user_%d",path,i+1);

        fp = fopen(file,"rb");
        if (fp == NULL)
        {
            print_debug("Error opening file %s \n", file);
            continue;
        }
        stat(file,&status);
        size = status.st_size ;

        if((curr_buf_len+size) > data_buf_len )
        {
            *user_index = i;
            goto end;
        }

        enc_data=malloc(size);
        if(!enc_data)
        {
            printf("Failed to allocate memory for %d user file \n",i+1);
            fclose(fp);
            continue;
        }
        fread(enc_data,1,size,fp);
        fclose(fp);
        fp = NULL;
        memcpy(data_ptr,enc_data,size);
        data_ptr = (data_ptr + size );
        free(enc_data);
        curr_buf_len += size;

        *user_len    = size;
        *no_of_users += 1;
        memset(file, 0, 100);
    }
    *user_index = 0;
end:
    if(fp) fclose(fp);
    return SUCCESS;
}


/* Reads the partitions key store size from the file*/
Uint32 ReadKeyStoreSize(char *path, Uint32 *keyStoresize, Uint32 *sslSize, Uint32 *acclrDevSize, Uint32 index)
{
    char file [100] = {};
    FILE *fp = NULL;
    char *buf = NULL;
    size_t  size = 0;

    sprintf(file,"%s/partition_%d/partition_crypto_size",path,index);

    fp = fopen(file,"rb");
    if(fp == NULL)
    {
        printf("Failed to open %s file or directory does not exists \n",file);
        return -1;
    }

    //    fread(size,sizeof(int),1,fp);
    while(EOF != getline(&buf, &size, fp))
    {
        if(strstr(buf,"key store"))
        {
            sscanf(buf, "key store size: %d\n", keyStoresize);
        }
        else if(strstr(buf,"ssl ctx"))
        {
            sscanf(buf, "ssl ctx store size: %d\n", sslSize);
        }
        else if(strstr(buf,"acclr dev"))
        {
            sscanf(buf, "acclr dev size: %d\n", acclrDevSize);
        }
    }

    fclose(fp);

    return SUCCESS;
}

/*Reads Configuration data to restore */
Uint32 ReadConfig(char *path, Uint8 *config, Uint32 *config_len, Uint32 index,
        Uint32 IsHSM, Uint8* pname)
{
    char file[256] = {};
    FILE        *fp = NULL;
    Uint32      count = 0;
    struct stat config_status;

    if (IsHSM)
        sprintf(file,"%s/partition_%d/enc_config",path,index);
    else
        sprintf(file,"%s/enc_config",path);

    fp = fopen(file,"rb");
    if (fp == NULL)
    {
        printf("Failed to open the config file: %s\n",file);
        return -1;
    }
    stat(file,&config_status);
    *config_len = config_status.st_size ;

    count = fread(config,1,*config_len,fp);
    if(count != *config_len )
    {
        printf("Failed to read the encrypted configuration : %s\n",file);
        fclose(fp);
        return -1;
    }
    fclose(fp);

    if(IsHSM && index)
    {
        int pname_len = 0;

        if(!pname)
        {
            printf("Error! received NULL buffer \n");
            return -1;
        }

        /* Read the name of the partition */
        memset(file, 0, 256);
        sprintf(file,"%s/partition_%d/partition_name",path,index);

        fp = fopen(file,"rb");
        if (fp == NULL)
        {
            printf("Failed to open the config file: %s\n",file);
            return -1;
        }
        stat(file,&config_status);
        pname_len = config_status.st_size ;

        count = fread(pname,1,pname_len,fp);
        if(count != pname_len)
        {
            printf("Failed to read the encrypted configuration : %s\n",file);
            fclose(fp);
            return -1;
        }
        fclose(fp);
    }

    return SUCCESS;
}


Uint32 SaveConfig(Uint8 *config, Uint32 config_len,
        Uint8 isHSM,
        char *dir,
        Uint8 part_index,
        Uint32 ulKeyStoreSize,
        Uint32 sslCtxStoreSize,
        Uint32 acclrDevSize,
        Uint8  *pname)
{
    Uint32  ulRet = 0, count = 0;
    DIR     *Dir = NULL;
    FILE    *fp = NULL;
    char     file[100] = {},path[100] = {},option = 0;
    //Uint32  *pPartitionSize = NULL;
    char buffer[128] = {};
    struct stat file_status;

    if(isHSM)
    {
        sprintf(path,"%s/partition_%d",dir,part_index);
        if(NULL == (Dir = opendir(path)))
        {
            ulRet = mkdir(path,0x666);
            if( ulRet != 0)
            {
                printf("Failed to create Directory %s \n",path);
                return -1;
            }
        }

        if(!part_index)
        {
            sprintf(file, "%s/enc_config", path);
            goto save;
        }


        sprintf(file,"%s/partition_crypto_size",path);

        //pPartitionSize = &ulPartitionSize;
        ulRet = stat(file,&file_status);
        if(ulRet == 0)
        {
            printf("\n\tDo you want to overwrite the existing files in this directory?\n");
            printf("\tEnter 'y' to overwrite or 'n' otherwise \n");
            option = getchar();
            getchar();
            if((option == 'y') || (option == 'Y'))
            {
                unlink(file);
                goto size;
            }
            else if((option == 'n') || (option == 'N'))
            {
                printf("\tNew Backup Data is not saved \n");
                return 1;
            }
            else
            {
                printf("Invalid Option \n");
                return SUCCESS;
            }
        }
size :
        fp = fopen(file,"wb");
        if (fp == NULL)
        {
            printf("Failed to open the file or %s directory does not exist \n", file);
            return -1;
        }

        //count = fwrite(pPartitionSize,4,1,fp);

        /* Write key store size */
        memset(buffer, 0 , sizeof(buffer));

        sprintf(buffer, "key store size: %d\n", ulKeyStoreSize);
        count = fwrite(buffer, 1, strlen(buffer),fp);
        if(count != strlen(buffer) )
        {
            printf("Failed to save the encrypted configuration : %s\n",file);
            return -1;
        }

        /* Write ssl contexts store size */
        memset(buffer, 0 , sizeof(buffer));

        sprintf(buffer, "ssl ctx store size: %d\n", sslCtxStoreSize);
        count = fwrite(buffer, 1, strlen(buffer),fp);
        if(count != strlen(buffer) )
        {
            printf("Failed to save the encrypted configuration : %s\n",file);
            return -1;
        }
        /* Write acclr dev size */
        memset(buffer, 0 , sizeof(buffer));

        sprintf(buffer, "acclr dev size: %d\n", acclrDevSize);
        count = fwrite(buffer, 1, strlen(buffer),fp);
        if(count != strlen(buffer) )
        {
            printf("Failed to save the encrypted configuration : %s\n",file);
            return -1;
        }

        fclose(fp);

        /* Write the name of the partition */
        if(0 != part_index)
        {
            fp = NULL;
            memset(file,0,sizeof(file));

            sprintf(file,"%s/partition_name",path);

            fp = fopen(file,"wb");
            if (fp == NULL)
            {
                printf("Failed to open the file or %s directory does not exist \n", file);
                return -1;
            }

            if(!pname)
            {
                printf("Error! could not receive the pname\n");
                fclose(fp);
                return -1;
            }

            memset(buffer, 0 , sizeof(buffer));
            sprintf(buffer, "%s", pname);

            count = fwrite(buffer, 1, strlen(buffer),fp);

            if(count != strlen(buffer) )
            {
                printf("Failed to save the encrypted configuration : %s\n",file);
                fclose(fp);
                return -1;
            }
            fclose(fp);
        }
        fp = NULL;
        memset(file,0,sizeof(file));

        sprintf(file,"%s/enc_config",path);
    }
    else
        sprintf(file,"%s/enc_config",dir);

    if(!isHSM)
    {
        ulRet = stat(file,&file_status);
        if(ulRet == 0)
        {
            printf("\n\tDo you want to overwrite the existing files \n");
            printf("\tEnter y to overwrite or n for no \n");
            option = getchar();
            getchar();
            if((option == 'y') || (option == 'Y'))
            {
                unlink(file);
                goto save ;
            }
            else if((option == 'n') || (option == 'N'))
            {
                printf("\tNew Backup Data is not saved \n");
                return 1;
            }
            else
            {
                printf("\tInvalid Option \n");
                return SUCCESS;
            }
        }
    }

save :
    if(isHSM)
        unlink(file);

    fp = fopen(file,"w");
    if (fp == NULL)
    {
        printf("Failed to open the file or directory does not exist %s \n", file);
        return -1;
    }

    count = fwrite(config ,1,config_len,fp);
    if(count != config_len)
    {
        printf("Failed to save the encrypted configuration : %s\n",file);
        fclose(fp);
        return -1;
    }

    fclose(fp);

    return SUCCESS;
}

/*Save COs/CUs of a Partition */
Uint32 SaveUserInfo(Uint8  *user_info,
        Uint32  user_len,
        Uint32  user_count,
        Uint32  user_index,
        Uint8   user_type,
        Uint8   isHSM,
        char   *dir,
        Uint8   part_index)
{
    Uint8  *data = NULL;
    char file[100] = {},path[100] = {};
    Uint32 i = 0, count  = 0;
    FILE   *fp = NULL;

    data = (Uint8 *)user_info;

    if(isHSM)
        sprintf(path,"%s/partition_%d",dir,part_index);
    else
        sprintf(path,"%s",dir);

    for(i = user_index ; i < (user_index + user_count); i++)
    {
        if(user_type == CN_CRYPTO_USER)
            sprintf(file,"%s/Crypto_user_%d",path,i+1);
        else if(user_type == CN_APPLIANCE_USER)
            sprintf(file,"%s/Appliance_user_%d",path,i+1);
        else if(user_type == CN_CRYPTO_OFFICER)
            sprintf(file,"%s/Crypto_officer_%d",path,i+1);
        else
        {
            printf("Obtained Invalid User type %s:%d \n",__FUNCTION__,__LINE__);
            return -1;
        }

        unlink(file);

        fp = fopen(file, "wb");
        if (fp == NULL)
        {
            printf("Failed to open the file: %s \n", file);
            data = (Uint8 *)(data + (user_len));
            continue;
        }

        count = fwrite( data, 1, user_len, fp);
        if(count != user_len )
        {
            if(user_type == CN_CRYPTO_OFFICER)
                printf("Failed to write the encrypted Crytpo Officer into file : %s\n",file);
            else
                printf("Failed to write the encrypted Crytpo User into file : %s\n",file);
        }
        fclose(fp);
        data = (((Uint8 *)data) + user_len);
    }

    return SUCCESS;
}

Uint32 ReadKeyInfo(char *path, Uint8  *key_info, Uint32 *key_info_len)
{
    if(RET_OK != read_file(path, key_info, key_info_len))
        return RET_ERROR;
    else
        return SUCCESS;
}

Uint32 SaveKeyInfo(char *path, Uint8  *key_info, Uint32 key_info_len,
        Uint64 kh, Uint8 part_index, Uint32 isHSM )
{
    char fName[256] = {};
    memset(fName, 0, sizeof(fName));

    if(isHSM)
        sprintf(fName, "%s/partition_%d/key_%lld", path, part_index, kh);
    else
        sprintf(fName, "%s/key_%lld", path, kh);

    print_debug("Writing key %lld into file %s\n", kh, fName);

    if(RET_OK != write_file(fName, key_info, key_info_len))
        return RET_ERROR;
    else
        return SUCCESS;
}

Uint32 prepare_mxn_user_details( Uint32 session_handle,
        Uint32 ulMValue,
        MxNAuth **ppAuth)
{
    Uint32  i = 0, ulRet = 0;
    MxNAuth *pAuth = NULL;

    pAuth = (MxNAuth *)malloc(sizeof(MxNAuth));
    if (pAuth == NULL)
    {
        printf("Failed to allocate memory for MxNAuth structure\n");
        return ERR_MEMORY_ALLOC_FAILURE;
    }

    for (i = 0; i<ulMValue; i++)
    {
        printf("\nEnter CO-%d details\n", i);
        printf("\tusername:");
        scanf("%s",pAuth->User[i].ucName);
        pAuth->User[i].ulNameLen = htobe32(strlen((char *)pAuth->User[i].ucName));

        printf("\tpassword:");
        scanf("%s",pAuth->User[i].ucPswd);
        pAuth->User[i].ulPswdLen = strlen((char *)pAuth->User[i].ucPswd);

        ulRet = encrypt_pswd(session_handle,
                (Uint8 *)&(pAuth->User[i].ucPswd[0]),
                pAuth->User[i].ulPswdLen,
                (Uint8 *)&(pAuth->User[i].ucPswd[0]),
                &(pAuth->User[i].ulPswdLen));

        pAuth->User[i].ulPswdLen = htobe32(pAuth->User[i].ulPswdLen);
    }
    if (ulRet != RET_OK)
    {
        printf("password encryption failed ulRet %d: %s\n",ulRet,
                Cfm2ResultAsString(ulRet));
        return ulRet;
    }
    pAuth->ulMval = ulMValue;
    *ppAuth = pAuth;
    return ulRet;
}

