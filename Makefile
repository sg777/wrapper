CC=cc
CFLAGS=-w -I/var/www/html/hsm/update/build_73/cnn35xx-nfbe-kvm-xen-pf/software/include -I/var/www/html/hsm/update/build_73/cnn35xx-nfbe-kvm-xen-pf/software/apps/openssl-1.0.2c/include -I/var/www/html/hsm/update/build_73/cnn35xx-nfbe-kvm-xen-pf/software/wrapper
LDFLAGS=-L/usr/local/ssl/lib/engines -lpkcs11 -L/usr/lib64 -lcrypto -lssl -L/var/www/html/hsm/update/build_73/cnn35xx-nfbe-kvm-xen-pf/software/api -lcavium -pthread -ldl


Cfm2Util.o : Cfm2Util.c Cfm2Util.h
	cc -c Cfm2Util.c $(CFLAGS) $(LDFLAGS)

Cfm2Helper.o : Cfm2Helper.c Cfm2Helper.h
	cc -c Cfm2Helper.c $(CFLAGS) $(LDFLAGS)


wrapper: Cfm2Helper.o Cfm2Util.o
	cc -o wrapper wrapper.c Cfm2Helper.o Cfm2Util.o $(CFLAGS) $(LDFLAGS)

all: wrapper

clean: 
	rm -rf *.o wrapper
