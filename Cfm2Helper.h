/*
 * Copyright (c) 2003-2017 Cavium Networks (support@cavium.com). All rights 
 * reserved.
 * 
 * Redistribution and use in source and binary forms, with or without 
 * modification, are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, 
 * this list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form must reproduce the above copyright notice, 
 *    this list of conditions and the following disclaimer in the documentation 
 *    and/or other materials provided with the distribution.
 * 
 * 3. All manuals,brochures,user guides mentioning features or use of this software 
 *    must display the following acknowledgement:
 * 
 *   This product includes software developed by Cavium Networks
 * 
 * 4. Cavium Networks' name may not be used to endorse or promote products 
 *    derived from this software without specific prior written permission.
 * 
 * 5. User agrees to enable and utilize only the features and performance 
 *    purchased on the target hardware.
 * 
 * This Software,including technical data,may be subject to U.S. export control 
 * laws, including the U.S. Export Administration Act and its associated 
 * regulations, and may be subject to export or import regulations in other 
 * countries.You warrant that You will comply strictly in all respects with all 
 * such regulations and acknowledge that you have the responsibility to obtain 
 * licenses to export, re-export or import the Software.
 
 * TO THE MAXIMUM EXTENT PERMITTED BY LAW, THE SOFTWARE IS PROVIDED "AS IS" AND 
 * WITH ALL FAULTS AND CAVIUM MAKES NO PROMISES, REPRESENTATIONS OR WARRANTIES, 
 * EITHER EXPRESS,IMPLIED,STATUTORY, OR OTHERWISE, WITH RESPECT TO THE SOFTWARE,
 * INCLUDING ITS CONDITION,ITS CONFORMITY TO ANY REPRESENTATION OR DESCRIPTION, 
 * OR THE EXISTENCE OF ANY LATENT OR PATENT DEFECTS, AND CAVIUM SPECIFICALLY 
 * DISCLAIMS ALL IMPLIED (IF ANY) WARRANTIES OF TITLE, MERCHANTABILITY, 
 * NONINFRINGEMENT,FITNESS FOR A PARTICULAR PURPOSE,LACK OF VIRUSES, ACCURACY OR
 * COMPLETENESS, QUIET ENJOYMENT, QUIET POSSESSION OR CORRESPONDENCE TO 
 * DESCRIPTION. THE ENTIRE RISK ARISING OUT OF USE OR PERFORMANCE OF THE 
 * SOFTWARE LIES WITH YOU.
 *
 */

#ifndef _HELPER_H_
#define _HELPER_H_
#include "eTPkcs11.h"
#include "pkcs11t.h"

#ifdef BACKUP_WITH_SMARTCARD
//#define pkcs11_path             "libeTPkcs11.so"
//#define pkcs11_path             "/usr/lib64/libeTPkcs11.so"
#define pkcs11_path             "/lib64/libeToken.so.8.1"

Uint32 initSmartCard();
int shutdownSmartCard();

Uint32  find_object (CK_SESSION_HANDLE hSess, CK_ATTRIBUTE * t, CK_ULONG size );
#endif

#define DF_INITIALIZE              1 
#define DF_GET_SIGNATURE           2
#define DF_GENERATE_KEY_PAIR       3
#define DF_GET_KEY                 4
#define DF_CHANGE_PASSWORD         5
#define SC_BACKUP_GET_KEY          6
#define SC_BACKUP_UNWRAP_KBK       7
#define SC_RESTORE_CREATE_OBJECT   8
#define SC_RESTORE_WRAP_KBK        9
#define DF_END                     10

#define KBK_IMPORT_MODULUS_SIZE    256
#define KBK_IMPORT_EXPONENT_SIZE   3

#define KBK_UNWRAPPED_WIH_KEK_FILE "kbk"

Uint32 ReadKeyInfo(char *path, Uint8  *key_info, Uint32 *key_info_len);
Uint32 SaveKeyInfo(char *path, Uint8  *key_info, Uint32 key_info_len,
        Uint64 kh, Uint8 part_index, Uint32 isHSM );
int OpenSocketConnection(const char *hostname, int port);
SSL_CTX* InitSSL_CTX(void);

Uint32 ImportKBKtoSC(Uint8 p_no, char *pname);
Uint32 ExportKBKfromSC(int partition_index, char *label, char *pname);

Uint32 ImportKBKtoSC2(Uint8 p_no, char *pname, char * user_password, SSL *ssl);
Uint32 ExportKBKfromSC2(int partition_index, char *label, char* pname, char* user_password, SSL *ssl);
Uint32 ImportKBKtoHost(char* pPath, Uint8 p_no, Uint32 ulMech);
Uint32 ExportKBKfromHost(int partition_index, Uint8* pData, Uint32 ulDataLen, Uint32 ulMech );

Uint32 ReadUserInfo(Uint8   user_type,
        Uint32 *no_of_users,
        Uint32  data_buf_len,
        Uint8  *data,
        Uint32 *user_index,
        Uint32 *user_len,
        Uint8   isHSM,
        char   *dir,
        Uint8   part_index);

Uint32 ReadKeyStoreSize(char *path, Uint32 *keyStoresize, Uint32 *sslSize, Uint32 *acclrDevSize, Uint32 index);
Uint32 ReadConfig(char *path, Uint8 *config, Uint32 *config_len, Uint32 index, Uint32 IsHSM, Uint8* pname);
Uint32 SaveConfig(Uint8 *config, Uint32 config_len,
        Uint8 isHSM,
        char *dir,
        Uint8 part_index,
        Uint32 ulKeyStoreSize,
        Uint32 sslCtxStoreSize,
        Uint32 acclrDevSize,
        Uint8* pname );
Uint32 SaveUserInfo(Uint8  *user_info,
        Uint32  user_len,
        Uint32  user_count,
        Uint32  user_index,
        Uint8   user_type,
        Uint8   isHSM,
        char   *dir,
        Uint8   part_index);

Uint32 prepare_mxn_user_details( Uint32 session_handle,
        Uint32 ulMValue,
        MxNAuth **ppAuth);

#include <dualFactor.h>
#endif
